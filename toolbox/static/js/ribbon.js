(function($){
  $.fn.ribbon = function(settings){
    var config = {
      'foo': 'bar'
    };
    if (settings){$.extend(config, settings);}

    var $wrap = $('<div class="ribbon"><div class="ribbon-wrapper"><div class="ribbon-front"></div></div></div>');
    return this.each(function(){
      $(this).wrap($wrap);
      $(this).parent().parent().append('<div class="ribbon-edge-left-top"></div><div class="ribbon-edge-right-top"></div><div class="ribbon-edge-left-bottom"></div><div class="ribbon-edge-right-bottom"></div><div class="ribbon-back-left"></div><div class="ribbon-back-right"></div>');
    });
  };
})(jQuery);
