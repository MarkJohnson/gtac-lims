(function($){
    $.fn.photo = function(settings){
        var config = {
            'foo': 'bar'
        };
        if (settings){$.extend(config, settings);}
 
        return this.each(function(){
            
        var $wrap = $('<div class="photowrap"></div>')
        .css({ 
          width: $(this).innerWidth(),
          height: $(this).innerHeight()
          });
        $(this).wrap($wrap);
        });
    };
})(jQuery);
