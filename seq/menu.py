import menus

#menus.named_view_factory('flowcell-create', "Flowcell", '[Seq] Flowcell')
menus.named_view_factory('run-create', "Run Create", '[Seq] Run')
menus.named_view_factory('run-browse', "Run Browse", '[Seq] Run Browse')
menus.named_view_factory('flowcell-browse', "Flowcell Browse", '[Seq] Flowcell Browse')
menus.named_view_factory('sample-queue', "Sample Queue", '[Seq] Sample Queue')
menus.named_view_factory('myseq-sample-queue', "MiSeq Sample Queue", '[Seq] MiSeq Sample Queue')
