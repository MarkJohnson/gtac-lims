from django.contrib import admin
from seq import models
from base.admin import CoreModelAdmin


class ManifoldInline(admin.TabularInline):
    model = models.Manifold

class SequencingStatusAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)
admin.site.register(models.SequencingStatus, SequencingStatusAdmin)

class SequencerModelAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)
admin.site.register(models.SequencerModel, SequencerModelAdmin)

class ClusterStationModelAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)
admin.site.register(models.ClusterStationModel, ClusterStationModelAdmin)

class ClusterStationAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'model', 'is_active')
admin.site.register(models.ClusterStation, ClusterStationAdmin)

class SequencerAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'model', 'is_active')
    inlines = [ ManifoldInline ]
admin.site.register(models.Sequencer, SequencerAdmin)

class LaneInline(admin.TabularInline):
    model = models.Lane
    extra = 8
    max_num = 8
    
class FlowcellModelAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)
admin.site.register(models.FlowcellModel, FlowcellModelAdmin)

class FlowcellAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'runtype' )
    list_filter = ['runtype',]
    search_fields = ['name', ]
    inlines = [ LaneInline ]
admin.site.register(models.Flowcell, FlowcellAdmin)

class RunTypeAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'read', 'length', 'num_lanes', 'sequencer_model', 'custom', 'is_active')
admin.site.register(models.RunType, RunTypeAdmin)


class SampleSequencingDetailAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'sample', 'runtype')
    list_filter = ['runtype']
admin.site.register(models.SampleSequencingDetail, SampleSequencingDetailAdmin)

class LaneAdmin(CoreModelAdmin):
    list_display = ('__unicode__', 'flowcell')
    search_fields = ['flowcell__name', ]
admin.site.register(models.Lane, LaneAdmin)

class RunAdmin(CoreModelAdmin):
    list_display = ('__unicode__', 'manifold', 'flowcell', 'status', 'sequencing_date', 'software_version')
    list_filter = ['manifold', 'status', 'sequencing_date', 'software_version']
    search_fields = ['flowcell__name', 'flowcell__lane__sequencingsample__name']
    raw_id_fields = ['flowcell', ]
admin.site.register(models.Run, RunAdmin)

class SequencingBillAdmin(admin.ModelAdmin):
    list_display = ('run', 'lane', 'basecode', 'multipliers', 'total_charge', 'recharge', 'stockroom', 'fund93')
    
admin.site.register(models.SequencingBill, SequencingBillAdmin)

class RunResultsAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)

admin.site.register(models.RunResults, RunResultsAdmin)

class LaneResultsAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)

admin.site.register(models.LaneResults, LaneResultsAdmin)

class ReadResultsAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)

admin.site.register(models.ReadResults, ReadResultsAdmin)
