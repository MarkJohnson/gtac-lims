from lastnav.util import sitenav, SingleViewPageFactory, ModelPageFactory, FlatpageFactory
from seq import models

#sitenav.register(SingleViewPageFactory, 'run-detail')
sitenav.register(SingleViewPageFactory, 'sequencer-list')
sitenav.register(SingleViewPageFactory, 'run-list')
sitenav.register(ModelPageFactory, 'run-detail', models.Run, models.Run.objects.all()[:5])
sitenav.register(ModelPageFactory, 'sequencer-detail', models.Sequencer, models.Sequencer.objects.all()[:5])
#for fp in models.AmityPage.objects.all():
#    sitenav.register(FlatpageFactory, fp.url)
