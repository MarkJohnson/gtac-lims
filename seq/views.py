from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.contrib import messages
from base import views as baseviews
from base.views import prepareRawQuery
from seq import models as seqmodels
from base import models as basemodels
from libprep import models as lpmodels
from sample import models as sampmodels
from django.shortcuts import render
from seq import forms
from django.core import serializers
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.db.models import Max, Min, Count
from django.conf import settings
from util import file_manager
from xlwt import Formula
from frontend import util
import string
import json
import logging


logger = logging.getLogger('myproject.custom')

@login_required
def run_browse(request):
    if not request.GET.get('sEcho', False):
        return render(request, 'seq/run-browse.html', {})

    isBilling = True if request.GET.get("isBilling") else False

    SORT_COLS = ('num',
                'flowcell__runtype',
                'flowcell',
                ('flowcell__lanes__sequencing_sample__sampleprepdetails__sample__specimen__submitted_by__lab__first_name',
                 'flowcell__lanes__sequencing_sample__sampleprepdetails__sample__specimen__submitted_by__lab__last_name'),
                ('flowcell__lanes__sequencing_sample__sampleprepdetails__sample__specimen__submitted_by__person__first_name',
                 'flowcell__lanes__sequencing_sample__sampleprepdetails__sample__specimen__submitted_by__person__last_name'),
                'manifold__sequencer',
                'manifold',
                'sequencing_date',
                'status',
                'billing_date'
                  )
    
    runs = seqmodels.Run.objects.all()
    if isBilling:
        runs = runs.filter(status__in=['Success', 'Failure']).annotate(billing_date=Max('sequencingbill__created'))
    dtr  = baseviews.DataTableResponse(request)
    qs   = dtr.filter_qs(runs, SORT_COLS, search_fields=('num',))
    
    run_link = { }
    flowcell_link = { }

    for run in runs:
        run_link[run.id]      = '<a href="' + reverse('run-detail', args=[run.num]) + '">' + str(run.num) + '</a>'
        flowcell_link[run.id] = '<a href="' + reverse('flowcell-detail', args=[run.flowcell.name]) + '">' + run.flowcell.name + '</a>'
    return render(request, 'seq/run-browse-data.json', {
        'dtr': dtr,
        'runs': qs,
        'run_link': run_link,
        'flowcell_link': flowcell_link,
        'isBilling': isBilling
    })
    
    
def sequencingLabActivity(request, id):
    lab =  get_object_or_404(basemodels.Lab, id=id)
    if not request.GET.get('sEcho', False):
        return render(request, 'seq/sequencing-activity.html', {'lab': lab})
    
    return sequencingActivity(request, None, id)


def sequencingContactActivity(request, id):
    client = get_object_or_404(basemodels.Client, id=id)
    if not request.GET.get('sEcho', False):
        return render(request, 'seq/sequencing-activity.html', {'client': client})

    return sequencingActivity(request, id, None)


def sequencingActivity(request, contact_id, lab_id):
    SORT_COLS = ('flowcell__run__num', 
                 'num',
                 'sequencing_sample__sampleprepdetails__sample__submission__client__lab',
                 'flowcell__run__sequencing_date',
                 'sequencing_sample__sampleprepdetails__sample__submission__client')
    
    if contact_id:
        lanes = seqmodels.Lane.objects.filter(sequencing_sample__sampleprepdetails__sample__submission__client__id=contact_id).distinct()
    else:
        lanes = seqmodels.Lane.objects.filter(sequencing_sample__sampleprepdetails__sample__submission__client__lab__id=lab_id).distinct()

    dtr  = baseviews.DataTableResponse(request, seqmodels.Lane)
    qs   = dtr.filter_qs(lanes, SORT_COLS, search_fields=('num',))
    
    return render(request, 'seq/sequence-activity-data.json', {
        'dtr': dtr,
        'lanes': qs,
    })


def create_laneformsets(laneformsets, startIndex, run=None):
    data = {}
    if run:
        for lane in run.flowcell.lanes.all():
            data[lane.num] = []
            for ld in lane.loadingdetail_set.all():
                 data[lane.num].append({'phix_spike': lane.phix_spike, 
                                        'primer': lane.primer, 
                                        'sequencing_sample': ld.sequencing_sample,
                                        'loading_concentration': lane.loading_concentration, 
                                        'submitted_concentration': lane.submitted_concentration, 
                                        'percent_of_lane': ld.percent_of_lane})
 
    for i in range(startIndex + 1, 9):
        initial_data = [{'phix_spike': 1, 'primer': 'Genomic + Index', 'percent_of_lane': 100}]
        if i ==  8:
            initial_data = [{'phix_spike': 100, 'primer': 'Genomic', 'percent_of_lane': 100}]
        initial_data = data[i] if i in data else initial_data
        laneformsets.append(forms.LaneFormset(initial=initial_data, prefix='lane' + str(i)))

    phix_control_qs = lpmodels.SequencingSample.objects.filter(name__startswith='PhiX').order_by('-submitted_to_sequencing')
        
    if not 8 in data and phix_control_qs.exists():
        phix_control = phix_control_qs[0]
        laneformsets[7][0].initial['sequencing_sample'] = phix_control

    return laneformsets 

@login_required
@transaction.atomic
def flowcell_create(request, run=None):
    isLaneformsetValid = True
    flowcell = run.flowcell if run else None
    if request.method == 'POST':
        form = forms.FlowcellForm(request.POST, request.FILES, instance=flowcell, prefix='flowcell')
        runform = forms.RunForm(request.POST, instance=run, prefix='run')
        laneformsets = [ ]
        laneformset_num = 0

        if request.POST['number_of_lanes']:
            laneformset_num = int(request.POST['number_of_lanes'])
        
        for i in range(1, laneformset_num + 1):
            laneformsets.append(forms.LaneFormset(request.POST, prefix='lane' + str(i)))
            isLaneformsetValid = isLaneformsetValid & laneformsets[i - 1].is_valid()

        if isLaneformsetValid and form.is_valid() and runform.is_valid():
            flowcell = form.save()
            run = runform.save(commit=False)
            run.flowcell = flowcell
            run.manifold = form.cleaned_data['manifold']
            run.folder_name = form.cleaned_data['folder_name']
            run.save()
            for laneformset in laneformsets:
                lane = None
                for lane_form in laneformset:
                    cd = lane_form.cleaned_data
                    
                    if not lane:
                        lane, created = seqmodels.Lane.objects.get_or_create(num = cd['num'], flowcell = flowcell)
                        if not created:
                            lane.loadingdetail_set.all().delete()
                        lane.primer = cd['primer']
                        lane.phix_spike = cd['phix_spike']
                        lane.loading_concentration = cd['loading_concentration']
                        lane.submitted_concentration = cd['submitted_concentration']
                        lane.save()
                    
                    ld = seqmodels.LoadingDetail(lane = lane,
                              sequencing_sample = cd['sequencing_sample'],
                              percent_of_lane = cd['percent_of_lane'])

                    ld.save()

            messages.success(request, "Run  %s created/edited successfully." % (run.num))
            return HttpResponseRedirect(reverse('run-browse'))
        else:
            laneformsets = create_laneformsets(laneformsets, laneformset_num) 
    else:
        form = forms.FlowcellForm(instance=flowcell, prefix='flowcell')
        runform = forms.RunForm(instance=run, prefix='run')
        if run:
            form.initial['manifold'] = run.manifold
            form.initial['folder_name'] = run.folder_name
        runform.initial['index_length'] = 0 
        laneformsets = create_laneformsets([], 0, run)

    runtypes = seqmodels.RunType.objects.all()
    
    return render(request, 'seq/flowcell-create.html', {
        'form': form,
        'runform': runform,
        'laneformsets': laneformsets,
        'runtypes' : runtypes,
        'isLaneformsetValid': isLaneformsetValid,
        },
    )


@login_required
def flowcell_edit(request, key):
    isLaneformsetValid = True
    run = get_object_or_404(seqmodels.Run, num=key) 
    return flowcell_create(request, run)


@login_required
def flowcell_json(request):
    flowcell     = request.GET.get('name', None)
    json_payload = None
    if flowcell:
        json_payload = serializers.serialize("json", seqmodels.Flowcell.objects.filter(name=flowcell))
    else: 
        json_payload = serializers.serialize("json", seqmodels.Flowcell.objects.all())
    return HttpResponse(json_payload, content_type="application/json")

@login_required
def flowcell_browse(request):
    if not request.GET.get('sEcho', False):
        return render(request, 'seq/flowcell-browse.html', {})
    SORT_COLS = ('name', 'runtype')

    flowcells = seqmodels.Flowcell.objects.all().order_by('-date')
    dtr       = baseviews.DataTableResponse(request)
    qs        = dtr.filter_qs(flowcells, SORT_COLS, search_fields=('name',))

    return render(request, 'seq/flowcell-browse-data.json', {
        'dtr': dtr,
        'flowcells': qs,
        })


@login_required
def flowcell_detail(request, key=None):
    flowcell = get_object_or_404(seqmodels.Flowcell, name=key)
    lanes    = seqmodels.Lane.objects.filter(flowcell=flowcell.id)
     
    return render(request, "seq/flowcell-detail.html", {
        'flowcell': flowcell,
        'lanes': lanes, 
    })


@login_required
@transaction.atomic
def run_detail(request, key=None):
    run   = get_object_or_404(seqmodels.Run, num=key)
    lanes = seqmodels.Lane.objects.filter(flowcell=run.flowcell.id)
    status = ''
    if request.method == "POST":
        form = forms.RunChangeStatusForm(request.POST, instance=run, prefix='f')
        formset = forms.laneStatusFormset(request.POST, prefix='ls')
        if form.is_valid():
            run = form.save()
            if run.status == 'In Progress':
                util.send_run_hiseq_mail(run, request)
            elif run.status == 'Success':
                util.send_run_completed_mail(run, request)
                if formset.is_valid():
                    for frm in formset:
                        lane = frm.save()
            elif run.status == 'Failure':
                lanes.update(status = 'fail')

            status = 'Run status updated successfully'
    else:
        form = forms.RunChangeStatusForm(instance=run, prefix='f')
        formset = forms.laneStatusFormset(queryset=lanes, prefix='ls')
   
    samples = []
    for lane in lanes:
        for seq in lane.sequencing_sample.all():
            samples += seq.sampleprepdetails.all()
    submissions = sampmodels.Submission.objects.filter(sample__libprep__in=samples).distinct().defer('description')

    return render(request, "seq/run-detail.html", {
        'run': run,
        'lanes': lanes,
        'form': form,
        'formset': formset,
        'status': status,
        'submissions': submissions
    })


def seq_sample_queue_json(request, SORT_COLS=None, SEARCH_FIELDS=None):
    if request.GET.get('sEcho'):
        run_status = request.GET.get('status')
        direct_to_seq = request.GET.get('directToSeq') if request.GET.get('directToSeq') else ''
        run = request.GET.get("run_id") if request.GET.get("run_id") else 'NULL'
        run_type_id = request.GET.get("run_type_id")
         
        status = lpmodels.PrepStatus.objects.get(name='Cancel')
        seqqueue = lpmodels.SequencingSample.objects.exclude(sampleprepdetails__status=status).distinct()

        if run_type_id:
            seqqueue = seqqueue.filter(sampleprepdetails__sample__seq__runtype_id=run_type_id)
        
        if request.GET.get('miseq') == 'True':
            seqqueue = seqqueue.filter(sampleprepdetails__sample__seq__runtype__sequencer_model__name__icontains='MiSeq')
        elif direct_to_seq == 'true':
            seqqueue = seqqueue.filter(sampleprepdetails__preptype__isnull=True)
        
        if direct_to_seq != 'true':
            SORT_COLS = ('id', '', 'name', 
                ('sampleprepdetails__sample__specimen__submitted_by__lab__first_name',
                 'sampleprepdetails__sample__specimen__submitted_by__lab__last_name'), 
                ('sampleprepdetails__sample__specimen__submitted_by__person__first_name',
                 'sampleprepdetails__sample__specimen__submitted_by__person__last_name'),
                'sampleprepdetails__preptype__name',
                'submitted_to_sequencing',
                'sampleprepdetails__sample__seq__runtype',
                '', '', 'multiplex_type')

            SEARCH_FIELDS = ('name', 'multiplex_type', 'sampleprepdetails__preptype__name',
                'sampleprepdetails__sample__specimen__submitted_by__lab__first_name',
                'sampleprepdetails__sample__specimen__submitted_by__lab__last_name',  
                'sampleprepdetails__sample__specimen__submitted_by__person__first_name',
                'sampleprepdetails__sample__specimen__submitted_by__person__last_name',
                'sampleprepdetails__sample__seq__runtype__sequencer_model__name')
        
        SEARCH_COLS = ('name',
            ('sampleprepdetails__sample__specimen__submitted_by__lab__first_name',
             'sampleprepdetails__sample__specimen__submitted_by__lab__last_name'),
            ('sampleprepdetails__sample__specimen__submitted_by__person__first_name',
             'sampleprepdetails__sample__specimen__submitted_by__person__last_name'), 
            'sampleprepdetails__preptype__name',
            'submitted_to_sequencing',
            'sampleprepdetails__sample__seq__runtype__id',
            'multiplex_type')

        dtr = baseviews.DataTableResponse(request, lpmodels.SequencingSample)
        seqqueue = dtr.filter_qs(seqqueue, SORT_COLS, search_fields=SEARCH_FIELDS, search_cols=SEARCH_COLS,)
        
        query = str(seqqueue.query)
        if 'RawQuery:' in query:
            query = query.replace("<RawQuery: '", "").replace("'>", "") 
        query = addExtraJoinTables(query, run_status, run)
        seqqueue = prepareRawQuery(lpmodels.SequencingSample, query)

        if request.GET.get('forCombo'):
            resp = {"aaData": [[q.id, q.name] for q in seqqueue]}

            return HttpResponse(json.dumps(resp), content_type="application/json" )
        else:
            dtr.totaldisplay = getTotalCount(lpmodels.SequencingSample, query)
            
            if direct_to_seq == "true":
                return dtr, seqqueue
            else:
                lanes_needed = { }
                for q in seqqueue:
                    lanes_needed[q.id] = q.lanes_needed if q.lanes_needed else 0
                
                return render(request, 'seq/flowcell-sample-queue.json', {
                    'dtr': dtr,
                    'seqqueue': seqqueue,
                    'lanes_needed': lanes_needed,
                    }, content_type="application/json")
    else:
        seqId = request.GET.get('seqId')
        seqqueue = lpmodels.SequencingSample.objects.get(id=seqId)
        resp = {"data":[seqqueue.id, seqqueue.sampleprepdetails.all()[0].sample.seq.phix_spike,
                        seqqueue.loading_concentration, seqqueue.submitted_concentration, seqqueue.primer, 
                        seqqueue.sampleprepdetails.all()[0].index_length]}

        return HttpResponse(json.dumps(resp), content_type="application/json" )


def getLanesNeeded(seq) :
    result = seq.sampleprepdetails.all().aggregate(num_lanes=Max('sample__seq__num_lanes'))
    wanted = 0 if(result['num_lanes'] is None) else result['num_lanes']
    sequenced = seq.sequencing_sample.exclude(flowcell__run__status='Failure').count()

    return 0 if(wanted <= sequenced) else wanted - sequenced


@login_required
def sequence_sample_queue(request):
    return render(request, 'seq/sequence-sample-queue.html', {
        'datatable_select_color': settings.DATATABLE_SELECT_COLOR,
        'runtypes': seqmodels.RunType.active.all(),
    })


@login_required
def miseq_sequence_sample_queue(request):
    return render(request, 'seq/sequence-sample-queue.html', {
        'datatable_select_color': settings.DATATABLE_SELECT_COLOR,
        'runtypes': seqmodels.RunType.active.filter(sequencer_model__name='MiSeq'),
        'miseq': True,
    })


def addExtraJoinTables(query, status, run='NULL'):
    left_Join = ' LEFT OUTER JOIN (SELECT  LIBPREP_SEQUENCINGSAMPLEDETAIL.SEQUENCINGSAMPLE_ID as SEQUENCINGSAMPLE_ID, \
        max(SEQ_SAMPLESEQUENCINGDETAIL.NUM_LANES) As max_num_lanes FROM LIBPREP_SEQUENCINGSAMPLEDETAIL \
        LEFT OUTER JOIN LIBPREP_SAMPLEPREPDETAIL ON (LIBPREP_SEQUENCINGSAMPLEDETAIL.SAMPLEPREPDETAIL_ID = LIBPREP_SAMPLEPREPDETAIL.ID) \
        LEFT OUTER JOIN SAMPLE_SAMPLE On (LIBPREP_SAMPLEPREPDETAIL.SAMPLE_ID = SAMPLE_SAMPLE.ID) \
        LEFT OUTER JOIN SEQ_SAMPLESEQUENCINGDETAIL On (SAMPLE_SAMPLE.ID = SEQ_SAMPLESEQUENCINGDETAIL.SAMPLE_ID) \
        GROUP BY LIBPREP_SEQUENCINGSAMPLEDETAIL.SEQUENCINGSAMPLE_ID ORDER BY LIBPREP_SEQUENCINGSAMPLEDETAIL.SEQUENCINGSAMPLE_ID) TT2 \
        ON (LIBPREP_SEQUENCINGSAMPLE.ID = TT2.SEQUENCINGSAMPLE_ID)'

    left_Join += " LEFT OUTER JOIN (SELECT DISTINCT U1.SEQUENCING_SAMPLE_ID AS SEQUENCING_SAMPLE_ID,\
            SUM(CASE WHEN U4.STATUS = 'Failure' THEN 1 ELSE 0 END) AS  sum_Failure,\
            SUM(CASE WHEN U4.STATUS = 'Loading' THEN 1 ELSE 0 END) AS sum_Loading,\
            SUM(CASE WHEN U4.STATUS = 'In Progress' THEN 1 ELSE 0 END) AS sum_Progress,\
            SUM(CASE WHEN U4.STATUS = 'Success' THEN 1 ELSE 0 END) AS sum_Success\
            FROM SEQ_LOADINGDETAIL U1\
            INNER JOIN SEQ_LANE U2 ON (U1.LANE_ID = U2.ID)\
            INNER JOIN SEQ_FLOWCELL U3 ON (U2.FLOWCELL_ID = U3.ID)\
            INNER JOIN SEQ_RUN U4 ON (U3.ID = U4.FLOWCELL_ID)\
            WHERE U1.SEQUENCING_SAMPLE_ID IS NOT NULL AND U4.NUM != '"+ run +"' AND U2.STATUS = 'success' \
            GROUP BY U1.SEQUENCING_SAMPLE_ID\
            ORDER BY U1.SEQUENCING_SAMPLE_ID) TT1\
            ON (TT1.SEQUENCING_SAMPLE_ID = LIBPREP_SEQUENCINGSAMPLE.ID)"

    left_Join += " LEFT OUTER JOIN(SELECT DISTINCT X2.Sequencing_Sample_Id As SEQUENCING_SAMPLE_ID, \
            CASE WHEN X1.STATUS = 'Failure' THEN 0 ELSE 1 END  run_status\
            From Seq_Run X1 \
            INNER JOIN (SELECT DISTINCT U1.Sequencing_Sample_Id As Sequencing_Sample_Id, max(U4.Modified), max(U4.id) as run_id\
                  FROM SEQ_LOADINGDETAIL U1\
                  INNER JOIN SEQ_LANE U2 ON (U1.LANE_ID = U2.ID)\
                  INNER JOIN SEQ_FLOWCELL U3 ON (U2.FLOWCELL_ID = U3.ID)\
                  INNER JOIN SEQ_RUN U4 ON (U3.ID = U4.FLOWCELL_ID)\
                  Where U1.Sequencing_Sample_Id Is Not Null\
                  GROUP BY U1.Sequencing_Sample_Id) X2 On ( X2.run_id = X1.ID)\
            ORDER BY X2.Sequencing_Sample_Id) TT3 ON (TT3.SEQUENCING_SAMPLE_ID = LIBPREP_SEQUENCINGSAMPLE.ID)"

    condition = ''
    if status in 'completed':
        condition = ' WHERE (TT2.max_num_lanes IS NULL OR (TT2.max_num_lanes - TT1.sum_Success) <= 0) '

    if status in 'needed':
        condition = ' WHERE (TT2.max_num_lanes IS NOT NULL AND ((TT2.max_num_lanes - TT1.sum_Loading - TT1.sum_Progress - TT1.sum_Success) > 0 \
                            OR (TT1.sum_Progress IS NULL and TT2.max_num_lanes > 0)))'

    if query.__contains__('WHERE') and query.index('WHERE') < query.index('ORDER BY'): 
        token = ' WHERE'
        condition = condition + 'AND ' if condition else ' WHERE'
    else:
        condition += ' ORDER BY'
        token = ' ORDER BY'

    left_Join += condition
    select_part = 'SELECT DISTINCT  TT2.max_num_lanes, TT3.run_status,' \
                  '(CASE WHEN TT1.sum_loading IS NOT NULL THEN (TT2.max_num_lanes - TT1.sum_Loading - TT1.sum_Progress - TT1.sum_Success) '\
                  'ELSE TT2.max_num_lanes END) As lanes_needed, '

    query = query.replace('SELECT DISTINCT', select_part)
    query = query.replace('ORDER BY', 'ORDER BY TT3.run_status, ')
    query = query.replace(token, left_Join, 1)
    return query


def getTotalCount(module, query):
    startIndex = query.index('SELECT DISTINCT')
    endIndex = query.rindex('ASC') + 3
    subQuery = query[startIndex: endIndex]
    seqQuery = prepareRawQuery(module, subQuery)

    return len(list(seqQuery))


def miseq_spedsheet(request):
    seqId = request.GET.get('seqid')
    seq = get_object_or_404(lpmodels.SequencingSample, id=seqId)
    filename = 'MiSeq v2 Sample sheet'
    spreadsheet = file_manager.Spreadsheet(filename, [filename])
    
    reads = []
    for lane in seq.sequencing_sample.all():
        for r in lane.results.all():
            reads.append({0: r.num_reads})
    
    row = 14 + len(reads)
    headers = { 
                0: {0: "[Header]"},
                1: {0: "IEMFileVersion"},
                2: {0: "Investigator Name"},
                3: {0: "Project Name"},
                4: {0: "Experiment Name"},
                5: {0: "Date"},
                6: {0: "Workflow"},
                7: {0: "Application"},
                8: {0: "Assay"},
                9: {0: "Description"},
                10: {0: "Chemistry"},
                12: {0: "[Reads]"},
                row : {0: "[Settings]"},
                row + 1: {0: "Adapter"},
                row + 3: {0: "[Data]"},
                row + 4: {0: "Sample_ID", 1: "Sample_Name", 2: "Sample_Plate", 3: "Sample_Well", 
                            4: "I7_Index_ID", 5: "index", 6: "Sample_Project", 7: "Description", 8: "GenomeFolder"}
            }
            
    spreadsheet.set_font('Calibri', 220, True)
    spreadsheet.write_data(filename, headers)
    spreadsheet.set_font('Calibri', 220, False)
    
    spreadsheet.write_row(filename, 6, {1: 'Resequencing'})
    spreadsheet.write_row(filename, 7, {1: 'Resequencing'})
    spreadsheet.write_row(filename, row + 1, {1: 'AGATCGGAAGAGCACACGTC'})

    for i, r in enumerate(reads):
        spreadsheet.write_row(filename, 13 + i, r)

    row += 5
    project = None
    for spd in seq.sampleprepdetails.all(): 
        index_id = "".join(str(index) for index in spd.index.all())
        data = {0: str(spd.sample.specimen.gtac_id), 1: str(spd.sample), 4: str(index_id)}
        spreadsheet.write_row(filename, row, data)
        for p in spd.projects.all():
            project = p
        row += 1
    
    if project:
        spreadsheet.write_row(filename, 2, {1: str(project.prep_owner)})
        spreadsheet.write_row(filename, 3, {1: str(project.name)})
    
    return spreadsheet.create_excel_sheet()


def sequencing_calculation(request):
    run   = get_object_or_404(seqmodels.Run, num=request.GET.get('run_id'))
    lanes = seqmodels.Lane.objects.filter(flowcell=run.flowcell.id)
            
    spreadsheet = file_manager.Spreadsheet('Sequencing Calculations', ['Sheet1'])
    sheet = 'Sheet1'
    
    load_concen = [0, 0, 0, 0, 0, 0, 0, 0]
    subm_concen = [0, 0, 0, 0, 0, 0, 0, 0]
    for lane in lanes:
        for seq in lane.sequencing_sample.all():
            load_concen[lane.num] = int(seq.loading_concentration) if seq.loading_concentration else  0
            subm_concen[lane.num] = float(seq.submitted_concentration) if seq.submitted_concentration else 0
    
    lanes = ["Lane 1", "Lane 2", "Lane 3", "Lane 4", "Lane 5", "Lane 6", "Lane 7", "Lane 8"]
    temp = [8, 8, 8, 8, 8, 0, 2, 2]
    
    headers = {
            2: {0: "DNA Template (ul)", 4: 'Loading Concentration (pM)', 8: 'Submitted Concentration(nM)'},
            12: {0:'Denatured Concentration (nm)', 4: 'Denatured Concentration (pM)', 8: 'Diluted Denatured DNA (ul)'},
            23: {0: 'Master Mix 1', 4: 'Master Mix 2', 8: 'Master Mix 3'},
            35: {0: 'Buffer'}
        }
    
    spreadsheet.set_font('Calibri', 240, False, True)
    spreadsheet.write_data(sheet, headers)
    
    spreadsheet.set_font('Calibri', 220, True)
    spreadsheet.set_border(True, True, True, True)
    headers = {15: 'Loading pM', 16: 'Submitted nM', 17: 'Denatured nM', 20: 'DNA Template', 21: 'MM', 22: 'Denatured DNA', 23: 'HT1'}
    spreadsheet.write_row(sheet, 2, headers)

    spreadsheet.set_font('Calibri', 220, False)

    for i in range(8):
        spreadsheet.set_num_format_str(file_manager.FORMATS[0])
        data = {0: lanes[i], 1: temp[i], 4: lanes[i], 5: load_concen[i], 8: lanes[i], 9: subm_concen[i],
                14: lanes[i], 15: Formula('F'+ str(4 + i)), 
                19: lanes[i], 20: Formula('B'+ str(4 + i)), 21: 'mm1'}
        spreadsheet.write_row(sheet, 3 + i, data)

        data = {0: lanes[i], 4: lanes[i], 5: Formula('B'+ str(14 + i) +'*1000'), 8: lanes[i], 10: Formula('J'+str(14 + i)+'>8')}
        spreadsheet.write_row(sheet, 13 + i, data)
        
        spreadsheet.set_num_format_str(file_manager.FORMATS[2]) 
        data ={16: Formula('J'+ str(4 + i)), 17: Formula('B'+ str(14 + i)), 22: Formula('J'+ str(14 + i)),}
        spreadsheet.write_row(sheet, 3 + i, data)
        
        data = {1: Formula('B'+str(4 + i)+'/20*(J'+str(4 + i)+')'), 9: Formula('F'+str(4 + i)+'*1000/(F'+ str(14 + i) +')')}
        spreadsheet.write_row(sheet, 13 + i, data)
        
        spreadsheet.set_num_format_str(file_manager.FORMATS[1]) 
        spreadsheet.write_col(sheet, 3 + i, 23, Formula('B'+ str(37 + i)))
        
    spreadsheet.set_num_format_str(file_manager.FORMATS[0])
    master_mix = ["DNA Volume ul", "Lanes needed +1", "EB Volume ul", "NaOH ul", "Total NaOH ul", "Total EB ul", "Volume of MM1"]
    master_mix_data = [
        {0: master_mix[0], 2: 8, 4: master_mix[0], 6: 8, 8: master_mix[0], 10: 7},
        {0: master_mix[1], 2: Formula('COUNTIF(B4:B11, C25)+1'), 4: master_mix[1], 6: Formula('COUNTIF(B4:B11, G25)+1'), 
         8: master_mix[1], 10: Formula('COUNTIF(B4:B11, K25)+1')},
        {0: master_mix[2], 2: Formula('20-(C25+1)'), 4: master_mix[2], 6: Formula('20-(G25+1)'), 8: master_mix[2], 10: Formula('20-(K25+1)')},
        {0: master_mix[3], 2: 1, 4: master_mix[3], 6: 1, 8: master_mix[3], 10: 1},
        {0: master_mix[4], 2: Formula('C28*C26'), 4: master_mix[4], 6: Formula('G28*G26'), 8: master_mix[4], 10: Formula('K28*K26')},
        {0: master_mix[5], 2: Formula('C27*C26'), 4: master_mix[5], 6: Formula('G27*G26'), 8: master_mix[5], 10: Formula('K27*K26')},
        {0: master_mix[6], 2: Formula('C27+1'), 4: master_mix[6], 6: Formula('G27+1'), 8: master_mix[6], 10: Formula('K27+1')},
    ]
    
    spreadsheet.set_border(False, False, False, False)
    for i in range(7):
        spreadsheet.write_row(sheet, 24 + i, master_mix_data[i])
    
    spreadsheet.set_border(True, True, True, True)
    for i in range(8):
        spreadsheet.set_num_format_str(file_manager.FORMATS[0]) 
        spreadsheet.write_col(sheet, 36 + i, 0, lanes[i])
        spreadsheet.set_num_format_str(file_manager.FORMATS[2]) 
        spreadsheet.write_col(sheet, 36 + i, 1, Formula('1000-J'+ str(14 + i)))

    return spreadsheet.create_excel_sheet()
