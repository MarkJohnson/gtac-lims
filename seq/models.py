from django.db import models
from core import models as coremodels
from sample import models as sampmodels
from libprep.models import SequencingSample
from django.core.validators import MaxValueValidator, MinValueValidator
from django.template import defaultfilters
import logging
import string
import os

logger = logging.getLogger('myproject.custom')

def get_upload_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (filename.split('.')[0][:238], ext)
    return os.path.join('sequencing', filename)


class ActiveManager(models.Manager):
    def get_queryset(self):
        return super(ActiveManager, self).get_queryset().filter(is_active=True)


class SampleSequencingDetail(models.Model):
    sample = models.OneToOneField(sampmodels.Sample, related_name='seq', help_text='')
    runtype = models.ForeignKey("RunType", help_text='')
    num_lanes = models.IntegerField(default=1, help_text="Number of lanes sample will be run on")
    ready_date = models.DateField(help_text='Date the sample should be released for sequencing.')
    status = models.ForeignKey("SequencingStatus", help_text='')
    phix_spike = models.IntegerField(default=1, blank=True, null=True)

    def __unicode__(self):
        return u'%d' % (self.id)


    def lanes(self):
        "Returns a list of lanes that this sample was on"
        return Lane.objects.filter(denaturedaliquots__sequencingsample__sequencingsampledetail__sampleprepdetail__sample=self.sample)
    
    def processed_lanes(self):
        return [l for l in self.lanes() if l.postprocess_set.count() > 0]

    @property
    def runs(self):
        # FIXME  when denaturedaliquot is working in here, the path should be shorter
        # TODO
        return
    
    
    
    @models.permalink
    def get_absolute_url(self):
        return ('samplesequencingdetail-view', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass

class SequencingStatus(models.Model):
    name = models.CharField(max_length=255, help_text='')    

    def __unicode__(self):
        return u'%s' % (self.name)
    
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        verbose_name_plural = "Sequencing Status"
        pass

class SequencerModel(models.Model):
    name = models.CharField(max_length=255, help_text='')    
    image = models.ImageField(blank=True, null=True, upload_to='sequencer/', help_text='')
        
    def __unicode__(self):
        return u'%s' % (self.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('sequencermodel-view', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass

class ClusterStationModel(models.Model):
    name = models.CharField(max_length=255, help_text='')    
    image = models.ImageField(blank=True, null=True, upload_to='clusterstation/', help_text='')

        
    def __unicode__(self):
        return u'%s' % (self.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('clusterstationmodel-view', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass


class Instrument(coremodels.CoreModel):
    name = models.CharField(max_length=255,)
    serial = models.CharField(max_length=255, help_text='')
    #expired = models.DateField(blank=True, null=True, help_text='')
    is_active = models.BooleanField(default=True, help_text='')
    slug = models.SlugField(unique=True, editable=False, help_text='A "Slug" is a unique web-friendly title.')

    objects = models.Manager()
    active = ActiveManager()
    def __unicode__(self):
        return u'%s' % (self.name)

    class Meta:
        abstract = True
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        
    def save(self, *args, **kwargs):
        self.slug = defaultfilters.slugify(self.name)
        super(Instrument, self).save(*args, **kwargs)

class ClusterStation(Instrument):        
    model = models.ForeignKey(ClusterStationModel, help_text='')

    @models.permalink
    def get_absolute_url(self):
        return ('clusterstation-detail', [str(self.slug)])

class Sequencer(Instrument):        
    model = models.ForeignKey(SequencerModel, help_text='')
    max_runs_per_week = models.FloatField(blank=True, null=True, help_text='') 

    @models.permalink
    def get_absolute_url(self):
        return ('sequencer-detail', [str(self.slug)])

class Manifold(models.Model):
    name = models.CharField(max_length=10, help_text='')
    sequencer = models.ForeignKey(Sequencer, related_name='manifolds', help_text='')
    
    def __unicode__(self):
        return u'%s-%s' % (self.sequencer,self.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('manifold-view', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass

class FlowcellModel(models.Model):
    name = models.CharField(max_length=255, help_text='')
    model = models.ForeignKey(SequencerModel)
    num_lanes = models.SmallIntegerField("Number of Lanes", help_text='Number of Lanes', validators=[MinValueValidator(1),MaxValueValidator(8)]) 
   
    def __unicode__(self):
        return u'%s' % (self.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('flowcellmodel-view', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass

class RunType(coremodels.CoreModel):
    run_name = models.CharField(max_length=255, null=True, blank=True)
    read = models.CharField(max_length=10, choices=(('SR', 'Single Read'), ('PE', 'Paired End')), help_text='')
    length = models.IntegerField(help_text='')
    sequencer_model = models.ForeignKey(SequencerModel, help_text='')
    num_lanes = models.SmallIntegerField("Number of Lanes", help_text='Number of Lanes', validators=[MinValueValidator(1),MaxValueValidator(8)])
    custom = models.BooleanField(default=False, help_text='')
    is_active = models.BooleanField(default=True)
    basecode = models.ForeignKey('billing.BaseCode', blank=True, null=True)
    objects = models.Manager()
    active = ActiveManager()
    
    def __unicode__(self):
        return self.run_name
    
    @property
    def name(self):
        return self.run_name

    @models.permalink
    def get_absolute_url(self):
        return ('runtype-view', [str(self.id)])
    
    def generate_name(self):
        speed = 'standard'
        if (self.sequencer_model.name == 'HiSeq 2500'):
            if (self.num_lanes == 2):
                speed = 'rapid'
            elif (self.num_lanes == 8):
                speed = 'standard'
            else:
                speed = "%d lanes" % (self.num_lanes) 
        return u'%s %s - %d - %s' % (self.sequencer_model, self.get_read_display(), self.length, speed)
    
    def save(self, *args, **kwargs):
        if not self.run_name:
            self.run_name = self.generate_name()
        
        super(RunType, self).save(*args, **kwargs)
    
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass


class Flowcell(coremodels.CoreModel):
    name            = models.CharField("FC Serial Number", max_length=255, unique=True, db_index=True)
    runtype         = models.ForeignKey(RunType, help_text='')
    cluster_station = models.ForeignKey(ClusterStation, blank=True, null=True, verbose_name="cBOT")
    sbs_box1_lot    = models.CharField(max_length=255, blank=True, null=True, verbose_name="SBS Rapid Lot #")
    sbs_box2_lot    = models.CharField(max_length=255, blank=True, null=True, verbose_name="Rapid Cluster Kit Lot #") 
    index_lot       = models.CharField(max_length=255, blank=True, null=True, verbose_name="Index Lot #")
    cbot_lot        = models.CharField(max_length=255, blank=True, null=True, verbose_name="Cbot Rapid Duo Lot #")
    manifold_lot    = models.CharField(max_length=255, blank=True, null=True, verbose_name="Manifold Lot #")
    turnaround_lot  = models.CharField(max_length=255, blank=True, null=True, verbose_name="Turnaround Lot #")
    rehyb_lot       = models.CharField(max_length=255, blank=True, null=True, verbose_name="Re-hybrization Lot/Kit #")
    pr2_buffer      = models.CharField(max_length=255, blank=True, null=True, verbose_name="PR2 buffer #")
    box1            = models.CharField(max_length=255, blank=True, null=True, verbose_name="Box 1 #")
    box2            = models.CharField(max_length=255, blank=True, null=True, verbose_name="Box 2 #")
    phix_lot        = models.CharField(max_length=255, blank=True, null=True, verbose_name="PhiX Lot #")
    ht1             = models.CharField(max_length=255, blank=True, null=True, verbose_name="HT1 #")
    hp3             = models.CharField(max_length=255, blank=True, null=True, verbose_name="HP3 #")
    h2o             = models.CharField(max_length=255, blank=True, null=True, verbose_name="H2O #")
    flowcell_lot    = models.CharField(max_length=255, blank=True, null=True, verbose_name="Flowcell lot #")
    date            = models.DateField("Generation Date", db_column='Gen_date', help_text='')
    index_cycles    = models.IntegerField(default=0, help_text='')
    sample_denaturation = models.FileField(max_length=255, blank=True, null=True, upload_to=get_upload_path, verbose_name='Sample Denaturation')
    sample_sheet    = models.FileField(max_length=255, blank=True, null=True, upload_to=get_upload_path, verbose_name='Sample Sheet')

    
    def __unicode__(self):
        return u'%s' % (self.name)

    def save(self, *args, **kwargs):
        val = getattr(self, 'name', False)
        if val:
            setattr(self, 'name', val.upper())
        super(Flowcell, self).save(*args, **kwargs)
    
    @models.permalink
    def get_absolute_url(self):
        return ('flowcell-detail', [str(self.name)])
        
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass


LANE_STATUS = (
    ('fail', 'Fail'),
    ('success', 'Success')
)

class Lane(coremodels.CoreModel):    
    flowcell = models.ForeignKey(Flowcell, related_name='lanes', help_text='')
    sequencing_sample = models.ManyToManyField(SequencingSample, related_name='sequencing_sample', through='LoadingDetail')
    num = models.SmallIntegerField("Lane Number", help_text='Flowcell lane number.', validators=[MinValueValidator(1),MaxValueValidator(8)])
    primer = models.CharField(max_length=255, help_text='')
    phix_spike = models.CharField(max_length=255, help_text='')    
    loading_concentration = models.CharField(max_length=255, help_text='')    
    submitted_concentration = models.CharField(max_length=255, blank=True, null=True) 
    status = models.CharField(choices=LANE_STATUS, default='success', max_length=20, help_text='')

    def __unicode__(self):
        return u'Flowcell %s: Lane: %d' % (self.flowcell.name, self.num)
    
    @property
    def samples(self):
        return sampmodels.Sample.objects.all().filter(seq__denaturedaliquot=self.denaturedaliquot)

    @property
    def sequencingsamples_pretty(self):
        names = [ ] 
        for ss in self.sequencing_sample.all():
            names.append(ss.name)
        return string.join(names, ',')
        
         
    @models.permalink
    def get_absolute_url(self):
        return ('lane-detail', [str(self.flowcell.name), str(self.num)])
        
    class Meta:
        # get_latest_by = ""
        ordering = ['flowcell', 'num']
        unique_together = (('flowcell','num'), )
        # verbose_name_plural = ""
        
    def save(self, *args, **kwargs):
        super(Lane, self).save(*args, **kwargs)

class LoadingDetail(coremodels.CoreModel):
    lane = models.ForeignKey(Lane)
    sequencing_sample = models.ForeignKey(SequencingSample)
    percent_of_lane = models.CharField(max_length=3)

class Run(coremodels.CoreModel):  #NEED NEW NAME
    STATUS_CHOICES = [(x,x) for x in ['Loading', 'In Progress', 'Failure', 'Success']]
    INDEX_TYPES = (
        ('none', 'None'), 
        ('single', 'Single'),
        ('dual', 'Dual'),
    )
    
    num = models.CharField("Run Number", max_length=255,  help_text='', unique=True)
    flowcell = models.OneToOneField(Flowcell, help_text='')
    manifold = models.ForeignKey(Manifold, verbose_name="Machine/Manifold", blank=True, null=True, related_name="runs", help_text='')
    sequencing_date = models.DateField(blank=True, null=True, db_index=True, help_text='')
    #turnaround_date = models.DateField(blank=True, null=True, help_text='')
    #sequencing_kit = models.CharField(max_length=255, help_text='')
    folder_name = models.CharField(max_length=255, verbose_name="Run Folder", help_text='')
    software_version = models.CharField(max_length=255, help_text='')
    status = models.CharField(choices=STATUS_CHOICES, default="Loading", max_length=20, help_text='')
    index_length = models.SmallIntegerField(blank=True, null=True)
    note = models.CharField(blank=True, null=True, max_length=2000)
    index_note = models.CharField(blank=True, null=True, max_length=2000)
    index_type = models.CharField(choices=INDEX_TYPES, blank=False, null=False, max_length=255, default="none")

    def __unicode__(self):
        return u'%s' % (self.num)    
    
    @models.permalink
    def get_absolute_url(self):
        return ('run-detail', [str(self.num)])
        
    class Meta:
        # get_latest_by = ""
        ordering = [ '-num', ]
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass


class SequencedSample(models.Model):
    """Sample that has finished being sequenced"""
    
    samplesequencingdetail = models.ForeignKey(SampleSequencingDetail, help_text='')
    
    def __unicode__(self):
        return u'%s' % (self.sequencingsample.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('sequencedsample-view', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass

class RunResults(models.Model):
    class Meta:
        verbose_name_plural = 'Run Results'
    
    ill_run_id = models.CharField(max_length=255)
    total_cycles = models.IntegerField()
    read_cycles = models.IntegerField()
    index_cycles = models.IntegerField()
    folder_location = models.CharField(max_length=255)
    control_lane = models.IntegerField(null=True)
    rta_version = models.CharField(max_length=255)
    run = models.OneToOneField(Run, related_name='results', help_text='')

    def __unicode__(self):
        return u'%s' % (self.run.num)

class LaneResults(models.Model):
    class Meta:
        verbose_name_plural = 'Lane Results'
    
    lane = models.ForeignKey("Lane", related_name='results', help_text='')
    cluster_density =  models.FloatField()
    ivc_plot = models.ImageField(upload_to='ivc_plot/', null=True, blank=True)

    def __unicode__(self):
        return u'%s' % (self.lane.flowcell.name + ' - Lane ' + str(self.lane.num))

class ReadResults(models.Model):
    class Meta:
        verbose_name_plural = 'Read Results'

    laneresult = models.ForeignKey("LaneResults", related_name='readresults', help_text='')
    read_num = models.IntegerField()
    num_cycles = models.IntegerField()
    num_reads = models.IntegerField()
    percent_pass_filter = models.FloatField()
    percent_phasing = models.FloatField()
    percent_prephasing = models.FloatField()
    percent_q30 = models.FloatField()
    error_rate = models.FloatField()
    percent_aligned = models.FloatField()

    def __unicode__(self):
        return u'%s' % (self.laneresult.lane.flowcell.name  + ' - Lane ' + str(self.laneresult.lane.num) + ' - Read ' + str(self.read_num))

class SequencingBill(coremodels.CoreModel):
    run = models.ForeignKey(Run)
    lane = models.ForeignKey(Lane)
    basecode = models.ForeignKey('billing.BaseCode', blank=True, null=True)
    total_charge = models.FloatField(blank=True, null=True)
    recharge = models.FloatField(blank=True, null=True)
    stockroom = models.FloatField(blank=True, null=True)
    fund93 = models.FloatField(blank=True, null=True)
    scc_subsidy = models.FloatField(blank=True, null=True)
    icts_subsidy = models.FloatField(blank=True, null=True)

    @property
    def multipliers(self):
        multipliers = ''
        for billmultiplier in self.sequencingbillmultiplier_set.all():
            multipliers += str(billmultiplier) + ', '
        return multipliers[:-2]


class SequencingBillMultiplier(models.Model):
    sequencingBill = models.ForeignKey(SequencingBill)
    multiplier = models.ForeignKey('billing.MultiplierType', blank=True, null=True)

    def __unicode__(self):
        return u'%s' % (self.multiplier)
