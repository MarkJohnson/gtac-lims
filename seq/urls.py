from django.conf.urls import *


urlpatterns = patterns('',
    #url(r'^seqsamples/$', 'seq.views.sequencing_samples', name='seq-sequencing-samples' ),
    #url(r'^flowcell/create/$', 'seq.views.flowcell_create', name='flowcell-create'),
    url(r'^flowcell/samplequeue/$', 'seq.views.sequence_sample_queue', name='sample-queue'),
    url(r'^flowcell/miseqsamplequeue/$', 'seq.views.miseq_sequence_sample_queue', name='myseq-sample-queue'),
    url(r'^flowcell/seqsamplequeuejson/$', 'seq.views.seq_sample_queue_json', name='sample-queue-json'),
    url(r'^spredsheet/$', 'seq.views.miseq_spedsheet', name='miseq-spredsheet'),
    url(r'^sequencing_calculation/$', 'seq.views.sequencing_calculation', name='sequencing-calculation'),
    url(r'^run/create/$', 'seq.views.flowcell_create', name='run-create'),
    url(r'^run/edit/(?P<key>[\w-]+)/$', 'seq.views.flowcell_edit', name='run-edit'),
    url(r'^run/browse/$', 'seq.views.run_browse', name='run-browse'),
    url(r'^sequencing/contact/(?P<id>[\d-]+)/$', 'seq.views.sequencingContactActivity', name='sequencing-contact-activity'),
    url(r'^sequencing/lab/(?P<id>[\d-]+)/$', 'seq.views.sequencingLabActivity', name='sequencing-lab-activity'),
    url(r'^run/(?P<key>[\w-]+)/$', 'seq.views.run_detail', name='run-detail'),
    url(r'^flowcell/browse/$', 'seq.views.flowcell_browse', name='flowcell-browse'),
    url(r'^flowcell/(?P<key>[\w-]+)/$', 'seq.views.flowcell_detail', name='flowcell-detail' ),
)

