# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import django.core.validators
import django_extensions.db.fields
import seq.models


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0001_initial'),
        ('libprep', '0001_initial'),
        ('sample', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClusterStation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(max_length=255)),
                ('serial', models.CharField(help_text=b'', max_length=255)),
                ('is_active', models.BooleanField(default=True, help_text=b'')),
                ('slug', models.SlugField(help_text=b'A "Slug" is a unique web-friendly title.', unique=True, editable=False)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ClusterStationModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
                ('image', models.ImageField(help_text=b'', null=True, upload_to=b'clusterstation/', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Flowcell',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(unique=True, max_length=255, verbose_name=b'FC Serial Number', db_index=True)),
                ('sbs_box1_lot', models.CharField(max_length=255, null=True, verbose_name=b'SBS Rapid Lot #', blank=True)),
                ('sbs_box2_lot', models.CharField(max_length=255, null=True, verbose_name=b'Rapid Cluster Kit Lot #', blank=True)),
                ('index_lot', models.CharField(max_length=255, null=True, verbose_name=b'Index Lot #', blank=True)),
                ('cbot_lot', models.CharField(max_length=255, null=True, verbose_name=b'Cbot Rapid Duo Lot #', blank=True)),
                ('manifold_lot', models.CharField(max_length=255, null=True, verbose_name=b'Manifold Lot #', blank=True)),
                ('turnaround_lot', models.CharField(max_length=255, null=True, verbose_name=b'Turnaround Lot #', blank=True)),
                ('rehyb_lot', models.CharField(max_length=255, null=True, verbose_name=b'Re-hybrization Lot/Kit #', blank=True)),
                ('pr2_buffer', models.CharField(max_length=255, null=True, verbose_name=b'PR2 buffer #', blank=True)),
                ('box1', models.CharField(max_length=255, null=True, verbose_name=b'Box 1 #', blank=True)),
                ('box2', models.CharField(max_length=255, null=True, verbose_name=b'Box 2 #', blank=True)),
                ('phix_lot', models.CharField(max_length=255, null=True, verbose_name=b'PhiX Lot #', blank=True)),
                ('ht1', models.CharField(max_length=255, null=True, verbose_name=b'HT1 #', blank=True)),
                ('hp3', models.CharField(max_length=255, null=True, verbose_name=b'HP3 #', blank=True)),
                ('h2o', models.CharField(max_length=255, null=True, verbose_name=b'H2O #', blank=True)),
                ('flowcell_lot', models.CharField(max_length=255, null=True, verbose_name=b'Flowcell lot #', blank=True)),
                ('date', models.DateField(help_text=b'', verbose_name=b'Generation Date', db_column=b'Gen_date')),
                ('index_cycles', models.IntegerField(default=0, help_text=b'')),
                ('sample_denaturation', models.FileField(max_length=255, upload_to=seq.models.get_upload_path, null=True, verbose_name=b'Sample Denaturation', blank=True)),
                ('sample_sheet', models.FileField(max_length=255, upload_to=seq.models.get_upload_path, null=True, verbose_name=b'Sample Sheet', blank=True)),
                ('cluster_station', models.ForeignKey(verbose_name=b'cBOT', blank=True, to='seq.ClusterStation', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FlowcellModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
                ('num_lanes', models.SmallIntegerField(help_text=b'Number of Lanes', verbose_name=b'Number of Lanes', validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(8)])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Lane',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('num', models.SmallIntegerField(help_text=b'Flowcell lane number.', verbose_name=b'Lane Number', validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(8)])),
                ('primer', models.CharField(help_text=b'', max_length=255)),
                ('phix_spike', models.CharField(help_text=b'', max_length=255)),
                ('loading_concentration', models.CharField(help_text=b'', max_length=255)),
                ('submitted_concentration', models.CharField(max_length=255, null=True, blank=True)),
                ('status', models.CharField(default=b'success', help_text=b'', max_length=20, choices=[(b'fail', b'Fail'), (b'success', b'Success')])),
                ('flowcell', models.ForeignKey(related_name='lanes', to='seq.Flowcell', help_text=b'')),
            ],
            options={
                'ordering': ['flowcell', 'num'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LaneResults',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cluster_density', models.FloatField()),
                ('ivc_plot', models.ImageField(null=True, upload_to=b'ivc_plot/', blank=True)),
                ('lane', models.ForeignKey(related_name='results', to='seq.Lane', help_text=b'')),
            ],
            options={
                'verbose_name_plural': 'Lane Results',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LoadingDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('percent_of_lane', models.CharField(max_length=3)),
                ('lane', models.ForeignKey(to='seq.Lane')),
                ('sequencing_sample', models.ForeignKey(to='libprep.SequencingSample')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Manifold',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'', max_length=10)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReadResults',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('read_num', models.IntegerField()),
                ('num_cycles', models.IntegerField()),
                ('num_reads', models.IntegerField()),
                ('percent_pass_filter', models.FloatField()),
                ('percent_phasing', models.FloatField()),
                ('percent_prephasing', models.FloatField()),
                ('percent_q30', models.FloatField()),
                ('error_rate', models.FloatField()),
                ('percent_aligned', models.FloatField()),
                ('laneresult', models.ForeignKey(related_name='readresults', to='seq.LaneResults', help_text=b'')),
            ],
            options={
                'verbose_name_plural': 'Read Results',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Run',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('num', models.CharField(help_text=b'', unique=True, max_length=255, verbose_name=b'Run Number')),
                ('sequencing_date', models.DateField(help_text=b'', null=True, db_index=True, blank=True)),
                ('folder_name', models.CharField(help_text=b'', max_length=255, verbose_name=b'Run Folder')),
                ('software_version', models.CharField(help_text=b'', max_length=255)),
                ('status', models.CharField(default=b'Loading', help_text=b'', max_length=20, choices=[(b'Loading', b'Loading'), (b'In Progress', b'In Progress'), (b'Failure', b'Failure'), (b'Success', b'Success')])),
                ('index_length', models.SmallIntegerField(null=True, blank=True)),
                ('note', models.CharField(max_length=2000, null=True, blank=True)),
                ('index_note', models.CharField(max_length=2000, null=True, blank=True)),
                ('index_type', models.CharField(default=b'none', max_length=255, choices=[(b'none', b'None'), (b'single', b'Single'), (b'dual', b'Dual')])),
                ('flowcell', models.OneToOneField(to='seq.Flowcell', help_text=b'')),
                ('manifold', models.ForeignKey(related_name='runs', blank=True, to='seq.Manifold', help_text=b'', null=True, verbose_name=b'Machine/Manifold')),
            ],
            options={
                'ordering': ['-num'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RunResults',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ill_run_id', models.CharField(max_length=255)),
                ('total_cycles', models.IntegerField()),
                ('read_cycles', models.IntegerField()),
                ('index_cycles', models.IntegerField()),
                ('folder_location', models.CharField(max_length=255)),
                ('control_lane', models.IntegerField(null=True)),
                ('rta_version', models.CharField(max_length=255)),
                ('run', models.OneToOneField(related_name='results', to='seq.Run', help_text=b'')),
            ],
            options={
                'verbose_name_plural': 'Run Results',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RunType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('run_name', models.CharField(max_length=255, null=True, blank=True)),
                ('read', models.CharField(help_text=b'', max_length=10, choices=[(b'SR', b'Single Read'), (b'PE', b'Paired End')])),
                ('length', models.IntegerField(help_text=b'')),
                ('num_lanes', models.SmallIntegerField(help_text=b'Number of Lanes', verbose_name=b'Number of Lanes', validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(8)])),
                ('custom', models.BooleanField(help_text=b'')),
                ('is_active', models.BooleanField(default=True)),
                ('basecode', models.ForeignKey(blank=True, to='billing.BaseCode', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SampleSequencingDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('num_lanes', models.IntegerField(default=1, help_text=b'Number of lanes sample will be run on')),
                ('ready_date', models.DateField(help_text=b'Date the sample should be released for sequencing.')),
                ('phix_spike', models.IntegerField(default=1, null=True, blank=True)),
                ('runtype', models.ForeignKey(help_text=b'', to='seq.RunType')),
                ('sample', models.OneToOneField(related_name='seq', to='sample.Sample', help_text=b'')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SequencedSample',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('samplesequencingdetail', models.ForeignKey(help_text=b'', to='seq.SampleSequencingDetail')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sequencer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(max_length=255)),
                ('serial', models.CharField(help_text=b'', max_length=255)),
                ('is_active', models.BooleanField(default=True, help_text=b'')),
                ('slug', models.SlugField(help_text=b'A "Slug" is a unique web-friendly title.', unique=True, editable=False)),
                ('max_runs_per_week', models.FloatField(help_text=b'', null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SequencerModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
                ('image', models.ImageField(help_text=b'', null=True, upload_to=b'sequencer/', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SequencingBill',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('total_charge', models.FloatField(null=True, blank=True)),
                ('recharge', models.FloatField(null=True, blank=True)),
                ('stockroom', models.FloatField(null=True, blank=True)),
                ('fund93', models.FloatField(null=True, blank=True)),
                ('scc_subsidy', models.FloatField(null=True, blank=True)),
                ('icts_subsidy', models.FloatField(null=True, blank=True)),
                ('basecode', models.ForeignKey(blank=True, to='billing.BaseCode', null=True)),
                ('lane', models.ForeignKey(to='seq.Lane')),
                ('run', models.ForeignKey(to='seq.Run')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SequencingBillMultiplier',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('multiplier', models.ForeignKey(blank=True, to='billing.MultiplierType', null=True)),
                ('sequencingBill', models.ForeignKey(to='seq.SequencingBill')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SequencingStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
            ],
            options={
                'verbose_name_plural': 'Sequencing Status',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='sequencer',
            name='model',
            field=models.ForeignKey(help_text=b'', to='seq.SequencerModel'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='samplesequencingdetail',
            name='status',
            field=models.ForeignKey(help_text=b'', to='seq.SequencingStatus'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='runtype',
            name='sequencer_model',
            field=models.ForeignKey(help_text=b'', to='seq.SequencerModel'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='manifold',
            name='sequencer',
            field=models.ForeignKey(related_name='manifolds', to='seq.Sequencer', help_text=b''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='lane',
            name='sequencing_sample',
            field=models.ManyToManyField(related_name='sequencing_sample', through='seq.LoadingDetail', to='libprep.SequencingSample'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='lane',
            unique_together=set([('flowcell', 'num')]),
        ),
        migrations.AddField(
            model_name='flowcellmodel',
            name='model',
            field=models.ForeignKey(to='seq.SequencerModel'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='flowcell',
            name='runtype',
            field=models.ForeignKey(help_text=b'', to='seq.RunType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='clusterstation',
            name='model',
            field=models.ForeignKey(help_text=b'', to='seq.ClusterStationModel'),
            preserve_default=True,
        ),
    ]
