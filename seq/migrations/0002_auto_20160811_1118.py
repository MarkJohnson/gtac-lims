# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('seq', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clusterstation',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='clusterstation',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='clusterstation',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='flowcell',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='flowcell',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='flowcell',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='lane',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='lane',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='lane',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='loadingdetail',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='loadingdetail',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='loadingdetail',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='run',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='run',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='run',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='runtype',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='runtype',
            name='custom',
            field=models.BooleanField(default=False, help_text=b''),
        ),
        migrations.AlterField(
            model_name='runtype',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='runtype',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sequencer',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sequencer',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sequencer',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sequencingbill',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sequencingbill',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sequencingbill',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
    ]
