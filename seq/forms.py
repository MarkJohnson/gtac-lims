from django import forms
from seq import models as seqmodels
from libprep import models as lpmodels
from django.forms.models import modelformset_factory
from django.forms.formsets import formset_factory

class FlowcellForm(forms.ModelForm):
    cluster_station = forms.ModelChoiceField(seqmodels.ClusterStation.active.all(), label="cBot", required=False)
    runtype = forms.ModelChoiceField(seqmodels.RunType.active.all())
    manifold = forms.ModelChoiceField(seqmodels.Manifold.objects.filter(sequencer__is_active=True), label="Machine/Manifold", required=False)
    folder_name = forms.CharField(required=False, label='Folder Name')

    class Meta:
        model = seqmodels.Flowcell
        exclude = ('notes','index_cycles')
    
    def clean(self):
        cdata = self.cleaned_data
  
        if 'runtype' in cdata and 'HiSeq' in cdata['runtype'].run_name :
            if 'cluster_station' in cdata and not cdata['cluster_station']:
                self._errors["cluster_station"] = self.error_class(['This field is required'])
            if not cdata['sbs_box1_lot']:
                self._errors["sbs_box1_lot"] = self.error_class(['This field is required'])
            if not cdata['sbs_box2_lot']:
                self._errors["sbs_box2_lot"] = self.error_class(['This field is required'])
            if 'rapid' not in cdata['runtype'].run_name and not cdata['index_lot']:
                self._errors["index_lot"] = self.error_class(['This field is required'])
            if not cdata['cbot_lot']:
                self._errors["cbot_lot"] = self.error_class(['This field is required'])
            if not cdata['manifold_lot']:
                self._errors["manifold_lot"] = self.error_class(['This field is required'])
            if not cdata['manifold']:
                self._errors["manifold"] = self.error_class(['This field is required'])
        elif 'runtype' in cdata and 'MiSeq' in cdata['runtype'].run_name :
            if not cdata['pr2_buffer']:
                self._errors["pr2_buffer"] = self.error_class(['This field is required'])
            if not cdata['box1']:
                self._errors["box1"] = self.error_class(['This field is required'])
            if not cdata['box2']:
                self._errors["box2"] = self.error_class(['This field is required'])
            if not cdata['phix_lot']:
                self._errors["phix_lot"] = self.error_class(['This field is required'])
            if not cdata['ht1']:
                self._errors["ht1"] = self.error_class(['This field is required'])
            if not cdata['hp3']:
                self._errors["hp3"] = self.error_class(['This field is required'])
            if not cdata['h2o']:
                self._errors["h2o"] = self.error_class(['This field is required'])
            if not cdata['flowcell_lot']:
                self._errors["flowcell_lot"] = self.error_class(['This field is required'])
            if not cdata['folder_name']:
                self._errors["folder_name"] = self.error_class(['This field is required'])

        return cdata

class LaneForm(forms.Form):
     num                      = forms.IntegerField()
     phix_spike               = forms.CharField(label="phiX spike-in")
     loading_concentration    = forms.CharField(label="Loading Concentration")
     submitted_concentration  = forms.CharField(label="Submitted Concentration")
     primer                   = forms.CharField()
     percent_of_lane          = forms.CharField()
     sequencing_sample        = forms.ModelChoiceField(lpmodels.SequencingSample.objects.all().order_by('-submitted_to_sequencing'))

LaneFormset = formset_factory(LaneForm, extra=0) 



class RunForm(forms.ModelForm):
    #manifold = forms.ModelChoiceField(seqmodels.Manifold.objects.filter(sequencer__is_active=True), required=False)
    index_note = forms.CharField(widget= forms.Textarea(attrs={'rows': 3, 'cols': 20}), label='Index Note', required=False)
    class Meta:
        model = seqmodels.Run
        exclude = ('software_version', 'status', 'flowcell', 'note', 'manifold', 'folder_name')
        widgets = {
            'index_length' : forms.TextInput,
        }
 
class RunChangeStatusForm(forms.ModelForm):
    num = forms.CharField(widget = forms.TextInput(attrs={'readonly':'readonly'}))
    note = forms.CharField(widget= forms.Textarea(attrs={'rows': 5, 'cols': 20}), label='Note', required=False)
    class Meta:
        model = seqmodels.Run
        exclude = ('folder_name', 'software_version', 'flowcell', 'manifold', 'index_length', 'sequencing_date', 'index_type')


class LaneChangeStatusForm(forms.ModelForm):
    class Meta:
        model = seqmodels.Lane
        fields = ['id', 'status']

laneStatusFormset = modelformset_factory(seqmodels.Lane, form = LaneChangeStatusForm, extra = 0)
