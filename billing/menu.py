import menus

menus.named_view_factory('billing-libprep', "Libprep Billing", '[Billing] Libprep')
menus.named_view_factory('billing-sequence', "Sequence Billing", '[Billing] Sequence')
menus.named_view_factory('basecode-list', "Libprep Billing Codes", '[Billing] Libprep Billing Codes')
