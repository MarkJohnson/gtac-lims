from django import forms
from django.forms.models import BaseModelFormSet
from django.forms.models import inlineformset_factory, modelformset_factory
from django.forms.formsets import formset_factory
from base import models as basemodels
from billing import models as billmodels
from libprep import models as lpmodels
from seq import models as seqmodels

class SampleBillingDetailForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(SampleBillingDetailForm, self).__init__(*args, **kwargs)
        self.filter_queryset()

    def filter_queryset(self):
        if self.instance:
            qs = billmodels.LibprepCode.objects.filter(preptype=self.instance.sample.libprep.preptype).filter(location=self.instance.sample.submission.client.lab.location)
            self.fields['libprep_code'].queryset = qs


# class LibprepBillingCodeForm(BaseModelFormSet):
    # class Meta:
        # model = billmodels.SampleBillingDetail


# LibprepBillingCodeFormset = modelformset_factory(billmodels.SampleBillingDetail, form=SampleBillingDetailForm, extra=0)


class LibprepProjectForm(forms.Form):
    projects = forms.ModelMultipleChoiceField(lpmodels.Project.objects.all())


class LibprepBillingForm(forms.Form):
    project = forms.ModelChoiceField(lpmodels.Project.objects.all())
    basecode = forms.ModelChoiceField(billmodels.BaseCode.objects.all())
    batch_multiplier = forms.ModelChoiceField(billmodels.MultiplierType.objects.filter(name__icontains='batch'), required=False)
    assoc_multiplier = forms.ModelMultipleChoiceField(billmodels.MultiplierType.objects.filter(name__icontains='Affiliation'), required=False)
    misc_multiplier = forms.ModelChoiceField(billmodels.MultiplierType.objects.filter(name__icontains='Misc'), required=False)
    ext_multiplier = forms.ModelChoiceField(billmodels.MultiplierType.objects.filter(name__icontains='Location'), required=False)
    no_of_chips = forms.IntegerField(initial = 1, widget=forms.TextInput(attrs={'size': '3', 'maxlength': '3'}))

    def clean(self):
        cdata = self.cleaned_data
        if 'basecode' in cdata:
            multipliers = billmodels.BaseCodeMultiplier.objects.filter(basecode=cdata['basecode'])
            if cdata['basecode'].name != 'BioA' and not cdata['batch_multiplier']:
                self._errors["batch_multiplier"] = self.error_class(["This field is required"])
            elif cdata['basecode'].name != 'BioA' and not multipliers.filter(type=cdata['batch_multiplier']):
                    raise forms.ValidationError("Selected batch miltiplier is not related to basecode of %s. " % cdata['project'])
        
            if cdata['assoc_multiplier'] and len(multipliers.filter(type__in=cdata['assoc_multiplier'])) != len(cdata['assoc_multiplier']):
                raise forms.ValidationError("Selected affiliation multiplier is not related to basecode of %s " % cdata['project'])
            if cdata['misc_multiplier'] and not multipliers.filter(type=cdata['misc_multiplier']):
                raise forms.ValidationError("Selected misc miltiplier is not related to basecode of %s. " % cdata['project'])
        
        return cdata


def make_libprep_billing_formset(num_forms):
    return formset_factory(LibprepBillingForm, extra=num_forms)


class SequenceRunForm(forms.Form):
    runs = forms.ModelMultipleChoiceField(seqmodels.Run.objects.exclude(status='In Progress'))


class SequenceBillingForm(forms.Form):
    lane = forms.ModelChoiceField(seqmodels.Lane.objects.all())
    basecode = forms.ModelChoiceField(billmodels.BaseCode.objects.all())
    assoc_multiplier = forms.ModelMultipleChoiceField(billmodels.MultiplierType.objects.filter(name__icontains='Affiliation'), required=False)
    misc_multiplier = forms.ModelChoiceField(billmodels.MultiplierType.objects.filter(name__icontains='Misc'), required=False)
    ext_multiplier = forms.ModelChoiceField(billmodels.MultiplierType.objects.filter(name__icontains='Location'), required=False)

    def clean(self):
        cdata = self.cleaned_data
        if 'assoc_multiplier' in cdata and cdata['assoc_multiplier'] and 'basecode' in cdata:
            multipliers = billmodels.BaseCodeMultiplier.objects.filter(type__in=cdata['assoc_multiplier'], basecode=cdata['basecode'])
            if not multipliers and len(multipliers) != len(cdata['assoc_multiplier']):
                raise forms.ValidationError("Selected affiliation multiplier is not related to basecode of %s " % cdata['lane'])
        if 'misc_multiplier' in cdata and cdata['misc_multiplier'] and 'basecode' in cdata:
            multiplier = billmodels.BaseCodeMultiplier.objects.filter(type=cdata['misc_multiplier'], basecode=cdata['basecode'])
            if not multiplier:
                raise forms.ValidationError("Selected misc miltiplier is not related to basecode of %s. " % cdata['lane'])
        
        return cdata


def make_Sequence_billing_formset(num_forms):
    return formset_factory(SequenceBillingForm, extra=num_forms)


bccformset = inlineformset_factory(billmodels.BaseCode, billmodels.BaseCodeChargeCode, can_delete=False, extra=0, fields='__all__')
bcmcformset = inlineformset_factory(billmodels.BaseCodeMultiplier, billmodels.BaseCodeMultiplierCharge, can_delete=False, extra=0, fields='__all__')
