# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import billing.models
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sample', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BaseCode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
                ('description', models.TextField(help_text=b'')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BaseCodeChargeCode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('cost', models.DecimalField(help_text=b'', max_digits=10, decimal_places=4)),
                ('basecode', models.ForeignKey(related_name='basecode_chargecodes', to='billing.BaseCode')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BasecodeChargecodeHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('cost', models.DecimalField(help_text=b'', max_digits=10, decimal_places=4)),
                ('basecodeChargecode', models.ForeignKey(to='billing.BaseCodeChargeCode')),
            ],
            options={
                'db_table': 'billing_basecodechargecode93e0',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BaseCodeMuliplierChgHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('value', models.DecimalField(max_digits=10, decimal_places=4)),
            ],
            options={
                'db_table': 'billing_basecodemuliplierc4e0a',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BaseCodeMultiplier',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('basecode', models.ForeignKey(to='billing.BaseCode')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BaseCodeMultiplierCharge',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('value', models.DecimalField(max_digits=10, decimal_places=4)),
                ('is_active', models.BooleanField(default=True)),
                ('basecodemultiplier', models.ForeignKey(to='billing.BaseCodeMultiplier')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ChargeCode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
                ('shortname', models.CharField(help_text=b'', max_length=255)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ChargeCodeMultiplier',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('value', models.DecimalField(max_digits=10, decimal_places=4)),
                ('chargecode', models.ForeignKey(to='billing.ChargeCode')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ICTSResearch',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('research_name', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MultiplierType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
                ('code', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PurchaseOrder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('number', models.CharField(max_length=255, null=True, db_column=b'po_number', blank=True)),
                ('file', models.FileField(null=True, upload_to=billing.models.get_po_path, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RechargeCore',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(max_length=512)),
                ('core', models.CharField(max_length=255, verbose_name=b'Core (Fund Code)')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SampleAssociation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SampleBillingAssociation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('association_grant', models.CharField(help_text=b'', max_length=255, null=True, blank=True)),
                ('project_id', models.CharField(help_text=b'', max_length=255, null=True, blank=True)),
                ('association', models.ForeignKey(verbose_name=b'Supplementary Funds', to='billing.SampleAssociation', help_text=b'')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SampleICTSAssociation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('investigator_first_name', models.CharField(max_length=255, null=True, verbose_name=b'Investigator First Name', blank=True)),
                ('investigator_last_name', models.CharField(max_length=255, null=True, verbose_name=b'Investigator Last Name', blank=True)),
                ('fund_grant_number', models.CharField(max_length=255, null=True, verbose_name=b'Fund Grant Number', blank=True)),
                ('funding_agency', models.CharField(max_length=255, null=True, verbose_name=b'Fund Agency', blank=True)),
                ('grant_title', models.CharField(max_length=255, null=True, verbose_name=b'Grant Title', blank=True)),
                ('grant_pi_first_name', models.CharField(max_length=255, null=True, verbose_name=b'Grant PI First Name', blank=True)),
                ('grant_pi_last_name', models.CharField(max_length=255, null=True, verbose_name=b'Grant PI Last Name', blank=True)),
                ('approval_status', models.CharField(max_length=255, verbose_name=b'Approval Status')),
                ('active_protocol_title', models.CharField(max_length=255, null=True, verbose_name=b'Active HRPO/IFB Protocol Title', blank=True)),
                ('active_number', models.CharField(max_length=255, null=True, verbose_name=b'Active HRPO/IFB Number', blank=True)),
                ('approval_date', models.DateTimeField(null=True, verbose_name=b'HRPO/IRB Approval Date', blank=True)),
                ('iacuc_number', models.IntegerField(null=True, verbose_name=b'IACUC Number', blank=True)),
                ('iacuc_approval_date', models.DateTimeField(null=True, verbose_name=b'IACUC Approval Date', blank=True)),
                ('research', models.ManyToManyField(to='billing.ICTSResearch')),
                ('sampleBillingAssociation', models.ForeignKey(related_name='billing_association', to='billing.SampleBillingAssociation')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SubmissionBillingDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('department', models.CharField(help_text=b'', max_length=255, null=True, verbose_name=b'Department #', blank=True)),
                ('fund', models.CharField(help_text=b'', max_length=255, null=True, blank=True)),
                ('purchase_order', models.ForeignKey(related_name='submission', blank=True, to='billing.PurchaseOrder', null=True)),
                ('submission', models.ForeignKey(related_name='billing', to='sample.Submission')),
            ],
            options={
                'db_table': 'billing_submissionbillingd6aa0',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='samplebillingassociation',
            name='submissionbillingdetail',
            field=models.ForeignKey(related_name='associations', blank=True, to='billing.SubmissionBillingDetail', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='multipliertype',
            name='affiliation',
            field=models.ForeignKey(blank=True, to='billing.SampleAssociation', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='chargecodemultiplier',
            name='type',
            field=models.ForeignKey(to='billing.MultiplierType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='basecodemultipliercharge',
            name='chargecode',
            field=models.ForeignKey(to='billing.ChargeCode'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='basecodemultiplier',
            name='type',
            field=models.ForeignKey(to='billing.MultiplierType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='basecodemuliplierchghistory',
            name='baseCodeMultiplierCharge',
            field=models.ForeignKey(to='billing.BaseCodeMultiplierCharge'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='basecodechargecode',
            name='chargecode',
            field=models.ForeignKey(related_name='basecode_chargecodes', to='billing.ChargeCode'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='basecodechargecode',
            name='rechargecore',
            field=models.ForeignKey(to='billing.RechargeCore'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='basecodechargecode',
            unique_together=set([('basecode', 'chargecode')]),
        ),
    ]
