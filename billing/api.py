from tastypie.resources import ModelResource
#from tastypie import fields
from billing import models
#from tastypie.authorization import Authorization

class BatchMultiplierResource(ModelResource):
    class Meta:
        queryset = models.BaseCodeMultiplier.objects.all()

class BaseCodeResource(ModelResource):
    class Meta:
        queryset = models.BaseCode.objects.all()

