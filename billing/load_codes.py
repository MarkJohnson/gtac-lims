#!/usr/bin/env python

from django.db import transaction
from billing import models
from libprep import models as lpmodels
import csv

lines = list(csv.reader(open('lpcodes.csv', 'r')))


def add_code(l, pt, index, min, max, loc, exp):
    bc = models.LibprepCode.objects.get_or_create(preptype=pt, sample_min=min, sample_max=max, location=loc, expedited=exp)[0]
    if l[index] and float(l[index]) != 0.0:
        li = bc.lineitems.get_or_create(type=models.LibprepLineType.objects.get(name='Recharge'))[0]
        li.cost = l[index]
        li.save()
    if l[index+1] and float(l[index+1]) != 0.0:
        li = bc.lineitems.get_or_create(type=models.LibprepLineType.objects.get(name='Stockroom'))[0]
        li.cost = l[index+1]
        li.save()
    if l[index+2] and float(l[index+2]) != 0.0:
        li = bc.lineitems.get_or_create(type=models.LibprepLineType.objects.get(name='93 Fund'))[0]
        li.cost = l[index+2]
        li.save()
    
def do_group(l, loc, exp):
        if '/' in l[0]:
            preptypes = l[0].split('/')
        else:
            preptypes = l[:1]

        for preptype in preptypes:
            ptobj = lpmodels.PrepType.objects.get(name=preptype)
            add_code(l=l, pt=ptobj, index=2, min=1, max=5, loc=loc, exp=exp)
            add_code(l=l, pt=ptobj, index=6, min=6, max=12, loc=loc, exp=exp)
            add_code(l=l, pt=ptobj, index=10, min=13, max=47, loc=loc, exp=exp)
            add_code(l=l, pt=ptobj, index=14, min=48, max=95, loc=loc, exp=exp)
            add_code(l=l, pt=ptobj, index=18, min=96, max=96, loc=loc, exp=exp)

@transaction.commit_manually
def go():
    for l in lines[6:16]:
      loc = 'internal'
      exp = False
      try:
          do_group(l, loc, exp) 
          transaction.commit()
      except Exception, m:
          transaction.rollback()
          print m,l
       
    for l in lines[25:35]:
      loc = 'external'
      exp = False
      try:
          do_group(l, loc, exp) 
          transaction.commit()
      except Exception, m:
          transaction.rollback()
          print m,l

go()
