from django.conf.urls import *
from billing import views
from billing import forms

urlpatterns = patterns('',
    url(r'^libprep/$', 'billing.views.libprep', name='billing-libprep' ),
    url(r'^libprep-codes/$', 'billing.views.libprepcode_list', name='billing-libprepcode-list' ),
    url(r'^libprep-project-step1/$', 'billing.views.libprep_project_step1', name='billing-libprep-project-step1' ),
    url(r'^libprep-project-step2/$', 'billing.views.libprep_project_step2', name='billing-libprep-project-step2' ),
    url(r'^sequence/$', 'billing.views.sequence', name='billing-sequence' ),
    url(r'^basecodes/$', 'billing.views.get_basecode_list', name='basecode-list' ),
    url(r'^basecodes/(?P<id>\d+)/$', 'billing.views.basecode_detail_view', name='billing-detail' ),
    #url(r'^libprep-project-wizard', views.ProjectBillingWizard.as_view([forms.LibprepProjectForm, forms.LibprepBillingFormset])),
)
