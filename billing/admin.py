from django.contrib import admin
import models


class SampleBillingAssociationInline(admin.TabularInline):
    model = models.SampleBillingAssociation
    extra = 0

class BaseCodeMultiplierInline(admin.TabularInline):
    model = models.BaseCodeMultiplier
    extra = 0

class BaseCodeMultiplierChargeInline(admin.TabularInline):
    model = models.BaseCodeMultiplierCharge
    extra = 0

class ChargeCodeMultiplierInline(admin.TabularInline):
    model = models.ChargeCodeMultiplier
    extra = 0

class BaseCodeChargeCodeInline(admin.TabularInline):
    model = models.BaseCodeChargeCode
    extra = 0

class PurchaseOrderAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)
    # fieldsets = []
    # list_filter = []
    # search_fields = []
    # filter_horizontal = []
    # inlines = [ ModelInline ]
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.PurchaseOrder, PurchaseOrderAdmin)

class SampleAssociationAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)
    # fieldsets = []
    # list_filter = []
    # search_fields = []
    # filter_horizontal = []
    # inlines = [ ModelInline ]
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.SampleAssociation, SampleAssociationAdmin)

class ChargeCodeAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', )
    # fieldsets = []
    # list_filter = []
    # search_fields = []
    # filter_horizontal = []
    #inlines = [ ChargeCodeTypeMultiplierInline, ]
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.ChargeCode, ChargeCodeAdmin)

class MultiplierTypeAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', )
    # fieldsets = []
    # list_filter = []
    # search_fields = []
    # filter_horizontal = []
    #inlines = [ MultiplierInline ]
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.MultiplierType, MultiplierTypeAdmin)

class ChargeCodeMultiplierAdmin(admin.ModelAdmin):
    list_display = ('chargecode', 'type', 'value')
    # fieldsets = []
    list_filter = ['chargecode', 'type']
    # search_fields = []
    # filter_horizontal = []
    # inlines = [ ModelInline ]
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.ChargeCodeMultiplier, ChargeCodeMultiplierAdmin)

class RechargeCoreAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', )
    # fieldsets = []
    # list_filter = []
    # search_fields = []
    # filter_horizontal = []
    # inlines = [ ModelInline ]
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.RechargeCore, RechargeCoreAdmin)

class BaseCodeAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'cost')
    # fieldsets = []
    # list_filter = []
    search_fields = ['name', 'description',]
    # filter_horizontal = []
    inlines = [ BaseCodeChargeCodeInline, BaseCodeMultiplierInline ]
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.BaseCode, BaseCodeAdmin)

class BaseCodeMultiplierAdmin(admin.ModelAdmin):
    list_display = ('basecode', 'type')
    # fieldsets = []
    # list_filter = []
    #search_fields = ['name', 'description',]
    # filter_horizontal = []
    inlines = [ BaseCodeMultiplierChargeInline ]
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.BaseCodeMultiplier, BaseCodeMultiplierAdmin)

class BasecodeChargecodeHistoryAdmin(admin.ModelAdmin):
    list_display = ('basecodeChargecode', 'created', 'cost', )
    
admin.site.register(models.BasecodeChargecodeHistory, BasecodeChargecodeHistoryAdmin)

class BaseCodeMuliplierChgHistoryAdmin(admin.ModelAdmin):
    list_display = ('baseCodeMultiplierCharge', 'created', 'value')

admin.site.register(models.BaseCodeMuliplierChgHistory, BaseCodeMuliplierChgHistoryAdmin)