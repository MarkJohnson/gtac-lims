from django.db import models
from django.db.models.signals import post_save
from sample import models as sampmodels
from libprep import models as lpmodels
from seq.models import SequencedSample
from seq import models as seqmodels
from base.models import Lab
from core import models as coremodels
from datetime import date
import os
import uuid
from decimal import Decimal, ROUND_DOWN
import operator


class SampleBillingAssociation(coremodels.CoreModel):
    submissionbillingdetail = models.ForeignKey("SubmissionBillingDetail", related_name='associations', blank=True, null=True)
    association = models.ForeignKey("SampleAssociation", verbose_name='Supplementary Funds', help_text='')
    association_grant = models.CharField(max_length=255, blank=True, null=True, help_text='')
    project_id = models.CharField(max_length=255, blank=True, null=True, help_text='')

    def __unicode__(self):
        return u'%d' % (self.id)

class SubmissionBillingDetail(coremodels.CoreModel):
    submission = models.ForeignKey(sampmodels.Submission, related_name='billing')
    purchase_order = models.ForeignKey("PurchaseOrder", blank=True, null=True, related_name='submission')
    department = models.CharField("Department #", blank=True, null=True,  max_length=255, help_text='')
    fund = models.CharField(max_length=255, blank=True, null=True, help_text='')

    class Meta:
        db_table = 'billing_submissionbillingd6aa0'
        
    def __unicode__(self):
        return u'%d' % (self.id)

class SampleAssociation(models.Model):
    name = models.CharField(max_length=255, help_text='')
    
    def __unicode__(self):
        return u'%s' % (self.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('sampleassociation-view', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass

def get_po_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('submission/billing', filename)

class SampleICTSAssociation(models.Model):
    sampleBillingAssociation = models.ForeignKey("SampleBillingAssociation", related_name="billing_association")
    investigator_first_name =  models.CharField(max_length=255, verbose_name="Investigator First Name", blank=True, null=True)
    investigator_last_name =  models.CharField(max_length=255, verbose_name="Investigator Last Name", blank=True, null=True)
    fund_grant_number = models.CharField(max_length=255, blank=True, null=True, verbose_name="Fund Grant Number")
    funding_agency = models.CharField(max_length=255, verbose_name="Fund Agency", blank=True, null=True)
    grant_title = models.CharField(max_length=255, verbose_name="Grant Title", blank=True, null=True)
    grant_pi_first_name = models.CharField(max_length=255, verbose_name="Grant PI First Name", blank=True, null=True)
    grant_pi_last_name = models.CharField(max_length=255, verbose_name="Grant PI Last Name", blank=True, null=True)
    approval_status = models.CharField(max_length=255, verbose_name="Approval Status")
    active_protocol_title = models.CharField(max_length=255, verbose_name="Active HRPO/IFB Protocol Title", blank=True, null=True)
    active_number = models.CharField(max_length=255, verbose_name="Active HRPO/IFB Number",blank=True, null=True)
    approval_date = models.DateTimeField(verbose_name="HRPO/IRB Approval Date", blank=True, null=True)
    iacuc_number = models.IntegerField(verbose_name="IACUC Number", blank=True, null=True)
    iacuc_approval_date = models.DateTimeField(verbose_name="IACUC Approval Date", blank=True, null=True)
    research = models.ManyToManyField("ICTSResearch")


class ICTSResearch(models.Model):
    research_name = models.CharField(max_length=255)


class PurchaseOrder(coremodels.CoreModel):
    number = models.CharField(max_length=255, blank=True, null=True, db_column='po_number')
    file = models.FileField(upload_to=get_po_path, blank=True, null=True)

    def __unicode__(self):
        return u'%s' % (self.number)
    
    @models.permalink
    def get_absolute_url(self):
        return ('purchaseorder-view', [str(self.id)])

class BaseCode(coremodels.CoreModel):
    name = models.CharField(max_length=255, help_text='')
    description = models.TextField(help_text='')
    #cost = models.FloatField(help_text='')

    @property
    def cost(self):
        return sum([cc.cost for cc in self.basecode_chargecodes.all()])
    
    def __unicode__(self):
        return u'%s' % (self.name)

    @property
    def batch_multipliers(self):
        return MultiplierType.objects.filter(name='Batch', multipliers__chargecode__basecode=self).distinct()
    
    @models.permalink
    def get_absolute_url(self):
        return ('basecode-view', [str(self.id)])
        
    class Meta(coremodels.CoreModel.Meta):
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass


class RechargeCore(coremodels.CoreModel):
    name = models.CharField(max_length=512,)
    core = models.CharField("Core (Fund Code)", max_length=255)

    def __unicode__(self):
        return u'%s' % (self.core)
    

class ChargeCode(coremodels.CoreModel):
    name = models.CharField(max_length=255, help_text='')
    shortname = models.CharField(max_length=255, help_text='')

    def __unicode__(self):
        return u'%s' % (self.name)
    
class BaseCodeChargeCode(coremodels.CoreModel):
    basecode = models.ForeignKey(BaseCode, related_name='basecode_chargecodes')
    chargecode = models.ForeignKey(ChargeCode, related_name='basecode_chargecodes')
    rechargecore = models.ForeignKey(RechargeCore)
    cost = models.DecimalField(max_digits=10, decimal_places=4, help_text='')

    def __unicode__(self):
        return u'%s - %s' % (self.basecode, self.chargecode.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('basecode-chargecode-view', [str(self.id)])
        
    class Meta(coremodels.CoreModel.Meta):
        unique_together = ('basecode', 'chargecode')
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass

class BasecodeChargecodeHistory(coremodels.CoreModel):
    basecodeChargecode = models.ForeignKey(BaseCodeChargeCode)
    cost = models.DecimalField(max_digits=10, decimal_places=4, help_text='')
    
    class Meta:
        db_table = "billing_basecodechargecode93e0" 
    
    def __unicode__(self):
        return u'%s' % (self.basecodeChargecode)


class BaseCodeMultiplier(coremodels.CoreModel):
    type = models.ForeignKey("MultiplierType",)
    basecode = models.ForeignKey(BaseCode)

    def __unicode__(self):
        return u'%s: %s' % (self.basecode, self.type)

class BaseCodeMultiplierCharge(coremodels.CoreModel):
    basecodemultiplier = models.ForeignKey(BaseCodeMultiplier)
    chargecode = models.ForeignKey(ChargeCode)
    value = models.DecimalField(max_digits=10, decimal_places=4, )
    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s: %s - %s' % (self.basecodemultiplier, self.chargecode, self.value)


class BaseCodeMuliplierChgHistory(coremodels.CoreModel):
    baseCodeMultiplierCharge = models.ForeignKey(BaseCodeMultiplierCharge)
    value = models.DecimalField(max_digits=10, decimal_places=4, )
    
    class Meta:
        db_table = "billing_basecodemuliplierc4e0a"
    
    def __unicode__(self):
        return u'BaseCodeMuliplierChgHistory: %s: %s' % (self.created, self.value)


class MultiplierType(coremodels.CoreModel):
    name = models.CharField(max_length=255, help_text='')
    code = models.CharField(max_length=255)
    affiliation = models.ForeignKey(SampleAssociation, blank=True, null=True)

    def __unicode__(self):
        return u'%s: %s' % (self.name, self.code)
    
    @models.permalink
    def get_absolute_url(self):
        return ('multipliertype-view', [str(self.id)])
        
    class Meta(coremodels.CoreModel.Meta):
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass

class ChargeCodeMultiplier(coremodels.CoreModel):
    type = models.ForeignKey(MultiplierType,)
    chargecode = models.ForeignKey(ChargeCode,)
    value = models.DecimalField(max_digits=10, decimal_places=4)

    def __unicode__(self):
        return u'%s: %s' % (self.chargecode, self.type)
    
    @models.permalink
    def get_absolute_url(self):
        return ('multiplier-view', [str(self.id)])
        
    class Meta(coremodels.CoreModel.Meta):
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass

def update_basecodeChargecodeHistory(sender, instance, **kwargs):
    basecodeChgCodeHist = BasecodeChargecodeHistory(basecodeChargecode=instance, cost=instance.cost)
    basecodeChgCodeHist.save();


def update_basecodeMultiplierChgHistory(sender, instance, **kwargs):
    bsMulChgHist = BaseCodeMuliplierChgHistory(baseCodeMultiplierCharge=instance, value=instance.value)
    bsMulChgHist.save();

post_save.connect(update_basecodeChargecodeHistory, sender=BaseCodeChargeCode)
post_save.connect(update_basecodeMultiplierChgHistory, sender=BaseCodeMultiplierCharge)

