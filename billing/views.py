from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import transaction
from billing import models as billmodels
from libprep import models as lpmodels
from seq import models as seqmodels
from billing import forms
from base import views as baseview
from django.core import serializers
from django.conf import settings
from util import file_manager
from formtools.wizard.views import CookieWizardView
from decimal import Decimal, ROUND_DOWN
import operator


@login_required
def libprep(request):
    if request.method == 'POST':
        page = 1
        if request.POST.get('page') == 'page1':
            form = forms.LibprepProjectForm(request.POST, prefix='pf')
            if form.is_valid():
                page = 2
                request.session['projects'] = form.cleaned_data['projects']
                FS = forms.make_libprep_billing_formset(0)
                data = []
                for project in request.session['projects']:
                    getInitialFormData(data, project)
                formset = FS(initial=data)
        else:
            page = 2
            FS = forms.make_libprep_billing_formset(0)
            formset = FS(request.POST)

            if formset.is_valid():
                projects = []
                for form in formset:
                    projects.append(form.cleaned_data)

                return project_billing(request, projects)

        if page == 2:
            chipTypes = getChipTypes(request.session['projects'])

            return render(request, 'billing/libprep-project-step2.html', {
                'chipTypes': chipTypes,
                'formset': formset,
            })
    else:
        form = forms.LibprepProjectForm(prefix='pf')

    return render(request, 'billing/libprep.html', {
        'form': form,
        'projects': lpmodels.Project.objects.filter(status__in=['prep completed','failed']),
        'datatable_select_color': settings.DATATABLE_SELECT_COLOR,
        })


def getInitialFormData(data, project):
    count = project.sampleprepdetail.all().count();
    batchtype = ''
    for type in billmodels.MultiplierType.objects.filter(name__icontains='batch'):
        code = type.code.split('-')
        if len(code) == 1 and int(code[0]) == count:
            batchtype = type
            break
        elif len(code) == 2 and int(code[0]) <= count and count <= int(code[1]):
            batchtype =  type
            break;
    
    spds = project.sampleprepdetail.all()
    form_data = {'project': project, 'basecode': project.preptype.basecode, 'batch_multiplier': batchtype,}
    form_data.update(getAffilAndExternalType(spds))
    data.append(form_data)


def getAffilAndExternalType(spds):
    data = {'assoc_multiplier': '', 'ext_multiplier': ''}
    if spds:
        for sub_bill in spds[0].sample.submission.billing.all():
            data['assoc_multiplier'] = []
            for x in sub_bill.associations.all():
                data['assoc_multiplier'] += x.association.multipliertype_set.all()
            break
        
        if spds[0].sample.specimen.submitted_by.lab.location == 'external':
            for type in billmodels.MultiplierType.objects.filter(name__icontains='Location'):
                data['ext_multiplier'] = type
                break;
    
    return data

def getAffiliationType(spds):
    submissionBill = spds[0].sample.submission.billing.all()
    if submissionBill:
        affiliation = ', '.join([ str(x.association) for x in submissionBill[0].associations.all()])
        for type in billmodels.MultiplierType.objects.filter(name__icontains='Affiliation'):
            if type.code in affiliation:
                return type
        
    return ''


def sequence(request):
    if request.method == 'POST':
        page = 1
        if request.POST.get('page') == 'page1':
            form = forms.SequenceRunForm(request.POST, prefix='pf')
            if form.is_valid():
                page = 2
                request.session['runs'] = form.cleaned_data['runs']
                FS = forms.make_Sequence_billing_formset(0)
                data = []
                for run in request.session['runs']:
                    for lane in seqmodels.Lane.objects.filter(flowcell=run.flowcell):
                        spds = []
                        for seq in lane.sequencing_sample.all():
                            spds = seq.sampleprepdetails.all()
                            if spds:
                                break;
                        lab = spds[0].sample.specimen.submitted_by.lab
                        baseCode = run.flowcell.runtype.basecode
                        if str(lab).lower() == 'wu gps':
                            baseCode = billmodels.BaseCode.objects.get(name='HS-PE101-Clinical')

                        form_data = {'lane': lane, 'basecode': baseCode}
                        form_data.update(getAffilAndExternalType(spds))
                        data.append(form_data)
                formset = FS(initial=data)
        else:
            page = 2
            FS = forms.make_Sequence_billing_formset(0)
            formset = FS(request.POST)
            if formset.is_valid():
                runs = {}
                for form in formset:
                    cdata = form.cleaned_data
                    run = cdata['lane'].flowcell.run
                    if runs.has_key(run):
                        runs[run].append(cdata)
                    else:
                        runs[run] = [cdata]
                
                return run_lane_billing(request, runs)
        
        if page == 2:
            runs, lanes = {}, {}
            for run in request.session['runs']:
                    for lane in seqmodels.Lane.objects.filter(flowcell=run.flowcell):
                        runs[lane.id] = run
                        lanes[lane.id] = 'Lane %d'%lane.num
                        for seq in lane.sequencing_sample.all():
                            spds = seq.sampleprepdetails.all()
                            if spds:
                                lanes[lane.id] = str(spds[0].sample.specimen.submitted_by.lab) + ': '+ lanes[lane.id]
                                break
                                
            return render(request, 'billing/sequence-run-step2.html', {
                'runs': runs,
                'lanes': lanes,
                'formset': formset,
            })
    else:
        form = forms.SequenceRunForm(prefix='pf')
    
    return render(request, 'billing/sequence.html', {
        'form': form,
        'runs': seqmodels.Run.objects.exclude(status='In Progress'),
        'datatable_select_color': settings.DATATABLE_SELECT_COLOR,
        })


@login_required
def libprepcode_list(request):
    return render(request, 'billing/libprepcode-list.html', {
        'codes': billmodels.LibprepCode.objects.all(),
        })

@login_required
def libprep_project_step1(request):
    return render(request, 'billing/libprep-project-step1.html', {
        'form': forms.LibprepProjectForm(),
        'projects': lpmodels.Project.objects.filter(status='Complete'),
        'datatable_select_color': settings.DATATABLE_SELECT_COLOR,        
    })

@login_required
def libprep_project_step2(request):
    #formset = forms.LibprepBillingFormset()
    formset = forms.make_libprep_billing_formset(7)

    return render(request, 'billing/libprep-project-step2.html', {
        'formset': formset,
    })

@transaction.atomic
def project_billing(request, projects):
    filename = 'Project_Billing'
    spreadsheet = billing_spreadsheet(filename, True)
    int_row = ext_row = 3
    prjCompDates = getCompletionDate(request.session['projects'])
    chiptypes = getChipTypes(request.session['projects'])
    
    for cdata in projects:
        first_row = True
        filename += '_' + str(cdata['project'].id)
        cdate = prjCompDates[cdata['project'].id] if cdata['project'].id in prjCompDates else ''
        chipType = chiptypes.get(cdata['project'].id)
        
        charges, is_external = createProjectBill_instance(cdata)
        for spd in cdata['project'].sampleprepdetail.all().order_by('sample__specimen__gtac_id'):
            billSpecdata = [cdate, spd.sample.specimen.gtac_id, str(spd), str(spd.preptype), 1, 91040, chipType]            
            row_data = create_row(cdata, [spd,], billSpecdata, charges, False, first_row)    	         
            if row_data['is_external']:
                ext_row += (2 if first_row else 1)
                spreadsheet.write_row('external', ext_row, row_data['row'])
            else:
                int_row += (2 if first_row else 1)
                spreadsheet.write_row('internal', int_row, row_data['row'])
            
            if cdata['basecode'].name == 'BioA':
                charges = {'total': '', 'stockroom': '', 'recharge': '', '93Fund':'', 'scc': '', 'icts': ''}
            first_row = False

    spreadsheet.filename = filename
    return spreadsheet.create_excel_sheet()


def createProjectBill_instance(cdata):
    spd = cdata['project'].sampleprepdetail.all()[0]
    client = spd.sample.specimen.submitted_by
    submissionBill = spd.sample.submission.billing.all()
    affiliation = ', '.join([ str(x.association) for x in submissionBill[0].associations.all()]) if submissionBill else ''
    is_external = True if client.lab.location == 'external' else False
    charges = get_charges(cdata, is_external)
    
    projectBill = lpmodels.ProjectBill()
    projectBill.project = cdata['project']
    projectBill.basecode = cdata['basecode']
    projectBill.total_charge = charges['total']
    projectBill.recharge = charges['recharge']
    projectBill.stockroom = charges['stockroom']
    projectBill.scc_subsidy = charges['scc']
    projectBill.icts_subsidy = charges['icts']
    projectBill.save()

    if 'batch_multiplier' in cdata:
        projectBill.projectbillmultiplier_set.create(multiplier=cdata['batch_multiplier'])
    for multiplier in cdata['assoc_multiplier']:
        projectBill.projectbillmultiplier_set.create(multiplier=multiplier)
    if cdata['misc_multiplier']:
        projectBill.projectbillmultiplier_set.create(multiplier=cdata['misc_multiplier'])
        
    if is_external:
        projectBill.fund93 = charges['93Fund']
        projectBill.projectbillmultiplier_set.create(multiplier=cdata['ext_multiplier'])
        
    projectBill.save()
    is_external = True if is_external or 'HHMI' in affiliation else False
    return charges, is_external


def getChipTypes(projects):
    chipTypes = {}

    prepdatadef = lpmodels.PrepDataDef.objects.filter(per_sample=False, prepstep__name='Results', label='Chip Type')
    prepdatas = lpmodels.PrepData.objects.filter(sampleprepdetail__isnull=True, prepdatadef__in=prepdatadef, project__in=projects)

    for p in prepdatas:
        chipTypes[p.project.id] = p.value

    return chipTypes


def getCompletionDate(projects):
    prepdatadef = lpmodels.PrepDataDef.objects.filter(per_sample=False, 
			prepstep__name__in=['Prep Data', 'Posthyb Data', 'Results'], label='Finish Date')
    prepdata = lpmodels.PrepData.objects.filter(prepdatadef__in=prepdatadef, project__in=projects).order_by('project')
     
    data = {}
    for pd in prepdata:
        if not pd.project.id in data:
            data[pd.project.id] = {pd.prepdatadef.prepstep.name: str(pd.value)}
        else:
            data[pd.project.id].update({pd.prepdatadef.prepstep.name: str(pd.value)})
        
    prjCompDates = {}
    for key, value in data.items():
        if 'Prep Data' in value:
            prjCompDates[key] = value['Prep Data']
        elif 'Posthyb Data' in value:
            prjCompDates[key] = value['Posthyb Data']
        elif 'Results' in value:
            prjCompDates[key] = value['Results']

    return prjCompDates


def run_lane_billing(request, runs):
    filename = 'Sequence_Billing'
    spreadsheet = billing_spreadsheet(filename)
    int_row = ext_row = clinical_row = 3

    for run in runs.keys():
        first_row = True
        filename += '_' + str(run.num)
        seqDate = run.sequencing_date.strftime('%m-%d-%Y') if run.sequencing_date else ''
        for cdata in runs[run]:
            run_lane = str(run.num) +'_'+ str(cdata['lane'].num)
            lane_status = cdata['lane'].status
            spds = []
            for seq in cdata['lane'].sequencing_sample.all():
                spds += seq.sampleprepdetails.all()

            lab_name = str(spds[0].sample.specimen.submitted_by.lab).lower()
            clinical_data = True if lab_name == 'wu gps' else False
            billSpecdata = [seqDate, run_lane, '', str(run.flowcell.runtype), 1, 91070 if clinical_data else 91050, lane_status]
            charges = createSequencingBill_instance(cdata, run, spds)
            row_data = create_row(cdata, spds, billSpecdata, charges, clinical_data)

            if clinical_data:
                clinical_row += 2
                spreadsheet.write_row('clinical', clinical_row, row_data['row'])          
            elif row_data['is_external']:
                ext_row += 2
                spreadsheet.write_row('external', ext_row, row_data['row'])
            else:
                int_row += 2
                spreadsheet.write_row('internal', int_row, row_data['row'])
            first_row = False
    spreadsheet.filename = filename
    return spreadsheet.create_excel_sheet()


def createSequencingBill_instance(cdata, run, spds):
    client = spds[0].sample.specimen.submitted_by
    is_external = True if client.lab.location == 'external' else False
    charges = get_charges(cdata, is_external)
    
    sequencingBill = seqmodels.SequencingBill()
    sequencingBill.run = run
    sequencingBill.lane = cdata['lane']
    sequencingBill.basecode = cdata['basecode']
    sequencingBill.total_charge = charges['total']
    sequencingBill.recharge = charges['recharge']
    sequencingBill.stockroom = charges['stockroom']
    sequencingBill.scc_subsidy = charges['scc']
    sequencingBill.icts_subsidy = charges['icts']
    sequencingBill.save()
    
    for multiplier in cdata['assoc_multiplier']:
        sequencingBill.sequencingbillmultiplier_set.create(multiplier=multiplier)
    if cdata['misc_multiplier']:
        sequencingBill.sequencingbillmultiplier_set.create(multiplier=cdata['misc_multiplier'])
        
    if is_external:
        sequencingBill.fund93 = charges['93Fund']
        sequencingBill.sequencingbillmultiplier_set.create(multiplier=cdata['ext_multiplier'])
    
    sequencingBill.save()
    return charges


INTERNAL_HEADERS = ['Dept#', 'PI (Last)', 'PI (First)', 'Contact', 'Completion Date', 'GTAC ID', 'Sample Names', 'Activity', 'Number', 'Notes',
                    'Billing Code', 'Total Charge', 'Recharge', 'Stockroom', 'Center', 'Affiliation', 'SCC subsidy', 'ICTS subsidy', 
                    'Internal Notes' ]

EXTERNAL_HEADERS = ['Institution', 'PI (Last)', 'PI (First)', 'Contact', 'PO#', 'Completion Date', 'GTAC ID', 'Number', 'Sample Names', 
                    'Activity', 'Notes', 'Billing Code', 'Total Charge', 'Recharge', 'Stockroom', '93 Fund', 'Center', 'Affiliation', 
                    'SCC subsidy', 'ICTS subsidy', 'Internal Notes']

CLINICAL_HEADERS = INTERNAL_HEADERS

def billing_spreadsheet(filename, isProject=False):
    sheet_names = ['internal', 'external']
    if not isProject:
        sheet_names.append('clinical')

    spreadsheet = file_manager.Spreadsheet(filename, sheet_names)
    spreadsheet.set_font('Calibri', 220, True)
   
    headers = ['Sample Status', 'Lane Status']
    for item in headers:
        if item in INTERNAL_HEADERS and item in EXTERNAL_HEADERS and item in CLINICAL_HEADERS:
            INTERNAL_HEADERS.remove(item)
            EXTERNAL_HEADERS.remove(item)
            CLINICAL_HEADERS.remove(item)

    extraHeader = 'Lane Status'
    if isProject:
        extraHeader = 'Sample Status'

    INTERNAL_HEADERS.append(extraHeader)
    EXTERNAL_HEADERS.append(extraHeader)
    CLINICAL_HEADERS.append(extraHeader)

    spreadsheet.write_row('internal', 3, INTERNAL_HEADERS)
    spreadsheet.write_row('external', 3, EXTERNAL_HEADERS)

    if not isProject:
        spreadsheet.write_row('clinical', 3, CLINICAL_HEADERS)

    spreadsheet.set_font('Calibri', 220, False)
    return spreadsheet


def create_row(cdata, spds, billSpecdata, charges, clinical_data, first_row=True):
    samples = ', '.join ([ss.name for ss in cdata['lane'].sequencing_sample.all()]) if 'lane' in cdata else ', '.join([str(spd) for spd in spds])
    client = spds[0].sample.specimen.submitted_by
    spec = spds[0].sample.specimen
    submissionBill = spds[0].sample.submission.billing.all()
    is_external = True if client.lab.location == 'external' else False
    affiliation = ', '.join([str(x.association) for x in submissionBill[0].associations.all().exclude(association__name='ICTS')]) if submissionBill else ''

    for x in cdata['assoc_multiplier']:
        if x.code not in affiliation:
            affiliation = affiliation + ', '+ x.code if affiliation else x.code

    affiliation = affiliation + " subsidy applied" if affiliation else ''
    
    dept = str(client.lab.department) if first_row else ''
    institution = str(client.lab.institution) if first_row else ''    

    if clinical_data:
        headers = CLINICAL_HEADERS
    elif is_external or 'HHMI' in affiliation:
        headers = EXTERNAL_HEADERS
    else:
        headers = INTERNAL_HEADERS

    row_data = {headers.index('PI (Last)'): client.lab.last_name, 
                headers.index('PI (First)'): client.lab.first_name,
                headers.index('Contact'): str(client.person),
                headers.index('Completion Date'): billSpecdata[0],
                headers.index('GTAC ID'): billSpecdata[1],
                headers.index('Sample Names'): samples,
                headers.index('Activity'): billSpecdata[3],
                headers.index('Number'): billSpecdata[4],
                headers.index('Center'): billSpecdata[5],
                headers.index('Billing Code'): billSpecdata[6] if billSpecdata[3] == 'Bioanalyzer only' else str(cdata['basecode']),
                headers.index('Total Charge'):charges['total'],
                headers.index('Recharge'): charges['recharge'],
                headers.index('Stockroom'): charges['stockroom'],
                headers.index('Affiliation'): affiliation,
                headers.index('SCC subsidy'): '' if charges['scc'] == 0 else charges['scc'],
                headers.index('ICTS subsidy'): '' if charges['icts'] == 0 else charges['icts'],
                }

    if is_external:
        row_data[headers.index('PO#')] = str(submissionBill[0].purchase_order) if submissionBill else ''
        row_data[headers.index('93 Fund')] = charges['93Fund']
        row_data[headers.index('Institution')] = institution
       
    if clinical_data:
        row_data[headers.index('Dept#')] = dept 
    elif 'HHMI' in affiliation:
        row_data[headers.index('Institution')] = dept  
    elif not is_external:
        row_data[headers.index('Dept#')] = dept

    if 'CDI' in affiliation:
        row_data[headers.index('Notes')] ='CDI Project ' + submissionBill[0].associations.all()[0].project_id

    if 'Sample Status' in headers:
        partial_prep = spds[0].sample.submission.partial_prep
        status = spds[0].status.name if spds[0].status.name == 'Failed' else 'Partial Prep' if partial_prep else '' 
        row_data[headers.index('Sample Status')] = status

    if 'Lane Status' in headers:
        row_data[headers.index('Lane Status')] = billSpecdata[6]

    return {'is_external': is_external or 'HHMI' in affiliation, 'row': row_data}


def get_charges(cdata, is_external = True):
    t = {}
    basecode = cdata['basecode']
    for cc in basecode.basecode_chargecodes.all():
        t[cc.chargecode] = [cc.cost, 1]
    
    if 'batch_multiplier' in cdata and cdata['batch_multiplier']:
        bcm = basecode.basecodemultiplier_set.filter(type=cdata['batch_multiplier'])[0]
        for m in bcm.basecodemultipliercharge_set.all():
            t[m.chargecode][1] = m.value

    if cdata['assoc_multiplier']:
        for bcm in basecode.basecodemultiplier_set.filter(type__in=cdata['assoc_multiplier']):
            for m in bcm.basecodemultipliercharge_set.all():
                t[m.chargecode].append(m.value)
    
    if cdata['misc_multiplier']:
        bcm = basecode.basecodemultiplier_set.filter(type=cdata['misc_multiplier'])[0]
        for m in bcm.basecodemultipliercharge_set.all():
            t[m.chargecode].append(m.value)
    
    for charge in t.keys():
        if 'Reagents' in charge.name:
            r = reduce(operator.mul, t[charge])
        elif 'Annuals' in charge.name:
            a = reduce(operator.mul, t[charge])
        elif 'Labor' in charge.name:
            l = reduce(operator.mul, t[charge])
    
    charges = {'total': make_round(r + a + l), 'stockroom': make_round(r), \
                'recharge': make_round(a + l), '93Fund':0, 'scc': 0, 'icts': 0}

    if is_external:
        total = 0
        bcm = basecode.basecodemultiplier_set.filter(type=cdata['ext_multiplier'])[0]
        for m in bcm.basecodemultipliercharge_set.all():
            total += reduce(operator.mul, t[m.chargecode]) * m.value
        
        charges['93Fund'] = make_round(total - charges['total'])
        charges['total'] = make_round(total)

    labor = billmodels.ChargeCode.objects.get(name='Labor')
    scc_multiplier = None
    for m in cdata['assoc_multiplier']:
        if 'SCC' in m.code:
            scc_multiplier = m
    
    if scc_multiplier:
        bcm = basecode.basecodemultiplier_set.filter(type=scc_multiplier)[0]
        for m in bcm.basecodemultipliercharge_set.all():
            if 'Labor' in m.chargecode.name: 
                charges['scc'] = make_round(t[labor][0] * t[labor][1] * (1 - m.value))
                break

    if 'no_of_chips' in cdata:
        for key in charges:
            charges[key] *= int(cdata['no_of_chips'])

    return charges


def make_round(value):
    return Decimal(value).quantize(Decimal('0'))


@transaction.atomic
def basecode_detail_view(request, id):
    charge_codes, recharge_cores, bcmcformset_dict = {}, {}, {}
    is_error = False
    basecode = billmodels.BaseCode.objects.get(id=id)          
    
    for cc in billmodels.ChargeCode.objects.all():
        charge_codes[cc.id] = str(cc.name)

    for rc in billmodels.RechargeCore.objects.all():
        recharge_cores[rc.id] = str(rc.core)

    if request.method == 'POST':
        bcc_formset = forms.bccformset(request.POST, instance=basecode, prefix='bcc')
        if bcc_formset.is_valid():
            bcc_formset.save()
            messages.add_message(request, messages.INFO, str(basecode)+" : Updated successfully.")
        else:
            is_error = True

        for bcm in basecode.basecodemultiplier_set.all():
            bcmc_formset = forms.bcmcformset(request.POST, instance=bcm, prefix=bcm)
            if bcmc_formset.is_valid():
                bcmc_formset.save()
                messages.add_message(request, messages.INFO, str(bcm.type)+" : Updated successfully.")
            else:
                is_error = True
            bcmcformset_dict[bcm] = bcmc_formset
    else:
        bcc_formset = forms.bccformset(prefix='bcc', instance=basecode)
        for bcm in basecode.basecodemultiplier_set.all():
            bcmcformset_dict[bcm] = forms.bcmcformset(prefix=bcm, instance=bcm)

    return render(request, 'billing/baseCode.html', {
        'is_error'      : is_error,
        'basecode'      : basecode,
        'chargecodes'   : charge_codes,
        'rechargecores' : recharge_cores,
        'bccformset'    : bcc_formset,
        'bcmcformset'   : bcmcformset_dict,
    })


def get_basecode_list(request):
    if not request.GET.get('sEcho', False):
        return render(request, 'billing/basecode-list.html', {})
    SORT_COLS = ('name', 'description')
    basecodes = billmodels.BaseCode.objects.all()

    dtr = baseview.DataTableResponse(request)
    qs = dtr.filter_qs(basecodes, SORT_COLS, search_fields=('name', 'description'))
  
    return render(request, 'billing/basecode-data.json', {
            'dtr': dtr,
            'basecodes': qs,
            })
