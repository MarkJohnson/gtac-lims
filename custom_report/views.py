from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, render_to_response, get_object_or_404
from sample import models as sampmodels
from libprep import models as lpmodels
from seq import models as seqmodels
from custom_report import forms
from util import file_manager as filemng
from datetime import date, datetime
from collections import OrderedDict
import json


def custom_report(request):
    if request.method == 'POST':	
        form = forms.CustomReport(request.POST, prefix='report')
        if form.is_valid():
            cdata = form.cleaned_data
            return generate_Spreadsheet(request, cdata)
    else:
        form = forms.CustomReport(prefix='report')
        
    return render(request, 'custom_report/custom_report.html', {
        'form': form,
    })


def get_model_fields(model):
    return {f.name: f.verbose_name.title() for f in model._meta.fields if f.name not in ('created', 'modified', 'key', '_order')}


def get_fields_verbose_name(model, fields):
    field_dict = get_model_fields(model)
    return [field_dict.get(f) for f in fields]


def get_result_value_list(obj, fields):
    list = []
    for f in fields:
        value = getattr(obj, f)
        value = value.strftime('%Y-%m-%d') if type(value) == datetime else value
        list.append(str(value))
    return list


def get_result_rows(object, fields):
    return [get_result_value_list(obj, fields) for obj in object]


def get_prepdata_fields(request, prepsteps):
    fields = []
    for ps in prepsteps:
        id_list = request.POST.getlist(ps.name)
        fields.extend([pdd.label for pdd in ps.prepdatadef_set.filter(id__in=id_list)])
    return fields    


def get_prepdata_fields_value(request, prepsteps, sample):
    values = []
    for ps in prepsteps:
        prepdatas = {p.prepdatadef.id: p for p in  lpmodels.PrepData.objects.filter(sampleprepdetail__sample=sample, prepdatadef__prepstep=ps, attempt=1).order_by('prepdatadef__id')}
        for f in request.POST.getlist(ps.name):
            if int(f) in prepdatas:
	        values.append(prepdatas[int(f)].value)
            else:
                values.append('')
    return values


def generate_Spreadsheet(request, cdata):
    spreadsheet = filemng.Spreadsheet('Custome Report', ['Report'])
    spreadsheet.set_font('Calibri', 240, True)
    if cdata['models'] == 'sample':
        prepsteps = []
        if cdata['preptype']:
            prepsteps = lpmodels.PrepStep.objects.filter(preptypeversion__preptype=cdata['preptype'])
        samples = lpmodels.SamplePrepDetail.objects.filter(sample__specimen__gtac_id__in=cdata['samples'])
        sample_fields = request.POST.getlist('Samplefields')
        specimen_fields = request.POST.getlist('Specimenfields')
        submission_fields = request.POST.getlist('Submissionfields')
        seq_fields = request.POST.getlist('Sequencing_Samplefields')

        fields = []
        fields.extend(get_fields_verbose_name(lpmodels.SamplePrepDetail, sample_fields))
        fields.extend(get_fields_verbose_name(sampmodels.Specimen, specimen_fields))
        fields.extend(get_fields_verbose_name(sampmodels.Submission, submission_fields))
        fields.extend(get_prepdata_fields(request, prepsteps))
        fields.extend(get_fields_verbose_name(lpmodels.SequencingSample, seq_fields))
        spreadsheet.write_row('Report', 0, fields)
        
        spreadsheet.set_font('Calibri', 220, False)
        row_num = 1
        for sample in samples:
            row= []
            row.extend(get_result_value_list(sample, sample_fields))
            row.extend(get_result_value_list(sample.sample.specimen, specimen_fields))
            row.extend(get_result_value_list(sample.sample.submission, submission_fields))
            row.extend(get_prepdata_fields_value(request, prepsteps, sample))           
            if seq_fields:
                for seq in sample.sequencingsamples.all():
                    result = get_result_value_list(seq, seq_fields)
                    result[0:0] = row
                    spreadsheet.write_row('Report', row_num, result)
                    row_num += 1
                
            if not seq_fields or not sample.sequencingsamples.all():
                    spreadsheet.write_row('Report', row_num, row)
                    row_num += 1
    else:
        seqSamples =  cdata['seqSamples']
        seq_fields = request.POST.getlist('Sequencing_Samplefields')
        lane_fields = request.POST.getlist('Lanefields')
        flowcell_fields = request.POST.getlist('Flowcellfields')
        run_fields = request.POST.getlist('Runfields')
        run_stats_fields = request.POST.getlist('Run Statsfields')
        
        fields = []
        fields.extend(get_fields_verbose_name(lpmodels.SequencingSample, seq_fields))
        fields.extend(get_fields_verbose_name(seqmodels.Lane, lane_fields))
        fields.extend(get_fields_verbose_name(seqmodels.Flowcell, flowcell_fields))
        fields.extend(get_fields_verbose_name(seqmodels.Run, run_fields))
        fields.extend(get_fields_verbose_name(seqmodels.ReadResults, run_stats_fields))
        spreadsheet.write_row('Report', 0, fields)
        
        spreadsheet.set_font('Calibri', 220, False)
        row_num = 1
        for seq in seqSamples:
            result_1 = get_result_value_list(seq, seq_fields)
            for lane in seq.sequencing_sample.all():
                readResultObj = seqmodels.ReadResults.objects.filter(laneresult__lane = lane).order_by('-id')[0] if seqmodels.ReadResults.objects.filter(laneresult__lane = lane) else None
                result_2 = []
                result_2 += result_1
                result_2.extend(get_result_value_list(lane, lane_fields))
                result_2.extend(get_result_value_list(lane.flowcell, flowcell_fields))
                result_2.extend(get_result_value_list(lane.flowcell.run, run_fields))
                if readResultObj:
                    result_2.extend(get_result_value_list(readResultObj, run_stats_fields))
                spreadsheet.write_row('Report', row_num, result_2)
                row_num += 1
            
            if not seq.sequencing_sample.all():
                spreadsheet.write_row('Report', row_num, result_1)
                row_num += 1
                
    return spreadsheet.create_excel_sheet()


def get_prepsteps_prepdata_fields(request):
    preptype  = request.GET.get('preptype')
    columns = []
    if preptype:
        preptypeversion =  lpmodels.PrepTypeVersion.objects.filter(preptype__id=preptype)[0]
        for ps in preptypeversion.steps.all():
            columns.append({'name': ps.name, 'fields': {pdd.id: pdd.label for pdd in  ps.prepdatadef_set.all().filter(per_sample=True)}})
    return columns


def get_model_columns(request):
    modelName = request.GET.get('modelname')
    if modelName == 'sample':
	columns = [{'name': 'Samplefields', 'fields':  get_model_fields(lpmodels.SamplePrepDetail)}, 
                    {'name':'Specimenfields', 'fields' : get_model_fields(sampmodels.Specimen)},
                    {'name':'Submissionfields', 'fields': get_model_fields(sampmodels.Submission)},
                    {'name':'Sequencing_Samplefields', 'fields': get_model_fields(lpmodels.SequencingSample)}]
        columns.extend(get_prepsteps_prepdata_fields(request)) 
    else:
        columns = [{'name': 'Sequencing_Samplefields', 'fields': get_model_fields(lpmodels.SequencingSample)},
                   {'name': 'Lanefields', 'fields': get_model_fields(seqmodels.Lane)},
                   {'name': 'Flowcellfields', 'fields': get_model_fields(seqmodels.Flowcell)},
                   {'name': 'Runfields', 'fields': get_model_fields(seqmodels.Run)},
                   {'name': 'Run Statsfields', 'fields': get_model_fields(seqmodels.ReadResults) }]
    
    return HttpResponse(json.dumps({"columns": columns}), content_type="application/json" )


def get_model_filters(request):
    modelName = request.GET.get('modelname')
    if modelName == 'sample':
        preptype  = request.GET.get('preptype')
        samples = lpmodels.SamplePrepDetail.objects.values_list('sample__specimen__gtac_id').order_by('sample__specimen__gtac_id')
        if preptype:
            result = {s[0]: s[0] for s in samples.filter(preptype__id=preptype).distinct()} 
        else:
            result = {s[0]: s[0] for s in samples.filter(preptype__isnull=True).distinct()} 
    return HttpResponse(json.dumps(result), content_type="application/json")
