from django import forms
from libprep import models as lpmodels
from sample import models as sampmodels


models = (('sample', 'Sample'), ('SequencingSample', 'Sequencing Sample')) 

class CustomReport(forms.Form):
    models = forms.ChoiceField(required=True, choices=models, label="Select Model")
    preptype = forms.ModelChoiceField(lpmodels.PrepType.active.all().order_by('name'), empty_label="None - Direct to Sequencing", label="Prep Type",  required=False)
    samples = forms.ModelMultipleChoiceField(queryset=sampmodels.Sample.objects.order_by('specimen__gtac_id').distinct(), required=False)
    seqSamples = forms.ModelMultipleChoiceField(queryset=lpmodels.SequencingSample.objects.all().order_by('name'), required=False)
