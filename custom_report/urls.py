from django.conf.urls import *

urlpatterns = patterns('',
    url(r'^report$', 'custom_report.views.custom_report', name="custom-report" ),
    url(r'^model_columns/$', 'custom_report.views.get_model_columns' ),
    url(r'^model_filters/$', 'custom_report.views.get_model_filters' )
    )
