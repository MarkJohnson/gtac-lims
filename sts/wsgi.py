import os

from django.core.wsgi import get_wsgi_application


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sts.settings")
os.environ.setdefault("LIMS_WSGI", "1")

application = get_wsgi_application()
