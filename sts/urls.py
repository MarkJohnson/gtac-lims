from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
admin.autodiscover()

import menus
menus.autodiscover()

from tastypie.api import Api
#from core import api as coreapi
from billing import api as billingapi

v1_api = Api(api_name='v1')
v1_api.register(billingapi.BatchMultiplierResource())
v1_api.register(billingapi.BaseCodeResource())

urlpatterns = patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^client/', include('base.urls')),
    #url(r'^submit/$', 'sample.views.submit', name='submit' ),
    url(r'^specimen-submit/$', 'sample.views.specimen_submit', name='specimen-submit' ),
    url(r'^ngsubmit/$', 'sample.views.nextgen_submit', name='nextgen-submit' ),
    url(r'^specPegination/$', 'sample.views.getSpecimenPaginatedData', name='getSpecimenPaginatedData' ),
    url(r'^libprep/', include('libprep.urls')),
    url(r'^seq/', include('seq.urls')),
    url(r'^samp/', include('sample.urls')),
    url(r'^analysis/', include('analysis.urls')),
    url(r'^billing/', include('billing.urls')),
    url(r'^fr/', include('frontend.urls')),
    url(r'^submission/browse/$', 'sample.views.submission_browse', name='submission-browse' ),
    url(r'^run/browse/$', 'seq.views.run_browse', name='run-browse'),
    url(r'^signin/$', 'django.contrib.auth.views.login', {
        'template_name': 'frontend/signin.html',
    }),
    url(r'^signout/$', 'django.contrib.auth.views.logout', {
        'next_page': '/signin/',
    }, name="signout"),
    url(r'^favicon\.ico$', 'django.views.generic.RedirectView', {'url': '/static/img/favicon.ico'}),
    url(r'^api/', include(v1_api.urls)),
    url(r'^$', 'base.views.dashboard', name='dashboard' ),
    url(r'^seq/flowcell_json$', 'seq.views.flowcell_json', name='flowcell-json'),
    url(r'^custom_report/', include('custom_report.urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
