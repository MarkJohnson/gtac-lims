from django.shortcuts import render_to_response, get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.db import transaction
from django.core.urlresolvers import reverse

from base import models as basemodels
from seq import models as seqmodels
from sample import models as sampmodels
from libprep import models as lpmodels

def home(request):
    return render(request, 'gtac/home.html')
    
def flowcell_detail(request, name=None):
    return render(request, 'gtac/flowcell-detail.html', {
        'fc': get_object_or_404(seqmodels.Flowcell, name=name),
        })
        
def clusterstation_detail(request, slug=None):
    return render(request, 'gtac/clusterstation-detail.html', {
        'cs': get_object_or_404(seqmodels.ClusterStation, slug=slug),
        })
        

def run_detail(request, num=None):
    return render(request, 'gtac/run-detail.html', {
        'run': get_object_or_404(seqmodels.Run, num=num),
        })
        
def run_list(request):
    return render(request, 'gtac/run-list.html', {
        'runs': seqmodels.Run.objects.all(),
        })


def lane_detail(request, name=None, lane=None):
    return render(request, 'gtac/lane-detail.html', {
        'lane': get_object_or_404(seqmodels.Lane, flowcell__name=name, number=lane),
        })

def sequencer_detail(request, slug=None):
    return render(request, 'gtac/sequencer-detail.html', {
        'sequencer': get_object_or_404(seqmodels.Sequencer, slug=slug),
        })

def sequencer_list(request):
    return render(request, 'gtac/sequencer-list.html', {
        'sequencers': seqmodels.Sequencer.objects.all(),
        })

def sample_detail(request, id=None):
    return render(request, 'gtac/sample-detail.html', {
        'sample': get_object_or_404(sampmodels.Specimen, id=id),
        })

def sample_list(request):
    return render(request, 'gtac/sample-list.html', {
        'sequencers': sampmodels.Specimen.objects.all(),
        })

def submission_detail(request, id=None):
    return render(request, 'gtac/submission-detail.html', {
        'submission': get_object_or_404(sampmodels.Submission, id=id),
        })

def submission_list(request):
    return render(request, 'gtac/sequencer-list.html', {
        'submissions': sampmodels.Submission.objects.all(),
        })

def sequencingsample_detail(request, id=None):
    return render(request, 'gtac/sequencingsample-detail.html', {
        'sample': get_object_or_404(lpmodels.SequencingSample, id=id),
        })

def sequencingsample_list(request):
    return render(request, 'gtac/sequencingsample-list.html', {
        'samples': lpmodels.SequencingSample.objects.all(),
        })

def client_detail(request, id=None):
    return render(request, 'gtac/client-detail.html', {
        'client': get_object_or_404(basemodels.Client, id=id),
        })

def client_list(request):
    return render(request, 'gtac/client-list.html', {
        'clients': basemodels.Client.objects.all(),
        })

def lab_detail(request, id=None):
    return render(request, 'gtac/lab-detail.html', {
        'lab': get_object_or_404(basemodels.Lab, id=id),
        })

def lab_list(request):
    return render(request, 'gtac/lab-list.html', {
        'labs': basemodels.Lab.objects.all(),
        })

def turnaround(request):
    return render(request, 'gtac/turnaround.html', {
        'samples': sampmodels.Sample.objects.all()[:50],
        })


def latestpooledsample(request, ssname=None):
    """
    Given a sequencing sample name, return the latest submission date of all the samples.
    FIXME: Should probably be in a more standard format like JSON in the future
    FIXME: Maybe should just return a JSON serialized list of samples in the future?
    """
    try:
        seqsamp = lpmodels.SequencingSample.objects.get(name__iexact=ssname)
    except:
        return HttpResponse()

    return HttpResponse(seqsamp.samples.latest().submission.submitted)


def analysis_tracking(request):
    return render(request, 'gtac/analysis-tracking.html', {
        'lanes': seqmodels.Lane.objects.all(),
        })
    
def tracking(request):
    return render(request, 'gtac/tracking.html', {
        'lanes': seqmodels.Lane.objects.all().order_by('flowcell__run'),
        })
    
    
def analysis_launch(request, run=None, lane=None):
    l = get_object_or_404(seqmodels.Lane, number=lane, flowcell__run__num=run)
    customer = l.samples[0].submission.client.lab
    species = l.samples[0].sample.organism.name
    exomecapturekit = l.sequencingsample.sequencingsampledetail_set.all()[0].index.exomecapturekit.name
    si_pairs = []
    for ssd in l.sequencingsample.sequencingsampledetail_set.all():
       si_pairs.append( (ssd.sample.specimen.name, ssd.index.sequence) ) 

    return HttpResponse(si_pairs)
