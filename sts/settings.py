import django.conf.global_settings as DEFAULT_SETTINGS
import os


INTERNAL_IPS = ('10.39.121.119', '127.0.0.1')
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'

DJROOT = os.path.dirname(os.path.dirname(__file__))

DEBUG = False
ALLOWED_HOSTS = [ '127.0.0.1', 'localhost', '.elasticbeanstalk.com' ]

if (os.environ.has_key('DEBUG')):
    if (os.environ['DEBUG'] == 'True'):
        DEBUG = True
        ALLOWED_HOSTS = '*'

DEBUG  = True 

EMAIL_BACKEND            = 'django_ses.SESBackend'

DEFAULT_FROM_EMAIL       = os.environ['DEFAULT_FROM_EMAIL'] 
DIR_TO_SEQ_RECIPIENT     = [os.environ['DIR_TO_SEQ_RECIPIENT']]
QC_REPORT_RECIPIENT      = [os.environ['QC_REPORT_RECIPIENT']]
RUN_COMPLETED_RECIPIENTS = os.environ['RUN_COMPLETED_RECIPIENTS'].split(';') 
RUN_HISEQ_RECIPIENTS     = [os.environ['RUN_HISEQ_RECIPIENTS']] 

AWS_ACCESS_KEY_ID         = os.environ['AWS_ACCESS_KEY_ID']
AWS_AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
AWS_SES_REGION_NAME       = os.environ['AWS_SES_REGION_NAME']
AWS_SES_REGION_ENDPOINT   = os.environ['AWS_SES_REGION_ENDPOINT']
AWS_SES_RETURN_PATH       = os.environ['AWS_SES_RETURN_PATH']
AWS_STORAGE_BUCKET_NAME   = os.environ['AWS_STORAGE_BUCKET_NAME']

TEMPLATE_DEBUG = DEBUG

GTAC_ID_OFFSET = 12000

if (os.environ.has_key('GTAC_ID_OFFSET')):
    GTAC_ID_OFFSET = os.environ['GTAC_ID_OFFSET']

ALIGNMENT_GENOME_DIR   = '/srv/seq/analysis1/reference_sequences'
DATATABLE_SELECT_COLOR = '#1E90FF'

if (not os.path.exists(ALIGNMENT_GENOME_DIR)):
    ALIGNMENT_GENOME_DIR='/tmp'

ADMINS = (
    ('GTAC Geeks', 'gtac-bioinformatics@path.wustl.edu'),
)

MANAGERS = ADMINS

if ('RDS_DB_NAME' in os.environ):
    DB_ENGINE = 'django.db.backends.postgresql_psycopg2'
    DB_NAME   = os.environ['RDS_DB_NAME']
    DB_USER   = os.environ['RDS_USERNAME']
    DB_PASS   = os.environ['RDS_PASSWORD']
    DB_HOST   = os.environ['RDS_HOSTNAME']
    DB_PORT   = os.environ['RDS_PORT']
else:
    DB_ENGINE = 'django.db.backends.sqlite3'
    DB_NAME   = os.path.join(DJROOT, 'db')
    DB_USER   = ''
    DB_PASS   = ''
    DB_HOST   = ''
    DB_PORT   = ''

DATABASES = {
    'default': {
        'ENGINE':   DB_ENGINE,
        'NAME':     DB_NAME,
        'USER':     DB_USER,
        'PASSWORD': DB_PASS, 
        'HOST':     DB_HOST, 
        'PORT':     DB_PORT, 
    },
}

TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

USE_TZ = True

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
#ADMIN_MEDIA_PREFIX = '/static/admin/'

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = os.environ['SECRET_KEY'] 

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    ('django.template.loaders.cached.Loader', (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )),
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
)

ROOT_URLCONF = 'sts.urls'

WSGI_APPLICATION = 'sts.wsgi.application'

TEMPLATE_CONTEXT_PROCESSORS = DEFAULT_SETTINGS.TEMPLATE_CONTEXT_PROCESSORS + (
"django.core.context_processors.request",
)

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.flatpages',
    'django_extensions',
    'formtools',
    'mptt',
    'tastypie',
    'sample',
    'core',
    'toolbox',
    'base',
    'libprep',
    'seq',
    'analysis',
    'billing',
    'frontend',
    'menus',
    'feincms',
    'theme',
    'custom_report',
    'markup_deprecated',
    'debug_toolbar',
    'storages',
]

if os.path.exists(os.path.join(DJROOT, 'simpla')):
    INSTALLED_APPS.append('simpla')

LOGIN_URL = '/signin/'

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

LOGIN_URL = '/signin/'

FILE_UPLOAD_HANDLERS = (
    'django.core.files.uploadhandler.TemporaryFileUploadHandler',
)

USE_X_FORWARDED_HOST = True

TEST_RUNNER = 'django.test.runner.DiscoverRunner'

AWS_S3_CUSTOM_DOMAIN  = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME

STATICFILES_LOCATION = 'static'
STATIC_URL           = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, STATICFILES_LOCATION)

MEDIAFILES_LOCATION = 'media'
MEDIA_URL           = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, MEDIAFILES_LOCATION)

STATICFILES_STORAGE  = 'custom_storages.StaticStorage'
DEFAULT_FILE_STORAGE = 'custom_storages.MediaStorage'

CONN_MAX_AGE = 600
