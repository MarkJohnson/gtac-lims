from django.http import HttpResponse, HttpResponseRedirect
import mimetypes
import xlwt
import xlrd
from xlwt import Borders

FORMATS = [
    'General',
    '0',
    '0.0',
    '0.00',
    ]
    
    
class Spreadsheet(object):
    style = xlwt.XFStyle()
    
    def __init__(self, filename, sheet_names, cell_overwrite_ok=False):
        self.workbook = xlwt.Workbook(encoding='utf-8')
        self.filename = filename
        self.ws_list = {}
        for name in sheet_names:
            self.ws_list[name] = self.workbook.add_sheet(name, cell_overwrite_ok)
        self.set_font('Calibri', 220, True)
    
    def set_font(self, name, height, bold, underline=False):
        font = xlwt.Font() # Create the Font 
        font.name = name
        font.height = height
        font.bold = bold  
        font.underline = underline
        self.style.font = font
        
    def set_border(self, top, right, bottom, left):
        borders = Borders()
        borders.top = Borders.THIN if top else Borders.NO_LINE
        borders.right = Borders.THIN if right else Borders.NO_LINE
        borders.bottom = Borders.THIN if bottom else Borders.NO_LINE
        borders.left = Borders.THIN if left else Borders.NO_LINE
        self.style.borders = borders
    
    def set_num_format_str(self, format):
        self.style.num_format_str = format
   
    def set_portrait(self, isPortrait):
        for key, sheet in self.ws_list.items():
            sheet.portrait = isPortrait

    def set_paper_size(self, code):
        for key, sheet in self.ws_list.items():
            sheet.paper_size_code = code

    def set_fit_num_pages(self, num):
        for key, sheet in self.ws_list.items():
            sheet.fit_num_pages = num

    def write_data(self, sheet_name, data):
        for row in data.keys():
            self.write_row(sheet_name, row, data[row])
    
    def write_row(self, sheet_name, row, data):
        if type(data) == type(dict()):
            for col in data.keys():
                self.write_col(sheet_name, row, col, data[col])
        else:
            for col in range(0, len(data)):
                self.write_col(sheet_name, row, col, data[col])
    
    def write_col(self, sheet_name, row, col, data):
        self.ws_list[sheet_name].write(row, col, data, self.style)

    def write_merge(self, sheet_name, startRow, startCol, endRow, endCol, data):
        style = xlwt.easyxf('align: vertical top, horizontal left, wrap true;'
                    'borders: left thin, right thin, top thin, bottom thin;')
        style.font = self.style.font
        self.ws_list[sheet_name].write_merge(startRow, endRow, startCol, endCol, data, style)
    
    def create_excel_sheet(self):
        mimetype_tuple = mimetypes.guess_type("%s.xls" % self.filename)
        response = HttpResponse(content_type='%s' % mimetype_tuple[0])
        response['Content-Disposition'] = 'attachment; filename="%s.xls"' % self.filename

        self.workbook.save(response)
        
        return response
