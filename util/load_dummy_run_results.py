#!/usr/bin/env python

import os
import random
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
sys.path.append('../sts')
sys.path.append('../')

from seq import models as seqmodels
from sys import argv


run = seqmodels.Run.objects.get(num=argv[1])

run.status = 'Success'

run.save()

ymd    = str(run.sequencing_date.year)[2:] + \
         str(run.sequencing_date.month).zfill(2) + \
         str(run.sequencing_date.day).zfill(2)

run_id = ymd + '_SN' + run.manifold.sequencer.serial + '_'  + \
         str(run.num).zfill(4) + '_' + run.flowcell.name + '_' + str(run.num)

read_cycles  = run.flowcell.runtype.length
index_cycles = run.index_length
total_cycles = 0

if (run.flowcell.runtype.read == 'PE'):
    total_cycles = read_cycles + read_cycles + index_cycles
else:
    total_cycles = read_cycles + index_cycles 
 
folder_location = '/srv/seq/fasteddie/' + run_id

control_lane = None

if (run.flowcell.runtype.num_lanes == 8):
    control_lane = 8

rta_version = '1.17.20'

run_results = seqmodels.RunResults(run=run, ill_run_id=run_id, 
              read_cycles=read_cycles, index_cycles=index_cycles, 
              total_cycles=total_cycles, folder_location=folder_location, 
              control_lane=control_lane, rta_version=rta_version)

run_results.save()

for lane in run.flowcell.lanes.all():

    reads = [ ]

    if (run.flowcell.runtype.read == 'PE'):
        reads = [1, 2, 3]
    else:
        reads = [1, 2]

    for read_number in (reads):

        num_cycles = 0
        num_reads = 159900640
        percent_pass_filter = 96.2
        percent_phasing = 0.0808
        percent_prephasing = 0.1658
        cluster_density = float(random.randrange(62733, 122484)) 
        percent_q30 = round(random.random() * 100, 2)
        error_rate = random.random()
        percent_aligned = round(random.random() * 100, 2)

        if ((read_number == 1) or (read_number == 2)):
            num_cycles = read_cycles
        else:
            num_cycles = index_cycles

        lane_results = seqmodels.LaneResults(lane=lane, read_num=read_number, 
                       num_cycles=num_cycles, num_reads = num_reads, 
                       percent_pass_filter=percent_pass_filter, 
                       percent_phasing=percent_phasing, 
                       percent_prephasing=percent_prephasing,
                       cluster_density=cluster_density, percent_q30=percent_q30,
                       error_rate=error_rate, percent_aligned=percent_aligned)
        
        lane_results.save()
