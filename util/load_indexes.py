#!/usr/bin/env python

import csv
import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
sys.path.append('../sts')
sys.path.append('../')

from django.db import transaction
from libprep import models as lpmodels
from sys import argv

@transaction.commit_on_success
def doit():
   
    for record in (csv.DictReader(open(argv[1]))):
        index          = lpmodels.Index()
        index.name     = record['name']
        index.sequence = record['seq']  
        index.save()

doit()
