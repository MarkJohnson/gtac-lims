#!/usr/bin/env python

import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
sys.path.append('../sts')
sys.path.append('../')

from django.db import transaction
from libprep import models as lpmodels

@transaction.commit_on_success
def doit():
    
    indexes = lpmodels.Index.objects.all()

    for index in indexes:
        index.name = 'GTAC ' + index.name
        index.save()

doit()
