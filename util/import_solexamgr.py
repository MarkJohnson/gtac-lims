#!/usr/bin/env python

import os
import sys
import datetime
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from django.core import serializers
from django.db import transaction
from django.contrib.auth.models import User
import subprocess

@transaction.commit_on_success
def go():
    p = subprocess.Popen(['/usr/bin/ssh', 'svc2', '/srv/stuff1/vhosts/cgs/manage.py dumpdata solexamgr',], stdout=subprocess.PIPE )
    for obj in serializers.deserialize("json", p.stdout):
        if obj.object._meta.object_name == 'Contact':
            username = obj.object.name.strip().lower().replace(' ', '_')[:30]
            obj.object.user = User.objects.get_or_create(
                username=username,
                email=obj.object.email,
                first_name=obj.object.name.split(' ',1)[0],
                last_name=obj.object.name.split(' ',1)[-1],
                )[0]
        obj.save()

go()
