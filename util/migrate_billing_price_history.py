import csv
import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
sys.path.append('../sts')
sys.path.append('../')

from django.db import transaction
from billing import models as billmodels
from sys import argv

@transaction.commit_on_success
def Migrate_BasecodeChargeCodeHistory():
    for basecode in billmodels.BaseCodeChargeCode.objects.all():
        basecode.save()

    print "BasecodeChargeCodeHistory migrate complete"

Migrate_BasecodeChargeCodeHistory()

@transaction.commit_on_success
def Migrate_BasecodeMuliplierChgHistory():
    for basecode in billmodels.BaseCodeMultiplierCharge.objects.all():
        basecode.save()

    print "BaseCodeMultiplierChgHistory migrate complete"


Migrate_BasecodeMuliplierChgHistory()