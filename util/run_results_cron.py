#!/usr/bin/env python

import json
import os
import random
import sys
import tempfile
import logging
import django

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

from django.core.files   import File
from boto.s3.connection  import S3Connection
from boto.s3.key         import Key
from boto.sqs.connection import SQSConnection

from seq import models as seqmodels
from libprep import models as lpmodels
from frontend import util

from sys import argv


django.setup()

logging.basicConfig(filename='cron_error_log.log',
                            filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=logging.DEBUG)


def get_plot(plot_key):

    s3conn = conn = S3Connection(aws_access_key, aws_secret_key)

    for b in conn.get_all_buckets():

        if (b.name == 'gtac_lims_ivc_plots'):

            tmp_file = tempfile.NamedTemporaryFile()

            k     = Key(b)
            k.key = plot_key 
        
            k.get_contents_to_file(tmp_file)
            b.delete_key(k)
            tmp_file.seek(0)

            return tmp_file        
 

aws_access_key = os.environ['AWS_ACCESS_KEY_ID'] 
aws_secret_key = os.environ['AWS_SECRET_ACCESS_KEY']

conn = SQSConnection(aws_access_key, aws_secret_key)

for q in conn.get_all_queues():

    name = q.id.split("/")[2]

    if (name == 'lims_publish_stats'):

        for m in q.get_messages(10):

            json_stats = m.get_body()
            runinfo    = json.loads(json_stats)

            if (not seqmodels.Flowcell.objects.filter(name=runinfo['flowcell_id'].upper())):
                print >> sys.stderr, 'received martian flowcell %s - ignoring' % runinfo['flowcell_id'].upper()
                continue

            flowcell = seqmodels.Flowcell.objects.get(name=runinfo['flowcell_id'].upper())
            run      = seqmodels.Run.objects.get(flowcell=flowcell)           
 
            run.status = 'Success'

            run.save()

            try:
                util.send_run_completed_mail(run)
            except Exception, ex:
                logging.exception(" Error: in util/run_results_cron.py")
            logging.debug("Log completed ... ")

            if (seqmodels.RunResults.objects.filter(run=run).exists()):
                seqmodels.RunResults.objects.filter(run=run).delete() 

            cl = None

            if ('control_lane' in runinfo):
                cl = runinfo['control_lane']
 
            run_results = seqmodels.RunResults(run=run,
                                               ill_run_id=runinfo['run_id'], 
                                               read_cycles=runinfo['read_cycles'], 
                                               index_cycles=runinfo['index_cycles'], 
                                               total_cycles=runinfo['total_cycles'], 
                                               folder_location=runinfo['folder_location'], 
                                               control_lane=cl, 
                                               rta_version=runinfo['rta_version'])

            run_results.save()

            for lane_number in runinfo['lanes']:

                laneinfo = runinfo['lanes'][lane_number]

                flowcell = seqmodels.Flowcell.objects.get(name=runinfo['flowcell_id'].upper())
                lane     = seqmodels.Lane.objects.get(flowcell=flowcell, num=lane_number)

                if (seqmodels.LaneResults.objects.filter(lane=lane).exists()):
                    seqmodels.LaneResults.objects.filter(lane=lane).delete()
     
                lane_results = seqmodels.LaneResults(lane=lane,
                                                     cluster_density=laneinfo['cluster_density'])

                ivc_plot_key = "%s_s_%d_percent_all.png" % (runinfo['run_id'], int(lane_number))
                
                ivc_plot_fh = get_plot(ivc_plot_key)
 
                lane_results.ivc_plot.save(ivc_plot_key, File(ivc_plot_fh))
 
                lane_results.save()

                for readinfo in laneinfo['reads']:

                    read_number = read_num=readinfo['read_number']

                    if (seqmodels.ReadResults.objects.filter(laneresult=lane_results, read_num=read_number).exists()):
                        seqmodels.ReadResults.objects.filter(laneresult=lane_results, read_num=read_number).delete()
                     
                    read_results = seqmodels.ReadResults(laneresult=lane_results,
                                                         read_num=read_number, 
                                                         num_cycles=readinfo['num_cycles'], 
                                                         num_reads = readinfo['num_reads'], 
                                                         percent_pass_filter=readinfo['percent_pass_filter'], 
                                                         percent_phasing=0, 
                                                         percent_prephasing=0,
                                                         percent_q30=readinfo['percent_q30'],
                                                         error_rate=readinfo['error_rate'], 
                                                         percent_aligned=readinfo['percent_aligned'])     

                    read_results.save()
 
            for lane in run.flowcell.lanes.all():
                for seq in lane.sequencing_sample.all():
                    lpmodels.SamplePrepDetail.objects.filter(sequencingsamples=seq).update(status=lpmodels.PrepStatus.objects.get(name='Analysis'))

            q.delete_message(m)

