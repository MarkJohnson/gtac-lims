#!/usr/bin/env python

import csv
import datetime
import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from sample.models import Submission, Sample, Project
from libprep.models import SequencingSample, SequencingSampleDetail
from base import models as basemodels
from seq.models import SampleSequencingDetail, SequencedSample, Sequencer, RunPostProcess, LanePostProcess, PostProcessPipeline, FlowcellGeneration, SequencerModel, ClusterStationModel, RunType, Flowcell, Manifold, Lane, Run, ClusterStation, DenaturedAliquot
from libprep import models as lpmodels
from seq import models as seqmodels
from sample import models as sampmodels
from solexamgr import models as sm
from solexamgr import models as solmodels
from billing.models import BillingCode, SequencingBill
from django.db import transaction
from django.contrib.auth.models import User
import re

# FIXME use this regular expression to fix sequencingsample names

#lanes = solmodels.Lane.objects.filter(sample_id__iregex=r'.*GTAC[:-]\s*(%s|%s)(?:$|_.*)' % (re.escape(seqs.name), re.escape(spd.name)))

multiprepped = [x for x in csv.DictReader(open('multiprepped.csv'))]

#phix = User.objects.get_or_create(username='phiX')[0]

def log(msg, level=1, nl=True):
    sys.stdout.write("%s%s" % ('\t' * level, msg))
    if nl:
        sys.stdout.write('\n')



def migrate_project(client, subdate):
    # Group projects by user and submission date
    
    if client.person.user == phix:
        pname = "phiX"
    else:
        pname = subdate.strftime("%Y-%m-%d")
    prj,c = Project.objects.get_or_create(name=pname,client=client)
    if c:
        log("CREATE: Project(%s,%s)" % (prj, client))
    else:
        log("GET: Project(%s,%s)" % (prj, client))
    return prj
    
def get_submitted_date(l):
    if l.dropoff_date:
        submitted = l.dropoff_date
    else:
        submitted = l.run.sequencing_date    
    return submitted

def migrate_instrument(l):
    names = {
        '157': 'Buckets',
        '155': 'Fast Eddie',
        '440': 'Can-o-corn',
        '158': 'Psghetti',
        }
    
    old_i = l.run.instrument
    im = SequencerModel.objects.get_or_create(name=old_i.model.split('-')[0])[0]
    i = Sequencer.objects.get_or_create(
        model=im,
        name=names[old_i.serial],
        serial=old_i.serial,
        defaults={
            'max_runs_per_week': old_i.max_runs_per_week,
            'expired': old_i.expired,
            },
        )[0]
    if len(old_i.model.split('-')) > 1:
        iman = Manifold.objects.get_or_create(sequencer=i, name=old_i.model.split('-')[1])[0]
    else:
        iman = Manifold.objects.get_or_create(sequencer=i, name='A')[0]
        
    return i,im,iman
    
def migrate_flowcell(l, im):
    pe = l.run.is_paired_end
    if pe:
        read='PE'
    else:
        read='SR'
    rt = RunType.objects.get_or_create(
        read=read,
        length=l.run.cycles.cycles,
        sequencer_model=im,
        expired=l.run.instrument.expired,
        )[0]
    fcid = l.run.flowcell_id
    fc,c = Flowcell.objects.get_or_create(name=fcid, runtype=rt)
    if c:
        log("CREATE: Flowcell(%s,%s)" % (fcid, rt))
    else:
        log("GET: Flowcell(%s,%s)" % (fcid, rt))
        
    if l.run.cluster_kit.cluster_kit.startswith('CB'):
        csmodel = ClusterStationModel.objects.get_or_create(name="CBOT")[0]
        if l.run.description.lower().startswith('j'):
            cs = ClusterStation.objects.get_or_create(name="Jipper Chipper", model=csmodel, serial="2")[0]
        elif l.run.description.lower().startswith('d'):
            cs = ClusterStation.objects.get_or_create(name="Djibouti", model=csmodel, serial="3")[0]
        else:
            print "not sure what cluster station to assign %s" % l
            cs = ClusterStation.objects.get_or_create(name="Jipper Chipper", model=csmodel, serial="2")[0]
    else:
        csmodel = ClusterStationModel.objects.get_or_create(name="Cluster Station")[0]
        cs = ClusterStation.objects.get_or_create(name="Bertha", model=csmodel, serial="1")[0]

    fcgen = FlowcellGeneration.objects.get_or_create(flowcell=fc, kit=l.run.cluster_kit.cluster_kit, date=l.run.flowcell_date, cluster_station=cs)[0]
    return fc, rt

def migrate_sequencingsample(l, name, subdate, rt):
    if name.startswith('GTAC: '):
        name = name[6:]
    ss,c = SequencingSample.objects.get_or_create(
        name=name, defaults={
        'loading_concentration' : l.dna_conc.amount,
        'submitted_concentration' : l.submitted_conc.amount,
        'primer': l.primer.name,
        })
    if c:
        log("CREATE: SequencingSample(%s,%s)" % (name, rt))
    else:
        log("GET: SequencingSample(%s,%s)" % (name, rt))
        
    ss.created = subdate
    ss.save()
    return ss,c


def migrate_sequencedsample(ssd):
    ss = SequencedSample.objects.create(samplesequencingdetail=ssd)
    return ss

def make_unique_sid(l,sid,u):
    for l2 in sm.Lane.objects.filter(sample_id__regex=r'%s(_\d+%%)?$' % re.escape(sid)).exclude(id=l.id).order_by('run__num'):
        # More than one lane with that sample_id
        for c in l.contacts.all():
            if c in l2.contacts.all():
                # sample used more than once, but same user
                break
        else:
            # same sample id but different users!!  Need to make it unique
            log("UNIQUE: '%s' -> '%s (%s)'" % (sid, sid, u.username))
            return "%s (%s)" % (sid, u.username)
    return sid

def clean_sid(sid):
    #GTAC_SAMPLE=r'(?P<foo>.*)GTAC[:-]\s*(\w+)(?:$|_(?P<percent>.*))'

    match = re.match('(.*?)_?(\d+)%.*', sid)
    if match:
        phix_percent = match.groups()[1]
        s = match.groups()[0]
    else:
        phix_percent = 0
        s = sid


    match = re.match('.*GTAC[:-]\s*(\w+)(?:$|_.*)', sid)
    if match:
        is_gtac = True
        s = match.groups()[0]
    else:
        is_gtac = False
        

    #if sid.endswith('%'):
    #    s,phix_percent = sid.rstrip('%').rsplit('_',1)    
    #    log("CLEAN: %s -> %s (%s%%)" % (sid,s, phix_percent))
    #else:
    #    s = sid
    #    phix_percent = None
    log("CLEAN: %s -> " % (sid) + str((is_gtac ,s, phix_percent)))
    return is_gtac, s,phix_percent
    
def migrate_submission(prj,client, subdate):
    if Submission.objects.filter(project=prj, client=client).exists():
        sub = Submission.objects.filter(project=prj, client=client)[0]
    else:
        sub = Submission.objects.create(project=prj, client=client)
        sub.submitted = subdate
        sub.save()
    return sub


class Seq(object):
    """
    Migrate a lane of solexamgr data over to GTAC Lims.

    """
    def __init__(self, lane=None, sequencingsample=None, samplesubmission=None, verbose=False):
    
        # FIXME - I think we should only accept either a lane or a samplesubmission?
        # if samplesubmission THEN THIS IS A GTAC SAMPLE, find all its sequencingsamples, grab their names and look for them inside solexamgr lanes.
        # if given oldlane, then
        #     figure out the sequencingsample name from lane.sample_id
        #     figure out if it's gtac or cgs
        #     if gtac, we should find the sequencingsample already in the Lims
        #         if we don't, ERROR OUT
        #     if not gtac:
        #         do our best to get_or_create a client, project, submission, sample, samplesubmission, sequencingsample, samplesequencingdetail
         
    
        self._sequencingsample = sequencingsample
        # samplesubmission will be none if this is not a GTAC sample
        self._samplesubmission = samplesubmission
        self.verbose = verbose
        self.l = lane
        self.level = 0

    def migrate_gtac_sample(self, subdate):
        # We're a GTAC Sample.  Find all sequencing samples
        seqs = self._samplesubmission.libprep.sequencingsamples.all()
        if len(seqs) == 0:
            raise Exception, "Can't find any sequencingsamples for submission, but should"
        for seqsample in seqs:
            lanes = self.find_oldlanes(seqsample)
            if len(lanes) == 0:
                raise Exception, "Can't find any lanes for samplesubmission, but should"
            for l in lanes:
                sl = SeqLane(l, seqsample, verbose=self.verbose)
                sl.migrate()
                ssd,c = self.get_or_create(seqmodels.SampleSequencingDetail, 
                    samplesubmission=self._samplesubmission, 
                    runtype=sl.runtype,
                    ready_date=subdate,
                    )

                ssd.denaturedaliquot.add(sl.denaturedaliquot)




    def find_oldlanes(self, sequencingsample):
        reg = r'.*GTAC[:-]\s*%s(?:$|\s*_.*)' % re.escape(sequencingsample.name)
        return solmodels.Lane.objects.filter(sample_id__iregex=reg).order_by('run__num')

    def build_submission_from_lane(self):

        #samples,percent_phix = self.lane_samples()
        #for sid, client, percent in samples:
        if self.l.lab.department.lower().strip() == 'external':
            location = 'external'
            department = ''
        else:
            location = 'internal'
            department = self.l.lab.department
            
        lab,c = self.get_or_create(basemodels.Lab, name=self.l.lab.name, 
            defaults={
                'notes': self.l.lab.billing_address,
                'location': location,
                })     

        con = self.l.contactdetail_set.filter(type='main').order_by('contact__name')
        if not con:
            fn = 'PhiX'
            ln = ''
        else:
            fn,ln = con[0].contact.name().split(None, 1)

        person = self.get_or_create(basemodels.Person, first_name=fn, last_name=ln)[0]
        client = self.get_or_create(basemodels.Client, person=person, defaults={'lab':lab})[0]

        if self.l.dropoff_date:
            subdate = self.l.dropoff_date
        else:
            subdate = self.l.run.sequencing_date    
        if client.person.first_name == 'PhiX':
            pname = "PhiX"
        else:
            pname = subdate.strftime("Project: %Y-%m-%d")
        prj = self.get_or_create(sampmodels.Project, name=pname, client=client)[0]
        unknown_org = self.get_or_create(sampmodels.SampleSpecies, name='Unknown')[0]
        submission = migrate_submission(prj, client, subdate)
        submission = self.get_or_create(sampmodels.Submission,
            project=prj,
            client=client,
            defaults={'submitted': subdate},
            )[0]
        sample,c = self.get_or_create(sampmodels.Specimen, name=self.l.sample_id, organism=unknown_org, defaults={
            'submitted_from': submission,
            })
            
        samplesubmission,c = self.get_or_create(sampmodels.Sample, sample=sample, submission=submission)
        spd = self.get_or_create(lpmodels.SamplePrepDetail, 
            samplesubmission=samplesubmission,
            preptype=self.get_or_create(lpmodels.PrepType, name="Self Prepped")[0],
            complete_date=subdate,
            complete=True,
            )[0]
        
        seqsamp = self.get_or_create(lpmodels.SequencingSample, 
            name=self.l.sample_id, 
            defaults={
            'loading_concentration' : self.l.dna_conc.amount,
            'submitted_concentration' : self.l.submitted_conc.amount,
            'primer': self.l.primer.name,
            })[0]

        sequencingsampledetails = self.get_or_create(lpmodels.SequencingSampleDetail, sampleprepdetail=spd, percent_of_pool=100,
            sequencingsample=seqsamp)[0]
        
        
    def migrate_lane(self):
        if self.is_gtac:
            raise Exception, "Trying to migrate a lane directly from solexamgr, but this is a GTAC Sample"
        else:
            self.build_submission_from_lane()
            raise Exception, "Make samplesubmissions from a solexamgr lane!"

    def migrate(self, subdate):
        if self._samplesubmission:
            # We're a GTAC Sample.
            self.migrate_gtac_sample(subdate)
        elif self.l:
            self.migrate_lane()


    @property
    def name(self):
        if not getattr(self, '_name', None):
            self.clean_sid()
        return self._name
    
    @property
    def is_gtac(self):
        if getattr(self, '_is_gtac', None) is None:
            self.clean_sid()
        return self._is_gtac
    
    def log(self, msg, limit=120):
        msg = (msg[:limit]+'..') if len(msg) > limit else msg
        msg = msg.replace('\n', ' ')
        print "%s%s" % (' ' * 4 * self.level, (msg[:limit]+'..') if len(msg) > limit else msg)

    def get_or_create(self, modelobject, manager='objects', **kwargs):
        obj, c = getattr(modelobject, manager).get_or_create(**kwargs)
        if c:
            if self.verbose: 
                self.log("CREATE: %s(%s)" % (modelobject._meta.object_name, ', '.join(['='.join((str(k),str(v))) for k,v in kwargs.items()])))
        elif self.verbose:
            self.log("GET: %s(%s)" % (modelobject._meta.object_name, ', '.join(['='.join((str(k),str(v))) for k,v in kwargs.items()])))
        return obj,c

    def lane_samples(self):
        """
        Given a lane, return the name, user, and percentage of the 
        samples in that lane using the multiprep csv if necessary.
        Also include the % phix used in the aliquot.
        """
        for mp in multiprepped:
            if self.l.sample_id == mp['Sequencing name']:
                samples = []
                    
                for x in range(1,4):
                    if not mp['Prepped sample name %d' % x]: continue
                    try:
                        c = basemodels.Client.objects.get(last_name=mp['Contact%d' % x].split(None,1)[1])
                    except basemodels.DoesNotExist:
                        c = basemodels.Cleint.objects.get(first_name='PhiX')
                    s = mp['Prepped sample name %d' % x]
                    pcs = mp['%%sample%d' % x]
                    samples.append((s,c,int(pcs)))

                phix_percent = mp['%PhiX']
                return (samples, phix_percent)
        else:
            return (( 
                (self.clean_sid()[0], get_lane_user(self.l), 100), ),
                 0.0)

    def clean_sid(self):
        #GTAC_SAMPLE=r'(?P<foo>.*)GTAC[:-]\s*(\w+)(?:$|_(?P<percent>.*))'

        match = re.match('(.*?)_?(\d+)%.*', self.l.sample_id)
        if match:
            phix_percent = match.groups()[1]
            self._name = match.groups()[0]
        else:
            phix_percent = 0
            self._name = self.l.sample_id

        match = re.match('.*GTAC[:-]\s*(\w+)(?:$|\s*_.*)', self.l.sample_id, re.I)
        if match:
            self._is_gtac = True
            self._name = match.groups()[0]
        else:
            self._is_gtac = False
        log("clean_sid CLEAN: %s -> " % (self.l.sample_id) + str((self._is_gtac ,self._name, phix_percent)))
        return self._is_gtac, self._name,phix_percent

    #@property
    #def samplesquencingdetail(self):
    #    if not getattr(self, '_ssd', None):
    #        #FIXME argh... this whole class should be by samplesubmission, not sequencing sample so I could use this method.
    #        #self.get_or_create(seqmodels.SampleSequencingDetail, samplesubmission=sequencingsample, self.samplesubmission.seq.get_or_create()[0]

    @classmethod
    def oldlanes(self, sequencingsample):
        reg = r'.*GTAC[:-]\s*%s(?:$|\s*_.*)' % re.escape(sequencingsample.name)
        print reg
        return solmodels.Lane.objects.filter(sample_id__iregex=reg).order_by('run__num')
            
    @property
    def instrument(self):
        if not getattr(self, '_instrument', None):
            self.migrate_instrument()
        return self._instrument

    @property
    def instrumentmanifold(self):
        if not getattr(self, '_instrument', None):
            self.migrate_instrument()
        return self._instrumentmanifold

    @property
    def instrumentmodel(self):
        if not getattr(self, '_instrument', None):
            self.migrate_instrument()
        return self._instrumentmodel

    @property
    def denaturedaliquot(self):
        if not getattr(self, '_denaturedaliquot', None):
            is_gtac, lid, phix_percent = self.clean_sid()

            samples, phix_percent1 = self.lane_samples( ) # FIXME do I now need all this?

            if phix_percent is None:
                phix_percent = phix_percent1
        
            self._denaturedaliquot,c = self.get_or_create(seqmodels.DenaturedAliquot,
                sequencingsample=self._sequencingsample,
                loading_concentration=self.l.dna_conc.amount,
                phix_spike=phix_percent,
                )
        return self._denaturedaliquot

    def make_denaturedaliquot(self, sequencingsample, lane):
        is_gtac, lid, phix_percent = self.clean_sid()
        da,c = self.get_or_create(seqmodels.DenaturedAliquot,
            sequencingsample=sequencingsample,
            loading_concentration=lane.dna_conc.amount,
            phix_spike=phix_percent,
            )
        
        

    @property
    def lane(self):
        if not getattr(self, '_lane', None):
            self._lane,c = self.get_or_create(seqmodels.Lane,
                flowcell=self.flowcell, 
                denaturedaliquot=self.denaturedaliquot, 
                primer=self.l.primer.name,
                number=self.l.num,
                )
        return self._lane

    def make_lane(self):
        return self.lane


    @property
    def run(self):
        if not getattr(self, '_run', None):
            self._run, c = self.get_or_create(seqmodels.Run,
                num=self.l.run.num, 
                flowcell=self.flowcell, 
                sequencing_date=self.l.run.sequencing_date, 
                manifold=self.instrumentmanifold,
                folder_name=self.l.run.run_folder or '',
                software_version=self.l.run.scs_version.version,
                sequencing_kit=self.l.run.sequencing_kit.sequencing_kit,
                turnaround_date=self.l.run.read2_sequencing_date,
                status=self.l.run.status,
                )
        return self._run

    def migrate_instrument(self):
        names = {
            '157': 'Buckets',
            '155': 'Fast Eddie',
            '440': 'Can-o-corn',
            '158': 'Psghetti',
            }
        
        old_i = self.l.run.instrument
        self._instrumentmodel,c = self.get_or_create(seqmodels.SequencerModel, name=old_i.model.split('-')[0])
        self._instrument,c = self.get_or_create(seqmodels.Sequencer,
            model=self._instrumentmodel,
            name=names[old_i.serial],
            serial=old_i.serial,
            defaults={
                'max_runs_per_week': old_i.max_runs_per_week,
                'expired': old_i.expired,
                },
            )
            
        if len(old_i.model.split('-')) > 1:
            self._instrumentmanifold,c = self.get_or_create(seqmodels.Manifold, sequencer=self._instrument, name=old_i.model.split('-')[1])
        else:
            self._instrumentmanifold,c = self.get_or_create(seqmodels.Manifold, sequencer=self._instrument, name='A')


    def make_runtype(self, lane):
        pe = self.l.run.is_paired_end
        if pe:
            read='PE'
        else:
            read='SR'
        rt, c = self.get_or_create(seqmodels.RunType,
            read=read,
            length=lane.run.cycles.cycles,
            sequencer_model=self.instrumentmodel,
            expired=lane.run.instrument.expired,
            )
        return rt


    @property
    def flowcell(self):
        if not hasattr(self, '_flowcell'):
            pe = self.l.run.is_paired_end
            if pe:
                read='PE'
            else:
                read='SR'

            fcid = self.l.run.flowcell_id
            self._flowcell,c = self.get_or_create(seqmodels.Flowcell, name=fcid, runtype=self.runtype)
                
            if self.l.run.cluster_kit.cluster_kit.startswith('CB'):
                csmodel,c = self.get_or_create(seqmodels.ClusterStationModel, name="CBOT")
                if self.l.run.description.lower().startswith('j'):
                    cs,c = self.get_or_create(ClusterStation, name="Jipper Chipper", model=csmodel, serial="2")
                elif self.l.run.description.lower().startswith('d'):
                    cs,c = self.get_or_create(ClusterStation, name="Djibouti", model=csmodel, serial="3")
                else:
                    print "not sure what cluster station to assign %s" % self.l
                    cs, c = self.get_or_create(ClusterStation, name="Jipper Chipper", model=csmodel, serial="2")
            else:
                csmodel,c = self.get_or_create(ClusterStationModel, name="Cluster Station")
                cs,c = self.get_or_create(ClusterStation, name="Bertha", model=csmodel, serial="1")

            fcgen,c = self.get_or_create(FlowcellGeneration, flowcell=self._flowcell, kit=self.l.run.cluster_kit.cluster_kit, date=self.l.run.flowcell_date, cluster_station=cs)
        return self._flowcell


class SeqLane(object):
    def __init__(self, oldlane, sequencingsample, verbose=False):
        self.l = oldlane
        self._sequencingsample = sequencingsample
        self.verbose = verbose
        self.level = 0
    
    def migrate(self):
        print self.flowcell
        print self.lane
        print self.run
        print self.denaturedaliquot
        self.migrate_runpostprocess()

    def log(self, msg, limit=120):
        msg = (msg[:limit]+'..') if len(msg) > limit else msg
        msg = msg.replace('\n', ' ')
        print "%s%s" % (' ' * 4 * self.level, (msg[:limit]+'..') if len(msg) > limit else msg)

    def get_or_create(self, modelobject, manager='objects', **kwargs):
        obj, c = getattr(modelobject, manager).get_or_create(**kwargs)
        if c:
            if self.verbose: 
                self.log("CREATE: %s(%s)" % (modelobject._meta.object_name, ', '.join(['='.join((str(k),str(v))) for k,v in kwargs.items()])))
        elif self.verbose:
            self.log("GET: %s(%s)" % (modelobject._meta.object_name, ', '.join(['='.join((str(k),str(v))) for k,v in kwargs.items()])))
        return obj,c        

    def clean_sid(self):
        #GTAC_SAMPLE=r'(?P<foo>.*)GTAC[:-]\s*(\w+)(?:$|_(?P<percent>.*))'

        match = re.match('(.*?)_?(\d+)%.*', self.l.sample_id)
        if match:
            phix_percent = match.groups()[1]
            self._name = match.groups()[0]
        else:
            phix_percent = 0
            self._name = self.l.sample_id

        match = re.match('.*GTAC[:-]\s*(\w+)(?:$|\s*_.*)', self.l.sample_id, re.I)
        if match:
            self._is_gtac = True
            self._name = match.groups()[0]
        else:
            self._is_gtac = False
        log("clean_sid CLEAN: %s -> " % (self.l.sample_id) + str((self._is_gtac ,self._name, phix_percent)))
        return self._is_gtac, self._name,phix_percent    

    def lane_samples(self):
        """
        Given a lane, return the name, user, and percentage of the 
        samples in that lane using the multiprep csv if necessary.
        Also include the % phix used in the aliquot.
        """
        for mp in multiprepped:
            if self.l.sample_id == mp['Sequencing name']:
                samples = []
                    
                for x in range(1,4):
                    if not mp['Prepped sample name %d' % x]: continue
                    try:
                        u = User.objects.get(username=mp['Contact%d' % x].strip().lower().replace(' ','_')[:30])
                    except User.DoesNotExist:
                        u = phix
                    s = mp['Prepped sample name %d' % x]
                    pcs = mp['%%sample%d' % x]
                    samples.append((s,u,int(pcs)))

                phix_percent = mp['%PhiX']
                return (samples, phix_percent)
        else:
            return (( 
                (self.clean_sid()[0], get_lane_user(self.l), 100), ),
                 0.0)

    @property
    def flowcell(self):
        if not hasattr(self, '_flowcell'):
            pe = self.l.run.is_paired_end
            if pe:
                read='PE'
            else:
                read='SR'

            fcid = self.l.run.flowcell_id
            self._flowcell,c = self.get_or_create(seqmodels.Flowcell, name=fcid, runtype=self.runtype)
                
            if self.l.run.cluster_kit.cluster_kit.startswith('CB'):
                csmodel,c = self.get_or_create(seqmodels.ClusterStationModel, name="CBOT")
                if self.l.run.description.lower().startswith('j'):
                    cs,c = self.get_or_create(ClusterStation, name="Jipper Chipper", model=csmodel, serial="2")
                elif self.l.run.description.lower().startswith('d'):
                    cs,c = self.get_or_create(ClusterStation, name="Djibouti", model=csmodel, serial="3")
                else:
                    print "not sure what cluster station to assign %s" % self.l
                    cs, c = self.get_or_create(ClusterStation, name="Jipper Chipper", model=csmodel, serial="2")
            else:
                csmodel,c = self.get_or_create(ClusterStationModel, name="Cluster Station")
                cs,c = self.get_or_create(ClusterStation, name="Bertha", model=csmodel, serial="1")

            fcgen,c = self.get_or_create(FlowcellGeneration, flowcell=self._flowcell, kit=self.l.run.cluster_kit.cluster_kit, date=self.l.run.flowcell_date, cluster_station=cs)
        return self._flowcell
        
    @property
    def instrument(self):
        if not getattr(self, '_instrument', None):
            self.migrate_instrument()
        return self._instrument

    @property
    def instrumentmanifold(self):
        if not getattr(self, '_instrument', None):
            self.migrate_instrument()
        return self._instrumentmanifold

    @property
    def instrumentmodel(self):
        if not getattr(self, '_instrument', None):
            self.migrate_instrument()
        return self._instrumentmodel

    @property
    def denaturedaliquot(self):
        if not getattr(self, '_denaturedaliquot', None):
            is_gtac, lid, phix_percent = self.clean_sid()

            samples, phix_percent1 = self.lane_samples( ) # FIXME do I now need all this?

            if phix_percent is None:
                phix_percent = phix_percent1
        
            self._denaturedaliquot,c = self.get_or_create(seqmodels.DenaturedAliquot,
                sequencingsample=self._sequencingsample,
                loading_concentration=self.l.dna_conc.amount,
                phix_spike=phix_percent,
                )
        return self._denaturedaliquot

    def migrate_runpostprocess(self):
        for s in self.l.run.summary_set.all():
            pipe = self.get_or_create(seqmodels.PostProcessPipeline, version=s.pipeline_version.version)[0]
            date = s.date
            if s.control_lane:
                try:
                    ctrl_lane = seqmodels.Lane.objects.get(flowcell__run__num=s.control_lane.run.num, number=s.control_lane.num)
                except seqmodels.Lane.DoesNotExist:
                    #seq = Seq(lane=s.control_lane)
                    #if seq.is_gtac:
                    print "WARNING: can't set %s as control lane for %s.  Isn't in database yet" % (str(s.control_lane), self.run)
                    ctrl_lane = None
                    #else:
                    #    seq.migrate(s.control_lane.dropoff_date)
            else:
                ctrl_lane = None
            rpp = self.get_or_create(seqmodels.RunPostProcess, 
                    run=self.run,
                    results_file='run_post_process/nothing',
                    analysis_path=s.analysis_path,
                    pipeline=pipe,
                    date=date,
                    defaults={'control_lane': ctrl_lane},
                    )[0]
            if ctrl_lane and not rpp.control_lane:
                rpp.control_lane = ctrl_lane
                rpp.save()

            for ls in s.lanesummary_set.filter(lane=self.l):
                lpp = self.get_or_create(seqmodels.LanePostProcess,
                    lane=self.lane,
                    run_post_process=rpp
                    )[0]
                if s.percent_phasing:
                    lpp.postprocessvalue_set.create(name="Percent phasing", type="float", value=s.percent_phasing)
                if s.percent_prephasing:
                    lpp.postprocessvalue_set.create(name="Percent prephasing", type="float", value=s.percent_prephasing)
                if s.num_cycles:
                    lpp.postprocessvalue_set.create(name="Number of cycles", type="int", value=s.num_cycles)
                if ls.num_reads:
                    lpp.postprocessvalue_set.create(name="Number of reads", type="int", value=ls.num_reads)
                if ls.first_intensity:
                    lpp.postprocessvalue_set.create(name="First intensity", type="float", value=ls.first_intensity)
                if ls.twenty_intensity:
                    lpp.postprocessvalue_set.create(name="Twenty intensity", type="float", value=ls.twenty_intensity)
                if ls.percent_pass_filter:
                    lpp.postprocessvalue_set.create(name="Percent pass filter", type="float", value=ls.percent_pass_filter)
                if ls.num_tiles:
                    lpp.postprocessvalue_set.create(name="Number of tiles", type="int", value=ls.num_tiles)
                if ls.clusters_per_tile:
                    lpp.postprocessvalue_set.create(name="Clusters per tile", type="int", value=ls.clusters_per_tile)




    def migrate_instrument(self):
        names = {
            '157': 'Buckets',
            '155': 'Fast Eddie',
            '440': 'Can-o-corn',
            '158': 'Psghetti',
            }
        
        old_i = self.l.run.instrument
        self._instrumentmodel,c = self.get_or_create(seqmodels.SequencerModel, name=old_i.model.split('-')[0])
        self._instrument,c = self.get_or_create(seqmodels.Sequencer,
            model=self._instrumentmodel,
            name=names[old_i.serial],
            serial=old_i.serial,
            defaults={
                'max_runs_per_week': old_i.max_runs_per_week,
                'expired': old_i.expired,
                },
            )
            
        if len(old_i.model.split('-')) > 1:
            self._instrumentmanifold,c = self.get_or_create(seqmodels.Manifold, sequencer=self._instrument, name=old_i.model.split('-')[1])
        else:
            self._instrumentmanifold,c = self.get_or_create(seqmodels.Manifold, sequencer=self._instrument, name='A')

    @property
    def runtype(self):
        if not hasattr(self, '_runtype'):
            pe = self.l.run.is_paired_end
            if pe:
                read='PE'
            else:
                read='SR'
            self._runtype, c = self.get_or_create(seqmodels.RunType,
                read=read,
                length=self.l.run.cycles.cycles,
                sequencer_model=self.instrumentmodel,
                expired=self.l.run.instrument.expired,
                )
        return self._runtype

    @property
    def run(self):
        if not getattr(self, '_run', None):
            self._run, c = self.get_or_create(seqmodels.Run,
                num=self.l.run.num, 
                flowcell=self.flowcell, 
                sequencing_date=self.l.run.sequencing_date, 
                manifold=self.instrumentmanifold,
                folder_name=self.l.run.run_folder or '',
                software_version=self.l.run.scs_version.version,
                sequencing_kit=self.l.run.sequencing_kit.sequencing_kit,
                turnaround_date=self.l.run.read2_sequencing_date,
                status=self.l.run.status,
                )
        return self._run        

    @property
    def lane(self):
        if not getattr(self, '_lane', None):
            self._lane,c = self.get_or_create(seqmodels.Lane,
                flowcell=self.flowcell, 
                denaturedaliquot=self.denaturedaliquot, 
                primer=self.l.primer.name,
                number=self.l.num,
                )
        return self._lane
        
            
def migrate(l):
    """
    - must clean lane id
        - strip off phix % (if it exists)
        - make id unique (if two users have same id, append user name to id string... stripping it off at the end?)
        
    - sequencingsample id = lane id  (with lane cleaned)
    - most lanes' sequencingsample contains just 1 sample (at 100%) unless it's in the multiprep csv
        - if a sequencingsample has 1 sample then the sequencingsample and sample ids are the same
        - else sample ids are in the multiprep csv
    
    """

    # Easy stuff we do regardless of whether this is GTAC sample or not
    i,im,iman = migrate_instrument(l)
    fc,rt = migrate_flowcell(l,im)
    subdate = get_submitted_date(l)    
    is_gtac, lid, phix_percent = clean_sid(l.sample_id)

    luser = get_lane_user(l)

    seqingsample_id = make_unique_sid(l, lid, luser)     
    seqingsample,sscreated = migrate_sequencingsample(l, seqingsample_id, subdate, rt)

    samples, phix_percent1 = lane_samples(l)
    if phix_percent is None:
        phix_percent = phix_percent1
        
    aliquot = DenaturedAliquot.objects.create(
        sequencingsample=seqingsample,
        loading_concentration=l.dna_conc.amount,
        phix_spike=phix_percent,
        )
    lane = Lane.objects.create(
        flowcell=fc, 
        denaturedaliquot=aliquot, 
        primer=l.primer.name,
        number=l.num,
        )

    #
    #  Migrate Run  (l, fc, iman)
    #

    run, c = Run.objects.get_or_create(
            num=l.run.num, 
            flowcell=fc, 
            sequencing_date=l.run.sequencing_date, 
            manifold=iman,
            folder_name=l.run.run_folder or '',
            software_version=l.run.scs_version.version,
            sequencing_kit=l.run.sequencing_kit.sequencing_kit,
            turnaround_date=l.run.read2_sequencing_date,
            status=l.run.status,
            )
    if c:
        log("CREATE: Run(%d,%s)" % (run.num, fc))
    else:
        log("GET: Run(%d,%s)" % (run.num, fc))

    return

    for sid,user,samplepercent in samples:

        lab = migrate_lab(l, user)
        client = migrate_client(user, lab)
        prj = migrate_project(client, subdate)
        submission = migrate_submission(prj, client, subdate)
        sample,c = sampmodels.Specimen.objects.get_or_create(name=sid, client=client, organism=unknown_org, defaults={
            'submitted_from': submission,
            })
        if c:
            sample.created = subdate
            sample.save()
            
        samplesubmission,c = Sample.objects.get_or_create(sample=sample, submission=submission)
        if c:
            log("CREATE: Sample(sample=%s, submission=%s)" % (sample, submission))
        else:
            log("GET: Sample(sample=%s, submission=%s)" % (sample, submission))
        
        ssd = SampleSequencingDetail.objects.get_or_create(samplesubmission=samplesubmission, runtype=rt, defaults={'ready_date':subdate})[0]
        ssd.denaturedaliquot.add(aliquot)
        ss = migrate_sequencedsample(ssd)
        migrate_billing(l,ss,user,lab)

        seqingsampledetail,c = SequencingSampleDetail.objects.get_or_create(sequencingsample=seqingsample, samplesubmission=samplesubmission, percent_of_pool=float(samplepercent))
        if c:
            log("CREATE: SeqSampleDetail(%s,%s)" % (seqingsample, samplesubmission))
        else:
            log("GET: SeqSampleDetail(%s,%s)" % (seqingsample, samplesubmission))
        migrate_contacts(l,sample)
        
def migrate_postprocess(l, run):
        for summary in l.run.summary_set.all():
            pipe = Pipeline.objects.get_or_create(version=summary.pipeline_version.version)[0]
            date = summary.date
            rpp = RunPostProcess.objects.get_or_create(
                run=run, 
                results_file='run_post_process/nothing',
                analysis_path=summary.analysis_path,
                pipe=pipe,
                date=date,
                control_lane=Lane.objects.get(run__num=summary.control_lane.run.num, num=summary.control_lane.num),
                )[0]
            
def migrate_runpostprocess(summaryqs):
    for s in summaryqs: 
        pipe = PostProcessPipeline.objects.get_or_create(version=s.pipeline_version.version)[0]
        date = s.date
        #print s.run.num
        run = Run.objects.get(num=s.run.num)
        if s.control_lane:
            print type(s.control_lane)
            ctrl_lane = Lane.objects.get(flowcell__run__num=s.control_lane.run.num, number=s.control_lane.num)
        else:
            ctrl_lane = None
        rpp = RunPostProcess.objects.get_or_create(
                run=run,
                results_file='run_post_process/nothing',
                analysis_path=s.analysis_path,
                pipeline=pipe,
                date=date,
                control_lane=ctrl_lane,
                )[0]

        for ls in s.lanesummary_set.all():
            lane = Lane.objects.get(flowcell__run=run, number=ls.lane.num)
            lpp = LanePostProcess.objects.get_or_create(
                lane=lane,
                run_post_process=rpp
                )[0]
            if s.percent_phasing:
                lpp.postprocessvalue_set.create(name="Percent phasing", type="float", value=s.percent_phasing)
            if s.percent_prephasing:
                lpp.postprocessvalue_set.create(name="Percent prephasing", type="float", value=s.percent_prephasing)
            if s.num_cycles:
                lpp.postprocessvalue_set.create(name="Number of cycles", type="int", value=s.num_cycles)
            if ls.num_reads:
                lpp.postprocessvalue_set.create(name="Number of reads", type="int", value=ls.num_reads)
            if ls.first_intensity:
                lpp.postprocessvalue_set.create(name="First intensity", type="float", value=ls.first_intensity)
            if ls.twenty_intensity:
                lpp.postprocessvalue_set.create(name="Twenty intensity", type="float", value=ls.twenty_intensity)
            if ls.percent_pass_filter:
                lpp.postprocessvalue_set.create(name="Percent pass filter", type="float", value=ls.percent_pass_filter)
            if ls.num_tiles:
                lpp.postprocessvalue_set.create(name="Number of tiles", type="int", value=ls.num_tiles)
            if ls.clusters_per_tile:
                lpp.postprocessvalue_set.create(name="Clusters per tile", type="int", value=ls.clusters_per_tile)

def migrate_contacts(l,s):
    s.notes = ', '.join([("%s:%s" % (cd.contact, cd.type)) for cd in l.contactdetail_set.all()])
    s.save()

def migrate_billing(l,ss,u, lab):
    if u == phix: return
    cost = l.billingcode.cost.strip('$').split('+',1)[0].split('(',1)[0].replace(',','')
    bc = BillingCode.objects.get_or_create(name=l.billingcode.name, cost=cost)[0]
    bill = SequencingBill.objects.get_or_create(
        sequenced_sample=ss,
        billoffice_date=l.run.billing_date,
        lab=lab,
        code=bc,
        department=l.lab.department,
        )[0]

def migrate_submission_sample(l, u, sid, prj, org, subdate):
    """
    If sample_id is unique, then get_or_create a submission and create a new sample.
    If sample_id not unique, but contacts are different from the matching sample_ids then do same as above.
    If sample_id not unique and have matching contacts, create new submission but use existing sample (or create if it doesn't exist).
    """
    #if sm.Lane.objects.filter(sample_id=l.sample_id).count() == 1:
    #    sub,c = Submission.objects.get_or_create(project=prj, user=u)
    #    sub.submitted=subdate
    #    sub.save()
    #    sample = Sample(name=sid, user=u, organism=org)
    #    sample.save()
    #    sample.submissions.add(sub)
    #    return sub, sample
    #print l.__dict__
    #for l2 in sm.Lane.objects.filter(sample_id=l.sample_id).exclude(id=l.id).order_by('run__num'):
    print sid,l.sample_id
    for l2 in sm.Lane.objects.filter(sample_id__regex=r'%s(_\d+%%)?$' % re.escape(sid)).exclude(id=l.id).order_by('run__num'):
        # More than one lane with that sample_id
        for c in l.contacts.all():
            if c in l2.contacts.all():
                #print "%s: %s(%s), %s(%s)" % (l.sample_id, l.lab, l.contacts.all(), l2.lab, l2.contacts.all())
                # sample used more than once
                sample,c = sampmodels.Specimen.objects.get_or_create(name=sid, user=u, organism=org)
                if c:
                    print "Created sample '%s' that will be used more than once for %s." % (sid, u)
                    if Submission.objects.filter(project=prj, user=u).exists():
                        sub = Submission.objects.filter(project=prj, user=u)[0]
                    else:
                        sub = Submission.objects.create(project=prj, user=u)
                        sub.submitted = subdate
                        sub.save()
                    #sub,created = Submission.objects.get_or_create(project=prj, user=u)
                    #if created:
                    #    sub.submitted = subdate
                    #    sub.save()
                    #sub = Submission.objects.create(project=prj, user=u)
                    #sub.submitted = subdate
                    #sub.save()
                else:
                    print "Reusing sample %s... making sure to create a new submission for the sample" % sid
                    sub = Submission.objects.create(project=prj, user=u)
                sample.submissions.add(sub)
                return sub, sample

    #else we've got a unique sample.  create it
    if Submission.objects.filter(project=prj, user=u).exists():
        sub = Submission.objects.filter(project=prj, user=u)[0]
    else:
        sub = Submission.objects.create(project=prj, user=u)
        sub.submitted = subdate
        sub.save()
    print "Creating unique Sample %s, %s" % (sid, u)
    sample = sampmodels.Specimen(name=sid, user=u, organism=org)
    sample.save()
    sample.submissions.add(sub)

    return sub,sample

def lane_samples(l):
    """
    Given a lane, return the name, user, and percentage of the 
    samples in that lane using the multiprep csv if necessary.
    Also include the % phix used in the aliquot.
    """
    for mp in multiprepped:
        if l.sample_id == mp['Sequencing name']:
            samples = []
                
            for x in range(1,4):
                if not mp['Prepped sample name %d' % x]: continue
                try:
                    u = User.objects.get(username=mp['Contact%d' % x].strip().lower().replace(' ','_')[:30])
                except User.DoesNotExist:
                    u = phix
                s = mp['Prepped sample name %d' % x]
                pcs = mp['%%sample%d' % x]
                samples.append((s,u,int(pcs)))

            phix_percent = mp['%PhiX']
            return (samples, phix_percent)
    else:
        return (( 
            (self.clean_sid(l.sample_id)[0], get_lane_user(l), 100), ),
             0.0)
                    
            
def get_lane_user(l):
    c = l.contactdetail_set.filter(type='main').order_by('contact__name')
    if not c:
        u = User.objects.get_or_create(username='phiX')[0]
    else:
        u = User.objects.get(username=c[0].contact.name.strip().lower().replace(' ','_')[:30])
    return u

@transaction.commit_on_success
def go(run):
    lanes = sm.Lane.objects.all().order_by('run__num').filter(run__num=run)
    for x,l in enumerate(lanes):
        log("MIGRATE: %s(Run %d, Lane %d)" % (l.sample_id, l.run.num, l.num), level=0)
        if x % 10 == 0:
            sys.stderr.write("\r%d" % x)
            sys.stderr.flush()
        migrate(l)
    migrate_runpostprocess(sm.Summary.objects.all().filter(run__num=run))
        
if __name__ == '__main__':
    import sys
    run = sys.argv[1]
    go(run)
    #runs = sm.Run.objects.all()
    
    #migrate_lanes(lanes)

