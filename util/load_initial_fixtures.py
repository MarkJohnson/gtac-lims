
import csv
import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
sys.path.append('../sts')
sys.path.append('../')

from django.core.management import call_command

call_command("loaddata", "..\\analysis\\fixtures\\analysis_data.json")
call_command("loaddata", "..\\billing\\fixtures\\billing_data.json")
call_command("loaddata", "..\\libprep\\fixtures\\libprep_data.json")
call_command("loaddata", "..\\menus\\fixtures\\menus_data.json")
call_command("loaddata", "..\\sample\\fixtures\\sample_data.json")
call_command("loaddata", "..\\seq\\fixtures\\seq_data.json")