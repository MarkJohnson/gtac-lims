
import csv
import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
sys.path.append('../sts')
sys.path.append('../')

from django.db import transaction
from seq import models as seqmodels
from sys import argv

@transaction.commit_on_success
def migrate_run_name():
    for run in seqmodels.RunType.objects.all():
        run.save()

    print "run migrate complete"

migrate_run_name()