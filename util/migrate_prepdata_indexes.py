import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'sts.settings'

from django.db import transaction
from django.db.models import Max
from libprep import models as lpmodels

def get_indices():
    index_tuples = {}
    for index in lpmodels.Index.objects.all():
        index_tuples[index.name.lower()] = index

    return index_tuples

@transaction.commit_on_success
def migrate_indexes_from_prepdata(project):
    prepdata_attempt = project.prepdata.filter(project = project, prepdatadef__prepstep__name = 'Posthyb Data', \
                                               sampleprepdetail__isnull=False).aggregate(Max('attempt')).get('attempt__max')
    indexes = get_indices()

    if prepdata_attempt:
        for spd in project.sampleprepdetail.all():
            indices = list(spd.index.all())
            prepdata_index =  lpmodels.PrepData.objects.filter(sampleprepdetail = spd, prepdatadef__label = 'Index', \
                                                               attempt = prepdata_attempt)

            if prepdata_index:
                for index in prepdata_index[0].value.split(','):
                    index = indexes.get(index.strip().lower())
                    if index not in indices:
                        indices.append(index)

            spd.index = indices

        print "Migrated indexes for : ", project                

def start_migration():
    for project in lpmodels.Project.objects.all():
        migrate_indexes_from_prepdata(project)

start_migration()
