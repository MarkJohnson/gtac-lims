$(function () {
  window.Billing = {};

  Billing.BaseCodeType = Backbone.RelationalModel.extend({
    urlRoot: '/api/v1/basecodetype'
  });

  Billing.BaseCode = Backbone.RelationalModel.extend({
    urlRoot: '/api/v1/basecode'
  });

  Billing.ChargeCode = Backbone.RelationalModel.extend({
    urlRoot: '/api/v1/chargecode',
    relations: [
      {
        type: Backbone.HasOne,
        key: 'type',
        relatedModel: 'Billing.BaseCode',
        reverseRelation: {
          key: 'chargecodes'
        } 
      }
    ]
  });

  Billing.Multiplier = Backbone.RelationalModel.extend({
    urlRoot: '/api/v1/multiplier',
    relations: [
      {
        type: Backbone.HasOne,
        key: 'chargecode',
        relatedModel: 'Billing.ChargeCode',
        reverseRelation: {
          key: 'multipliers'
        } 
      }
    ]
  });

  Billing.BatchMultiplier = Backbone.RelationalModel.extend({
    urlRoot: '/api/v1/batchmultiplier'
  });

  Billing.BaseCodeCollection = Backbone.Collection.extend({
    urlRoot: '/api/v1/basecode',
    model: Billing.BaseCode
    });
  Billing.BaseCodeList = new Billing.BaseCodeCollection;

  Billing.BatchMultiplierList = Backbone.Collection.extend({
    urlRoot: '/api/v1/batchmultiplier',
    model: Billing.BatchMultiplier
    });
  Billing.BatchMultipliers = new Billing.BatchMultiplierList;

  Billing.BaseCodeView = Backbone.View.extend({
    tagName: "option",
    template: _.template($("#basecode-template").html()),

    initialize: function() {
      this.model.bind('change', this.render, this);
      this.model.bind('destroy', this.remove, this);
      this.model.fetchRelated('chargecodes');
      
    },
    render: function() {
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    }
  });

  Billing.BatchMultiplierView = Backbone.View.extend({
    tagName: "option",
    events: {
        //click .publish':'togglePublish',
        //'change .type input':'saveType',
        //'click .doedit':'doEdit',
        //'change input':'foo'
    },
    template: _.template($("#batch-template").html()),
    initialize: function() {
      this.model.bind('change', this.render, this);
      this.model.bind('destroy', this.remove, this);
      
      /*this.model.fetchRelated('emailaddress');
      var x = this.model.get('emailaddress');
      x.bind('change', this.render, this);*/
      
    },
    render: function() {
      var tmpl_data = this.model.toJSON()
      //var tmpl_data = _.extend(this.model.toJSON(), {is_published: is_published})
      this.$el.html(this.template(tmpl_data));
      var argh = this;
      return this;
    },

    doEdit: function(e) {
        e.stopImmediatePropagation();
        this.$('.display').hide();
        this.$('.edit').slideDown('slow');
    },
    clear: function() {
      this.$el.fadeAndRemove();
      this.model.clear();
    }
    });

  Billing.AppView = Backbone.View.extend({
    el: $("#billingapp"),
    events: {
      'change #basecode-list': "basecodeChange"
    },
    initialize: function() {
      //this.input = this.$("#new-email");

      Billing.BatchMultipliers.bind('add', this.addOne, this);
      Billing.BatchMultipliers.bind('reset', this.addAll, this);
      Billing.BaseCodeList.bind('add', this.addOneBC, this);
      Billing.BaseCodeList.bind('reset', this.addAllBC, this);

      Billing.BaseCodeList.fetch();
      
    },
    addOne: function(multi) {
      var view = new Billing.BatchMultiplierView({model: multi});
      this.$("#batch-list").append(view.render().el);
    },
    addOneBC: function(bc) {
      var view = new Billing.BaseCodeView({model: bc});
      this.$("#basecode-list").append(view.render().el);
    },
    addAllBC: function() {
      Billing.BaseCodeList.each(this.addOneBC);
    },
    addAll: function() {
      Billing.BatchMultipliers.each(this.addOne);
    },

    basecodeChange: function(e) {
      // populate batchmultipliers that are related to this basecode.
      Billing.BatchMultipliers.fetch();
    },
    createOnEnter: function(e) {
      if (e.keyCode != 13) return;
      if (!this.input.val()) return;

      Billing.EmailAddressList.create({email: this.input.val()});
      this.input.val('');

    }
    });

  Billing.App = new Billing.AppView();

});

$(document).ajaxError( function(e, xhr, options, error){
  notify("error", error);
});

