// delay to server side filtering while typing in datatable serach field

var iTimeout = undefined;
$(function(){
    $('.dataTables_filter input')
        .unbind('keypress keyup')
        .bind('keyup', function(e){
            if(iTimeout != undefined) { clearTimeout(iTimeout);}
            var sNewSearch = this.value;
            that = this
            iTimeout = setTimeout(function(){
                    oTable.fnFilter($(that).val());
                }, 800);
        });
})
