# Create your views here.
from django.shortcuts import render,get_object_or_404
from sample import models as sampmodels
from base import models as basemodels

def sample_infobox(request):
    return render(request, 'sample/sample-infobox.html', {
        'sample': get_object_or_404(sampmodels.Sample, key=request.GET.get('key'))
        })

def client_infobox(request):
    return render(request, 'frontend/client-infobox.html', {
        'client': get_object_or_404(basemodels.Client, key=request.GET.get('key'))
        })
