from django.conf.urls import *

urlpatterns = patterns('',

    url(r'^ib/sample/$', 'frontend.views.sample_infobox', name='sample-infobox'),
    url(r'^ib/client/$', 'frontend.views.client_infobox', name='client-infobox'),
    )
