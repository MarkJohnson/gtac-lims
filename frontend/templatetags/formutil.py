from django import template
import string
import re

register = template.Library()

@register.inclusion_tag('forms/formfield.html')
def formfield(f, classes="", label=None, help_text=None):
    if label is not None:
        f.label = label
    if help_text is not None:
        f.help_text = help_text
    return {'field': f,
        'type': f.field.__class__.__name__,
        'classes': classes}

def field(f, classes="", label=None, help_text=None):
    if label is not None:
        f.label = label
    if help_text is not None:
        f.help_text = help_text
    return {
        'field': f,
        'classes': classes,
        'type': f.field.__class__.__name__,
        }

@register.inclusion_tag('forms/thfield.html')
def thfield(f, classes="", label=None, help_text=None):
    return field(f, classes, label, help_text)

@register.inclusion_tag('forms/tdfield.html')
def tdfield(f, classes="", label=None, help_text=None):
    return field(f, classes, label, help_text)

@register.inclusion_tag('forms/tdErrorfield.html')
def tdErrorfield(f, classes="", label=None, help_text=None):
    return field(f, classes, label, help_text)

@register.inclusion_tag('forms/trfield.html')
def trfield(f, classes="", label=None, help_text=None):
    return field(f, classes, label, help_text)

@register.inclusion_tag('forms/pfield.html')
def pfield(f, classes="", label=None, help_text=None):
    return field(f, classes, label, help_text)

@register.filter
def get_item(dictionary, key):
    try:
        k = int(key)
    except:
        k = key

    return dictionary.get(k)

@register.filter
def get_runtype(qs):
    for s in qs:
        try:
            return s.sample.seq.runtype
        except:
            pass
    return ""

@register.filter
def has_data(ps, project):
    return project.prepdata.filter(prepdatadef__prepstep=ps).count()

@register.filter
def get_prepdatadef(ps):
    pdds = ps.prepdatadef_set.filter(per_sample=True)
    return pdds

@register.filter
def get_prepdata(prepdata, ps):
    prepdata = prepdata.filter(prepdatadef__prepstep=ps).order_by('attempt', 'prepdatadef__index')
    data = {}
    for pd in prepdata:
        if pd.attempt not in data:
            data[pd.attempt] = []
        if pd.prepdatadef.type == 'FILE':
            data[pd.attempt].append(pd.file)
        elif pd.prepdatadef.type == 'IMAGE':
            data[pd.attempt].append(pd.image)
        else:
            data[pd.attempt].append(pd.value)
    return data.items()

@register.filter
def get_global_prepdata(prepdata, ps):
    data = prepdata.filter(prepdatadef__prepstep=ps, prepdatadef__per_sample=False).exclude(prepdatadef__type='File')\
                        .order_by('attempt', 'prepdatadef__index')
    dataDict = {}
    for pd in data:
        dataDict[pd.prepdatadef.label] = pd.image if pd.prepdatadef.type == 'IMAGE' else pd.value

    data = prepdata.filter(prepdatadef__prepstep=ps, prepdatadef__per_sample=False, prepdatadef__type='File')\
                        .order_by('prepdatadef__index', 'attempt')
    for pd in data:
        dataDict[pd.prepdatadef.label] = pd.file
    
    return dataDict.items()

@register.filter
def field_type(field, ftype):
    return check_type(field.field.widget, ftype)

@register.filter()
def to_int(value):
    return int(re.findall('\d+', value)[0])

@register.filter()
def sub_string(string, char):
    return string[0:string.find(char)].strip()

@register.filter
def get_sample_fromField_map(fields):
    samples_fields = {}
    for f in fields:
        if '-' in f.name:
            value = int(f.name[f.name.index('-') + 1:])
            if not value in samples_fields:
                samples_fields[value]=[]
            samples_fields[value].append(f)
    return samples_fields


class SetVarNode(template.Node):
 
    def __init__(self, var_name, var_value):
        self.var_name = var_name
        self.var_value = var_value
 
    def render(self, context):
        try:
            value = template.Variable(self.var_value).resolve(context)
        except template.VariableDoesNotExist:
            value = ""
        context[self.var_name] = value
        return u""
 
def set_var(parser, token):
    """
        {% set <var_name>  = <var_value> %}
    """
    parts = token.split_contents()
    if len(parts) < 4:
        raise template.TemplateSyntaxError("'set' tag must be of the form:  {% set <var_name>  = <var_value> %}")
    return SetVarNode(parts[1], parts[3])
 
register.tag('set', set_var)

