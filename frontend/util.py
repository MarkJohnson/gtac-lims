from django.core import mail
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.contrib.sites.models import Site
from django.conf import settings
from django.template import Context

from base import models as basemodels

import threading


class Mail(object):
    from_email = settings.DEFAULT_FROM_EMAIL
    subject_template = 'email/subject.txt'
    html_template = 'email/base.html'
    text_template = None

    def __init__(self, to_list, subject=None, context=None, files=[]):
        self.to_list = to_list
        self.subject = subject
        self.context = context
        self.files = files

    def send_mail(self, request):
        if self.context is None:
            self.context = {}
        self.context['STATIC_URL'] = settings.STATIC_URL
        self.context['current_site'] = Site.objects.get_current()
        self.context['domain'] = request.META['HTTP_HOST'] if request else None #Site.objects.get_current().domain
         
        if self.subject is None:
            self.subject = render_to_string(self.subject_template, self.context)

        html_content = render_to_string(self.html_template, self.context)
        text_content = render_to_string(self.text_template, self.context) if self.text_template else ''
    
        connection = mail.get_connection(fail_silently=False) 
        msg = EmailMultiAlternatives(self.subject, html_content, self.from_email, self.to_list, connection=connection)
        msg.attach_alternative(html_content, "text/html")

        for f in self.files:
            msg.attach(f.name, f.read(), 'application/octet-stream')

        msg.send()


def send_dir_to_seq_mail(seqsample, request):
    subject = 'Direct to sequencing sample Report'
    to_list = settings.DIR_TO_SEQ_RECIPIENT 
    files = []
    if seqsample.upload_trace:
        files = [seqsample.upload_trace.file]
    
    context = Context({'sample': seqsample.sampleprepdetails.all()[0]})
    mail = Mail(to_list, subject, context, files)
    mail.html_template = 'email/dir_to_seq_report.html'
    mail.send_mail(request)    


def send_qc_report_mail(project, request):
    subject = "QC Completed"
    to_list = settings.QC_REPORT_RECIPIENT
    lab = project.sampleprepdetail.all()[0].sample.specimen.submitted_by.lab
    try:
        lab_client = basemodels.Client.objects.get(person__first_name=lab.first_name, person__last_name=lab.last_name)
        pi_email = lab_client.person.email_addresses.all()[0].email
    except :
        pi_email = ''
    
    prepdatas = project.prepdata.filter(prepdatadef__label__in=['Attach Bioanalyzer Trace', 'Upload Bioanalyzer Trace', 'Upload Trace'],
                    prepdatadef__prepstep__name__in=['Initial QC', 'Results'], sampleprepdetail__isnull=True)
    files = []
    for pd in prepdatas:
        files.append(pd.file.file)

    sample_qty = {} 
    prepdata = project.prepdata.filter(prepdatadef__label__in = ['Quantity', 'Quantity (ng)', 'Quantity (ug)'], prepdatadef__prepstep__name = 'Initial QC')

    for p in prepdata:
        sample_qty[p.sampleprepdetail.sample.id] = p.value

    context  = Context({'project': project, 'pi_email': pi_email, 'sample_qty': sample_qty})
    mail = Mail(to_list, subject, context, files)
    mail.html_template = "email/project_qc_report.html"
    mail.send_mail(request)


def send_run_hiseq_mail(run, request):
    seq_date = run.sequencing_date.strftime('%m/%d/%Y') if run.sequencing_date else ''
    subject = 'Run %s on %s %s' %(run.num, run.flowcell.runtype, seq_date) 
    to_list = settings.RUN_HISEQ_RECIPIENTS
    context = Context({'run': run, 'is_run_completed': False})
    mail = Mail(to_list, subject, context)
    mail.html_template = 'email/run_report_mail.html'
    mail.send_mail(request)
    

def send_run_completed_mail(run, request=None):
    subject = "Run "+ run.num + " completed" 
    to_list = settings.RUN_COMPLETED_RECIPIENTS 
    context = Context({'run': run, 'is_run_completed': True})
    mail = Mail(to_list, subject, context)
    mail.html_template = 'email/run_report_mail.html'
    mail.send_mail(request)
