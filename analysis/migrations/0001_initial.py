# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
        ('sample', '0001_initial'),
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AlignmentGenome',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
                ('source', models.TextField(help_text=b'')),
                ('active', models.BooleanField(default=True, help_text=b'')),
                ('approved', models.BooleanField(default=True, help_text=b'')),
                ('path', models.FilePathField(help_text=b'', path=b'/srv/seq/analysis1/reference_sequences', recursive=True, match=b'\\.ndx$')),
            ],
            options={
                'ordering': ['species', 'name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AnalysisFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(max_length=500)),
                ('description', models.TextField(null=True, blank=True)),
                ('path', models.CharField(max_length=500)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AnalysisNote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('note', models.TextField()),
                ('author', models.ForeignKey(to='core.Person')),
            ],
            options={
                'ordering': ('-created',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Assembly',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
                ('is_active', models.BooleanField(default=True)),
            ],
            options={
                'verbose_name_plural': 'Assemblies',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Pipeline',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
                ('description', models.TextField(null=True, blank=True)),
                ('is_active', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PipelineSummary',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pipeline', models.ForeignKey(to='analysis.Pipeline')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SampleAnalysisDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('finish_date', models.DateField(null=True, blank=True)),
                ('start_date', models.DateField(null=True, blank=True)),
                ('manual', models.BooleanField(default=False)),
                ('analysis_clients', models.ManyToManyField(to='base.Client', null=True, blank=True)),
                ('assigned_to', models.ForeignKey(blank=True, to='base.Employee', null=True)),
                ('pipeline', models.ForeignKey(blank=True, to='analysis.Pipeline', help_text=b'', null=True)),
                ('sample', models.OneToOneField(related_name='analysis', to='sample.Sample', help_text=b'')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Species',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'', max_length=255, null=True, blank=True)),
                ('common_name', models.CharField(help_text=b'', max_length=255)),
                ('is_active', models.BooleanField(default=True)),
                ('officially_supported', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name_plural': 'Species',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SummaryStat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
                ('type', models.CharField(help_text=b'', max_length=10, choices=[(b'int', b'Integer'), (b'float', b'Floating Point'), (b'percent', b'Percent'), (b'str', b'String')])),
                ('raw_value', models.CharField(help_text=b'', max_length=255)),
                ('pipelinesummary', models.ForeignKey(help_text=b'', to='analysis.PipelineSummary')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='sampleanalysisdetail',
            name='species',
            field=models.ForeignKey(blank=True, to='analysis.Species', help_text=b'', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='pipelinesummary',
            name='sampleanalysisdetail',
            field=models.ForeignKey(to='analysis.SampleAnalysisDetail'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assembly',
            name='species',
            field=models.ForeignKey(help_text=b'', to='analysis.Species'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='analysisnote',
            name='sampleanalysisdetail',
            field=models.ManyToManyField(related_name='notes', to='analysis.SampleAnalysisDetail'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='analysisfile',
            name='sampleanalysisdetail',
            field=models.ForeignKey(to='analysis.SampleAnalysisDetail'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='alignmentgenome',
            name='species',
            field=models.ForeignKey(help_text=b'', to='analysis.Species'),
            preserve_default=True,
        ),
    ]
