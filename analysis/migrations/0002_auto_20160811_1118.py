# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alignmentgenome',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='alignmentgenome',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='alignmentgenome',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='alignmentgenome',
            name='path',
            field=models.FilePathField(help_text=b'', path=b'/tmp', recursive=True, match=b'\\.ndx$'),
        ),
        migrations.AlterField(
            model_name='analysisfile',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='analysisfile',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='analysisfile',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='analysisnote',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='analysisnote',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='analysisnote',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='assembly',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='assembly',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='assembly',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='pipeline',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='pipeline',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='pipeline',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sampleanalysisdetail',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sampleanalysisdetail',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sampleanalysisdetail',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
    ]
