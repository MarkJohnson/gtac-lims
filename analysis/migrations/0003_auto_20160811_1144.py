# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0002_auto_20160811_1118'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sampleanalysisdetail',
            name='analysis_clients',
            field=models.ManyToManyField(to='base.Client', blank=True),
        ),
    ]
