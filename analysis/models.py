from django.db import models
from django.conf import settings
from sample import models as sampmodels
from core import models as coremodels
from base import models as basemodels


class SampleAnalysisDetail(coremodels.CoreModel):
    sample = models.OneToOneField(sampmodels.Sample, related_name='analysis', help_text='')
    assigned_to = models.ForeignKey(basemodels.Employee, blank=True, null=True)
    finish_date = models.DateField(blank=True, null=True)
    start_date = models.DateField(blank=True, null=True)
    pipeline = models.ForeignKey("Pipeline", blank=True, null=True, help_text='')
    species = models.ForeignKey("Species", blank=True, null=True, help_text='')
    analysis_clients = models.ManyToManyField(basemodels.Client, blank=True)
    manual = models.BooleanField(default=False)
    
    def __unicode__(self):
        return u'%d' % (self.id)

    def clients(self):
        if self.analysis_clients.count() == 0:
            return [self.sample.submission.client, ]
        else:
            return self.analysis_clients

class AnalysisNote(coremodels.CoreModel):
    sampleanalysisdetail = models.ManyToManyField(SampleAnalysisDetail, related_name="notes")
    author = models.ForeignKey(coremodels.Person)
    note = models.TextField()

    class Meta:
        ordering = ('-created', )

    def __unicode__(self):
        return u'%d' % (self.id)


class ActiveManager(models.Manager):
    def get_queryset(self):
        return super(ActiveManager, self).get_queryset().filter(is_active=True)

class Pipeline(coremodels.CoreModel):
    name = models.CharField(max_length=255, help_text='')
    description = models.TextField(blank=True, null=True)
    is_active = models.BooleanField(default=True)

    objects = models.Manager()
    active = ActiveManager()

    def __unicode__(self):
        return u'%s' % (self.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('pipeline-view', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass


class SpeciesActiveManager(models.Manager):
    def get_queryset(self):
        return super(SpeciesActiveManager, self).get_queryset().filter(is_active=True)

class Species(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True, help_text='')
    common_name = models.CharField(max_length=255, help_text='')
    is_active = models.BooleanField(default=True)
    officially_supported = models.BooleanField(default=False)
    
    objects = models.Manager()
    active = SpeciesActiveManager()

    def __unicode__(self):
        return u'%s' % (self.common_name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('species-view', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        verbose_name_plural = "Species"
        pass

class Assembly(coremodels.CoreModel):
    name = models.CharField(max_length=255, help_text='')
    species = models.ForeignKey(Species, help_text='')
    is_active = models.BooleanField(default=True)
    
    objects = models.Manager()
    active = ActiveManager()

    def __unicode__(self):
        return u'%s' % (self.name)

    @models.permalink
    def get_absolute_url(self):
        return ('assembly-view', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        verbose_name_plural = "Assemblies"
        pass

class AlignmentGenome(coremodels.CoreModel):
    species = models.ForeignKey(Species, help_text='')
    name = models.CharField(max_length=255, help_text='')
    source = models.TextField(help_text='')    
    active = models.BooleanField(default=True, help_text='')
    approved = models.BooleanField(default=True, help_text='')
    path = models.FilePathField(path=settings.ALIGNMENT_GENOME_DIR, recursive=True, match='\.ndx$', help_text='')
    
    def __unicode__(self):
        return u'%s (%s)' % (self.species, self.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('alignmentgenome-view', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        ordering = ['species', 'name',]
        # unique_together(('',''), )
        # verbose_name_plural = ""


class AnalysisFile(coremodels.CoreModel):
    name = models.CharField(max_length=500)
    description = models.TextField(blank=True, null=True)
    path = models.CharField(max_length=500)
    sampleanalysisdetail = models.ForeignKey(SampleAnalysisDetail)
   

    def __unicode__(self):
        return u'%s' % (self.path)
    
    @models.permalink
    def get_absolute_url(self):
        return ('analysis-file-download', [str(self.key)])
        
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = "Result Data"
        pass

class PipelineSummary(models.Model):
    pipeline = models.ForeignKey(Pipeline)
    sampleanalysisdetail = models.ForeignKey(SampleAnalysisDetail)

    def __unicode__(self):
        return u'%d' % (self.id)

class SummaryStat(models.Model):
    SS_TYPES = (
        ('int','Integer'),
        ('float','Floating Point'),
        ('percent','Percent'),
        ('str', 'String'),
        )

    name = models.CharField(max_length=255, help_text='')
    type = models.CharField(max_length=10, choices=SS_TYPES, help_text='')
    raw_value = models.CharField(max_length=255, help_text='')
    pipelinesummary = models.ForeignKey(PipelineSummary, help_text='')

    def __unicode__(self):
        return u'%d' % (self.id)

    @property
    def value(self):
        if self.type in ['float', 'percent']:
            v =  float(self.raw_value)
        elif self.type in ['integer',] :
            v = int(self.raw_value)
        else:
            v = self.raw_value

        return v

    @models.permalink
    def get_absolute_url(self):
        return ('summarystat-view', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = "Result Data"
        pass

