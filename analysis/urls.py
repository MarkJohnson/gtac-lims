from django.conf.urls import *


urlpatterns = patterns('',
    url(r'^indexed_lanes/(?P<id>\d+)/$', 'analysis.views.run_indexed_lanes_json', name='analysis-run-indexed-lanes-json'),
    url(r'^samplesheet_xml/(?P<id>\d+)/$', 'analysis.views.samplesheet_xml', name='analysis-samplesheet-xml'),
    url(r'^samplekey_json/(?P<id>R?\d+)/$', 'analysis.views.samplekey_json', name='analysis-sampleskey-json'),
)
