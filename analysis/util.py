#!/usr/bin/env python

from xml.etree import ElementTree

def xmltopython(xml):
    """
    <pipelinesummary>
    <samplesubmission id="384848">
    <parameter type="percent" name="exome_coverage_5" value="91.90"/>
    <parameter type="integer" name="num_aligned_pairs" value="51367606"/>
    ...
    </samplesubmission>
    <samplesubmission id="384848">
    ...
    </samplesubmission>
    ...
    </pipeline>
    """
    tree = ElementTree.fromstring(xml)
    samplesubmission = {}

    for sstree in tree.findall('samplesubmission'):
        for p in sstree.findall('parameter'):
            samplesubmission[sstree.get('id')] = dict(p.items())

    return samplesubmission


if __name__ == '__main__':
    s = open('example.xml', 'r').read()
    print xmltopython(s)
