import json
import re
import string

from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404, render
from analysis import models
from sample import models as sampmodels
from seq import models as seqmodels
from analysis.util import xmltopython
from xml.etree import ElementTree

def file_download(request, key=None):

     analysis_file = get_object_or_404(models.AnalysisFile, key=key)

     r = HttpResponse(open(analysis_file.path, 'r'), content_type='application/octet-stream')
     r['Content-Disposition'] = 'attachment; filename=%s.bam' % (analysis_file.sampleanalysisdetail.samplesubmission.sample.name)
     return r

def file_list(request, key=None):
     submission = get_object_or_404(sampmodels.Submission, key=key)
     return render(request, "analysis/submission-file-list.html",  {
         'submission': submission,
         }
         )

def pipelinestats_upload(request):
    if request.method != 'POST':
        raise Http404

    xml = request.POST['xml']

    samplesummaries = xmltopython(xml)

    for k,v in samplesummaries.items():
        s = sampmodels.Sample.objects.get(id=int(k))
        s.analysis
        #for 
        ss = models.SummaryStat.objects.create(name=v['name'], value=v['value'], type=v['type'])

def run_indexed_lanes_json(request, id=None):
    run = get_object_or_404(seqmodels.Run, num=id)
    
    is_indexed = { } 

    for lane in run.flowcell.lanes.all():
        
        is_indexed[lane.num] = 0

        for ss in lane.sequencing_sample.all():
            if (ss.multiplex_type is not None):
                if (re.search('index', ss.multiplex_type)):
                    is_indexed[lane.num] = 1
                    break

    return HttpResponse(json.dumps(is_indexed), content_type="application/json")

def samplekey_json(request, id=None):
    rn = get_object_or_404(seqmodels.Run, num=id)

    samplekeys = [ ]
    sampleinfo = { }

    for ln in rn.flowcell.lanes.all():
    
        for ss in ln.sequencing_sample.all():

            if (ss.multiplex_type is not None):
   
                if (re.search('index', ss.multiplex_type)):

                    for spd in ss.sampleprepdetails.all():


                        run_num  = rn.num
                        lane_num = ln.num
                        customer = spd.sample.specimen.submitted_by.lab.last_name.lower()
                     
                        pipeline = 'unknown'
                        species  = 'unknown'

                        if (spd.sample.analysis.pipeline is not None):
                            pipeline = spd.sample.analysis.pipeline.name.lower()

                        if (spd.sample.analysis.species is not None): 
                            species = spd.sample.analysis.species.common_name.lower()

                        if (run_num not in sampleinfo):
                            sampleinfo[run_num] = { }

                        if (lane_num not in sampleinfo[run_num]):
                            sampleinfo[run_num][lane_num] = { }

                        if (customer not in sampleinfo[run_num][lane_num]):
                            sampleinfo[run_num][lane_num][customer] = { }
                           
                        if (pipeline not in sampleinfo[run_num][lane_num][customer]):
                            sampleinfo[run_num][lane_num][customer][pipeline] = { }
 
                        if (species not in sampleinfo[run_num][lane_num][customer][pipeline]):
                            sampleinfo[run_num][lane_num][customer][pipeline][species] = [ ] 

                        dict = { }
 
                        sampname = spd.sample.specimen.name.lower()

                        sqs = [ind.sequence for ind in spd.index.all()]
                        sns = [ind.name.lower() for ind in spd.index.all()]
      
                        index_sq = string.join(sqs, '-')
                        index_nm = string.join(sns, '-')
        
                        if (len(sqs) > 0):

                            dict['index_name']  = index_nm
                            dict['index_seq']   = index_sq
                            dict['sample_name'] = sampname

                            sampleinfo[run_num][lane_num][customer][pipeline][species].append(dict)

    for r in sampleinfo:
        for l in sampleinfo[r]:
            for c in sampleinfo[r][l]:
                for p in sampleinfo[r][l][c]:
                    for s in sampleinfo[r][l][c][p]:
                        sk = { }
                        sk['samples'] = [ ]
                        sk['filename'] = '_'.join([r, str(l), c, p, s])
                        if (len(sampleinfo[r][l][c][p][s]) > 0):
                            for smp in sampleinfo[r][l][c][p][s]:
                                sk['samples'].append(smp) 
                            samplekeys.append(sk)

    return HttpResponse(json.dumps(samplekeys, ensure_ascii=False), content_type="application/json") 

def samplesheet_xml(request, id=None):
    rn = get_object_or_404(seqmodels.Run, num=id)

    samplesheet   = ElementTree.Element("sample_sheet")
    run           = ElementTree.SubElement(samplesheet, 'run')

    run.text = rn.num 

    for ln in rn.flowcell.lanes.all():
        
        for seqsample in ln.sequencing_sample.all():
       
            if (seqsample.multiplex_type is not None):
 
                if (re.search('index', seqsample.multiplex_type)):
            
                    lane        = ElementTree.SubElement(samplesheet, 'lane')
                    lane_number = ElementTree.SubElement(lane, 'lane_number')
                    
                    lane_number.text = str(ln.num) 
 
                    index_sequences = [ ]

                    for spd in seqsample.sampleprepdetails.all():

                        indexes    = [ind.sequence for ind in spd.index.all()]
                        index_text = string.join(indexes, '-')
       
                        if (len(indexes) > 0):
                            index_sequences.append(index_text)

                    if (len(index_sequences) > 0):
                        demultiplex = ElementTree.SubElement(lane, 'demultiplex')
                        tags        = ElementTree.SubElement(demultiplex, 'tags')
                   
                        for inseq in index_sequences:
                            tag      = ElementTree.SubElement(tags, 'tag')
                            tag.text = inseq 
 
    xstr = ElementTree.tostring(samplesheet)
    return HttpResponse(xstr, content_type='text/xml')
