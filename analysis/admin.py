from django.contrib import admin
from analysis import models
from base.admin import CoreModelAdmin

class AssemblyInline(admin.TabularInline):
    model = models.Assembly

class SampleAnalysisDetailAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'start_date', 'finish_date')
    # fieldsets = []
    # list_filter = []
    # search_fields = []
    raw_id_fields = ['sample', ]
    # filter_horizontal = []
    # inlines = [ ModelInline ]
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.SampleAnalysisDetail, SampleAnalysisDetailAdmin)

class AnalysisNoteAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', )
    # fieldsets = []
    # list_filter = []
    # search_fields = []
    # filter_horizontal = []
    # inlines = [ ModelInline ]
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.AnalysisNote, AnalysisNoteAdmin)

class PipelineAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)
    # fieldsets = []
    # list_filter = []
    # search_fields = []
    # filter_horizontal = []
    # inlines = [ ModelInline ]
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.Pipeline, PipelineAdmin)

class SpeciesAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'officially_supported', 'is_active')
    # fieldsets = []
    list_filter = ['officially_supported', 'is_active']
    # search_fields = []
    # filter_horizontal = []
    inlines = [ AssemblyInline, ]
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.Species, SpeciesAdmin)

class AlignmentGenomeAdmin(CoreModelAdmin):
    list_display = ('__unicode__', 'species', 'name', 'path', 'active')
    # fieldsets = []
    list_filter = ['species', 'active',]
    # search_fields = []
    # filter_horizontal = []
    save_as = True
    # inlines = [ AlignmentGenomeVersionInline, ]
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.AlignmentGenome, AlignmentGenomeAdmin)

class AnalysisFileAdmin(CoreModelAdmin):
    list_display = ('__unicode__', 'sampleanalysisdetail')
    # fieldsets = []
    # list_filter = []
    # search_fields = []
    # filter_horizontal = []
    # inlines = [ ResultDataInline ]
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.AnalysisFile, AnalysisFileAdmin)

