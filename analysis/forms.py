from django import forms
from base.forms import BaseModelForm
from models import AlignmentGenome

class AlignmentGenomeForm(BaseModelForm):
    class Meta:
        model = AlignmentGenome
        

