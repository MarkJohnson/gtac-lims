from django import template
from libprep import models

register = template.Library()

@register.inclusion_tag('libprep/project-prep-data-form.html', takes_context=True)
def prep_data_form(context, spd_or_project, definition):
    if isinstance(spd_or_project, models.SamplePrepDetail):
        qs = models.PrepData.objects.filter(sampleprepdetail=spd_or_project, prepdatadef=definition)
    elif isinstance(spd_or_project, models.Project):
        qs = models.PrepData.objects.filter(project=spd_or_project, prepdatadef=definition)

    if qs.exists():
        value = qs[0].value
    else:
        value = None
    return { 
        'value': value,
        'def': definition,
        'spd': spd_or_project,
        'project': spd_or_project,
        }
