import menus

menus.named_view_factory('libprep-projects', "Projects", '[LibPrep] Projects')
menus.named_view_factory('libprep-my-projects', "My Projects", '[LibPrep] My Projects')
menus.named_view_factory('libprep-project-add', "Add Project", '[LibPrep] Add Project')
menus.named_view_factory('admin:index', "Admin Access", '[LibPrep] Admin Access')
menus.named_view_factory('libprep-prep-sequencing', "Sequencing Samples", '[LibPrep] Sequencing Samples')
menus.named_view_factory('libprep-sequencing-samples', "Sequencing Samples", '[LibPrep] Sequencing Samples List')
menus.named_view_factory('libprep-direct-sequencing-samples', "Direct to Sequencing Samples", '[LibPrep] Direct to Sequencing Samples List')
