from django.template import loader
from cStringIO import StringIO
from django.template.loader import render_to_string
from django.http import HttpResponse
from django.db import transaction

def get_wa_dotfile(wa, state=None, depth=1):
    cs = wa.current_state().state

    if state is None:
        viewing_state = cs
    else:
        viewing_state = state

    states = set([viewing_state])
    trans = set()

    newstates = set([viewing_state])
    for i in range(depth):
        dostates = newstates
        newstates = set()
        for s in dostates:
            for t in s.transitions_into.all():
                trans.update([t])
                newstates.update([t.from_state])
            
        states.update(newstates)
        
    newstates = set([viewing_state])
    for i in range(depth):
        dostates = newstates
        newstates = set()
        for s in dostates:
            for t in s.transitions_from.all():
                trans.update([t])
                newstates.update([t.to_state])
            
        states.update(newstates)
        
    return loader.render_to_string('wa.dot', {'wa': wa, 'current_state': cs, 'viewing_state': viewing_state, 'states': states, 'trans': trans })
    
def get_wf_dotfile(workflow, active_state=None):
    """
    Given a workflow will return the appropriate contents of a .dot file for 
    processing by graphviz
    """
    return loader.render_to_string('workflow.dot', {'workflow': workflow, 'state':active_state})

def get_database_records(request, querySet, columnIndexNameMap, searchableColumns, jsonTemplatePath, *args):
    #Safety measure. If someone messes with iDisplayLength manually, we clip it to
    #the max value of 100.
    if not 'iDisplayLength' in request.GET or not request.GET['iDisplayLength']:
        iDisplayLength = 10 # default value
    else: 
        iDisplayLength = min(int(request.GET['iDisplayLength']),100)

    if not 'iDisplayStart' in request.GET or not request.GET['iDisplayStart']:
        startRecord = 0 #default value
    else:
        startRecord = int(request.GET['iDisplayStart'])
    endRecord = startRecord + iDisplayLength 

    #apply ordering 
    if not 'iSortingCols' in request.GET or not request.GET['iSortingCols']:
        iSortingCols = 0 #default value
    else:
        iSortingCols = int(request.GET['iSortingCols'])
    asortingCols = []
    
    if iSortingCols>0:
        for sortedColIndex in range(0, iSortingCols):
            sortedColName = columnIndexNameMap[int(request.GET['iSortCol_'+str(sortedColIndex)])]
            sortingDirection = request.GET['sSortDir_'+str(sortedColIndex)]
            if sortingDirection == 'desc':
                sortedColName = '-'+sortedColName
            asortingCols.append(sortedColName) 
            
        querySet = querySet.order_by(*asortingCols)
    
    #apply filtering by value sent by user
    if not 'sSearch' in request.GET or not request.GET['sSearch']:
        customSearch = '' #default value
    else:
        customSearch = request.GET['sSearch'].encode('utf-8');
    if customSearch != '':
        outputQ = None
        first = True
        for searchableColumn in searchableColumns:
            kwargz = {searchableColumn+"__icontains" : customSearch}
            q = Q(**kwargz)
            if (first):
                first = False
                outputQ = q
            else:
                outputQ |= q
        
        querySet = querySet.filter(outputQ)
        
    #count how many records match the final criteria
    iTotalRecords = iTotalDisplayRecords = querySet.count()
    
    #get the slice
    qs = querySet[startRecord:endRecord]
    
    #prepare the JSON with the response
    if not 'sEcho' in request.GET or not request.GET['sEcho']:
        sEcho = '0' #default value
    else:
        sEcho = request.GET['sEcho'] #this is required by datatables 
    jstonString = render_to_string(jsonTemplatePath, locals())
    
    response = HttpResponse(jstonString, content_type="application/javascript")
    return response





def clone(inst):
    m = inst.__class__.objects.get(id=inst.id)
    m.id = None
    m.pk = None
    return m

@transaction.atomic
def clone_preptypeversion(ptv):
    newptv = clone(ptv)
    newptv.version = newptv.version + 1
    newptv.is_active = False
    newptv.save()

    for step in ptv.steps.all():
        newstep = clone(step)
        newstep.preptypeversion = newptv
        newstep.save()
        for pdd in step.prepdatadef_set.all():
            newpdd = clone(pdd)
            newpdd.prepstep = newstep
            newpdd.save()

    return newptv

if __name__ == '__main__':
    from libprep import models
    oldptv =  models.PrepTypeVersion.objects.get(id=1)
    newptv = clone_preptypeversion(oldptv)
    print newptv
