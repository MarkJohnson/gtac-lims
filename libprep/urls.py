from django.conf.urls import *
from libprep import views

urlpatterns = patterns('',

    url(r'^projects/$', 'libprep.views.projects', name='libprep-projects'),
    url(r'^myprojects/$', 'libprep.views.my_projects', name='libprep-my-projects'),
    url(r'^projects/add/$', views.NewProjectView.as_view(), name='libprep-project-add'),
    url(r'^projects/(?P<id>\d+)/$', 'libprep.views.project_detail', name='libprep-project-detail'),
    url(r'^projectMockupSheet/(?P<id>\d+)/$', 'libprep.views.projectMockupSheet', name='libprep-project-MockupSheet'),
    url(r'^addsampletoproject/(?P<id>\d+)/$', 'libprep.views.add_samples_to_project', name='libprep-add-sample-to-project'),
    url(r'^new-submissions/$', 'libprep.views.new_submissions', name='new-submissions'),
    url(r'^new-submissions/$', 'libprep.views.new_submissions', name='new-submissions'),
    url(r'^sample/browse/$', 'libprep.views.sample_browse', name='libprep-sample-browse' ),
    url(r'^submission/assign/$', 'libprep.views.submission_assign', name='libprep-submission-assign'),
    url(r'^myqueue/$', 'libprep.views.my_queue', name='libprep-my-queue'),
    url(r'^prepsequencing/$', 'libprep.views.prep_for_sequencing', name='libprep-prep-sequencing' ),
    url(r'^seqsamples/(?P<key>[\w-]+)/$', 'libprep.views.seqsample_detail', name='libprep-seqsample-detail'),
    url(r'^seqsamples/$', 'libprep.views.sequencing_samples', name='libprep-sequencing-samples' ),
    url(r'^directseqsamples/$', 'libprep.views.direct_to_sequencing_samples', name='libprep-direct-sequencing-samples' ),
    #url(r'^samples/$', 'libprep.views.samples', name='libprep-samples'),
    url(r'^prepSampleBrowseData/$', 'libprep.views.prep_sample_browse_data', name='libprep-prep-sample-browse-data'),
    url(r'^cancelDirectToSeqSample/(?P<id>\d+)/$', 'libprep.views.cancel_direct_to_seq_sample', name='libprep-cancel-direct-seq-sample'),
    url(r'^cancelLibCoreSeqSample/(?P<id>\d+)/$', 'libprep.views.cancel_libcore_seq_sample', name='libprep-cancel-libcore-seq-sample'),
    url(r'^deletePrepdataFile/(?P<id>\d+)/$', 'libprep.views.delete_prepdata_file', name='libprep-delete-prepdata-file'),
    url(r'^seqsampleAllDetails/(?P<key>[\w-]+)/$', 'libprep.views.seqsample_all_details', name='libprep-seqsample-all-details'),
    url(r'^reSequenceSamples/$', 'libprep.views.resequence_sample', name='libprep-prep-resequence'),
    )
