#from workflow.models import Workflow, WorkflowActivity, Role
from django.db import models
from django.db.models.base import Model
from core import models as coremodels
from base import models as basemodels
from sample import models as sampmodels
from django.core.exceptions import ValidationError
from django_extensions.db.fields import AutoSlugField
from datetime import date
import pickle
import uuid
import os

ADMIN_ROLE="Prep Admin"
CLIENT_ROLE="Prep Client"

MULTIPLEX_CHOICES = (
    ('index', "Index"),
    ('dual index', "Dual Index"),
    ('barcoded', "Barcode"),
    )

class ActiveManager(models.Manager):
    def get_queryset(self):
        return super(ActiveManager, self).get_queryset().filter(is_active=True)


class SamplePrepDetailManager(models.Manager):
    def unassigned(self):
        return super(SamplePrepDetailManager, self).get_query_set().filter(assigned_to__isnull=True)

    def needs_project(self):
        return super(SamplePrepDetailManager, self).get_query_set().filter(complete=False).filter(projects=None)

    def preety_gtac_ids(self):
        qs = self.get_queryset()
        gtac_ids = []
        for s in qs:
            gtac_ids.append(s.sample.specimen.gtac_id)
        return sampmodels.pretty_integer_list(gtac_ids)       

class SamplePrepDetail(coremodels.CoreModel):
    """
    The front door to libprep.
    A one-to-one relationship between this one and the sample submission.
    This model contains details and status of the library prep process for a sample submission.
    
    All libprep models will get to a sample via this model rather than pointing 
    (ForeignKey, ManyToManyField) directly at the sample model.
    This will help to make libprep a more self-contained app.
    """
    # name is taken from the Sample Tracking Sheet.   Shouldn't really need name.... need to get rid of it.
    name = models.CharField(max_length=255, help_text='')
    
    sample = models.OneToOneField("sample.sample", related_name="libprep", help_text='')
    preptype = models.ForeignKey("PrepType", blank=True, null=True, help_text='')
    #diret to seq preptype field
    dts_preptype = models.CharField(max_length=255, blank=True, null=True)
    #history = models.ManyToManyField("PrepStatus", through="PrepHistory")
    status = models.ForeignKey("PrepStatus", default=1, help_text='')
    complete = models.BooleanField(default=False, help_text='')
    complete_date = models.DateField(blank=True, null=True, help_text='')
    multiplex_type = models.CharField(choices=MULTIPLEX_CHOICES, max_length=10, blank=True, null=True)
    assigned_to = models.ForeignKey(basemodels.Employee, blank=True, null=True)
    index = models.ManyToManyField("Index")
    index_length = models.IntegerField(default=0, blank=True, null=True) 
    objects = SamplePrepDetailManager()
    
    def __unicode__(self):
        return u'%s' % (self.sample.specimen.name)

    def preptype_name(self):
        if self.preptype is None:
            return "None - Direct to Sequencing";
        return self.preptype
    
    def turnaround_delta(self):
        return self.complete_date - self.sample.submission.submitted
    
    @models.permalink
    def get_absolute_url(self):
        return ('sampleprepdetail-view', [str(self.id)])
    
    class Meta:
        # get_latest_by = ""
        order_with_respect_to = 'sample'
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass


class PrepHistory(models.Model):
    sampleprepdetail = models.ForeignKey("SamplePrepDetail")
    status = models.ForeignKey("PrepStatus")
    time = models.DateTimeField(auto_now_add=True)
    notes = models.TextField(blank=True, null=True)

    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        pass
    
    def __unicode__(self):
        return u'%d' % (self.id)


class PrepStatus(models.Model):
    name = models.CharField(max_length=255, help_text='')

    def __unicode__(self):
        return u'%s' % (self.name)

    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        verbose_name_plural = "Prep Status"
        pass

class PrepType(models.Model):
    name = models.CharField(max_length=255, help_text='')
    submission_guidelines = models.TextField(help_text='')
    is_active = models.BooleanField(default=True, help_text='')
    basecode = models.ForeignKey('billing.BaseCode', blank=True, null=True)
    
    objects = models.Manager()
    active = ActiveManager()

    def __unicode__(self):
        return u'%s' % (self.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('preptype-view', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        ordering = ['name',]
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass
    
class Barcode(coremodels.CoreModel):
    name = models.CharField(max_length=255, unique=True, help_text='')
    sequence = models.CharField(max_length=255, help_text='')
    adapter1 = models.CharField(max_length=255, help_text='')
    adapter2 = models.CharField(max_length=255, help_text='')
    pcrprimer1 = models.CharField(max_length=255, help_text='')
    pcrprimer2 = models.CharField(max_length=255, help_text='')
    
    def __unicode__(self):
        return u'%s' % (self.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('barcode-view', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        ordering = ['created',]
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass

class ExomeCaptureKit(coremodels.CoreModel):
    name = models.CharField(max_length=255, unique=True, help_text='')
    capture_targets_file = models.FileField(upload_to='exomecapture/', blank=True, null=True)

    @models.permalink
    def get_absolute_url(self):
        return ('exomecapturekit-detail', [str(self.id)])

    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        pass

class Index(coremodels.CoreModel):
    name = models.CharField(max_length=255, unique=True, help_text='')
    sequence = models.CharField(max_length=255, help_text='')
    adapter1 = models.CharField(max_length=255, blank=True, null=True, help_text='')
    adapter2 = models.CharField(max_length=255, blank=True, null=True, help_text='')
    pcrprimer1 = models.CharField(max_length=255, blank=True, null=True, help_text='')
    pcrprimer2 = models.CharField(max_length=255, blank=True, null=True, help_text='')
    indexing_primer = models.CharField(max_length=255, blank=True, null=True, help_text='')
    exomecapture_kit = models.ForeignKey(ExomeCaptureKit, blank=True, null=True, help_text='')
    
    def __unicode__(self):
        return u'%s' % (self.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('index-detail', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        ordering = ['id']
        # unique_together(('',''), )
        verbose_name_plural = "Indexes"

class MultiplexingType(models.Model):
    name = models.CharField(max_length=255, help_text='')
    
    def __unicode__(self):
        return u'%s' % (self.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('multiplexingtype-view', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass


class SequencingSampleDetail(coremodels.CoreModel):
    """
    SequencingSampleDetail - "Through" table bridging the input (SamplePrepDetail) and output (SequencingSample) of libprep.
    
    This describes how submitted samples were pooled together, if they were at all.
    """
    sampleprepdetail = models.ForeignKey(SamplePrepDetail, help_text='')
    sequencingsample = models.ForeignKey("SequencingSample", help_text='')
    #cruft - not used
    library = models.ForeignKey("Library", blank=True, null=True, help_text='')
    #cruft - stored elsewhere
    barcode = models.ForeignKey(Barcode, blank=True, null=True, help_text='')
    percent_of_pool = models.FloatField(blank=True, null=True)
    
    def __unicode__(self):
        return u'%d' % (self.id)
    
    @models.permalink
    def get_absolute_url(self):
        return ('sequencingsampledetail-view', [str(self.id)])
        
    @property
    def sample(self):
        return self.sampleprepdetail.sample

    class Meta:
        # get_latest_by = ""
        ordering = ['sampleprepdetail__sample__specimen__gtac_id']
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass

def get_upload_trace_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('sequencingsample', filename)

class SequencingSample(coremodels.CoreModel):
    """
    SequencingSample
    
    This is the output of libprep.  It should be used by seq in order to get denaturedaliquots.
    
    """
    # do we really need some sort of arbitrary name?
    name = models.CharField(max_length=255, help_text='')
    
    sampleprepdetails = models.ManyToManyField(SamplePrepDetail, through=SequencingSampleDetail, related_name='sequencingsamples', help_text='')
    loading_concentration = models.CharField(max_length=255, blank=True, null=True, verbose_name='Loading Concentration (pM)')
    submitted_concentration = models.CharField(max_length=255, blank=True, null=True, verbose_name='Submitted Concentration (nM)')
    primer = models.CharField(max_length=255, blank=True, null=True, help_text='')
    prepped = models.DateField(blank=True, null=True, help_text='')
    client_prepped = models.BooleanField(default=False, help_text='')
    multiplex_type = models.CharField(choices=MULTIPLEX_CHOICES, max_length=10, blank=True, null=True)
    submitted_to_sequencing = models.DateField(db_index=True, blank=True, null=True)
    concentration = models.FloatField(blank=True, null=True, verbose_name='Sample Concentration (ng/ul)')
    size = models.IntegerField(blank=True, null=True, verbose_name='Average Size (bp)')
    upload_trace = models.FileField(blank=True, null=True, upload_to=get_upload_trace_path, verbose_name='Upload Trace')
    qpcr_molarity = models.FloatField(blank=True, null=True, verbose_name='QPCR Molarity')
    qubit_molarity = models.FloatField(blank=True, null=True, verbose_name='Qubit Molarity')
    bioa_molarity = models.FloatField(blank=True, null=True, verbose_name='BioA Molarity')

    def __unicode__(self):
        return u'%s' % (self.name)
    
    @property
    def sample(self):
        """shortcut to sample for convenience"""
        return sampmodels.Sample.objects.filter(libprep__sequencingsamples=self)

    @models.permalink
    def get_absolute_url(self):
        return ('sequencingsample-detail', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        ordering = ('-created',)
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass
        
class Library(coremodels.CoreModel):
    sampleprepdetail = models.ForeignKey(SamplePrepDetail, help_text='')
    
    def __unicode__(self):
        return u'Library: %s' % (self.sample.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('library-view', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        verbose_name_plural = "Libraries"


class ProjectSample(coremodels.CoreModel):
    project = models.ForeignKey("Project")
    sampleprepdetail = models.ForeignKey(SamplePrepDetail)

    def save(self, *args, **kwargs):
        if self.sampleprepdetail.preptype != self.project.preptype:
            raise ValidationError('Sample prep type does not match project prep type')
        super(ProjectSample, self).save(*args, **kwargs)

PROJECT_STATUS = (
    ('qc','QC'),
    ('qc complete','QC Complete'),
    ('hold','Hold'),
    ('prepping','Prepping'),
    ('failed', 'Failed'),
    ('prep completed','Prep Completed'),
    )

class Project(coremodels.CoreModel):
    name = AutoSlugField(populate_from='make_slug_field', overwrite=True)
    prep_owner = models.ForeignKey(basemodels.Employee, related_name='prep_owner', blank=True, null=True)
    assigned_to = models.ForeignKey(basemodels.Employee, related_name='qc_owner', blank=True, null=True)
    sampleprepdetail = models.ManyToManyField(SamplePrepDetail, through=ProjectSample, related_name='projects', help_text='')
    preptypeversion = models.ForeignKey("PrepTypeVersion")
    status = models.CharField(max_length=255, default='qc', choices=PROJECT_STATUS)
    note = models.CharField(max_length=2000, blank=True, null=True)

    @property
    def steps(self):
        return self.preptypeversion.steps.all()
    
    @property
    def make_slug_field(self):
        return "%s" % self.created.strftime('%Y-%m-%d')

    @property
    def preptype(self):
        return self.preptypeversion.preptype

    def __unicode__(self):
        return u'Project: %d' % (self.id)
    
    @models.permalink
    def get_absolute_url(self):
        return ('project-detail', [str(self.id)])

    def pretty_labs(self):
        labs = []
        for spd in self.sampleprepdetail.all():
            lab = spd.sample.specimen.submitted_by.lab
            labs.append(lab)
        pretty_labs = ', '.join(str(lab) for lab in sorted(set(labs)))
        return pretty_labs
        
    class Meta(coremodels.CoreModel.Meta):
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        pass
        # verbose_name_plural = "Libraries"

class PrepTypeVersion(coremodels.CoreModel):
    preptype = models.ForeignKey(PrepType)
    version = models.PositiveSmallIntegerField()
    is_active = models.BooleanField(default=False, help_text='')

    objects = models.Manager()
    active = ActiveManager()

    def __unicode__(self):
        return u'%s v%d' % (self.preptype.name, self.version)

    class Meta(coremodels.CoreModel.Meta):
        ordering = ['preptype__name', '-version']

class PrepStep(coremodels.CoreModel):
    preptypeversion = models.ForeignKey(PrepTypeVersion, related_name='steps')
    name = models.CharField(max_length=255)
    description = models.TextField()
    index = models.PositiveSmallIntegerField(default=10)
    formulae = models.CharField(max_length=512, blank=True, null=True)
    slug = AutoSlugField(populate_from='name', overwrite=True)
    
    def __unicode__(self):
        return u'%s' % (self.name,)
    
    class Meta(coremodels.CoreModel.Meta):
        ordering = ['preptypeversion', 'index']


DATA_TYPES = (
    ('FLOAT','Float'),
    ('INT','Integer'),
    ('IMAGE','Image'),
    ('FILE','File'),
    ('STR','String (text)'),
    ('DATE','Date'),
    ('TIME','Time'),
    ('BOOL','True/False'),
    )

class PrepDataDefManager(models.Manager):
    def per_sample(self):
        return self.get_query_set().filter(per_sample=True)
    def per_project(self):
        return self.get_query_set().filter(per_sample=False)

class PrepDataDef(coremodels.CoreModel):
    prepstep = models.ForeignKey(PrepStep)
    label = models.CharField(max_length=255)
    type = models.CharField(max_length=255, choices=DATA_TYPES)
    required = models.BooleanField(default=False) 
    units = models.TextField(max_length=255, blank=True, null=True)
    default = models.CharField(max_length=255, blank=True, null=True)
    index = models.PositiveSmallIntegerField(default=10)
    per_sample = models.BooleanField(default=True)
    slug = AutoSlugField(populate_from='label', overwrite=True)
    
    objects = PrepDataDefManager()
    def __unicode__(self):
        return u'%s' % (self.label,)
    
    class Meta(coremodels.CoreModel.Meta):
        ordering = ['prepstep', 'index']


def uniq_upload_to():
    return str(uuid.uuid4()) + '/'

def get_prepdata_upload_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (filename.split('.')[0][:238], ext)
    return os.path.join('libprep/prepdata', filename)

class PrepData(coremodels.CoreModel):
    prepdatadef = models.ForeignKey(PrepDataDef)
    sampleprepdetail = models.ForeignKey(SamplePrepDetail, related_name="prepdata", null=True)
    project = models.ForeignKey(Project, related_name="prepdata")
    raw_value = models.CharField(max_length=255, blank=True, null=True)
    unit = models.CharField(max_length=255, blank=True, null=True)
    file = models.FileField(max_length=255, blank=True, null=True, upload_to=get_prepdata_upload_path)
    image = models.FileField(max_length=255, blank=True, null=True, upload_to=get_prepdata_upload_path)
    attempt = models.IntegerField(default="1")

    def __unicode__(self):
        #return u'%s Data %d' % (self.prepdatadef.label, self.id)
        return u'%d' % (self.id)

    def _get_value(self):
        if self.prepdatadef.type == 'IMAGE':
            return self.image
        elif self.prepdatadef.type == 'FILE':
            return self.file
        else:
            return pickle.loads(str(self.raw_value))

    def _set_value(self, v):
        if self.prepdatadef.type == 'FILE':
            self.file = v
        elif self.prepdatadef.type == 'IMAGE':
            self.image = v
        else:
            self.raw_value = pickle.dumps(v)
    
    value = property(_get_value, _set_value)

    class Meta(coremodels.CoreModel.Meta):
        ordering = ['prepdatadef__index']


class ProjectBill(coremodels.CoreModel):
    project = models.ForeignKey(Project)
    basecode = models.ForeignKey('billing.BaseCode', blank=True, null=True)
    total_charge = models.FloatField(blank=True, null=True)
    recharge = models.FloatField(blank=True, null=True)
    stockroom = models.FloatField(blank=True, null=True)
    fund93 = models.FloatField(blank=True, null=True)
    scc_subsidy = models.FloatField(blank=True, null=True)
    icts_subsidy = models.FloatField(blank=True, null=True)
    
    @property
    def multipliers(self):
        multipliers = ''
        for billmultiplier in self.projectbillmultiplier_set.all():
            multipliers += str(billmultiplier) + ', '
        return multipliers[:-2]

    @property
    def sample_count(self):
        return len(self.project.sampleprepdetail.all())

    @property
    def grand_total(self):
        if self.basecode.name == 'BioA':
            total =  self.total_charge
        else:
            total = self.sample_count * self.total_charge
        return total
        

class ProjectBillMultiplier(models.Model):
    projectBill = models.ForeignKey(ProjectBill)
    multiplier = models.ForeignKey('billing.MultiplierType', blank=True, null=True)

    def __unicode__(self):
        return u'%s' % (self.multiplier)
