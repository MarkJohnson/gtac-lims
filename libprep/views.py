from __future__ import absolute_import
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db import transaction
from base import views as baseviews
from base import models as basemodels
from libprep import models as lpmodels
from sample import models as sampmodels
from seq import models as seqmodels
from seq import views as seqviews
from seq import models as seqmodels
from django.contrib import messages
from libprep import forms
from django.views.generic.edit import FormView
from django.conf import settings
from django.db.models import Q, Max, Count
from util import file_manager as filemng
from frontend import util
import datetime
import logging
import json
import re

logger = logging.getLogger('myproject.custom')

@login_required
def new_submissions(request):
    if request.method == "POST":
        #handle posted data
        formset = forms.NewSubmissionsFormset(request.POST)
        if formset.is_valid():
            # save the form data 
            pass
            messages.add_message(request, messages.INFO, "X submissions assigned.")
            return HttpResponseRedirect(reverse('new-submissions'))
    else: # make blank form(s) and pass to template
        formset = forms.NewSubmissionsFormset(queryset=lpmodels.SamplePrepDetail.objects.filter(samplesubmission__submission__submitted__gt=datetime.datetime(2011,10,1)))

    return render(request, "libprep/new-submissions.html", {
        'submissions': sampmodels.Submission.objects.all(),
        'formset': formset,
         })

@login_required
def sample_browse(request):
    if not request.GET.get('sEcho', False):
        return render(request, 'libprep/sample-browse.html', {})
    SORT_COLS = (
        'samplesubmission__sample__name',
        'samplesubmission__submission__client__person__first_name',
        'samplesubmission__submission__submitted',
        )
    
    sampprepdetails = lpmodels.SamplePrepDetail.objects.all()
    dtr = baseviews.DataTableResponse(request)
    qs = dtr.filter_qs(sampprepdetails, SORT_COLS, search_fields=('samplesubmission__sample__name',))
    return render(request, 'libprep/sample-browse-data.json', {
        'dtr': dtr,
        'sampprepdetails': qs,
        })

@login_required
def submission_assign(request):
    employee = basemodels.Employee.objects.get(person__user=request.user)
    if request.method == "POST":
        formset = forms.NewSubmissionsFormset(request.POST)
        if formset.is_valid():
            messages.add_message(request, messages.INFO, "X submissions assigned.")
            for form in formset:
                if not form.cleaned_data['assigned']:
                    continue

                for spd in lpmodels.SamplePrepDetail.objects.filter(samplesubmission__submission=form.instance):
                    spd.assigned_to = employee
                    spd.save()
                
            return HttpResponseRedirect(reverse('libprep-my-queue'))
    else:
        # make blank form(s) and pass to template
        formset = forms.NewSubmissionsFormset(queryset=sampmodels.Submission.objects.filter(samplesubmission__libprep__assigned_to__isnull=True).distinct())

    return render(request, "libprep/submission-assign.html", {
        'formset': formset,
         })

@login_required
def my_queue(request):
    spd = lpmodels.SamplePrepDetail.objects.filter(assigned_to__person__user=request.user).filter(status__name="New")

    return render(request, "libprep/my-queue.html", {
        'sampleprepdetails': spd,
        })

@login_required
@transaction.atomic
def prep_for_sequencing(request):
    if request.method == 'POST':
        page = 1
        if request.POST.get('page') == 'page1':
            form = forms.NewSeqSampleForm(request.POST, prefix='psf')
            if form.is_valid():
                page = 2
                request.session['samples'] = form.cleaned_data['samples']
        else:
            page = 2
            form = forms.SequencingSampleForm(request.POST, request.FILES, prefix='f')
            formset = forms.SequencingSampleDetailFormset(request.POST, prefix='fs')
            if form.is_valid() and formset.is_valid():
                seqsample = form.save()
                multiplex_type = None
                for frm in formset:
                    cdata = frm.cleaned_data
                    if cdata['index']:
                        cdata['sampleprepdetail'].index = lpmodels.Index.objects.filter(pk__in=cdata['index'])
                    seqsample.sequencingsampledetail_set.create(sampleprepdetail=cdata['sampleprepdetail'], \
                                             percent_of_pool=cdata['percent_of_pool'])
                    multiplex_type =  multiplex_type if multiplex_type else cdata['sampleprepdetail'].multiplex_type

                in_q_status = lpmodels.PrepStatus.objects.get(name='Seq Queue')
                lpmodels.SamplePrepDetail.objects.filter(sequencingsampledetail__sequencingsample=seqsample)\
                                                    .update(status=in_q_status)
                sampleSeqDetails = seqmodels.SampleSequencingDetail.objects.filter(sample__libprep__in=seqsample.sampleprepdetails.all())
                sampleSeqDetails.update(num_lanes=form.cleaned_data['lanes_per_sample'], 
                                    phix_spike=form.cleaned_data['phix_spike_per_sample'])
                
                seqsample.multiplex_type = multiplex_type
                seqsample.indexed = True
                prepdata = getPrepdataFinishDate(seqsample)
                if prepdata:
                    seqsample.prepped = prepdata[0].value
                seqsample.save()
                del request.session['samples']
                return HttpResponseRedirect(reverse('libprep-sequencing-samples'))

        if page == 2:
            gtac_ids, sample_names, formset_data = {}, {}, []
            default_percent = 100 / (len(request.session['samples']) * 1.0 )
            default_percent = int(default_percent * 100) / 100.0 #truncating digits after 2 decimal places
            for spd in request.session['samples'].order_by('sample__specimen__gtac_id'):
                gtac_ids[spd.id] = spd.sample.specimen.gtac_id
                sample_names[spd.id] = spd.sample.specimen.name
                runtype = spd.sample.seq.runtype
                indexes = [index.id for index in spd.index.all()]
                formset_data.append({'sampleprepdetail': spd, 'index': indexes, 'percent_of_pool': default_percent})

            if request.POST.get('page') == 'page1':
                lanes = request.session['samples'][0].sample.seq.num_lanes
                formdata = {'loading_concentration': '14pM', 'primer': 'Genomic + Index', 'lanes_per_sample': lanes}
                form = forms.SequencingSampleForm(initial=formdata, prefix='f')
                formset = forms.SequencingSampleDetailFormset(initial=formset_data, prefix='fs')
          
            preptype = request.session['samples'][0].preptype
            return render(request, "libprep/seqsample-detail.html", {
                        'form': form,
                        'formset': formset,
                        'gtac_ids': gtac_ids,
                        'sample_names': sample_names,
                        'runtype': runtype,
                        'preptype': preptype,
                    })
    else:
        form = forms.NewSeqSampleForm(prefix='psf')

    return render(request, "libprep/prep-sequencing.html", {
            'form': form,
            'datatable_select_color': settings.DATATABLE_SELECT_COLOR
        })

@login_required
def resequence_sample(request):
     samples = json.loads(request.GET.get('sample'))
     samp_status = lpmodels.PrepStatus.objects.get(name='Prep Completed')
     lpmodels.SamplePrepDetail.objects.filter(id__in=samples).update(status = samp_status)
     return HttpResponse("Success")

@login_required
def prep_sample_browse_data(request):
    if request.GET.get('sEcho'):
        q_object = Q(status__name='Prep Completed', preptype__isnull=False, sample__seq__runtype__isnull=False)
        if request.GET.get('showNotBeenProcessed') == "true":
             q_object.add(Q(sequencingsampledetail__isnull=True), Q.AND)

        if request.GET.get('sequencedSample') == "true":
             q_object = Q(status = lpmodels.PrepStatus.objects.get(name='Seq Queue'), preptype__isnull=False) | \
                        Q(status = lpmodels.PrepStatus.objects.get(name='Prep Completed'), preptype__isnull=False) | \
                        Q(status = lpmodels.PrepStatus.objects.get(name='Sequencing'), preptype__isnull=False) | \
                        Q(status = lpmodels.PrepStatus.objects.get(name='Analysis'), preptype__isnull=False) | \
                        Q(status = lpmodels.PrepStatus.objects.get(name='Complete'), preptype__isnull=False)

        SORT_COLS = ('id',
                    'sample__submission__submitted',
                    'sample__specimen__gtac_id',
                    'sample__specimen__name',
                    ('sample__submission__client__person__first_name',
                     'sample__submission__client__person__last_name'),
                    'sample__submission__client__lab__shortname',
                    'preptype__name'
                )

        sampprepdetails = lpmodels.SamplePrepDetail.objects.filter(q_object).exclude(preptype__name='Bioanalyzer only')
        dtr = baseviews.DataTableResponse(request)
        qs = dtr.filter_qs(sampprepdetails, SORT_COLS, search_fields=(
            'preptype__name',
            'sample__submission__submitted',
            'sample__specimen__gtac_id',
            'sample__submission__client__person__first_name',
            'sample__submission__client__person__last_name',
            'sample__submission__client__lab__shortname',
            'sample__specimen__name',),
            )

        return render(request, 'libprep/prep-sample-browse-data.json', {
                'dtr': dtr,
                'sampprepdetails': qs,
            })


@login_required
def direct_to_sequencing_samples(request):
    return render(request, 'libprep/sequencing-samples.html', {
           'dir_to_seq': True,
           'runtypes': seqmodels.RunType.active.all(),
           })


@login_required
def sequencing_samples(request):
    if not request.GET.get('sEcho', False):
        return render(request, 'libprep/sequencing-samples.html', {})
    
    direct_to_seq = request.GET.get('directToSeq')
    status = request.GET.get('status')
    
    SORT_COLS = ('id', 'name',
            'sampleprepdetails__sample__specimen__gtac_id',
            'sampleprepdetails__sample__specimen__name',
            ('sampleprepdetails__sample__specimen__submitted_by__lab__first_name',
             'sampleprepdetails__sample__specimen__submitted_by__lab__last_name'),
            ('sampleprepdetails__sample__specimen__submitted_by__person__first_name',
             'sampleprepdetails__sample__specimen__submitted_by__person__last_name'),
            'prepped',
            'sampleprepdetails__preptype__name',
            'sampleprepdetails__sample__seq__runtype__run_name',
            'submitted_to_sequencing',
            'sequence_date',
            )

    SEARCH_FIELDS = ('name',
                    'sampleprepdetails__sample__specimen__name',
                    'sampleprepdetails__sample__specimen__submitted_by__lab__first_name',
                    'sampleprepdetails__sample__specimen__submitted_by__lab__last_name',
                    'sampleprepdetails__sample__specimen__submitted_by__person__first_name',
                    'sampleprepdetails__sample__specimen__submitted_by__person__last_name',
                    'sampleprepdetails__preptype__name')

    if status:
        dtr, qs = seqviews.seq_sample_queue_json(request, SORT_COLS, SEARCH_FIELDS)
    else:
        seqsamples = lpmodels.SequencingSample.objects \
                        .annotate(sequence_date=Max('sequencing_sample__flowcell__run__sequencing_date')).distinct()

        if direct_to_seq in 'true':
            seqsamples = seqsamples.filter(sampleprepdetails__preptype__isnull=True)

        dtr = baseviews.DataTableResponse(request, lpmodels.SequencingSample)
        qs = dtr.filter_qs(seqsamples, SORT_COLS, search_fields=SEARCH_FIELDS)

    return render(request, 'libprep/seqsample-browse-data.json', {
        'dtr': dtr,
        'seqsamples': qs
        })


def cancel_direct_to_seq_sample(request, id):
    lpstatus = lpmodels.PrepStatus.objects.get(name='Cancel')
    lpmodels.SamplePrepDetail.objects.filter(sequencingsamples__id=id).update(status=lpstatus)
    return HttpResponse ("Success")


def cancel_libcore_seq_sample(request, id):
    lpstatus = lpmodels.PrepStatus.objects.get(name='Cancel')
    lpmodels.SamplePrepDetail.objects.filter(sequencingsamples__id=id).update(status=lpstatus)
    return HttpResponse ("Success")


def seqsample_all_details(request, key):
    seqsample = get_object_or_404(lpmodels.SequencingSample, key=key)

    return render(request, 'libprep/seqsample-all-details.html', {
        'seqsample': seqsample
        })


#@login_required
#def samples(request):
#    if not request.GET.get('sEcho', False):
#        return render(request, 'libprep/samples.html', {})
#
#    SORT_COLS = ( )
#    SEARCH_COLS = (
#        'submission__id',
#        'sample__name',
#        ('submission__client__person__first_name', 'submission__client__person__last_name'),
#        'libprep__preptype__name',
#        'submission__submitted'
#     )
#
#    samples = sampmodels.Sample.objects.all()
#    owner = request.GET.get('owner', None)
#    if owner == 'mine':
#        samples = samples.filter(libprep__assigned_to__person__user=request.user)
#    elif owner == 'new':
#        samples = samples.filter(libprep__assigned_to__isnull=True)
#    if not request.GET.get('completed', '') == 'true':
#        samples = samples.exclude(libprep__status__name='complete')
#
#    dtr = baseviews.DataTableResponse(request)
#    qs = dtr.filter_qs(samples, SORT_COLS, search_cols=SEARCH_COLS,)
#    return render(request, 'libprep/sample-browse-data.json', {
#        'dtr': dtr,
#        'samples': qs
#        })
#

@login_required
def seqsample_detail(request, key=None):
    seqsample = get_object_or_404(lpmodels.SequencingSample, key=key)
    seqDetail = seqsample.sampleprepdetails.all()[0].sample.seq
    preptype = seqsample.sampleprepdetails.all()[0].preptype
    gtac_ids, sample_names, formset_data = {}, {}, []
    for spd in seqsample.sampleprepdetails.all().order_by('sample__specimen__gtac_id'):
        gtac_ids[spd.id] = spd.sample.specimen.gtac_id
        sample_names[spd.id] = spd.sample.specimen.name
        indexes = [index.id for index in spd.index.all()]
        pool_percent = spd.sequencingsampledetail_set.values('percent_of_pool')[0].get('percent_of_pool')
        formset_data.append({'sampleprepdetail': spd, 'index': indexes, 'percent_of_pool': pool_percent})

    if request.method == "POST":
        form = forms.SequencingSampleForm(request.POST, request.FILES, instance=seqsample, prefix='f')
        formset = forms.SequencingSampleDetailFormset(request.POST, prefix='fs')

        if form.is_valid() and formset.is_valid():
            obj = form.save(commit=False)
            prepdata = getPrepdataFinishDate(seqsample)
            if prepdata:
                obj.prepped = prepdata[0].value
            
            sampleSeqDetails = seqmodels.SampleSequencingDetail.objects.filter(sample__libprep__in=seqsample.sampleprepdetails.all())
            sampleSeqDetails.update(num_lanes=form.cleaned_data['lanes_per_sample'], 
                                    phix_spike=form.cleaned_data['phix_spike_per_sample'])

            for frm in formset:
                cdata = frm.cleaned_data
                cdata['sampleprepdetail'].index = lpmodels.Index.objects.filter(pk__in=cdata['index'])
 
            obj.save()
            if form.cleaned_data['send_mail']:
                util.send_dir_to_seq_mail(seqsample, request)
            return HttpResponseRedirect(reverse('libprep-sequencing-samples'))
    else:
        initial_data = {'lanes_per_sample': seqDetail.num_lanes, 'phix_spike_per_sample': seqDetail.phix_spike if seqDetail.phix_spike else 1}
       
        form = forms.SequencingSampleForm(instance=seqsample, prefix='f', initial=initial_data)
        formset = forms.SequencingSampleDetailFormset(initial=formset_data, prefix='fs')

    return render(request, "libprep/seqsample-detail.html", {
        'form': form,
        'formset':formset,
        'gtac_ids': gtac_ids,
        'sample_names': sample_names,
        'runtype': seqDetail.runtype,
        'preptype': preptype
    })


def getPrepdataFinishDate(seqsample):
    prepdatadef = lpmodels.PrepDataDef.objects.filter(per_sample=False, prepstep__name__in=['Prep Data', 'Posthyb Data'], label='Finish Date')
    prepdata = lpmodels.PrepData.objects.filter(
                    sampleprepdetail__isnull=True, 
                    prepdatadef__in=prepdatadef, 
                    project__sampleprepdetail__in=seqsample.sampleprepdetails.all())
    return prepdata

@login_required
def my_projects(request):
    return render(request, 'libprep/projects.html', {
        'is_my_project': True,
    })

@login_required
def projects(request):
    if not request.GET.get('sEcho', False):
        return render(request, "libprep/projects.html", {
            'projects': lpmodels.Project.objects.all(),
        })

    isBilling = True if request.GET.get("isBilling") else False

    SORT_COLS = (
        'name',
        'sampleprepdetail__sample__specimen__gtac_id',
        ('sampleprepdetail__sample__specimen__submitted_by__lab__first_name', 
         'sampleprepdetail__sample__specimen__submitted_by__lab__last_name'),
        ('assigned_to__person__first_name', 'assigned_to__person__last_name',
         'prep_owner__person__first_name', 'prep_owner__person__last_name'),
        'preptypeversion__preptype__name',
        'status',
        '', 
        'billing_date'
    )

    SEARCH_FIELDS = (
        'name',
        'sampleprepdetail__sample__specimen__submitted_by__lab__first_name', 
        'sampleprepdetail__sample__specimen__submitted_by__lab__last_name',
        'assigned_to__person__first_name',
        'assigned_to__person__last_name',
        'prep_owner__person__first_name',
        'prep_owner__person__last_name',
        'preptypeversion__preptype__name',
        'status', 
    )

    SEARCH_COLS = (
        'name',
        ('sampleprepdetail__sample__specimen__submitted_by__lab__first_name', 
         'sampleprepdetail__sample__specimen__submitted_by__lab__last_name'),
        ('assigned_to__person__first_name', 'assigned_to__person__last_name',
         'prep_owner__person__first_name', 'prep_owner__person__last_name'),
        'preptypeversion__preptype__name',
        'status'
    )
    projects = lpmodels.Project.objects.all().distinct() 

    if isBilling:
        projects = projects.filter(status__in = ['failed', 'prep completed']).extra(
                select={'billing_date': '(SELECT max("libprep_projectbill"."created") FROM "libprep_projectbill"'+
            ' WHERE "libprep_projectbill"."project_id" = "libprep_project"."id" GROUP BY "libprep_projectbill"."project_id")'})
    elif request.GET.get('showAll') == 'false' :
        projects =  projects.filter(status__in = ['qc', 'qc complete', 'hold', 'prepping'])

    if request.GET.get('is_my_project') == 'True':
        projects = projects.filter(Q(assigned_to__person__user = request.user) | Q(prep_owner__person__user = request.user))

    dtr = baseviews.DataTableResponse(request)
    qs = dtr.filter_qs(projects, SORT_COLS, search_fields=SEARCH_FIELDS, search_cols=SEARCH_COLS,)

    return render(request, 'libprep/project-browse-data.json', {
        'dtr': dtr,
        'projects': qs,
        'isBilling': isBilling,
        })


#def save_data(project, formset, global_formsets, FILES):
#    for f in formset.forms:
#        for k,v in f.cleaned_data.items():
#            if k.startswith('pdd'):
#                pd = lpmodels.PrepData.objects.get_or_create(
#                         project=project, 
#                         sampleprepdetail=f.cleaned_data['sampleprepdetail'], 
#                         prepdatadef_id=k[3:])[0]
#                if pd.prepdatadef.type == 'FILE':
#                    pd.file = f.cleaned_data[k] #FILES[k]
#                elif pd.prepdatadef.type == 'IMAGE':
#                    pd.image = f.cleaned_data[k] #FILES[k]
#                else:
#                    pd.value = v
#                pd.save()
#    for fs in global_formsets:
#        for f in fs.forms:
#            for k,v in f.cleaned_data.items():
#                if k.startswith('gpdd'):
#                    pd = lpmodels.PrepData.objects.get_or_create(
#                             project=project, 
#                             sampleprepdetail=f.cleaned_data['sampleprepdetail'], 
#                             prepdatadef_id=k[4:])[0]


def save_prepdata_form(cdata, project, ps, per_sample=True, prefix='', files={}):
    if 'DELETE' in cdata and cdata['DELETE']:
        query = lpmodels.PrepData.objects.filter(project=project, attempt=cdata['attempt'],
                                         prepdatadef__per_sample=per_sample, prepdatadef__prepstep=ps)
        query.delete() 
    else:
        new_prepdatas = []
        qcStatus   = lpmodels.PrepStatus.objects.get(name='QC')
        failedStatus = lpmodels.PrepStatus.objects.get(name='Failed')
        if per_sample:
            pdd_tuple = [(spd, pdd) for spd in project.sampleprepdetail.all()
                                for pdd in ps.prepdatadef_set.filter(per_sample=per_sample).exclude(label='Index')]
        else:    
            pdd_tuple = [(None, pdd) for pdd in ps.prepdatadef_set.filter(per_sample=per_sample)]
        
        prepdata = lpmodels.PrepData.objects.filter(project=project, sampleprepdetail__isnull=(not per_sample), attempt=cdata['attempt'])

        old_prepdata = {}
        for pd in prepdata:
            key = 'pdd%d-%d' % (pd.prepdatadef.id, pd.sampleprepdetail.id) if per_sample else 'pdd%d' % pd.prepdatadef.id
            old_prepdata[key] = pd
        for spd, pdd in pdd_tuple:
            key = 'pdd%d-%d' % (pdd.id, spd.id) if per_sample else 'pdd%d' % pdd.id
            if not key in cdata:
                continue

            if pdd.type != 'FILE' and key in old_prepdata:
                 pdo = old_prepdata[key]
                 pdo._set_value(cdata[key])
                 pdo.save()
            elif pdd.type != 'FILE' and (pdd.type == 'BOOL' or cdata[key]):
                 pdo = lpmodels.PrepData(project=project, prepdatadef=pdd, sampleprepdetail=spd, attempt=cdata['attempt'])
                 pdo._set_value(cdata[key])
                 new_prepdatas.append(pdo)

            name = "%s-%s" %(prefix, key)
            if name in files:
                for file in files.getlist(name):
                    pdo = lpmodels.PrepData(project=project, prepdatadef=pdd, sampleprepdetail=spd, attempt=cdata['attempt'])
                    pdo._set_value(file)
                    new_prepdatas.append(pdo)        
                    
        lpmodels.PrepData.objects.bulk_create(new_prepdatas)


def update_sample_status(project, spds, status):
    if 'Initial QC' not in [ps.name for ps in project.steps.all()] and status not in ['hold', 'qc complete']:
        statuses = {'qc': 'QC', 'prepping': 'Prepping', 'failed': 'Failed','prep completed': 'Prep Completed'}
        spds.update(status=lpmodels.PrepStatus.objects.get(name=statuses[status]))
        return True

    if status == 'qc':
        spds.update(status=lpmodels.PrepStatus.objects.get(name='QC'))
    elif status == 'prepping':
        prepdata_attempt =  project.prepdata.filter(prepdatadef__label='Pass?',
                                             prepdatadef__prepstep__name='Initial QC', sampleprepdetail__isnull=False)\
                                             .aggregate(Max('attempt'))
        prepdatas = project.prepdata.filter(prepdatadef__label='Pass?', attempt=prepdata_attempt['attempt__max'],
                                             prepdatadef__prepstep__name='Initial QC', sampleprepdetail__isnull=False)
    
        passSpds = [p.sampleprepdetail.id for p in prepdatas if p.value ] 
        prepdatas = project.prepdata.filter(prepdatadef__label='Failed, Prepping Anyway', attempt=prepdata_attempt['attempt__max'], 
                                             prepdatadef__prepstep__name='Initial QC', sampleprepdetail__isnull=False)

        prepanywaySpds = [p.sampleprepdetail.id for p in prepdatas if p.value and p.sampleprepdetail.id not in passSpds ]

        if passSpds or prepanywaySpds:
            spds.filter(id__in=prepanywaySpds).update(status=lpmodels.PrepStatus.objects.get(name='QC Failed, Prepping Anyway'))
            prepanywaySpds.extend(passSpds)
            spds.exclude(id__in=prepanywaySpds).update(status=lpmodels.PrepStatus.objects.get(name='Failed'))
        else:
            return False
        spds.filter(id__in=passSpds).update(status=lpmodels.PrepStatus.objects.get(name='Prepping'))
    elif status == 'failed':
        spds.update(status=lpmodels.PrepStatus.objects.get(name='Failed'))
    elif status == 'prep completed':
        spds.exclude(status__name='Failed').update(status=lpmodels.PrepStatus.objects.get(name='Prep Completed'))
    
    return True


def getPrepstepFormulae(project):
    ps_formulae = {}
    pdd_fields = {}

    for ps in project.steps.all():
        formula = ps.formulae
        if not formula:
            continue

        prepdata_attempt = project.prepdata.filter(prepdatadef__prepstep__name = ps.name, \
                                             sampleprepdetail__isnull=False).aggregate(Max('attempt'))

        attempt = 1 if not prepdata_attempt.get('attempt__max') else prepdata_attempt.get('attempt__max')

        for pdd in ps.prepdatadef_set.filter(per_sample=True):
            if not pdd.label in pdd_fields:
                pdd_fields[pdd.label] = ["sps%d" % ps.id, "pdd%d" % pdd.id, attempt]

        fields = []

        for pdd_label in pdd_fields:
            if "("+pdd_label+")" in formula:
                formula = formula.replace("("+pdd_label+")", pdd_fields[pdd_label][1])
                fields.append(pdd_fields[pdd_label])

        formula = formula.split('\r\n')
       
        for f in formula:
            oprand_list = re.split(r'[+,/,*,\-,=]',f)
            for oprand in oprand_list:
                if not re.findall(r'\d',oprand):
                    if f in formula: formula.remove(f)
        ps_formulae['sps%d' %(ps.id)] = {'formula': formula, 'fields': fields}

    return ps_formulae


def get_indices():
    indices = {}
    for index in lpmodels.Index.objects.all():
        indices[index.name.lower()] = index

    return indices


@login_required
def project_detail(request, id=None):
    project = get_object_or_404(lpmodels.Project, id=id)
    samplePrepDetails = project.sampleprepdetail.all().order_by('sample__specimen__gtac_id')
    projectForm = None
    activeStepId = None
    statusForm_error = None
    indices_error = None
    samples = sampmodels.Sample.objects.filter(libprep__in=set(samplePrepDetails))
    submission = sampmodels.Submission.objects.filter(sample__in=set(samples)).defer('description').distinct()
    
    if request.method == "POST" and request.POST.get('submit') == 'Save Note':
        noteForm = forms.AddProjectNote(request.POST)
        if noteForm.is_valid():
            project.note = noteForm.cleaned_data['note']
            project.save()
    else:
        noteForm = forms.AddProjectNote(initial={'note': project.note})

    if request.method == "POST" and request.POST.get('submit') == 'Change Status':
        projectForm = forms.ChangeProjectStatus(request.POST);
        if projectForm.is_valid():
            cdata = projectForm.cleaned_data
            if update_sample_status(project, samplePrepDetails, cdata['status']):
                project.status = cdata['status']
                if cdata['status'] == 'qc':
                    project.assigned_to = cdata['qc_owner']
                elif cdata['status'] == 'qc complete':
                    util.send_qc_report_mail(project, request)
                elif cdata['status'] == 'prepping' and project.assigned_to != None:        
                    project.prep_owner = cdata['prep_owner']
                project.save()
                samplePrepDetails = project.sampleprepdetail.all().order_by('sample__specimen__gtac_id')
            else:
                statusForm_error = "Status did not change to 'prepping' because none of the samples passed"    
    else:
        project_data = {'status': project.status, 'qc_owner': project.assigned_to, 'prep_owner': project.prep_owner}
        projectForm = forms.ChangeProjectStatus(initial=project_data);

    steps = []
    for ps in project.steps.all():
        if not activeStepId:
            activeStepId = ps.id
        l = {'ps': ps}
        can_delete = (project.status == 'qc' or ps.name not in 'Initial QC')
        SFS = forms.make_per_sample_form(project, ps, can_delete)
        GFS = forms.make_single_global_form(ps, project.status)
        
        if request.method == "POST" and request.POST.get('ps') and ps.id == int(request.POST.get('ps')):
            activeStepId = ps.id
            if SFS:
                sample_formset = SFS(request.POST, initial=forms.prepstep_initial_global_data(ps, project), prefix="sps%d"% ps.id)
                for frm in sample_formset:
                    if frm.is_valid() and frm.has_changed():
                        save_prepdata_form(frm.cleaned_data, project, ps)
            if GFS:
                prefix = 'gps%d' % ps.id
                initial_data = forms.prepstep_initial_global_data(ps, project, False) 
                frm = GFS(request.POST, request.FILES, initial=initial_data, prefix=prefix)
                if frm.is_valid() and frm.has_changed():
                    save_prepdata_form(frm.cleaned_data, project, ps, False, prefix, request.FILES)
                
        l['sample_pd_formsets'] = SFS(initial=forms.prepstep_initial_global_data(ps, project),
                                          prefix="sps%d" % ps.id) if SFS else None            
        
        l['global_form'] = GFS(initial=forms.prepstep_initial_global_data(ps, project, False), 
                                          prefix="gps%d" % ps.id) if GFS else None
       
        prepDataFiles = lpmodels.PrepData.objects.filter(project=project, prepdatadef__prepstep=ps,
                                       sampleprepdetail__isnull=True, prepdatadef__type='FILE')
        fileList = {}
        for pd in prepDataFiles:
            if pd.prepdatadef.id not in fileList:                
                fileList[pd.prepdatadef.id] = [pd]
            else:
                fileList[pd.prepdatadef.id].append(pd)

        l['fileList'] = fileList 
        steps.append(l)

        if project.status in ['qc', 'qc complete', 'hold']:
            break;

    if request.method == "POST" and request.POST.get('submit') == 'Save Index':
        activeStepId = -1
        indexFormset = forms.SamplePrepDetailIndexFormset(request.POST, prefix='ifs')
        indices = get_indices()

        if indexFormset.is_valid():
            for form in indexFormset:
                invalid_indices = []
                if form.is_valid():
                    cdata = form.cleaned_data
                    indexes = []
                    for ind in cdata['index'].split(','):
                        index_name = ind.strip().lower()
                        if index_name:
                            index = indices.get(ind.strip().lower())
                            if index:
                                indexes.append(index)
                            else:
                                invalid_indices.append(ind)
                    if len(invalid_indices) == 0:
                        sample = lpmodels.SamplePrepDetail.objects.get(id = cdata['id'])
                        sample.index = indexes
                    else:
                        indexFormset.errors.append("Invalid Indices: " + ', '.join(invalid_indices))
                        indices_error = True
    else:
        formset_data = []
        passedSamplePrepDetails = project.sampleprepdetail.all().exclude(status__name='Failed').order_by('sample__specimen__gtac_id')
        for sample in passedSamplePrepDetails:
            formset_data.append({'id':sample.id, 'index':", ".join(ind.name for ind in sample.index.all()), 'multiplex_type':sample.multiplex_type})

        indexFormset = forms.SamplePrepDetailIndexFormset(initial = formset_data, prefix='ifs')

    statusList = []
    if not project.assigned_to:
        statusList = ['qc complete','hold','prepping', 'failed', 'prep completed']
    elif project.status == 'qc':
        statusList = ['prepping', 'prep completed']
    elif project.status in ['qc', 'qc complete', 'hold']:
        statusList = ['prep completed']
  
    return render(request, "libprep/project-detail.html", {
        'project': project,
        'steps': steps,
        'indexFormset': indexFormset,
        'activeStepId': activeStepId,
        'spd': samplePrepDetails,
        'projectForm': projectForm,
        'noteForm': noteForm,
        'submission': submission,
        'statusForm_error': statusForm_error,
        'ps_formulae': getPrepstepFormulae(project), 
        'statusList': statusList,
        'indices_error': indices_error
    })

def delete_prepdata_file(request, id):    
    lpmodels.PrepData.objects.filter(id = id).delete()
    return HttpResponse ("Success")

def add_samples_to_project(request, id=None):
    project = get_object_or_404(lpmodels.Project, id=id)
    preptype = project.preptypeversion.preptype
    queryset=lpmodels.SamplePrepDetail.objects.filter(complete=False, projects=None, preptype=preptype)

    if request.method == "POST":
        form =forms.AddSampleToProject(request.POST, queryset=queryset)
        if form.is_valid():
            qc_status = lpmodels.PrepStatus.objects.get(name='QC')
            for spd in form.cleaned_data['samples']:
                lpmodels.ProjectSample.objects.create(sampleprepdetail=spd, project=project)
                spd.status = qc_status
                
            return HttpResponseRedirect(reverse("libprep-project-detail", args=(project.id,)))
    else:
        form = forms.AddSampleToProject(queryset=queryset)

    return render(request, "libprep/project-add.html", {
        'project': project,
        'form': form,
        'datatable_select_color': settings.DATATABLE_SELECT_COLOR,
    })

class NewProjectView(FormView):
    form_class = forms.NewProject
    template_name = 'libprep/project-add.html'
    project_id = None

    def get_context_data(self, **kwargs):
        context = super(NewProjectView, self).get_context_data(**kwargs)
        context['datatable_select_color'] = settings.DATATABLE_SELECT_COLOR
        return context

    def get_success_url(self):
        return reverse('libprep-project-detail', args=(self.project_id,))

    def form_valid(self, form):
        project = lpmodels.Project()
        project.preptypeversion = lpmodels.PrepTypeVersion.active.filter(preptype=form.cleaned_data['prep_type'])[0]
        project.save()
        self.project_id = project.id
        qc_status = lpmodels.PrepStatus.objects.get(name='QC')
        for spd in form.cleaned_data['samples']:
            lpmodels.ProjectSample.objects.create(sampleprepdetail=spd, project=project)
            spd.status = qc_status
            spd.save()
        return super(NewProjectView, self).form_valid(form)


def projectMockupSheet(request, id=None):
    project = get_object_or_404(lpmodels.Project, id=id)
    filename = 'Project Mockup'
    sheet = 'Mockup'
    spreadsheet = filemng.Spreadsheet(filename, [sheet], True)
    spreadsheet.set_portrait(False)
    spreadsheet.set_paper_size(1)
    spreadsheet.set_fit_num_pages(1)
    spreadsheet.set_font('Calibri', 240, False)
    
    headers = {
               0: {0: "Samples",     1: str(project.sampleprepdetail.all().count())},
               1: {0: "Lab:",        1: project.pretty_labs()},
               2: {0: "Prep Type:",  1: project.preptype.name},
               3: {0: "Submitted:",  1: project.created.strftime('%m-%d-%Y')},
               4: {0: "QC owner:",   1: str(project.assigned_to)},
               5: {0: "Prep owner:", 1: str(project.prep_owner)},
              }
    
    spreadsheet.write_data(sheet, headers)
    note, count = '', 1
    samples = project.sampleprepdetail.values_list('sample')
    for sub in sampmodels.Submission.objects.filter(sample__in=samples).defer('description').distinct():
        for n in sub.notes.all():
            note += str(count) + ") " + n.note + "\n"
            count +=1

    row, col = 7, 2
    spreadsheet.write_row(sheet, row + 1, {0: 'GTAC ID', 1: 'Sample'})

    for ps in project.steps.exclude(name='Lot Numbers'):
        spreadsheet.write_col(sheet, row, col, str(ps))
        if ps.name == 'Sequence Sample':
            spreadsheet.write_row(sheet, row + 1, {col : 'Sequence Name', col + 1: 'Completion Date'})
            col += 2
            continue
        pdds =  ps.prepdatadef_set.filter(per_sample=True).exclude(label__in=['Pass?', 'Failed, Prepping Anyway'])
        for pdd in pdds:
            spreadsheet.write_col(sheet, row + 1, col, pdd.label)
            col += 1

    row += 2
    spreadsheet.set_font('Calibri', 220, False)
    for spd in project.sampleprepdetail.all().order_by('sample__specimen__gtac_id'):
        spreadsheet.write_row(sheet, row, {0: str(spd.sample.specimen.gtac_id), 1: str(spd)})
        row += 1
    
    spreadsheet.set_font('Calibri', 240, False)
    spreadsheet.write_col(sheet, row + 1, 3, 'Note: ')
    spreadsheet.write_merge(sheet, row + 2, 3, row + 5, 8, note)
    for ps in project.steps.filter(name='Lot Numbers'):
        spreadsheet.write_col(sheet, row + 1, 0, 'Lot Numbers:')
        row += 2
        for pdd in ps.prepdatadef_set.filter(per_sample=False).exclude(type='FILE'):
            spreadsheet.write_col(sheet, row, 0, pdd.label)
            row += 1

    return spreadsheet.create_excel_sheet()

