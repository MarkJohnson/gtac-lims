from django.contrib import admin
from libprep import util
import models
from core.admin import CoreModelAdmin
from django.contrib import messages
from django import forms, db

def mark_inactive(modeladmin, request, queryset):
    for o in queryset:
        o.is_active = False
        o.save() 
mark_inactive.short_description = "Mark selected prep types as inactive"

def mark_active(modeladmin, request, queryset):
    for o in queryset:
        o.is_active = True
        o.save() 
mark_active.short_description = "Mark selected prep types as active"
 
    
def clone_version(modeladmin, request, queryset):
    for o in queryset:
        n = util.clone_preptypeversion(o)
        messages.add_message(request, messages.INFO, "%s created" % n)
clone_version.short_description = "Clone a preptype version"

class MultiplexingTypeAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)
admin.site.register(models.MultiplexingType, MultiplexingTypeAdmin)

class PrepStatusAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)
admin.site.register(models.PrepStatus, PrepStatusAdmin)

class PrepTypeAdmin(admin.ModelAdmin):
    list_display = ('__unicode__','is_active')
    list_filter = ['is_active',]
    actions = [mark_active, mark_inactive]
admin.site.register(models.PrepType, PrepTypeAdmin)

class BarcodeAdmin(CoreModelAdmin):
    list_display = ('__unicode__',)# 'well_position', 'fwd_sequence', 'rev_sequence')
admin.site.register(models.Barcode, BarcodeAdmin)

class ExomeCaptureKitAdmin(CoreModelAdmin):
    list_display = ('name', 'capture_targets_file')
admin.site.register(models.ExomeCaptureKit, ExomeCaptureKitAdmin)

class IndexAdmin(CoreModelAdmin):
    list_display = ('name', 'sequence')
admin.site.register(models.Index, IndexAdmin)

class SequencingSampleDetailInline(admin.TabularInline):
    list_display = ('percent_of_pool')
    model = models.SequencingSampleDetail
    readonly_fields = ('sampleprepdetail',)
    exclude = ('library', 'barcode',)

class SamplePrepDetailAdmin(CoreModelAdmin):
    list_display = ('__unicode__', 'preptype', 'status', )
    list_filter = ['preptype', 'status']
    search_fields = ['sample__specimen__name',]
admin.site.register(models.SamplePrepDetail, SamplePrepDetailAdmin)

class SequencingSampleDetailAdmin(CoreModelAdmin):
    list_display = ('__unicode__', )
admin.site.register(models.SequencingSampleDetail, SequencingSampleDetailAdmin)

class SequencingSampleAdmin(CoreModelAdmin):
    list_display = ('__unicode__', 'multiplex_type', 'loading_concentration', )
    list_filter = ['multiplex_type',]
    search_fields = ['name',]
    inlines = [ SequencingSampleDetailInline ]
    exclude = ('barcoded', 'indexed',)
admin.site.register(models.SequencingSample, SequencingSampleAdmin)

class LibraryAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)
    search_fields = ['sample__name',]
admin.site.register(models.Library, LibraryAdmin)

class ProjectSampleInline(admin.TabularInline):
    model = models.ProjectSample
    raw_id_fields = ( 'project', 'sampleprepdetail' )

class ProjectAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'preptypeversion', 'status')
    list_filter = ['preptypeversion', 'status']
    inlines = [ ProjectSampleInline ]
admin.site.register(models.Project, ProjectAdmin)

class PrepStepInline(admin.TabularInline):
    model = models.PrepStep

class PrepDataDefInline(admin.TabularInline):
    model = models.PrepDataDef

class PrepTypeVersionAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'preptype', 'version')
    list_filter = ['preptype', ]
    actions = [ clone_version, ]
    inlines = [ PrepStepInline ]
admin.site.register(models.PrepTypeVersion, PrepTypeVersionAdmin)

class PrepStepAdminForm(forms.ModelForm):
    class Meta:
        model = models.PrepStep
        widgets = {
            'formulae': forms.Textarea(attrs={'cols': 70, 'rows': 5})
        }
        fields = '__all__'

class PrepStepAdmin(admin.ModelAdmin):
    form = PrepStepAdminForm
    list_display = ('__unicode__', 'preptypeversion', )
    list_filter = ['preptypeversion', ]
    inlines = [ PrepDataDefInline ]

admin.site.register(models.PrepStep, PrepStepAdmin)

class PrepDataAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'raw_value', 'file', 'image')
admin.site.register(models.PrepData, PrepDataAdmin)

class ProjectBillAdmin(admin.ModelAdmin):
    list_display = ('project', 'created','sample_count', 'basecode', 'multipliers', 'grand_total', 'total_charge', 'recharge', 'stockroom', 'fund93')

admin.site.register(models.ProjectBill, ProjectBillAdmin)
