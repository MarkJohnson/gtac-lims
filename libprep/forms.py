from django import forms
from libprep import models as lpmodels
from sample import models as sampmodels
from django.forms.models import modelformset_factory, inlineformset_factory
from django.forms.formsets import formset_factory
from base import models as basemodels
from django.utils.safestring import mark_safe
from django.utils.datastructures import SortedDict
from operator import itemgetter, attrgetter
import re
import logging

logger = logging.getLogger('myproject.custom')

class NewProject(forms.Form):
    prep_type = forms.ModelChoiceField(lpmodels.PrepType.active.filter(sampleprepdetail__complete=False, 
                       preptypeversion__is_active=True, preptypeversion__isnull=False).defer('submission_guidelines').distinct())
    samples = forms.ModelMultipleChoiceField(queryset=lpmodels.SamplePrepDetail.objects.filter(complete=False, projects=None))


class AddSampleToProject(forms.Form):
    samples = forms.ModelMultipleChoiceField(queryset=lpmodels.SamplePrepDetail.objects.none())
    
    def __init__(self, *args, **kwargs):
        queryset = kwargs.pop('queryset')
        super(AddSampleToProject, self).__init__(*args, **kwargs)
        self.fields['samples'].queryset = queryset


PROJECT_STATUS = (
    ('qc','QC'),
    ('qc complete','QC Complete'),
    ('hold','Hold'),
    ('prepping','Prepping'),
    ('failed', 'Failed'),
    ('prep completed','Prep Completed'),
    )

class ChangeProjectStatus(forms.Form):
    qc_owner = forms.ModelChoiceField(basemodels.Employee.objects.all(), label="QC Owner", required=False)
    prep_owner = forms.ModelChoiceField(basemodels.Employee.objects.all(), label="Prep Owner", required=False)
    status = forms.ChoiceField(choices=PROJECT_STATUS, required=True)

    def clean(self):
        cd = self.cleaned_data
        if 'status' in cd and cd['status']:
            if cd['status'] == 'qc' and not cd['qc_owner']:
                self._errors["qc_owner"] = self.error_class(['This field is required'])
            elif cd['status'] == 'prepping' and not cd['prep_owner']:
                self._errors["prep_owner"] = self.error_class(['This field is required'])
        return cd


class AddProjectNote(forms.Form):
     note = forms.CharField(widget=forms.Textarea(attrs={'cols': 40, 'rows': 9}), required=True)


class NewSubmissionsForm(forms.ModelForm):
    assigned = forms.BooleanField(required=False)

    class Meta:
        model = sampmodels.Submission
        fields = ['id',]

NewSubmissionsFormset = modelformset_factory(sampmodels.Submission, form=NewSubmissionsForm, extra=0)


class NewSeqSampleForm(forms.Form):
    samples = forms.ModelMultipleChoiceField(lpmodels.SamplePrepDetail.objects.all().filter(preptype__isnull=False))


class SequencingSampleForm(forms.ModelForm):
    lanes_per_sample = forms.IntegerField(label="Number of Lanes per Sample",  initial=1, min_value=0, required=False)
    phix_spike_per_sample = forms.IntegerField(label="PhiX Spike-in per Sample",  initial=1, min_value=0, required=False)
    send_mail = forms.BooleanField(label="Do you want to send email?", required=False)

    class Meta:
        model = lpmodels.SequencingSample
        exclude = ('sampleprepdetails', 'prepped', 'barcoded', 'indexed', 'client_prepped', 'multiplex_type')

    def clean(self):
        cd = self.cleaned_data
        if not cd['lanes_per_sample']:
            self._errors["lanes_per_sample"] = self.error_class(['This field is required.']) 
        return cd

SequencingSampleFormset = inlineformset_factory(lpmodels.SequencingSample, lpmodels.SequencingSampleDetail, extra=0, can_delete=True, fields='__all__')


class SequencingSampleDetailForm(forms.Form):
    index = forms.ModelMultipleChoiceField(queryset=lpmodels.Index.objects.all(), widget=forms.SelectMultiple(attrs={'class': 'chosen-select'}), 
                                           required=False)
    sampleprepdetail = forms.ModelChoiceField(lpmodels.SamplePrepDetail.objects.all())
    percent_of_pool = forms.CharField(label="% Pool", required=False)
    areSamplesPrepared = forms.BooleanField(required=False)

    def clean(self):
        cd = self.cleaned_data
        if not cd['areSamplesPrepared'] and not cd['index']:
            self._errors['index'] = self.error_class(['This field is required.'])

        return cd
    
SequencingSampleDetailFormset = formset_factory(SequencingSampleDetailForm, extra=0)


class PreppedSampleForm(forms.ModelForm):
    class Meta:
        model = lpmodels.SequencingSample
        exclude = ('sampleprepdetails',)

# class SelectedSampleForm(forms.Form):
    # sample = forms.ModelChoiceField(lpmodels.SamplePrepDetail.objects.all()) #, widget=forms.HiddenInput)
    ##sample_name = forms.CharField(widget=forms.HiddenInput)
    ##client_name = forms.CharField(widget=forms.HiddenInput)
    # idx = forms.ModelChoiceField(lpmodels.Index.objects.all(), label="Index", )
    # percent = forms.CharField()

# SelectedSampleFormSet = formset_factory(SelectedSampleForm, extra=3)


MULTIPLEX_CHOICES = (
    ('index', "Index"),
    ('dual index', "Dual Index"),
    ('barcoded', "Barcode"),
    )

class SamplePrepDetailIndexForm(forms.Form):
    id = forms.CharField()
    index = forms.CharField()
    multiplex_type = forms.ChoiceField(choices=MULTIPLEX_CHOICES)

SamplePrepDetailIndexFormset = formset_factory(form=SamplePrepDetailIndexForm, extra=0)


#class BasePrepStepFormSet(forms.formsets.BaseFormSet):
#    def __init__(self, prepstep, project, *args, **kwargs):
#        self.prepstep = prepstep
#        self.project = project
#        #kwargs['initial'] = kwargs.get('initial', self.initial_data())
#        self.initial = kwargs.pop('initial', self.initial_data())
#        super(BasePrepStepFormSet, self).__init__(*args, **kwargs)


def prepstep_initial_global_data(prepstep, project, per_sample=True):
    data = []
    prepdata = lpmodels.PrepData.objects.filter(project=project, prepdatadef__prepstep=prepstep,
                                                prepdatadef__per_sample=per_sample).order_by('attempt')
    if not per_sample:
        prepdata = prepdata.exclude(prepdatadef__type='FILE')

    idata = {}
    for pd in prepdata:
        if per_sample:
            info = pd.prepdatadef.id, pd.sampleprepdetail.id
            key = 'pdd%d-%d' % info
        else:
            key = 'pdd%d' % pd.prepdatadef.id
        if not idata.has_key(pd.attempt):
           idata[pd.attempt] = {'attempt': pd.attempt}
        if pd.prepdatadef.type == 'FILE':
            idata[pd.attempt][key] = pd.file
        elif pd.prepdatadef.type == 'IMAGE':
            idata[pd.attempt][key] = pd.image
        else:
            idata[pd.attempt][key] = pd.value

    for i in idata:
        data.append(idata[i])

    if(len(data) == 0):
        data = [{'attempt': '1'}]

    if not per_sample:
       return data[0]
    return data

#def prepstep_initial_data(prepstep, project, per_sample=True):
#    prepdata = lpmodels.PrepData.objects.filter(project=project, prepdatadef__prepstep=prepstep, prepdatadef__per_sample=per_sample)
#
#    if per_sample:
#        idata = []
#        for spd in project.sampleprepdetail.all():
#            d = {'sampleprepdetail': spd, 'sample': spd.name }
#            for pd in prepdata:
#                if spd == pd.sampleprepdetail:
#                    if pd.prepdatadef.type == 'FILE':
#                        d['pdd%d' % pd.prepdatadef.id] = pd.file
#                    elif pd.prepdatadef.type == 'IMAGE':
#                        d['pdd%d' % pd.prepdatadef.id] = pd.image
#                    else:
#                        d['pdd%d' % pd.prepdatadef.id] = pd.value
#            idata.append(d)
#    else:
#        idata = {}
#        spd = project.sampleprepdetail.all()[0]
#        for pd in prepdata:
#            if spd == pd.sampleprepdetail:
#                if pd.prepdatadef.type == 'FILE':
#                    idata['pdd%d' % pd.prepdatadef.id] = pd.file
#                elif pd.prepdatadef.type == 'IMAGE':
#                    idata['pdd%d' % pd.prepdatadef.id] = pd.image
#                else:
#                    idata['pdd%d' % pd.prepdatadef.id] = pd.value
#            
#    return idata
#
#class BasePrepStepForm(forms.BaseForm):
#    def __init__(self, sampleprepdetail, *args, **kwargs):
#        self.sampleprepdetail = sampleprepdetail
#        super(BasePrepStepForm, self).__init__(*args, **kwargs)
#
#class ReadOnlyInput(forms.HiddenInput):
#    def render(self, name, value, **kwargs):
#        return super(ReadOnlyInput, self).render(name, value, **kwargs) + mark_safe(value)
#
#def make_prep_step_form(prepstep): 
#    fields = SortedDict()
#    fields['sampleprepdetail'] = forms.ModelChoiceField(lpmodels.SamplePrepDetail.objects.all(), widget=forms.HiddenInput)
#    fields['sample'] = forms.CharField(widget=ReadOnlyInput, required=False)
#    for pdd in prepstep.prepdatadef_set.filter(per_sample=True):
#        fields['pdd%d' % pdd.id] = get_field(pdd)
#        #FIXME: add this: #fields[pdd.slug].help_text = pdd.description
#
#    return type('PrepStepForm', (forms.BaseForm,), { 'base_fields': fields })

def get_field(pdd, instance=None):
    field = None

    # Django 1.6 switched to new default widgets for numeric fields.
    # Library Core personnel *hate* them (the spinners specifically).
    if pdd.type == 'FLOAT':
        field = forms.FloatField(widget=forms.TextInput)
    elif pdd.type == 'INT':
        field = forms.IntegerField(widget=forms.TextInput)
    elif pdd.type == 'IMAGE':
        field = forms.ImageField()
    elif pdd.type == 'FILE':
        field = forms.FileField(widget=forms.FileInput(attrs={'multiple': 'multiple'}))
    elif pdd.type == 'STR':
        field = forms.CharField()
    elif pdd.type == 'DATE':
        field = forms.DateField()
    elif pdd.type == 'TIME':
        field = forms.TimeField()
    elif pdd.type == 'BOOL':
        field = forms.BooleanField()

    if instance:
        field.initial = instance.value

    field.initial = pdd.default
    field.label = pdd.label
    field.required = False
    return field


#class PrepDataForm(forms.ModelForm):
#    def __init__(self, project, pdd, *args, **kwargs):
#        self.project = project
#        self.pdd = pdd
#        self.spd = kwargs.pop('spd', None)
#        if self.spd:
#            spdid = self.spd.id
#            try:
#                kwargs['instance'] = self.inst = lpmodels.PrepData.objects.get(project=project, prepdatadef=pdd, sampleprepdetail=self.spd)
#            except lpmodels.PrepData.DoesNotExist:
#                kwargs['instance'] = self.inst = None
#        else:
#            spdid = 0
#        kwargs['prefix'] = "pdf%d.%d" % (pdd.id, spdid)
#        super(PrepDataForm, self).__init__(*args, **kwargs)
#        field = get_field(pdd, self.inst)
#        self.fields['value'] = field
#        self.Meta.fields.append('value')
#
#    class Meta:
#        model = lpmodels.PrepData
#        fields = [ ]
#    
#    def save(self, *args, **kwargs):
#        if self.inst and self.inst.value == self.cleaned_data['value']:
#            return self.inst
#        instance = super(PrepDataForm, self).save(*args, commit=False)
#        instance.prepdatadef = self.pdd
#        instance.sampleprepdetail = self.spd
#        instance.project = self.project
#        instance.value = self.cleaned_data['value']
#        instance.save()
#        return instance
#
#def make_prepdata_forms(prepstep, project, request=None):
#    forms = []
#    pddlist = prepstep.prepdatadef_set.filter(per_sample=True)
#    for spd in project.sampleprepdetail.all().order_by('sample__specimen__gtac_id'):
#        d = {'sample': spd,
#             'forms': [],
#            }
#        for pdd in pddlist:
#            if request:
#                d['forms'].append(PrepDataForm(project, pdd, request.POST, request.FILES, spd=spd))
#            else:
#                d['forms'].append(PrepDataForm(project, pdd, spd=spd))
#        forms.append(d)
#
#    return forms


def make_per_sample_form(project, prepstep, can_delete):
    fields = SortedDict()
    pddlist = prepstep.prepdatadef_set.filter(per_sample=True).exclude(label='Index')
    if prepstep.name == 'Initial QC':
        sampleprepdetails = project.sampleprepdetail.all().order_by('sample__specimen__gtac_id')
    else:
        sampleprepdetails = project.sampleprepdetail.all().exclude(status__name='Failed').order_by('sample__specimen__gtac_id')

    for spd in sampleprepdetails:
        for pdd in pddlist:
            fields['pdd%d-%d' % (pdd.id, spd.id)] = get_field(pdd)
       
        functions = [] 
        for pdd in pddlist:
            if prepstep.formulae:
                formula = prepstep.formulae
                formulas = formula.split('\r\n')

                for formula in formulas:
                    if pdd.label in formula:
                        finalField = formula[0:formula.find('=')].strip()
                        finalField = finalField[1:len(finalField)-1]
                        pid = prepstep.prepdatadef_set.filter(label = finalField).values("id");
                        if pid and pdd.label != finalField: 
                            pddId = pid[0].get('id')
                            f = 'execute_sps%d_pdd%d(this);' % (prepstep.id, pddId)
                            if f not in functions:
                                functions.append(f)

                            fields['pdd%d-%d' % (pdd.id, spd.id)].widget.attrs['onchange'] = ' '.join(functions)

    if fields:
        fields['attempt'] = forms.CharField(widget=forms.HiddenInput())
        form_class = type('PDDSForm', (forms.BaseForm,), {'base_fields': fields})
        return formset_factory(form=form_class, extra=0, can_delete=can_delete)
    
    return None

def make_single_global_form(prepstep, project_status):
    fields = SortedDict()
    readonly = True if prepstep.name == 'Initial QC' and project_status not in ['qc','qc complete'] else False
 
    for pdd in prepstep.prepdatadef_set.filter(per_sample=False):
        fields['pdd%d' % pdd.id] = get_field(pdd)    
        if readonly:
            fields['pdd%d' % pdd.id].widget.attrs['disabled'] = 'disabled'

    if fields:
        fields['attempt'] = forms.CharField(widget=forms.HiddenInput())
        return type('PDDForm', (forms.BaseForm,), {'base_fields': fields})
    return None

