# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('libprep', '0002_auto_20150407_1253'),
    ]

    operations = [
        migrations.AlterField(
            model_name='barcode',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='barcode',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='barcode',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='exomecapturekit',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='exomecapturekit',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='exomecapturekit',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='index',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='index',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='index',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='library',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='library',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='library',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='prepdata',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='prepdata',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='prepdata',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='prepdatadef',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='prepdatadef',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='prepdatadef',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='prepstep',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='prepstep',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='prepstep',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='preptypeversion',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='preptypeversion',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='preptypeversion',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='projectbill',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='projectbill',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='projectbill',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='projectsample',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='projectsample',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='projectsample',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sampleprepdetail',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sampleprepdetail',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sampleprepdetail',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sequencingsample',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sequencingsample',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sequencingsample',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sequencingsampledetail',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sequencingsampledetail',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='sequencingsampledetail',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
    ]
