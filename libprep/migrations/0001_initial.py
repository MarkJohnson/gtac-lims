# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import libprep.models
import django.utils.timezone
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
        ('billing', '0001_initial'),
        ('sample', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Barcode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(help_text=b'', unique=True, max_length=255)),
                ('sequence', models.CharField(help_text=b'', max_length=255)),
                ('adapter1', models.CharField(help_text=b'', max_length=255)),
                ('adapter2', models.CharField(help_text=b'', max_length=255)),
                ('pcrprimer1', models.CharField(help_text=b'', max_length=255)),
                ('pcrprimer2', models.CharField(help_text=b'', max_length=255)),
            ],
            options={
                'ordering': ['created'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ExomeCaptureKit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(help_text=b'', unique=True, max_length=255)),
                ('capture_targets_file', models.FileField(null=True, upload_to=b'exomecapture/', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Index',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(help_text=b'', unique=True, max_length=255)),
                ('sequence', models.CharField(help_text=b'', max_length=255)),
                ('adapter1', models.CharField(help_text=b'', max_length=255, null=True, blank=True)),
                ('adapter2', models.CharField(help_text=b'', max_length=255, null=True, blank=True)),
                ('pcrprimer1', models.CharField(help_text=b'', max_length=255, null=True, blank=True)),
                ('pcrprimer2', models.CharField(help_text=b'', max_length=255, null=True, blank=True)),
                ('indexing_primer', models.CharField(help_text=b'', max_length=255, null=True, blank=True)),
                ('exomecapture_kit', models.ForeignKey(blank=True, to='libprep.ExomeCaptureKit', help_text=b'', null=True)),
            ],
            options={
                'ordering': ['id'],
                'verbose_name_plural': 'Indexes',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Library',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
            ],
            options={
                'verbose_name_plural': 'Libraries',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MultiplexingType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PrepData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('raw_value', models.CharField(max_length=255, null=True, blank=True)),
                ('unit', models.CharField(max_length=255, null=True, blank=True)),
                ('file', models.FileField(max_length=255, null=True, upload_to=libprep.models.get_prepdata_upload_path, blank=True)),
                ('image', models.FileField(max_length=255, null=True, upload_to=libprep.models.get_prepdata_upload_path, blank=True)),
                ('attempt', models.IntegerField(default=b'1')),
            ],
            options={
                'ordering': ['prepdatadef__index'],
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PrepDataDef',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('label', models.CharField(max_length=255)),
                ('type', models.CharField(max_length=255, choices=[(b'FLOAT', b'Float'), (b'INT', b'Integer'), (b'IMAGE', b'Image'), (b'FILE', b'File'), (b'STR', b'String (text)'), (b'DATE', b'Date'), (b'TIME', b'Time'), (b'BOOL', b'True/False')])),
                ('required', models.BooleanField(default=False)),
                ('units', models.TextField(max_length=255, null=True, blank=True)),
                ('default', models.CharField(max_length=255, null=True, blank=True)),
                ('index', models.PositiveSmallIntegerField(default=10)),
                ('per_sample', models.BooleanField(default=True)),
                ('slug', django_extensions.db.fields.AutoSlugField(editable=False, populate_from=b'label', blank=True, overwrite=True)),
            ],
            options={
                'ordering': ['prepstep', 'index'],
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PrepHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time', models.DateTimeField(auto_now_add=True)),
                ('notes', models.TextField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PrepStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
            ],
            options={
                'verbose_name_plural': 'Prep Status',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PrepStep',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('index', models.PositiveSmallIntegerField(default=10)),
                ('formulae', models.CharField(max_length=512, null=True, blank=True)),
                ('slug', django_extensions.db.fields.AutoSlugField(editable=False, populate_from=b'name', blank=True, overwrite=True)),
            ],
            options={
                'ordering': ['preptypeversion', 'index'],
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PrepType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
                ('submission_guidelines', models.TextField(help_text=b'')),
                ('is_active', models.BooleanField(default=True, help_text=b'')),
                ('basecode', models.ForeignKey(blank=True, to='billing.BaseCode', null=True)),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PrepTypeVersion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('version', models.PositiveSmallIntegerField()),
                ('is_active', models.BooleanField(default=False, help_text=b'')),
                ('preptype', models.ForeignKey(to='libprep.PrepType')),
            ],
            options={
                'ordering': ['preptype__name', '-version'],
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', django_extensions.db.fields.AutoSlugField(editable=False, populate_from=b'make_slug_field', blank=True, overwrite=True)),
                ('status', models.CharField(default=b'qc', max_length=255, choices=[(b'qc', b'QC'), (b'qc complete', b'QC Complete'), (b'hold', b'Hold'), (b'prepping', b'Prepping'), (b'failed', b'Failed'), (b'prep completed', b'Prep Completed')])),
                ('note', models.CharField(max_length=2000, null=True, blank=True)),
                ('assigned_to', models.ForeignKey(related_name='qc_owner', blank=True, to='base.Employee', null=True)),
                ('prep_owner', models.ForeignKey(related_name='prep_owner', blank=True, to='base.Employee', null=True)),
                ('preptypeversion', models.ForeignKey(to='libprep.PrepTypeVersion')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProjectBill',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('total_charge', models.FloatField(null=True, blank=True)),
                ('recharge', models.FloatField(null=True, blank=True)),
                ('stockroom', models.FloatField(null=True, blank=True)),
                ('fund93', models.FloatField(null=True, blank=True)),
                ('scc_subsidy', models.FloatField(null=True, blank=True)),
                ('icts_subsidy', models.FloatField(null=True, blank=True)),
                ('basecode', models.ForeignKey(blank=True, to='billing.BaseCode', null=True)),
                ('project', models.ForeignKey(to='libprep.Project')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProjectBillMultiplier',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('multiplier', models.ForeignKey(blank=True, to='billing.MultiplierType', null=True)),
                ('projectBill', models.ForeignKey(to='libprep.ProjectBill')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProjectSample',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('project', models.ForeignKey(to='libprep.Project')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SamplePrepDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
                ('dts_preptype', models.CharField(max_length=255, null=True, blank=True)),
                ('complete', models.BooleanField(default=False, help_text=b'')),
                ('complete_date', models.DateField(help_text=b'', null=True, blank=True)),
                ('multiplex_type', models.CharField(blank=True, max_length=10, null=True, choices=[(b'index', b'Index'), (b'dual index', b'Dual Index'), (b'barcoded', b'Barcode')])),
                ('index_length', models.IntegerField(default=0, null=True, blank=True)),
                ('assigned_to', models.ForeignKey(blank=True, to='base.Employee', null=True)),
                ('index', models.ManyToManyField(to='libprep.Index')),
                ('preptype', models.ForeignKey(blank=True, to='libprep.PrepType', help_text=b'', null=True)),
                ('sample', models.OneToOneField(related_name='libprep', to='sample.Sample', help_text=b'')),
                ('status', models.ForeignKey(default=1, to='libprep.PrepStatus', help_text=b'')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SequencingSample',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
                ('loading_concentration', models.CharField(max_length=255, null=True, verbose_name=b'Loading Concentration (pM)', blank=True)),
                ('submitted_concentration', models.CharField(max_length=255, null=True, verbose_name=b'Submitted Concentration (nM)', blank=True)),
                ('primer', models.CharField(help_text=b'', max_length=255, null=True, blank=True)),
                ('prepped', models.DateField(help_text=b'', null=True, blank=True)),
                ('barcoded', models.BooleanField(help_text=b'')),
                ('indexed', models.BooleanField(help_text=b'')),
                ('client_prepped', models.BooleanField(default=False, help_text=b'')),
                ('multiplex_type', models.CharField(blank=True, max_length=10, null=True, choices=[(b'index', b'Index'), (b'dual index', b'Dual Index'), (b'barcoded', b'Barcode')])),
                ('submitted_to_sequencing', models.DateField(db_index=True, null=True, blank=True)),
                ('concentration', models.FloatField(null=True, verbose_name=b'Sample Concentration (ng/ul)', blank=True)),
                ('size', models.IntegerField(null=True, verbose_name=b'Average Size (bp)', blank=True)),
                ('upload_trace', models.FileField(upload_to=libprep.models.get_upload_trace_path, null=True, verbose_name=b'Upload Trace', blank=True)),
                ('qpcr_molarity', models.FloatField(null=True, verbose_name=b'QPCR Molarity', blank=True)),
                ('qubit_molarity', models.FloatField(null=True, verbose_name=b'Qubit Molarity', blank=True)),
                ('bioa_molarity', models.FloatField(null=True, verbose_name=b'BioA Molarity', blank=True)),
            ],
            options={
                'ordering': ('-created',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SequencingSampleDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('percent_of_pool', models.FloatField(null=True, blank=True)),
                ('barcode', models.ForeignKey(blank=True, to='libprep.Barcode', help_text=b'', null=True)),
                ('library', models.ForeignKey(blank=True, to='libprep.Library', help_text=b'', null=True)),
                ('sampleprepdetail', models.ForeignKey(help_text=b'', to='libprep.SamplePrepDetail')),
                ('sequencingsample', models.ForeignKey(help_text=b'', to='libprep.SequencingSample')),
            ],
            options={
                'ordering': ['sampleprepdetail__sample__specimen__gtac_id'],
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='sequencingsample',
            name='sampleprepdetails',
            field=models.ManyToManyField(help_text=b'', related_name='sequencingsamples', through='libprep.SequencingSampleDetail', to='libprep.SamplePrepDetail'),
            preserve_default=True,
        ),
        migrations.AlterOrderWithRespectTo(
            name='sampleprepdetail',
            order_with_respect_to='sample',
        ),
        migrations.AddField(
            model_name='projectsample',
            name='sampleprepdetail',
            field=models.ForeignKey(to='libprep.SamplePrepDetail'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='project',
            name='sampleprepdetail',
            field=models.ManyToManyField(help_text=b'', related_name='projects', through='libprep.ProjectSample', to='libprep.SamplePrepDetail'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='prepstep',
            name='preptypeversion',
            field=models.ForeignKey(related_name='steps', to='libprep.PrepTypeVersion'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='prephistory',
            name='sampleprepdetail',
            field=models.ForeignKey(to='libprep.SamplePrepDetail'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='prephistory',
            name='status',
            field=models.ForeignKey(to='libprep.PrepStatus'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='prepdatadef',
            name='prepstep',
            field=models.ForeignKey(to='libprep.PrepStep'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='prepdata',
            name='prepdatadef',
            field=models.ForeignKey(to='libprep.PrepDataDef'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='prepdata',
            name='project',
            field=models.ForeignKey(related_name='prepdata', to='libprep.Project'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='prepdata',
            name='sampleprepdetail',
            field=models.ForeignKey(related_name='prepdata', to='libprep.SamplePrepDetail', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='library',
            name='sampleprepdetail',
            field=models.ForeignKey(help_text=b'', to='libprep.SamplePrepDetail'),
            preserve_default=True,
        ),
    ]
