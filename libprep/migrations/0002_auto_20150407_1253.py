# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('libprep', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sequencingsample',
            name='barcoded',
        ),
        migrations.RemoveField(
            model_name='sequencingsample',
            name='indexed',
        ),
    ]
