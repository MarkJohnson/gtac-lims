from django.contrib import admin
from core.admin import CoreModelAdmin
import models

class LabAddressInline(admin.TabularInline):
    model = models.LabAddress
    extra = 0

#class CoreModelAdmin(admin.ModelAdmin):
#    # Need to move 'notes' to the bottom of the default form
#    def get_fieldsets(self, *args, **kwargs):
#        res = super(CoreModelAdmin, self).get_fieldsets(*args, **kwargs)
#        if self.declared_fieldsets:
#            return res
#        res.append(('Advanced options', {
#            'classes': ('collapse',),
#            'fields': ('notes', 'created', 'modified', 'key')
#        }))
#            
#        fields = res[0][1]['fields']
#        for x in ('notes', 'created', 'modified', 'key'):
#            if x in fields:
#                fields.remove(x)
#        return res
#    readonly_fields = ('created', 'modified', 'key')

class EmployeeAdmin(CoreModelAdmin):
    list_display = ('__unicode__',)
    # fieldsets = []
    raw_id_fields = ('person' ,)
    # list_filter = ['lab',]
    # search_fields = ['phone']
    # filter_horizontal = []
    # inlines = [ ModelInline ]
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.Employee, EmployeeAdmin)

class DepartmentAdmin(CoreModelAdmin):
    pass
admin.site.register(models.Department, DepartmentAdmin)

class ClientAdmin(CoreModelAdmin):
    #def phone(self, obj):
        #if obj.person.phone_numbers.exists():
            #return obj.person.phone_numbers.all()[0]
    #def email(self, obj):
        #if obj.person.email_addresses.exists():
            #return obj.person.email_addresses.all()[0].email
    list_display = ('__unicode__', 'lab', 'created')
    # fieldsets = []
    list_filter = ['lab',]
    #search_fields = ['phone']
    # filter_horizontal = []
    # inlines = [ ModelInline ]
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.Client, ClientAdmin)

class LabAdmin(CoreModelAdmin):
    list_display = (
        '__unicode__', 'location', 'campus_box', 'primary_contact', 'created')
    # fieldsets = []
    # list_filter = []
    # search_fields = []
    # filter_horizontal = []
    inlines = [ LabAddressInline ]
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.Lab, LabAdmin)
