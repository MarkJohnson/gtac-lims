from django import forms
from django.forms.models import inlineformset_factory, BaseInlineFormSet
from django.forms.models import modelform_factory
from base import models as basemodels 
from core import models as coremodels 
from django.contrib.admin.widgets import AdminFileWidget
from django.contrib.auth.models import User
import localflavor.us.forms as usforms 
from django.core import validators


class Simple(forms.Form):
    email = forms.EmailField(label="Your email address")


class ClientAddForm(forms.ModelForm):
    first_name = forms.CharField(label='First Name', error_messages={
                    "required": "Please enter the first name",
                })
    last_name = forms.CharField(label='Last Name', error_messages={
                    "required": "Please enter the last name",
                })
    email = forms.EmailField(error_messages={
                    "required": "Please enter the email address",
                })
    phone = usforms.USPhoneNumberField(error_messages={
                    "required": "Please enter the phone number",
                    "invalid": "Phone numbers must be in (CCC) XXX-XXX-XXXX format.",
                },
                help_text="(CCC) XXX-XXX-XXXX", widget=forms.TextInput(attrs={'maxlength': '10', 'size': '13'}))
    extension = forms.IntegerField(label="Country Code", required=False, initial="1", widget=forms.TextInput(attrs={'maxlength': '3', 'size': '3'}))
    lab = forms.ModelChoiceField(basemodels.Lab.objects.all(), empty_label="New Lab", required=False)

    is_client_pi = forms.BooleanField(label="Is Client PI?", required=False)

    class Meta:
        model = coremodels.Person
        exclude = ('email', 'gender', 'user')

    def clean(self):
	cd = self.cleaned_data
	if 'phone' in cd and cd['phone'] and not cd['extension']:
	    self._errors["phone"] = self.error_class(['Phone numbers must be in (CCC) XXX-XXX-XXXX format.'])
	return cd
	

class LabAddForm(forms.ModelForm):
    department = forms.IntegerField(label="Department Number", required=False)
    last_name = forms.CharField(label='PI Last Name',
                error_messages={"required": "Please enter the PI last name ",},
                help_text="""
For labs headed by a PI: "FIRSTNAME LASTNAME Lab"

For other "labs" (centers, businesses): use the official name.
    """)
    first_name = forms.CharField(label='PI First Name', required=False)
    email = forms.EmailField(label="PI Email", error_messages={
                    "required": "Please enter the email address",
                })
    location = forms.ChoiceField(choices=(('','---------'),('internal', "Internal"), ('external', 'External')),
                        error_messages={"required": "Please select the lab's location.",},)
    
    class Meta:
        model = basemodels.Lab
        exclude = ('primary_contact', 'department')

    def save(self, *args, **kwargs):
        lab = super(LabAddForm, self).save(*args, commit=False, **kwargs)
        if self.cleaned_data['location'] == 'internal':
            lab.department = basemodels.Department.objects.get_or_create(number=self.cleaned_data['department'])[0]
        lab.save()
        return lab

    def clean(self):
        cd = self.cleaned_data
        if 'last_name' in cd and (basemodels.Lab.objects.filter(last_name=cd['last_name'], first_name=cd['first_name']).count() > 0):
            raise forms.ValidationError("PI Name is already in use'")
         
        if 'location' in cd and cd['location'] == 'internal':
            for x in ('department', 'campus_box'):
                if x in cd and not cd[x]:
                    self._errors[x] = self.error_class(['Internal labs must specify a %s.' % x,])
        if 'location' in cd and cd['location'] == 'external':
            if 'institution' in cd and not cd['institution']:
                self._errors['institution'] = self.error_class(['External labs must specify the institution.'])
        return cd

class BaseLabAddressFormSet(BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        super(BaseLabAddressFormSet, self).__init__(*args, **kwargs)
        self.forms[0].empty_permitted = False

LabAddressFormset = inlineformset_factory(basemodels.Lab, basemodels.LabAddress, extra=1, max_num=2, formset=BaseLabAddressFormSet, fields='__all__')
