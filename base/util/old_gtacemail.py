from django.template.loader import render_to_string
#from settings import ADMINS
from django.core.mail import EmailMultiAlternatives
from email.mime.image import MIMEImage
from django.contrib.sites.models import Site

def send_email(to_addrs, from_addr, subject, txt, html=None, html_template=None, text_template_name=None, imgs=None, context=None):
    if imgs is None: imgs = ()
    
    #subject, from_addr, to = 'GTAC Email Verification', ADMINS[0][1], client.user.email
    #if html:
    #    html = render_to_string(html_template, context)
    text = render_to_string(text_template, context)
    text = render_to_string(text_template, context)

    msg = EmailMultiAlternatives(subject, text, from_addr, to_addrs)
    msg.attach_alternative(html, "text/html")

    for img in imgs:
        i = MIMEImage(open(img).read())
        i.add_header('Content-ID', '<image0>')
        msg.attach(i)

    msg.send()


