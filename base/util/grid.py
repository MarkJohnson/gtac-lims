from xml.etree.ElementTree import Element,SubElement

def xmlbase(url='#', rownum=None):    
    roots = Element('roots')
    config = SubElement(roots, 'grid')
    rows = SubElement(roots, 'rows')
    #SubElement(config, 'pager').text = '#pager1'
    if rownum:
        SubElement(config, 'rowNum').text = str(rownum)

    SubElement(config, 'url').text = url
    SubElement(config, 'datatype').text = 'xml'
    SubElement(config, 'loadtext').text = 'Loading...'
    SubElement(config, 'loadui').text = 'enabled'
    SubElement(config, 'recordtext').text = 'Row(s)'
    SubElement(config, 'shrinkToFit').text = 'true'
    SubElement(config, 'mtype').text = 'GET'
    SubElement(config, 'altRows').text = 'true'
    SubElement(config, 'toolbar').text = 'false'
    SubElement(config, 'autowidth').text = 'true'    
    #SubElement(config, 'hiddengrid').text = 'true'
    #SubElement(config, 'caption').text = 'foo'
    


    r = SubElement(config, 'xmlReader')
    SubElement(r, 'root').text = 'rows'
    SubElement(r, 'row').text = 'row'
    SubElement(r, 'page').text = 'rows>page'
    SubElement(r, 'total').text = 'rows>total'
    SubElement(r, 'records').text = 'rows>records'
    #SubElement(r, 'cell').text = 'cell'
    SubElement(r, 'repeatitems').text = 'false'
    SubElement(r, 'id').text = '[id]'
    #SubElement(r, 'userdata').text = 'userdata'
    
    
    return roots, config, rows
    SubElement(r, 'page').text = '1'
    SubElement(r, 'total').text = '1'
    SubElement(r, 'records').text = '1'
    row = SubElement(r, 'row', {'id':'1'})
    SubElement(row, 'cell').text = '3'
    SubElement(row, 'cell').text = 'wooHoo'

def inst_colmodels(inst, config, fields=None):
    if fields is None:
        fields = inst.__dict__.keys()
    #d = inst.__dict__
    for f in fields:
        if f == 'id': continue
        SubElement(config, 'colNames').text = f
        
        c = SubElement(config, 'colModel')
        SubElement(c, 'name').text = f
        SubElement(c, 'index').text = f
        SubElement(c, 'width').text = '155'
        SubElement(c, 'resizeable').text = 'true'
    

def qs_rows(qs, rows, page, fields=None):
    for i,d in enumerate(qs.values()):
        r = SubElement(rows, 'row', {'id': str(d['id'])})
        for k in d:
            if fields is None or k in fields:
                SubElement(r, k).text = str(d[k])
    SubElement(rows, 'page').text = page
    SubElement(rows, 'total').text = page
    SubElement(rows, 'records').text = str(i+1)

