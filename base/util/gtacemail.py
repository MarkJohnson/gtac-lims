from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from sts import settings
import os
from email.mime.image import MIMEImage
from django.template import Context, Template
from django.contrib.sites.models import Site

HTML_EMAIL_WRAPPER="""{% extends "gtac/emailbase.html" %}

{% block title %}{{ title }}{% endblock title %}
{% block bd %}
{% load markup %}
{% filter markdown %}
{{ body }}
{% endfilter %}
{% endblock bd %}
"""


class GTACEmail(EmailMultiAlternatives):
    """Just like EmailMultiAlternatives except:
    
    It also excepts 'markup' and 'title' kwargs.  Markup is a boolean
    telling us whether we should make an HTML email out of the text 
    (using markdown).  title will be used in the html email in the header.
    If not included, then we'll use the subject as title.
    """
    subject_prefix="[GTAC] "
    
    def __init__(self, *args, **kwargs):
        self.markup = kwargs.pop('markup', True)
        self.title = kwargs.pop('title', None)
        super(GTACEmail, self).__init__(*args, **kwargs)
    
    def send(self, *args, **kwargs):
        if self.title is None:
            self.title = self.subject
        self.subject = self.subject_prefix + self.subject
        if self.markup: # Then we're doing an HTML Email
            c = Context({'body': self.body, 'title': self.title})
            html = Template(HTML_EMAIL_WRAPPER)
            self.attach_alternative(html.render(c), "text/html")
            #img = MIMEImage(open(os.path.join(settings.MEDIA_ROOT, 'img/gtac-white.png')).read(), 'png')
            #img.add_header('Content-Id', '<emaillogo>')
            #self.attach(img)

        return super(GTACEmail, self).send(*args, **kwargs)

