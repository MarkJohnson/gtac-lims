import subprocess
import settings
from xml.etree import ElementTree

def call_dot(type='png'):
    if not hasattr(settings, 'GRAPHVIZ_DOT_COMMAND'):
        # At least provide a helpful exception message
        raise Exception("GRAPHVIZ_DOT_COMMAND constant not set in settings.py"\
                " (to specify the absolute path to graphviz's dot command)")
    # Lots of "pipe" work to avoid hitting the file-system
    proc = subprocess.Popen('%s -T%s' % (settings.GRAPHVIZ_DOT_COMMAND, type),
                shell=True,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                )
    return proc

def get_imap(dotfile):
    proc = call_dot(type='imap')
    return proc.communicate(dotfile.encode('utf_8'))[0]
    #response = HttpResponse(mimetype='text/html')
    #response.write(proc.communicate(dotfile.encode('utf_8'))[0])
    #return response

def get_cmapx(dotfile):
    proc = call_dot(type='cmapx')
    cmap = proc.communicate(dotfile.encode('utf_8'))[0]
    t = ElementTree.fromstring(cmap)
    for e in t.findall('area'):
        e.set('coords', e.get('coords').replace(' ', ','))
    return ElementTree.tostring(t)

def get_png(dotfile):
    proc = call_dot(type='png')
    return proc.communicate(dotfile.encode('utf_8'))[0]
    #response = HttpResponse(mimetype='image/png')
    #response.write(proc.communicate(dotfile.encode('utf_8'))[0])
    #return response
