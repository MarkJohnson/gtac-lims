from django.db import models
from django_extensions.db.fields import AutoSlugField
from core import models as coremodels
from localflavor.us.models import USStateField

class EmailImage(models.Model):
    name = models.SlugField(help_text='')
    image = models.ImageField(upload_to='emailimages/', help_text='')
    
    def __unicode__(self):
        return u'%s' % (self.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('emailimage-view', [str(self.id)])
        
class Department(coremodels.CoreModel):
    number = models.IntegerField(db_column='DEPT_NUMBER')
    name = models.CharField(max_length=255, blank=True, null=True, help_text='')
    #slug = AutoSlugField(populate_from=('name', 'number'), overwrite=True)
    
    def __unicode__(self):
        return u'%d' % (self.number,)
    
    @models.permalink
    def get_absolute_url(self):
        return ('department-view', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        ordering = ['name', ]
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass


class LabAddress(coremodels.CoreModel):
    ADDRESS_TYPES = (
        ('billing', 'Billing'),
        ('shipping', 'Shipping'),
        ('mailing ', 'Mailing '),
    )
    lab = models.ForeignKey("Lab", related_name='addresses')
    street = models.CharField(max_length=255, help_text='')
    city = models.CharField(max_length=255, default="St. Louis", help_text='')
    state = USStateField(default="MO", help_text='')
    zipcode = models.CharField(max_length=20, help_text='')
    type = models.CharField(max_length=255, choices=ADDRESS_TYPES, default="billing", help_text='')
    primary = models.BooleanField(default=False, help_text='')
    publish = models.BooleanField(default=True, help_text='')

    def __unicode__(self):
        return u'%s %s, %s, %s' % (self.street, self.city, self.state, self.zipcode)
        
    class Meta(coremodels.CoreModel.Meta):
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        verbose_name_plural = "Addresses"

#LabAddress._meta.get_field('type')._choices = (
#        ('billing', 'Billing'), ('mailing', 'Mailing')
#        )


class Lab(coremodels.CoreModel):
    last_name = models.CharField(max_length=255 )
    first_name = models.CharField(max_length=255, blank=True, null=True )
    shortname = AutoSlugField(populate_from=('last_name', 'first_name'), unique=True)
    location = models.CharField(max_length=10, choices=(('internal', "Internal"), ('external', 'External')))
    institution = models.CharField(max_length=255, blank=True, null=True )
    campus_box = models.PositiveIntegerField(blank=True, null=True)
    department = models.ForeignKey(Department, blank=True, null=True)
    primary_contact = models.ForeignKey("Client", blank=True, null=True, related_name="lab_primary_set")
    
    def __unicode__(self):
        if (self.first_name is None):
            return u'%s' % str(self.last_name)
        else:
            return u'%s' % str(self.first_name +' ' + self.last_name)

    @models.permalink
    def get_absolute_url(self):
        return ('lab-detail', [str(self.id)])
        
    class Meta(coremodels.CoreModel.Meta):
        # get_latest_by = ""
        ordering = ['last_name', 'first_name']
        unique_together = ('last_name', 'first_name')
        # verbose_name_plural = ""
        pass

    def save(self, *args, **kwargs):
        if self.location == 'internal':
            for x in (self.campus_box, self.department):
                if not x: 
                    raise Exception("For internal labs, you must specify a campus_box and department")
        elif self.location == 'external':
            #for x in (self.city,self.state,self.zipcode,self.street):
                #if not x:
                    #raise ValidationError("For external labs, you must specify all address information.")
            pass

        super(Lab, self).save(*args, **kwargs)


class ActiveClientManager(models.Manager):
    def get_queryset(self):
        return super(ActiveClientManager, self).get_query_set().filter(person__user__is_active=True)

class Client(coremodels.PersonType):
    lab = models.ForeignKey(Lab, blank=True, null=True, help_text='')

    objects = models.Manager()
    active = ActiveClientManager()
        
    @models.permalink
    def get_absolute_url(self):
        return ('client-detail', [str(self.id)])

    def __unicode__(self):
        return u'%s [%s]' % (self.name, self.lab)
 
    class Meta(coremodels.PersonType.Meta):
        pass

class Employee(coremodels.PersonType):

    @models.permalink
    def get_absolute_url(self):
        return ('employee-detail', [str(self.id)])
    
