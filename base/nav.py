from lastnav.util import sitenav, SingleViewPageFactory, ModelPageFactory, FlatpageFactory
from base import models
from django.contrib.flatpages.models import FlatPage
#sitenav.register(SingleViewPageFactory, 'run-detail')
sitenav.register(SingleViewPageFactory, 'client-list')
sitenav.register(ModelPageFactory, 'client-detail', models.Client, models.Client.objects.all()[:5])

sitenav.register(SingleViewPageFactory, 'turnaround')

for fp in FlatPage.objects.all():
    sitenav.register(FlatpageFactory, fp.url)
