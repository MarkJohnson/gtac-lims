from django.conf.urls import *


urlpatterns = patterns('',
    url(r'^json/$', 'base.views.client_json', name='client-json'),
    url(r'^add/$', 'base.views.client_add', name='client-add' ),
    url(r'^browse/$', 'base.views.client_browse', name='client-browse' ),
    url(r'^client-list/$', 'base.views.client_list', name='combo-client-list'), 
    url(r'^(?P<id>\d+)/$', 'sts.views.client_detail', name='client-detail'),
    url(r'^$', 'sts.views.client_list', name='client-list'),
    )
