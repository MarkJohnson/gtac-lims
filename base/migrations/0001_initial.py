# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import localflavor.us.models
import django.utils.timezone
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
            ],
            options={
                'ordering': ['person__first_name', 'person__last_name'],
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('number', models.IntegerField(db_column=b'DEPT_NUMBER')),
                ('name', models.CharField(help_text=b'', max_length=255, null=True, blank=True)),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EmailImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.SlugField(help_text=b'')),
                ('image', models.ImageField(help_text=b'', upload_to=b'emailimages/')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('person', models.ForeignKey(to='core.Person', help_text=b'', unique=True)),
            ],
            options={
                'ordering': ['person__first_name', 'person__last_name'],
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Lab',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('last_name', models.CharField(max_length=255)),
                ('first_name', models.CharField(max_length=255, null=True, blank=True)),
                ('shortname', django_extensions.db.fields.AutoSlugField(editable=False, populate_from=(b'last_name', b'first_name'), blank=True, unique=True)),
                ('location', models.CharField(max_length=10, choices=[(b'internal', b'Internal'), (b'external', b'External')])),
                ('institution', models.CharField(max_length=255, null=True, blank=True)),
                ('campus_box', models.PositiveIntegerField(null=True, blank=True)),
                ('department', models.ForeignKey(blank=True, to='base.Department', null=True)),
                ('primary_contact', models.ForeignKey(related_name='lab_primary_set', blank=True, to='base.Client', null=True)),
            ],
            options={
                'ordering': ['last_name', 'first_name'],
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LabAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('street', models.CharField(help_text=b'', max_length=255)),
                ('city', models.CharField(default=b'St. Louis', help_text=b'', max_length=255)),
                ('state', localflavor.us.models.USStateField(default=b'MO', help_text=b'', max_length=2, choices=[(b'AL', b'Alabama'), (b'AK', b'Alaska'), (b'AS', b'American Samoa'), (b'AZ', b'Arizona'), (b'AR', b'Arkansas'), (b'AA', b'Armed Forces Americas'), (b'AE', b'Armed Forces Europe'), (b'AP', b'Armed Forces Pacific'), (b'CA', b'California'), (b'CO', b'Colorado'), (b'CT', b'Connecticut'), (b'DE', b'Delaware'), (b'DC', b'District of Columbia'), (b'FL', b'Florida'), (b'GA', b'Georgia'), (b'GU', b'Guam'), (b'HI', b'Hawaii'), (b'ID', b'Idaho'), (b'IL', b'Illinois'), (b'IN', b'Indiana'), (b'IA', b'Iowa'), (b'KS', b'Kansas'), (b'KY', b'Kentucky'), (b'LA', b'Louisiana'), (b'ME', b'Maine'), (b'MD', b'Maryland'), (b'MA', b'Massachusetts'), (b'MI', b'Michigan'), (b'MN', b'Minnesota'), (b'MS', b'Mississippi'), (b'MO', b'Missouri'), (b'MT', b'Montana'), (b'NE', b'Nebraska'), (b'NV', b'Nevada'), (b'NH', b'New Hampshire'), (b'NJ', b'New Jersey'), (b'NM', b'New Mexico'), (b'NY', b'New York'), (b'NC', b'North Carolina'), (b'ND', b'North Dakota'), (b'MP', b'Northern Mariana Islands'), (b'OH', b'Ohio'), (b'OK', b'Oklahoma'), (b'OR', b'Oregon'), (b'PA', b'Pennsylvania'), (b'PR', b'Puerto Rico'), (b'RI', b'Rhode Island'), (b'SC', b'South Carolina'), (b'SD', b'South Dakota'), (b'TN', b'Tennessee'), (b'TX', b'Texas'), (b'UT', b'Utah'), (b'VT', b'Vermont'), (b'VI', b'Virgin Islands'), (b'VA', b'Virginia'), (b'WA', b'Washington'), (b'WV', b'West Virginia'), (b'WI', b'Wisconsin'), (b'WY', b'Wyoming')])),
                ('zipcode', models.CharField(help_text=b'', max_length=20)),
                ('type', models.CharField(default=b'billing', help_text=b'', max_length=255, choices=[(b'billing', b'Billing'), (b'shipping', b'Shipping'), (b'mailing ', b'Mailing ')])),
                ('primary', models.BooleanField(default=False, help_text=b'')),
                ('publish', models.BooleanField(default=True, help_text=b'')),
                ('lab', models.ForeignKey(related_name='addresses', to='base.Lab')),
            ],
            options={
                'abstract': False,
                'verbose_name_plural': 'Addresses',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='lab',
            unique_together=set([('last_name', 'first_name')]),
        ),
        migrations.AddField(
            model_name='client',
            name='lab',
            field=models.ForeignKey(blank=True, to='base.Lab', help_text=b'', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='client',
            name='person',
            field=models.ForeignKey(to='core.Person', help_text=b'', unique=True),
            preserve_default=True,
        ),
    ]
