#!/usr/bin/env python

import gdata.spreadsheet.service
import gdata.spreadsheet
import gdata.docs.client
from settings import DJROOT
import os
import sys


class GTACData(object):
    def __init__(self, email, password):
        self.client = gdata.docs.client.DocsClient(source='GTAC-LIMS-v1')
        self.client.ssl = True                  # Force all API requests through HTTPS
        self.client.http_client.debug = False   # Set to True for debugging HTTP requests
        self.client.ClientLogin(email, password, self.client.source)

        self.sclient = gdata.spreadsheet.service.SpreadsheetsService(source='GTAC-LIMS-v1')
        self.sclient.ssl = True                 # Force all API requests through HTTPS
        self.sclient.http_client.debug = False  # Set to True for debugging HTTP requests
        self.sclient.ClientLogin(email, password, self.sclient.source)
        
    
    def switch_tokens(self):
        self.docs_token = self.client.auth_token
        self.client.auth_token = gdata.gauth.ClientLoginToken(self.sclient.GetClientLoginToken())

    def get_sampletracking(self):
        print "Exporting Sample Tracking Sheet 1: libprep-1.csv"
        if os.path.exists('/tmp/libprep-1.csv'):
            os.unlink('/tmp/libprep-1.csv')
        #libprep  = self.client.GetDoc('spreadsheet:0Ag75Q0BGB1H1dGJyeXV1LXVGd3lCb3J2T2dxSDdHVXc')
        libprep  = self.client.get_resource_by_id('spreadsheet:0Ag75Q0BGB1H1dGJyeXV1LXVGd3lCb3J2T2dxSDdHVXc')
        self.switch_tokens()
        #self.client.Export(libprep, '/tmp/libprep-1.csv')
        self.client.download_resource(libprep, '/tmp/libprep-1.csv', extra_params={'gid': 0, 'exportFormat': 'csv'})
        return '/tmp/libprep-1.csv'

    def get_sequencesubmission(self):
        print "Exporting Sequence Submission Sheet 1: seqsub-1.csv"
        if os.path.exists('/tmp/seqsub-1.csv'):
            os.unlink('/tmp/seqsub-1.csv')
        seqsub = self.client.get_resource_by_id('spreadsheet:0AuSEYSrsxvAZdGpvVGQyTUNNUWlSYXRmeTBoSUNNdWc')
        self.switch_tokens()
        self.client.download_resource(seqsub,   '/tmp/seqsub-1.csv', extra_params={'gid': 0, 'exportFormat': 'csv'})
        return '/tmp/seqsub-1.csv'

    def get_indexes(self):
        fname = '/tmp/indexes.csv'
        print "Exporting Indexes: indexes.csv"
        if os.path.exists(fname):
            os.unlink(fname)
        doc  = self.client.GetDoc('spreadsheet:0Ap7OCMRUVbRodGhWNmhMV3d0OXZhVDhjTXVwTmN5Mnc')
        self.switch_tokens()
        self.client.Export(doc, fname, gid=1)
        return fname

    def get_analysis(self):
        if os.path.exists('/tmp/analysis-1.csv'):
            os.unlink('/tmp/analysis-1.csv')
        analysis = self.client.GetDoc('spreadsheet:0Ap7OCMRUVbRodGhWNmhMV3d0OXZhVDhjTXVwTmN5Mnc')
        self.switch_tokens()
        print "Exporting Bioinformatics sheet 1: analysis-1.csv"
        self.client.Export(analysis, '/tmp/analysis-1.csv')
        return '/tmp/analysis-1.csv'

    def get_data(self):
        os.mkdir(os.path.join(DJROOT, 'tmp'))
        analysis = self.client.GetDoc('spreadsheet:0Ap7OCMRUVbRodGhWNmhMV3d0OXZhVDhjTXVwTmN5Mnc')
        libprep  = self.client.GetDoc('spreadsheet:0Ag75Q0BGB1H1dGJyeXV1LXVGd3lCb3J2T2dxSDdHVXc')
        seqsub   = self.client.GetDoc('spreadsheet:0AuSEYSrsxvAZdGpvVGQyTUNNUWlSYXRmeTBoSUNNdWc')
        self.switch_tokens()
        print "Exporting Bioinformatics sheet 1: analysis-1.csv"
        self.client.Export(analysis, os.path.join(DJROOT,'tmp','analysis-1.csv'))
        print "Exporting Bioinformatics sheet 2: indexes.csv"
        self.client.Export(analysis, os.path.join(DJROOT,'tmp','indexes.csv'), gid=1)
        print "Exporting Bioinformatics sheet 3: analysis-3.csv"
        self.client.Export(analysis, os.path.join(DJROOT,'tmp','analysis-3.csv'), gid=2)
        print "Exporting Bioinformatics sheet 4: analysis-4.csv"
        self.client.Export(analysis, os.path.join(DJROOT,'tmp','analysis-4.csv'), gid=3)
        print "Exporting Sample Tracking Sheet 1: libprep-1.csv"
        self.client.Export(libprep, os.path.join(DJROOT,'tmp','libprep-1.csv'))
        print "Exporting Sequence Submission Sheet 1: seqsub-1.csv"
        self.client.Export(seqsub, os.path.join(DJROOT,'tmp','seqsub-1.csv'))

        return
                

if __name__ == '__main__':
    import getpass
     
    email = raw_input("Email: ")
    password = getpass.getpass("Password: ")
    pd = GTACData(email, password)
    pd.get_data()

