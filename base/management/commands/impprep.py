#!/usr/bin/env python
"""
Sample Tracking Data import
"""


import os
import sys
from core import models as coremodels
from base import models as basemodels
from sample import models as smodels
from billing import models as billmodels
from seq import models as qmodels
from libprep import models as lpmodels
from analysis import models as anamodels
from solexamgr import models as solmodels
from django.db import transaction
from django.contrib.auth import models as authmodels
from settings import DJROOT
import csv
import re
from cStringIO import StringIO
import datetime
import traceback

admin_user,c = authmodels.User.objects.get_or_create(username='admin')
admin = coremodels.Person.objects.get_or_create(user=admin_user)[0]
status_unknown = lpmodels.PrepStatus.objects.get_or_create(name='Unknown')[0]

def cleandate(s):
    if not s:
        return None
    for fmt in ('%m/%d/%Y', '%m/%d/%y'):
        try:
            return datetime.datetime.strptime(s, fmt)
        except: 
            pass
    raise Exception, "No date format works for %s" % (s)

def cleanbarcode(s):
    s = s.replace(' ', '')
    if "Index" in s:
        s = s.replace('Index', 'Ind')
    try:
        int(s)
        s = 'Ind' + s
    except:
        pass
    #if 'ind' not in s.lower():
    if 'ind' in s:
        s = s.title()
    return s


def submitted_to_sequencing(l):
    if l['Sample name sub, to seq'] is None:
        return False
    if l['Seq. Sub. Date'] == 'PREP ONLY':
        return False
    return True

#FIXME not in use because all seq app stuff goes on in the solexamgr migrate script    
def get_runtype(l):
        if '/' in l['SR/PE']:
            rd,ln = l['SR/PE'].split('/')
        elif l['SR/PE'].strip() in ('SR', 'PE'):
            rd = l['SR/PE'].strip()
            if l['cycles'][:2] in ('SR', 'PE'):
                ln = l['cycles'][2:].strip()
        elif l['SR/PE'].strip()[:2] in ('SR', 'PE'):
            rd = l['SR/PE'][:2]
            ln = l['SR/PE'][2:].strip().strip('/')           
        elif l['cycles'][:2] in ('SR', 'PE'):
            rd = l['cycles'][:2]
            ln = l['cycles'][2:].strip().strip('/')
        else:
            rd,ln = l['SR/PE'].split()
        
        rd = rd.strip()
        ln = ln.strip()
        
        if ln == '75': 
            ln = '76'
        if ln == '36' and rd == 'PE':
            seqmodel = 'GAIIx'
        elif ln =='76' and rd == 'SR':
            seqmodel = 'GAIIx'
        else:
            seqmodel = 'HiSeq'

        return qmodels.RunType.objects.get_or_create(
                    expired=None, 
                    read=rd, 
                    length=ln,
                    sequencer_model=qmodels.SequencerModel.objects.get(name=seqmodel)
		    )[0]

def get_csv(fname):
    csvlines = open(fname).readlines()
    f = StringIO()
    f.write('\n'.join(csvlines[5:]))
    f.seek(0)
    return csv.DictReader(f)

class CGSSample(object):
    pass

class GTACSample(object):
    def __init__(self, fname, import_all=True, gtacid=None, start_id=4357, sample=None, only_complete=True, verbose=False):
        self.gtacid = gtacid
        self.sample = sample
        self.start_id = start_id
        self.only_complete = only_complete
        self.verbose = verbose
        self.level = 0
        self.import_all = import_all
        self.fname = fname


    def log(self, msg, limit=400):
        if self.verbose:
            msg = (msg[:limit]+'..') if len(msg) > limit else msg
            msg = msg.replace('\n', ' ')
            sys.stdout.write("%s%s\n" % (' ' * 4 * self.level, (msg[:limit]+'..') if len(msg) > limit else msg))
        
    
    def error(self, msg):
        sys.stderr.write("%s%s\n" % (' ' * 4 * self.level, msg))

    def get_or_create(self, model, manager='objects', **kwargs):
        obj, c = getattr(model, manager).get_or_create(**kwargs)
        if c:
            if self.verbose: 
                self.log("CREATE: %s(%s)" % (model._meta.object_name, ', '.join(['='.join((str(k),str(v))) for k,v in kwargs.items()])))
        elif self.verbose:
            #self.log("GET: %s(%s)" % (model._meta.object_name, ', '.join(['='.join((str(k),str(v))) for k,v in kwargs.items()])))
            pass
        return obj,c

    def clean_sid(self, sample_id):
        #GTAC_SAMPLE=r'(?P<foo>.*)GTAC[:-]\s*(\w+)(?:$|_(?P<percent>.*))'

        match = re.match('(.*?)_?(\d+)%.*', sample_id)
        if match:
            phix_percent = match.groups()[1]
            self._name = match.groups()[0]
        else:
            phix_percent = 0
            self._name = sample_id

        match = re.match('.*GTAC[:-]\s*(\w+)(?:$|\s*_.*)', sample_id, re.I)
        if match:
            self._is_gtac = True
            self._name = match.groups()[0]
        else:
            self._is_gtac = False
        log("clean_sid CLEAN: %s -> " % (self.l.sample_id) + str((self._is_gtac ,self._name, phix_percent)))
        return self._is_gtac, self._name,phix_percent


    @transaction.commit_on_success
    def migrate(self):
        samples = []
        errors = []
        for x,l in enumerate(get_csv(self.fname)):
          try:
            self.level = 0
            if self.only_complete:
                if l['Status'].lower() != 'complete': continue
            
            if not self.import_all:
                if self.sample:
                    if l['Sample name sub, to seq'] != self.sample: 
                        continue
                    else:
                        self.log("Found %s" % self.sample)
            
                elif self.gtacid:
                    if l['GTAC Prep'].lower() == self.gtacid.lower():
                        self.log("Found GTAC ID: %s" % self.gtacid)
                    else:
                        continue
                else:
                    self.log("Nothing to do")
                    break
            
            #
            #  Only import rows that are >= 4357
            #
            try:
                if '-' in l['GTAC Prep']:
                    if int(l['GTAC Prep'].split('-')[0])  <  self.start_id:
                        continue
                    else:
                        print "Cannot yet import GTAC IDs with a dash: %s" % l['GTAC Prep']

                else:
                    if int(l['GTAC Prep']) < self.start_id:
                        continue
            except ValueError:
                print "Can't handle GTAC ID: %s" % l['GTAC Prep']
                continue
            if cleandate(l['Date submitted']).date() < datetime.date(2012,1,1):
                continue
            
            #if lpmodels.SamplePrepDetail.objects.filter(name=l['GTAC Prep']).exists():
                #self.log("%s already exists in DB" % l['GTAC Prep'])
                #continue
                
            self.level += 1
            #
            #  Lab
            #
            if l['Dept #']:
                department,c = self.get_or_create(basemodels.Department, number=l['Dept #'], defaults={ 'name': l['Department'] })
                location = 'internal'
            else:
                department = None
                location = 'external'
            lab,c = self.get_or_create(basemodels.Lab, name=l['PI/Lab'], location=location, defaults={'campus_box': l['Box #'], 'department': department })

            #
            #  Client
            #
            fn = l['Name'].split()[0].strip()
            if len(l['Name'].split()) < 2:
                ln = fn
            else:
                ln = l['Name'].split(None,2)[1].strip()
                
            person,c = self.get_or_create(coremodels.Person, first_name=fn, last_name=ln)
            self.level += 1
            client,c = self.get_or_create(basemodels.Client, person=person, defaults={'lab': lab})
            self.level += 1
            self.get_or_create(coremodels.PhoneNumber, person=client.person, number=l['Phone'])
            self.get_or_create(coremodels.EmailAddress, person=client.person, email=l['Email'])
            self.level -= 1
            self.level -= 1


            #
            #  Project
            #
            project,c = self.get_or_create(smodels.Project,
                name=l['Date submitted'],
                client=client,
                #defaults=dict(submitted=cleandate(l['Date submitted'])),
                )
                
            #
            #  Submission
            #
            self.level += 1
            sub,c = self.get_or_create(smodels.Submission,
                project=project,
                client=client,
                submitted=cleandate(l['Date submitted']),
                )

            #
            #  Organism (Species)
            #
            sname = l['Species'].strip()
            if not sname:
                sname = 'Unknown'
            try:
                org = smodels.Species.objects.get(name__iexact=sname)
            except smodels.Species.DoesNotExist:
                org = smodels.Species.objects.create(name=sname)

            #
            #  Sample
            #
            if not l['QC Conc.ng/ul']:
                print "Warning: no submitted concentration value for GTAC ID: %s" % (l['GTAC Prep'])
            if not l['submitted volume']:
                print "Warning: no submitted volume value for GTAC ID: %s" % (l['GTAC Prep'])

            s, c = self.get_or_create(smodels.Specimen,
                name=l['Sub.Sample name'],
                organism=org,
                submitted_concentration=l['QC Conc.ng/ul'] or None,
                submitted_volume=l['submitted volume'] or None,
                #notes="Spreadsheet Row: %d\n%s" % (x, l['Notes']),
                defaults={'submitted_from': sub},
                )
            note = "Sample: %s, Spreadsheet Row: %d" % (s.name, x)
            if l['Notes']:
                note += ", Note: %s" % (l['Notes'])

            sub.notes.create(note=note, author=admin)
            
            #
            #  Sample
            #
            ssub,c = self.get_or_create(smodels.Sample,
                specimen=s,
                submission=sub,
                #notes="GTAC Prep ID: %s\nSpreadsheet Row: %d\n" % (l['GTAC Prep'],x),
                )
            note = "GTAC Prep ID: %s\nSpreadsheet Row: %d\n" % (l['GTAC Prep'],x),
            sub.notes.create(note=note, author=admin)
            self.level -= 1
            
            #
            #  LIBPREP MODELS
            #


            #
            # PrepType
            #
            pt, c = self.get_or_create(lpmodels.PrepType, name=l['Prep type'])

            #
            #  SamplePrepDetail
            #

            spd, c = self.get_or_create(lpmodels.SamplePrepDetail,
                name=l['GTAC Prep'],
                sample=ssub,
                preptype=pt,
                complete_date=cleandate(l['Date Prep complete']),
                complete=bool(l['Date Prep complete']),
                status=status_unknown,
                )

            # If not submitted for sequencing, we're done
            if not submitted_to_sequencing(l):
                if self.verbose: print "DONE Processing: Not submitted to sequencing."
                continue
            
            #
            # SampleBillingDetails
            #
            if department:
                dep_num = department.number
            else:
                dep_num = None
            
            if l['CDI y/n'].strip().lower() == 'y':
                association = billmodels.SampleAssociation.objects.get_or_create(name='CDI')[0]
            else:
                association = None

            sbd, c = self.get_or_create(billmodels.SampleBillingDetail,
                sample=ssub,
                billed_lab=lab,
                fund=l['Fund #'] or None,
                department=dep_num,
                association=association,
                )

            #
            # SampleAnalysisDetails
            #
            # FIXME: we're ignoring the Align To in the google doc... which we're deciding is OK for now.
            #
            anadetail = self.get_or_create(anamodels.SampleAnalysisDetail,
                sample=ssub,
                )[0]

            if l['Send Data to'].strip():
                names = [name.strip() for name in l['Send Data to'].split(',')]
                emails = [email.strip() for email in l['Send Data to Email'].split(',')]
                for name,email in zip(names,emails): 
                    print name
                    fn,ln = name.split(None, 2)
                    person,c = self.get_or_create(coremodels.Person, first_name=fn, last_name=ln)
                    client,c = self.get_or_create(basemodels.Client, person=person, defaults={'lab': lab})
                    self.get_or_create(coremodels.EmailAddress, person=client.person, email=email)
                    anadetail.analysis_clients.add(client)

            


            #
            # SequencingSample
            #
            #FIXME: need to get loading conc / primer from the "Sequence submission form" google doc

            if l['pM loaded'] and l['pM loaded'].lower().endswith('pm'):
                loaded = l['pM loaded'][:-2]
            else:
                loaded = l['pM loaded'] or None
                
            seqs,c  = self.get_or_create(lpmodels.SequencingSample,
                name=l['Sample name sub, to seq'], # or l['Sub.Sample name'], # let's take this out and see how it goes.
                prepped=cleandate(l['Date Prep complete']),
                indexed=l['Indexed'].lower() == 'index',
                barcoded=l['Indexed'].lower() == 'barcoded',
                defaults=dict(
                    primer=l['seq primers'] or None,
                    loading_concentration=loaded, #l['final library  Conc. ng/ul'] or None,
                    #submitted_concentration=l['Submitted Quantity (ug)'] and l['Submitted Quantity (ug)'].split()[0] or None,
                ),
                )

            #
            # Connect SamplePrepDetail with SequencingSample
            #
            #if seqs not in spd.sequencingsamples.all():
                #print "Adding SequencingSample to SamplePrepDetail"
                #spd.sequencingsamples.add(seqs)

            #
            # Connect SequencingSample with Samples through SequencingSampleDetail
            #
            self.level += 1
            seqsd,c = self.get_or_create(lpmodels.SequencingSampleDetail,
                sampleprepdetail=spd,
                sequencingsample=seqs,
                percent_of_pool=l['% library prep'] or 0,
                )
            if c: 
                if seqsd.percent_of_pool:
                    if l['Barcodes']:
                        if lpmodels.Index.objects.filter(name__iexact=cleanbarcode(l['Barcodes'])).exists():
                            seqsd.index = lpmodels.Index.objects.get(name__iexact=cleanbarcode(l['Barcodes']))
                            seqsd.save()
                            self.log("Adding INDEX: %s" % seqsd.index)
                        elif lpmodels.Barcode.objects.filter(name__iexact=cleanbarcode(l['Barcodes'])).exists():
                            seqsd.barcode = lpmodels.Barcode.objects.get(name__iexact=cleanbarcode(l['Barcodes']))
                            seqsd.save()
                            self.log("Adding BARCODE: %s" % seqsd.index)
                        else:
                            self.error("NO INDEX FOUND for %s (%s): in %s" % (l['Barcodes'],cleanbarcode(l['Barcodes']), l['GTAC Prep'] ))
                            raise "NO INDEX FOUND."
                    self.log("NO INDEX.")
            self.level -= 1

          except Exception, m:
              errors.append("%s: %s" % (l['GTAC Prep'], m))
              if l['Date submitted']:
                  date = cleandate(l['Date submitted']).date()
                  if date > (datetime.datetime.now().date() - datetime.timedelta(60)) or self.verbose:
                      self.error("Error Importing %s (%s): %s" % (l['GTAC Prep'], str(date), m))
              traceback.print_exc(file=sys.stderr)
              transaction.rollback()
              continue
          else:
              samples.append(ssub)
              transaction.commit()

        return errors,samples
                

class Seq(object):
    #def __init__(self, samplesubmissions=None):
    #    self.samplesubmissions = samplesubmissions
        
    def migrate_via_sequencingsample(self, ss, gtac=True):
        if gtac:
            reg = r'.*GTAC[:-]\s*%s(?:$|_.*)' % re.escape(ss.name)
            for lane in solmodels.Lane.objects.filter(sample_id__iregex=reg).order_by('run__num'):
                print lane
    
    
if __name__ == '__main__':

    with transaction.commit_manually():
        try:
            print basemodels.Client.objects.all()
            libprep(sys.argv[1])
            print basemodels.Client.objects.all()
            
        except Exception, m:
            print "Break"
            transaction.rollback()
            raise
        else:
            transaction.commit()
            #transaction.rollback()
