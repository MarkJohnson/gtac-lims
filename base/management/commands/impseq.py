#!/usr/bin/env python

import csv
import re
from settings import DJROOT
import os
from seq import models as seqmodels
from libprep import models as lpmodels
from sample import models as sampmodels
from base import models as basemodels
from solexamgr import models as solmodels

LOGLEVEL=0
VERBOSE=False

status_unknown = seqmodels.SequencingStatus.objects.get_or_create(name="Unknown")[0]

def log(msg, limit=120, level=0):
    msg = (msg[:limit]+'..') if len(msg) > limit else msg
    msg = msg.replace('\n', ' ')
    #print "%s%s" % (' ' * 4 * LOGLEVEL, (msg[:limit]+'..') if len(msg) > limit else msg)


def get_or_create(modelobject, manager='objects', **kwargs):
    try:
        obj, c = getattr(modelobject, manager).get_or_create(**kwargs)
    except:
        log("ERROR: %s(%s)" % (modelobject._meta.object_name, ', '.join(['='.join((str(k),str(v))) for k,v in kwargs.items()])))
        raise
    if c:
        typ = "CREATE"
    else:
        typ = "GET"
    if VERBOSE: 
        log("%s: %s(%s)" % (typ, modelobject._meta.object_name, ', '.join(['='.join((str(k),str(v))) for k,v in kwargs.items()])))
    return obj,c        

def import_samplesubmission(samplesubmission):
    seqs = samplesubmission.libprep.sequencingsamples.all()
    assert len(seqs) > 0, "Can't find any sequencingsamples for submission, but should if it was imported properly."

    for seqsample in seqs:
        reg = r'.*GTAC[:-]\s*%s(?:$|\s*_.*)' % re.escape(seqsample.name)
        lanes = solmodels.Lane.objects.filter(sample_id__iregex=reg).order_by('run__num')
        assert len(lanes) > 0, "Can't find any solexamgr lanes matching samplesubmission, but should."
        for l in lanes:
            sl = SeqLane(l, seqsample)
            sl.migrate()
            if l.dropoff_date:
                subdate = l.dropoff_date
            else:
                subdate = l.run.sequencing_date    

            ssd,c = get_or_create(seqmodels.SampleSequencingDetail, 
                samplesubmission=samplesubmission, 
                runtype=sl.runtype,
                defaults={'ready_date':subdate},
                status=status_unknown,
                )
            log("ADD: %s.denaturedaliquot.add(%s)" % (ssd._meta.object_name, sl.denaturedaliquot))
            ssd.denaturedaliquot.add(sl.denaturedaliquot)




class SeqLane(object):
    def __init__(self, oldlane, sequencingsample):
        self.l = oldlane
        self._sequencingsample = sequencingsample
        self.multiprepped = [x for x in csv.DictReader(open(os.path.join(DJROOT, 'util', 'multiprepped.csv')))]
    
    def migrate(self):
        print self.flowcell
        print self.lane
        print self.run
        print self.denaturedaliquot
        self.migrate_runpostprocess()


    def clean_sid(self):
        #GTAC_SAMPLE=r'(?P<foo>.*)GTAC[:-]\s*(\w+)(?:$|_(?P<percent>.*))'

        match = re.match('(.*?)_?(\d+)%.*', self.l.sample_id)
        if match:
            phix_percent = match.groups()[1]
            self._name = match.groups()[0]
        else:
            phix_percent = 0
            self._name = self.l.sample_id

        match = re.match('.*GTAC[:-]\s*(\w+)(?:$|\s*_.*)', self.l.sample_id, re.I)
        if match:
            self._is_gtac = True
            self._name = match.groups()[0]
        else:
            self._is_gtac = False
        log("clean_sid CLEAN: %s -> " % (self.l.sample_id) + str((self._is_gtac ,self._name, phix_percent)))
        return self._is_gtac, self._name,phix_percent    

    def lane_samples(self):
        """
        Given a lane, return the name, user, and percentage of the 
        samples in that lane using the multiprep csv if necessary.
        Also include the % phix used in the aliquot.
        """
        for mp in self.multiprepped:
            if self.l.sample_id == mp['Sequencing name']:
                samples = []
                    
                for x in range(1,4):
                    if not mp['Prepped sample name %d' % x]: continue
                    try:
                        u = User.objects.get(username=mp['Contact%d' % x].strip().lower().replace(' ','_')[:30])
                    except User.DoesNotExist:
                        u = phix
                    s = mp['Prepped sample name %d' % x]
                    pcs = mp['%%sample%d' % x]
                    samples.append((s,u,int(pcs)))

                phix_percent = mp['%PhiX']
                return (samples, phix_percent)
        else:
            return (( 
                (self.clean_sid()[0], get_lane_user(self.l), 100), ),
                 0.0)

    @property
    def flowcell(self):
        if not hasattr(self, '_flowcell'):
            if self.l.run.is_paired_end:
                read='PE'
            else:
                read='SR'

            self._flowcell,c = get_or_create(
                seqmodels.Flowcell, 
                name=self.l.run.flowcell_id, 
                runtype=self.runtype,
                )
                
            if self.l.run.cluster_kit.cluster_kit.startswith('CB'):
                csmodel,c = get_or_create(seqmodels.ClusterStationModel, name="CBOT")
                if self.l.run.description.lower().startswith('j'):
                    cs,c = get_or_create(seqmodels.ClusterStation, name="Jipper Chipper", model=csmodel, serial="2")
                elif self.l.run.description.lower().startswith('d'):
                    cs,c = get_or_create(seqmodels.ClusterStation, name="Djibouti", model=csmodel, serial="3")
                else:
                    log("WARNING: not sure what cluster station to assign %s" % self.l)
                    cs, c = get_or_create(seqmodels.ClusterStation, name="Jipper Chipper", model=csmodel, serial="2")
            else:
                csmodel,c = get_or_create(seqmodels.ClusterStationModel, name="Cluster Station")
                cs,c = get_or_create(seqmodels.ClusterStation, name="Bertha", model=csmodel, serial="1")

            fcgen,c = get_or_create(seqmodels.FlowcellGeneration, flowcell=self._flowcell, kit=self.l.run.cluster_kit.cluster_kit, date=self.l.run.flowcell_date, cluster_station=cs)
        return self._flowcell
        
    @property
    def instrument(self):
        if not getattr(self, '_instrument', None):
            self.migrate_instrument()
        return self._instrument

    @property
    def instrumentmanifold(self):
        if not getattr(self, '_instrumentmanifold', None):
            self.migrate_instrument()
        return self._instrumentmanifold

    @property
    def instrumentmodel(self):
        if not getattr(self, '_instrumentmodel', None):
            self.migrate_instrument()
        return self._instrumentmodel

    @property
    def denaturedaliquot(self):
        if not getattr(self, '_denaturedaliquot', None):
            is_gtac, lid, phix_percent = self.clean_sid()
            if phix_percent is None:
                phix_percent = 100
        
            self._denaturedaliquot,c = get_or_create(seqmodels.DenaturedAliquot,
                sequencingsample=self._sequencingsample,
                loading_concentration=self.l.dna_conc.amount,
                phix_spike=phix_percent,
                )
        return self._denaturedaliquot

    def migrate_runpostprocess(self):
        for s in self.l.run.summary_set.all():
            pipe = get_or_create(seqmodels.PostProcessPipeline, version=s.pipeline_version.version)[0]
            date = s.date
            if s.control_lane:
                try:
                    ctrl_lane = seqmodels.Lane.objects.get(flowcell__run__num=s.control_lane.run.num, number=s.control_lane.num)
                except seqmodels.Lane.DoesNotExist:
                    #seq = Seq(lane=s.control_lane)
                    #if seq.is_gtac:
                    print "WARNING: can't set %s as control lane for %s.  Isn't in database yet" % (str(s.control_lane), self.run)
                    ctrl_lane = None
                    #else:
                    #    seq.migrate(s.control_lane.dropoff_date)
            else:
                ctrl_lane = None
            rpp = get_or_create(seqmodels.RunPostProcess, 
                    run=self.run,
                    results_file='run_post_process/nothing',
                    analysis_path=s.analysis_path,
                    pipeline=pipe,
                    date=date,
                    defaults={'control_lane': ctrl_lane},
                    )[0]
            if ctrl_lane and not rpp.control_lane:
                rpp.control_lane = ctrl_lane
                rpp.save()

            for ls in s.lanesummary_set.filter(lane=self.l):
                lpp = get_or_create(seqmodels.LanePostProcess,
                    lane=self.lane,
                    run_post_process=rpp
                    )[0]
                if s.percent_phasing:
                    get_or_create(seqmodels.PostProcessValue, lane_post_process=lpp, name="Percent phasing", type="float", value=s.percent_phasing)
                if s.percent_prephasing:
                    get_or_create(seqmodels.PostProcessValue, lane_post_process=lpp, name="Percent prephasing", type="float", value=s.percent_prephasing)
                if s.num_cycles:
                    get_or_create(seqmodels.PostProcessValue, lane_post_process=lpp, name="Number of cycles", type="int", value=s.num_cycles)
                if ls.num_reads:
                    get_or_create(seqmodels.PostProcessValue, lane_post_process=lpp, name="Number of reads", type="int", value=ls.num_reads)
                if ls.first_intensity:
                    get_or_create(seqmodels.PostProcessValue, lane_post_process=lpp, name="First intensity", type="float", value=ls.first_intensity)
                if ls.twenty_intensity:
                    get_or_create(seqmodels.PostProcessValue, lane_post_process=lpp, name="Twenty intensity", type="float", value=ls.twenty_intensity)
                if ls.percent_pass_filter:
                    get_or_create(seqmodels.PostProcessValue, lane_post_process=lpp, name="Percent pass filter", type="float", value=ls.percent_pass_filter)
                if ls.num_tiles:
                    get_or_create(seqmodels.PostProcessValue, lane_post_process=lpp, name="Number of tiles", type="int", value=ls.num_tiles)
                if ls.clusters_per_tile:
                    get_or_create(seqmodels.PostProcessValue, lane_post_process=lpp, name="Clusters per tile", type="int", value=ls.clusters_per_tile)




    def migrate_instrument(self):
        names = {
            '157': 'Buckets',
            '155': 'Fast Eddie',
            '440': 'Can-o-corn',
            '158': 'Psghetti',
            '997': 'New Guy',
            }
        
        old_i = self.l.run.instrument
        self._instrumentmodel,c = get_or_create(seqmodels.SequencerModel, name=old_i.model.split('-')[0])
        self._instrument,c = get_or_create(seqmodels.Sequencer,
            model=self._instrumentmodel,
            name=names[old_i.serial],
            serial=old_i.serial,
            defaults={
                'max_runs_per_week': old_i.max_runs_per_week,
                'expired': old_i.expired,
                },
            )
            
        if len(old_i.model.split('-')) > 1:
            self._instrumentmanifold,c = get_or_create(seqmodels.Manifold, sequencer=self._instrument, name=old_i.model.split('-')[1])
        else:
            self._instrumentmanifold,c = get_or_create(seqmodels.Manifold, sequencer=self._instrument, name='A')

    @property
    def runtype(self):
        if not hasattr(self, '_runtype'):
            pe = self.l.run.is_paired_end
            if pe:
                read='PE'
            else:
                read='SR'
            self._runtype, c = get_or_create(seqmodels.RunType,
                read=read,
                length=self.l.run.cycles.cycles,
                sequencer_model=self.instrumentmodel,
                expired=self.l.run.instrument.expired,
                )
        return self._runtype

    @property
    def run(self):
        if not getattr(self, '_run', None):
            self._run, c = get_or_create(seqmodels.Run,
                num=self.l.run.num, 
                flowcell=self.flowcell, 
                sequencing_date=self.l.run.sequencing_date, 
                manifold=self.instrumentmanifold,
                folder_name=self.l.run.run_folder or '',
                software_version=self.l.run.scs_version.version,
                sequencing_kit=self.l.run.sequencing_kit.sequencing_kit,
                turnaround_date=self.l.run.read2_sequencing_date,
                status=self.l.run.status,
                )
        return self._run        

    @property
    def lane(self):
        if not getattr(self, '_lane', None):
            self._lane,c = get_or_create(seqmodels.Lane,
                flowcell=self.flowcell, 
                primer=self.l.primer.name,
                number=self.l.num,
                )
            if self.denaturedaliquot and self.denaturedaliquot not in self._lane.denaturedaliquots.all():
                self._lane.denaturedaliquots.add(self.denaturedaliquot) 
            
        return self._lane
