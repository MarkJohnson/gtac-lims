#!/usr/bin/env python
"""
Sequence Submission Data import
"""


import os
import sys
from core import models as coremodels
from base import models as basemodels
from sample import models as smodels
from billing import models as billmodels
from seq import models as qmodels
from libprep import models as lpmodels
from analysis import models as anamodels
from solexamgr import models as solmodels
from django.db import transaction
from settings import DJROOT
import csv
import re
from cStringIO import StringIO
import datetime
import traceback

admin = coremodels.Person.objects.get(user__username='admin')
status_unknown = lpmodels.PrepStatus.objects.get_or_create(name='Unknown')[0]

def cleandate(s):
    if not s:
        return None
    for fmt in ('%m/%d/%Y', '%m/%d/%y'):
        try:
            return datetime.datetime.strptime(s, fmt)
        except: 
            pass
    raise Exception, "No date format works for %s" % (s)


def get_csv(fname,header_line_num):
    csvlines = open(fname).readlines()
    f = StringIO()
    f.write('\n'.join(csvlines[header_line_num:]))
    f.seek(0)
    return csv.DictReader(f)


class GTACSeqSubmission(object):
    def __init__(self, fname, import_all=True, sample=None, only_complete=True, verbose=False):
        self.sample = sample
        self.only_complete = only_complete
        self.verbose = verbose
        self.level = 0
        self.import_all = import_all
        self.fname = fname


    def error(self, msg):
        sys.stderr.write("%s%s\n" % (' ' * 4 * self.level, msg))
   
 
    def get_or_create(self, model, manager='objects', **kwargs):
        obj, c = getattr(model, manager).get_or_create(**kwargs)
        if c:
            if self.verbose: 
                self.log("CREATE: %s(%s)" % (model._meta.object_name, ', '.join(['='.join((str(k),str(v))) for k,v in kwargs.items()])))
        elif self.verbose:
            #self.log("GET: %s(%s)" % (model._meta.object_name, ', '.join(['='.join((str(k),str(v))) for k,v in kwargs.items()])))
            pass
        return obj,c


    @transaction.commit_on_success
    def migrate(self):
        seqsamplesubmissions = []
        errors = []
        for x,l in enumerate(get_csv(self.fname,2)):
          try:
            if self.only_complete: # No complete column. Complete encoded in color.  Change this?
                if not l['Date'].strip(): continue
            if self.only_complete:
                if not l['Sample'].strip(): continue 

            if not self.import_all:
                if self.sample:
                    if l['Sample'].strip(): #* Okay?
                        self.log("Found sequencing sample submission: %s" % self.sample)
                else:
                    self.log("Nothing to do")
                    break

            #
            #  Number of lanes
            #
            if l['# lanes'].strip():
                num_lanes,c = self.get_or_create(qmodels.SampleSequencingDetail, num_lanes=l['# lanes'] )
            #else:
                #* Do anything?

            #
            #  GTAC Analysis
            #
            
            analysis_request = l['GTAC Analysis?'].strip()
            if analysis_request:
                analysis_request = analysis_request.lower()

                # Get prep type

                # Create preptype to pipeline dictionary

                # save to assume sequence submission form == whats in gtac sample tracking form?
                if (analysis_request == 'no') or (analysis_request == 'none'):
                    # Check if demuxed, then demux
                    p='none'
                elif analysis_request == 'demux':
                    p='demux'
                elif (analysis_request == 'yes') or (analysis_request == 'full'):
                    p='needs work'

                analysis_pipeline,c = self.get_or_create(anamodels.SampleAnalysisDetail, pipeline=p)
# add except and print errors

          except Exception, m:
              errors.append("%s: %s" % (l['Sample'], m)) 
              if l['Date'].strip():
                  date = cleandate(l['Date']).date()
                  if date > (datetime.datetime.now().date() - datetime.timedelta(60)) or self.verbose:
                      self.error("Error Importing %s (%s): %s" % (l['Sample'], str(date), m))
              traceback.print_exc(file=sys.stderr)
              transaction.rollback()
              continue
          else:
              # seqsamplesubmissions.append(ssub) #* not sure what to save here
              transaction.commit()

        return errors,seqsamplesubmissions

