from django.core.management.base import BaseCommand, CommandError
import os
import argparse
from settings import DJROOT
import sys
import itertools
from libprep import models as lpmodels
from sample import models as sampmodels
from seq import models as seqmodels
from analysis import models as anamodels
from base import models as basemodels
from django.db import transaction
import datetime

class Command(BaseCommand):

    @transaction.commit_on_success
    def handle(self, *args, **kwargs):
        parent_parser = argparse.ArgumentParser(add_help=False)
        parent_parser.add_argument('-v', '--verbose', action='store_true')
        parser = argparse.ArgumentParser(parents=[parent_parser,], prog="gtacimport", description='Migrate samplesubmissions (add all associated data) from various sources into GTAC Lims.')
        subparsers = parser.add_subparsers(help='sub-command help')
    
        parser_gtac_id = subparsers.add_parser('gtacid', parents=[parent_parser,], help='Migrate individual GTAC IDs from the Sample Tracking Worksheet.')
        parser_gtac_id.add_argument('id', nargs='+', help="GTAC IDs to migrate")
        parser_gtac_id.set_defaults(func=sampletracking)
    
        parser_prepname = subparsers.add_parser('prepname', parents=[parent_parser,], help='Migrate individual Prepped(pooled) Samples from the Sample Tracking Worksheet.')
        parser_prepname.add_argument('name', nargs='+', help="Prepped Names to migrate")
        parser_prepname.set_defaults(func=sampletracking)
    
        parser_libprep = subparsers.add_parser('libprep', parents=[parent_parser,], help="LibPrep")
        parser_libprep.set_defaults(func=import_libprep)
        
        parser_seqsub= subparsers.add_parser('seqsub', parents=[parent_parser,], help="SeqSub")
        parser_seqsub.set_defaults(func=import_seqsub)

        parser_analysis = subparsers.add_parser('analysis', parents=[parent_parser,], help="Analysis")
        parser_analysis.set_defaults(func=import_analysis)
        parser_indexes = subparsers.add_parser('indexes', parents=[parent_parser,], help="Import indexes from analysis spreadsheet")
        parser_indexes.set_defaults(func=import_indexes)
    
        parser_all = subparsers.add_parser('all', parents=[parent_parser,], help="Import everything.")
        parser_all.set_defaults(func=import_all)

        args = parser.parse_args(args)
        args.func(args)
    

def get_csvs():
    if os.path.exists(os.path.join(DJROOT, 'tmp', 'libprep-1.csv')):
       return 

    from base.management.commands.gtac_spreadsheets import GTACData
    import getpass

    email = raw_input("Email: ")
    password = getpass.getpass("Password: ")
    pd = GTACData(email, password)
    pd.get_data()


@transaction.commit_on_success
def sampletracking(args):
    get_csvs()
    import impprep
    import impseq
    samplesubmissions = []

    for id in getattr(args, 'id', []):
        samplesubmissions.extend(impprep.GTACSample(gtacid=id, verbose=True).migrate())

    for pn in getattr(args, 'name', []):
        samplesubmissions.extend(impprep.GTACSample(sample=pn, verbose=True).migrate())
    
    for ss in samplesubmissions:
      try:
        impseq.import_samplesubmission(ss)
      except Exception, m:
        print "WARNING: failed to import Seq info for SampleSubmission: %s, %s" % (str(ss), m)  
        continue

    #FIXME
    # Should be this instead
    # for ss in samplesubmissions:
    #   s = migrate_seq.Seq(samplesubmission=ss) or something like that
    #   
    
    #seqsamples = lpmodels.SequencingSample.objects.all().filter(sampleprepdetails__samplesubmission__in=samplesubmissions).distinct()
    #oldlanes = []
    #for ss in seqsamples:
    #    # Get every lane that used that sequencing sample
    #    for oldlane in migrate_seq.Seq.oldlanes(ss):
    #        print oldlane
    #        if oldlane in oldlanes:
    #            continue
    #        oldlanes.append(oldlane)
    #        seqstuff = migrate_seq.Seq(oldlane, ss, verbose=args.verbose)
    #        seqstuff.make_lane()
    #        print seqstuff.run

from django.core import serializers
from django.db import transaction
from django.contrib.auth.models import User
import subprocess
from django.core.management import call_command


def download_sampletracking():
    from base.management.commands.gtac_spreadsheets import GTACData

    email, password = open(os.path.join(DJROOT, 'mygmailcreds')).readline().split(':')
    g = GTACData(email,password)
    fname = g.get_sampletracking()
    return fname

def download_sequencesubmission():
    from base.management.commands.gtac_spreadsheets import GTACData

    email, password = open(os.path.join(DJROOT, 'mygmailcreds')).readline().split(':')
    g = GTACData(email,password)
    fname = g.get_sequencesubmission()
    return fname
    
@transaction.commit_on_success
def import_all(args):
    # 1. download everything
    import impprep
    import impseq
    import impseqsub
    #import_indexes()
    lp_fname = download_sampletracking()
    errors,samplesubmissions = impprep.GTACSample(lp_fname, verbose=False).migrate()
    
    ss_fname = download_sequencesubmission()
    errors,sequencesubmissions = impseqsub.GTACSeqSubmission(ss_fname, verbose=False).migrate()
     
    for ss in sampmodels.SampleSubmission.objects.all():
      try:
        impseq.import_samplesubmission(ss)
      except Exception, m:
        print "WARNING: failed to import Seq info for SampleSubmission: %s, %s" % (str(ss), m)  
        continue

    
@transaction.commit_on_success
def import_libprep(args=None):
    import impprep

    lp_fname = download_sampletracking()
    errors,samplesubmissions = impprep.GTACSample(lp_fname, verbose=False).migrate()

@transaction.commit_on_success
def import_seqsub(args=None):
    import impseqsub

    seqsub_fname = download_sequencesubmission()
    errors,samplesubmissions = impseqsub.GTACSeqSubmission(seqsub_fname, verbose=False).migrate()

@transaction.commit_on_success
def import_analysis(args=None):
    from base.management.commands.gtac_spreadsheets import GTACData
    import csv

    email, password = open(os.path.join(DJROOT, 'mygmailcreds')).readline().split(':')
    g = GTACData(email,password)
    fname = g.get_analysis()

    for l in csv.DictReader(open(fname, 'r')):
        run,lane = l['Run ID'].split('_')
        try:
            lane = seqmodels.Lane.objects.get(flowcell__run__num=run, number=lane)
        except seqmodels.Lane.DoesNotExist:
            print "Can't find Run/Lane %s/%s" % (run,lane)
            continue
        for ss in lane.samplesubmissions.all():
            a = anamodels.SampleAnalysisDetail.objects.get_or_create(samplesubmission=ss)[0]
            p = basemodels.Person.objects.get_or_create(first_name=l['Assigned to'], last_name='')[0]
            a.assigned_to = basemodels.Employee.objects.get_or_create(person=p)[0]
            try:
                a.start_date = datetime.datetime.strptime(l['Actual analysis start'], '%m/%d/%Y')
                a.finish_date = datetime.datetime.strptime(l['Actual analysis finish'], '%m/%d/%Y')
            except:
                pass
            a.save()
        
        print l['Actual analysis start'], l['Actual analysis finish']

@transaction.commit_on_success
def import_indexes(args=None):
    #
    # 0: "GTAC index name",  1: "Index",  2: "Reverse complement (Use this!)"
    #
    # 4: Illumina index name, 5: Index, 6: Reverse complement (Use this!)
    #
    # 8: Illumina small RNA index name, 9: GTAC's name for it, 10: Index 11: Reverse Complement (Use this!)
    #
    # 12: Amp-free index name. 13: Index (Use this!) 
    #
    from base.management.commands.gtac_spreadsheets import GTACData

    email, password = open(os.path.join(DJROOT, 'mygmailcreds')).readline().split(':')
    g = GTACData(email,password)
    fname = g.get_indexes()

    import csv
    from cStringIO import StringIO
    from libprep.models import Index
    for x,d in enumerate(csv.reader(open(fname))):
        if x == 0: continue # Skip column headers
        if d[0:1] and d[2:3]:
            print Index.objects.get_or_create(name=d[0], sequence=d[2])
        if d[4:5] and d[4:5] != ['']:
            print Index.objects.get_or_create(name=d[4], sequence=d[6])
        if d[8:9]:
            print Index.objects.get_or_create(name=d[9], sequence=d[11])
        if d[13:14] and d[13:14] != ['']:
            print Index.objects.get_or_create(name=d[13], sequence=d[14])
    


