from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from forms import *
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, render_to_response, get_object_or_404
from django.contrib import messages
from django.core.urlresolvers import reverse
from base import models as basemodels
from core import models as coremodels
from sample import models as sampmodels
from seq import models as seqmodels
from base import forms
from django.db import transaction
from django.db.models import Q
import json
import logging

from util import grid, get_database_records
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder

from xml.etree.ElementTree import Element, SubElement, tostring

from libprep import models as lpmodels

logger = logging.getLogger('myproject.custom')

class DataTableResponse(object):
    def __init__(self, request, module=None):
        self.module = module
        self.echo = request.GET.get('sEcho')
        self.search = request.GET.get('sSearch',None)
        self.searchcols = []
        for k,v in request.GET.items():
            if k.startswith('sSearch_'):
                self.searchcols.append((int(k[8:]), v))

        self.sortingcols = int(request.GET.get('iSortingCols',0))
        self.sortcol = []
        for x in range(0, self.sortingcols):
            colindex = int(request.GET.get('iSortCol_%d' % x))
            coldir = request.GET.get('sSortDir_%d' % x)
            self.sortcol.append((colindex, coldir))
        self.start = int(request.GET.get('iDisplayStart'))
        length = int(request.GET.get('iDisplayLength'))
        if length == -1:
            self.end = None
        else:
            self.end = self.start + length
        self.total = 0
        self.totaldisplay = 0

    def filter_qs(self, qs, cols, search_fields=None, search_cols=None, group_cols=None):
        self.total = qs.count()
        q = Q()
        if self.search and search_fields:
            for sf in [('%s__icontains' % x) for x in search_fields]:
                q = q | Q(**{sf: self.search})
        colq = Q()
        if self.searchcols and search_cols:
            for index,value in self.searchcols:
                if index < len(search_cols) and value:
                    if isinstance(search_cols[index], str):
                        s = '%s__icontains' % search_cols[index]
                        colq = colq & Q(**{s: value})
                    else:
                        subcolq = Q()
                        for field in search_cols[index]:
                           s = '%s__icontains' % field
                           subcolq = subcolq | Q(**{s: value})
                        colq = colq & subcolq
        q = q & (colq)
        qs = qs.filter(q)

        self.totaldisplay = qs.count()

        order_by = []
        if cols:
            for cindex, cdir in self.sortcol:
                n = cols[cindex]
                if type(n) is tuple:
                    for c in n:
                        order_by.append('-' + c if cdir == 'desc' else c)
                else:
                    order_by.append('-' + n if cdir == 'desc' else n)
        if not 'id' in order_by:
            order_by.append('id')
        if order_by:
            qs = qs.order_by(*order_by)
        if group_cols:
            qs = qs.annotate(*group_cols)
        if self.end:
            qs = qs[self.start:self.end]
        else:
            qs = qs[self.start:]

        if self.module:
            return check_column_ambiguity(qs, self.module)
        else:
            return qs


def check_column_ambiguity(qs, module):
    edited = False
    query = str(qs.query)
    if "SELECT DISTINCT" in query:
        selected_columns = str(query).split("SELECT DISTINCT")[1].split("FROM")[0].strip().split(',')
    else:
        selected_columns = str(query).rsplit("SELECT", 1)[1].split("FROM")[0].strip().split(',')

    group_columns = []
    if 'GROUP BY' in query:
        group_columns = query.split('GROUP BY')[1].split('ORDER BY')[0].strip().split(',')
    columns = {}
    for col in selected_columns:
        if ' AS ' in col:
            name = col.split(' AS ')[1].strip('"')
        else:
            name = col.split('.')[1].strip('"')

        count = 1
        if columns.get(name):
            count = columns.get(name) + 1
            col_name = "".join((col, ' AS "', name, '_', str(count), '"'))
            query = query.replace(col, col_name, 1)
            edited = True

        columns[name] = count
        try:
            group_columns.remove(col.split(' AS ')[0])
        except:
            pass

    for col in group_columns:
        query = query.replace(','+col, ' ')
        edited = True

    if edited:
        return prepareRawQuery(module, query)
    return qs

#
# If query contains any 'iContains' param then separate params value from query string and create list of values.
# and substitute '%s' instead of values.
# Then Create a raw query using modified query string and list of values.
#
def prepareRawQuery(module, query):
    q = []
    beg = 0
    while query.find('%', beg) > 0:
        beg = query.find("%", beg)
        end = query.find("%", beg+1)
        param = query[beg:end+1]
        query = query.replace(param, '%s', 1)
        q.append(param)
        beg = end + 1
    query =  query.replace('= True ', '= 1 ')
    query =  query.replace('= False ', '= 0 ')
    return module.objects.raw(query, q)

@login_required
def dashboard(request):
    submissions = sampmodels.Submission.objects.\
                  select_related('client', 'client__lab').\
                  prefetch_related('sample_set', 'sample_set__libprep', \
                                   'sample_set__libprep__preptype', \
                                   'sample_set__libprep__status').\
                  filter(sample__libprep__status__name__in=\
                  ['New', 'QC', 'Prepping']).defer('description').distinct()
    runs        = seqmodels.Run.objects.select_related('flowcell', \
                                                       'manifold').\
                  filter(status='In Progress')
    seqsamples  = lpmodels.SequencingSample.objects.\
                  prefetch_related('sampleprepdetails', 'sampleprepdetails__sample', \
                                   'sampleprepdetails__sample__seq', \
                                   'sampleprepdetails__sample__seq__runtype', \
                                   'sampleprepdetails__status').\
                  filter(sequencing_sample__flowcell__isnull=True).\
                  exclude(sampleprepdetails__status__name = 'Cancel').\
                  order_by('-submitted_to_sequencing')
    return render(request, 'frontend/dashboard.html', {
        'submissions': submissions,
        'runs': runs,
        'seqsamples': seqsamples,
    })

@login_required
def prefs(request):
    client = get_object_or_404(Client, user=request.user)
    if request.method == 'POST': 
        form = PrefsForm(request.POST, instance=client)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.INFO, 'Preferences Saved')
            return HttpResponseRedirect(reverse('gtac:prefs'))
    else:
        form = PrefsForm(instance=client)

    return render_to_response('gtac/prefs.html', {
            'form': form,
            },
            context_instance=RequestContext(request),
        )

@transaction.atomic
@login_required
def client_add(request):   
    is_ajax_request = request.GET.get('is_ajax') == 'true'
    valid = True
    if request.method == 'POST': 
        form = forms.ClientAddForm(request.POST, prefix='person')
        labform = forms.LabAddForm(request.POST, prefix='lab')
        labaddform = forms.LabAddressFormset(request.POST, prefix='labadd')
        valid = form.is_valid()
        if valid:        
            cd = form.cleaned_data

            if cd['lab']:
                lab = cd['lab']
            elif labform.is_valid():
                lab = labform.save()
                if lab.location == 'external' :
                    labaddform = forms.LabAddressFormset(request.POST, instance=lab, prefix='labadd')
                    if labaddform.is_valid():
                        labaddform.save()
                    else:
                        valid = False;                               
            else:
                valid = False
            
            if valid:
                person = form.save()
                basemodels.Client(person=person, lab=lab).save()
                person.phone_numbers.get_or_create(number=cd['phone'], extension=cd['extension'])
                person.email_addresses.get_or_create(email=cd['email'])
                if not cd['lab'] and not cd['is_client_pi']:
                    cdata = labform.cleaned_data
                    pi_person = coremodels.Person(first_name=lab.first_name, last_name=lab.last_name)
                    pi_person.save()
                    basemodels.Client(person=pi_person, lab=lab).save()
                    pi_person.email_addresses.get_or_create(email=cdata['email'])
                messages.add_message(request, messages.INFO, "Client %s created successfully." % (person.name))
                if not is_ajax_request:
                    return HttpResponseRedirect(reverse('specimen-submit'))
            else:
                transaction.rollback()

    if request.method == 'GET' or (is_ajax_request and valid):
        form = forms.ClientAddForm(prefix='person')
        labform = forms.LabAddForm(prefix='lab')
        labaddform = forms.LabAddressFormset(prefix='labadd')
    
    labs = basemodels.Lab.objects.select_related('department').filter(campus_box__isnull=False, department__isnull=False)

    return render(request, 'base/client_add.html', {
            'form': form,
            'labform': labform,
            'labaddform': labaddform,
            'labs': {l.id:[l.campus_box, l.department.number] for l in labs},
            'is_ajax_request': is_ajax_request
            })

@login_required
def client_browse(request):
    if not request.GET.get('sEcho', False):
        return render(request, 'base/client-browse.html', {})
    SORT_COLS = (
                 ('person__first_name', 
                  'person__last_name'),
                 ('lab__first_name',
                  'lab__last_name'),
                )
    clients = basemodels.Client.objects.all()

    dtr = DataTableResponse(request)
    qs = dtr.filter_qs(clients, SORT_COLS, search_fields=('person__first_name','person__last_name','lab__first_name','lab__last_name'))
    return render(request, 'base/client-browse-data.json', {
            'dtr': dtr,
            'clients': qs,
            })

def client_or_admin_required(func):
    @login_required
    def _new_func(request, *args, **kwargs):
        if not request.user.has_perm('Can delete project'):
            try:
                request.user.get_profile()
            except Client.DoesNotExist:
                raise Http404()
        return func(request, *args, **kwargs)
    return _new_func

def prep(request):
    if request.method == 'POST': 
        form = PrepForm(request.POST)
        if form.is_valid():
            return HttpResponse("You entered %s" % form.cleaned_data)
        
            return HttpResponseRedirect(reverse("gtac:register-complete"))
    else:
        form = PrepForm()

    return render_to_response('sample/prep_form.html', {
        'form': form,
    },
    context_instance=RequestContext(request),
    )

@login_required
def submission_create(request):
    if request.method == 'POST': 
        form = SubmissionForm(request.user, request.POST)
        if form.is_valid():
            prj = form.save(commit=False)
            prj.user = request.user
            prj.save()
            return HttpResponseRedirect(reverse("gtac:submission-detail", kwargs={'id':prj.id}))
        
            return HttpResponseRedirect(reverse("gtac:register-complete"))
    else:
        form = SubmissionForm(request.user)

    return render_to_response('sample/submission_form.html', {
        'form': form,
    },
    context_instance=RequestContext(request),
    )
    


@client_or_admin_required
def submit(request, id=None):
    submission = get_object_or_404(Submission, id=id)
    if request.method == 'POST': 
        form = SampleSubmitForm(request.POST)
        if form.is_valid():
            samp = form.save(commit=False)
            samp.user = submission.user
            samp.submission = submission
            samp.save()
            return HttpResponseRedirect(reverse('gtac:submission-detail', kwargs={'id': submission.id }))
        
    else:
        form = SampleSubmitForm()

    return render_to_response('sample/submit_form.html', {
        'form': form,
        'submission': submission,
    },
    context_instance=RequestContext(request),
    )

@login_required
def submission_detail(request, id=None):
    submission = get_object_or_404(Submission, user=request.user, id=id)

    if request.method == 'POST': 
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
            messages.add_message(request, messages.INFO, "Good!")
            return HttpResponseRedirect(reverse('gtac:submission-detail', kwargs={'id': submission.id }))
        
    else:
        form = UploadForm()
    return render_to_response('sample/submission_detail.html', {
        'submission': submission,
        'uploadform': form,
        },
        context_instance=RequestContext(request),
    )
    
@client_or_admin_required
def submission_json(request, id=None):
    qs = Submission.objects.filter(user=request.user)
    #colmap = dict([(x,y) for x,y in enumerate(Submission.__dict__.keys())])
    colmap = { 0: 'id', 1:'name' }
    return get_database_records(request, qs, colmap, [], 'sample/submission_json.txt')

@client_or_admin_required
def sample_json(request, id=None):
    qs = Sample.objects.filter(user=request.user, submission=id)
    #colmap = dict([(x,y) for x,y in enumerate(Submission.__dict__.keys())])
    colmap = { 0: 'id', 1:'name' }
    return get_database_records(request, qs, colmap, [], 'sample/submission_json.txt')


@client_or_admin_required
def submission_grid(request, id=None):
    numrows = request.GET.get('rows', '2')
    page = request.GET.get('page', '1')
    
    roots,config,rows = grid.xmlbase(rownum=numrows, url=reverse('gtac:submission-grid', kwargs={'id':id}))

    qs = Submission.objects.filter(user=request.user)
    #qs = get_object_or_404(Submission, id=id).sample_set.all()
    if 'sidx' in request.GET:
        qs = qs.order_by(request.GET['sidx'])
    if request.GET.get('sord', 'desc') != 'desc':
        qs = qs.reverse()
    start = (int(page) - 1) * int(numrows)
    stop = start+20
    qs = qs[start:stop]
    fields = ['name', 'created']
    grid.qs_rows(qs ,rows, page, fields=fields)
    grid.inst_colmodels(qs[0], config, fields=fields)
    
    return HttpResponse(tostring(roots), content_type='text/xml')

@client_or_admin_required
def sample_grid(request, id=None):
    numrows = request.GET.get('rows', '2')
    page = request.GET.get('page', '1')
    
    roots,config,rows = grid.xmlbase(rownum=numrows, url=reverse('gtac:sample-grid', kwargs={'id':id}))
    submission = get_object_or_404(Submission, id=id)
    qs = Sample.objects.filter(submission=submission)
    #qs = get_object_or_404(Submission, id=id).sample_set.all()
    if 'sidx' in request.GET:
        qs = qs.order_by(request.GET['sidx'])
    if request.GET.get('sord', 'desc') != 'desc':
        qs = qs.reverse()
    start = (int(page) - 1) * int(numrows)
    stop = start+20
    qs = qs[start:stop]
    fields = ['name', 'created']
    grid.qs_rows(qs ,rows, page, fields=fields)
    grid.inst_colmodels(qs[0], config, fields=fields)
    
    return HttpResponse(tostring(roots), content_type='text/xml')


@client_or_admin_required
def submission_dashboard(request):
    if request.method == 'POST': 
        form = SubmissionForm(request.user, request.POST)
        if form.is_valid():
            prj = form.save(commit=False)
            prj.user = request.user
            prj.save()
            return HttpResponseRedirect(reverse("gtac:submission-detail", kwargs={'id':prj.id}))
    else:
        form = SubmissionForm(request.user)
        
    return render_to_response('sample/submission_dashboard.html', {
        'submissions': request.user.submission_set.all(),
        'addform': form,
        },
        context_instance=RequestContext(request),
    )

@login_required
@client_or_admin_required
def sample_detail(request, id=None):
    if request.user.is_superuser:
        sample = get_object_or_404(Sample, id=id)
    else:
        sample = get_object_or_404(Sample, user=request.user, id=id)
    
    return render_to_response('sample/sample_detail.html', {
        'sample': sample,
        },
        context_instance=RequestContext(request),
    )

@login_required
def submission_excel(request):
    r = HttpResponse(libraryprepexcel(), content_type="application/vnd.ms-excel")
    r['Content-Disposition'] = 'attachment; filename=gtac_library_prep.xls'
    return r

@login_required
def submission_list(request):
    return render_to_response('submission/submission_list.html', {
        'submissions': Submission.objects.filter(user=request.user),
        },
        context_instance=RequestContext(request),
    )

@login_required
def client_json(request):
    client = get_object_or_404(basemodels.Client, id=request.GET.get('id', None))
    objs = [client, client.lab]
    if client.lab.department:
        objs.append(client.lab.department)
    p = serializers.serialize('json', objs)
    return HttpResponse(p, content_type='application/json')

@login_required
def client_list(request):
    clients = basemodels.Client.objects.all()
    resp = {"aData": [[c.id, str(c)] for c in clients]}
    return HttpResponse(json.dumps(resp), content_type="application/json")
