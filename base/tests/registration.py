import os
from django.test import TestCase
from django.test.client import Client
from gtac.models import Lab, Client as GTACClient
from django.core.urlresolvers import reverse
from gtac.forms.registration import RegistrationForm

class RegistrationTestCase2(TestCase):
    fixtures = [os.path.join(os.path.dirname(__file__),'registration.json'), ]

    def setUp(self):
        self.user_data = {
            'first_name': 'asdf',
            'last_name': 'wqewr',
            'email': 'test@koebbe.org',
            }
        self.client_data = {
            'password1': 'asdf',
            'password2': 'asdf',
            'phone': '11234445555',
            'alt_phone': '1234.332.3453',
            'lab': 1,
            }
        self.lab_data = {
            'name': 'Foo Bar Lab',
            'street': '234 foo',
            'city': 'St. Louis', 
            'state': 'MO',
            'zipcode': '23423',
            'primary_contact': 1,
            }
        self.post_data = self.client_data.copy()
        self.post_data.update(self.user_data)
        self.c = Client()

    def register(self, data=None):
        if data is None:
            data = self.post_data
        return self.c.post(reverse("gtac:register"), data) #, follow=True)

    def test_0_create_client(self):
        """Add client using Client.objects.create_client()."""
        lab = Lab.objects.all()[0]
        c = GTACClient.objects.create_client(
            "brian@koebbe.org", 
            "Brian", "Koebbe", "asdf", lab, "323234 2343",
            )
        self.failUnlessEqual(c.phone, '323-234-2343')
        self.failUnlessEqual(c.regprofile.status, 'unverified')
        c = GTACClient.objects.get(user__username='brian@koebbe.org',)
        
    def test_1_register_form(self):
        """Register user.  Verify.
        """
        d = self.post_data.copy()
        if 'lab' in d:
            del d['lab']
        if 'lab_name' in d:
            del d['lab_name']
        r = self.register(d)
        self.assertFormError(r, 'form', None, 
            "Must choose either an existing lab or enter a new lab.")

        for k in self.post_data.keys():
            if k in ['alt_phone', 'lab']: continue
            d = self.post_data.copy()
            del d[k]
            r = self.register(d)
            self.assertFormError(r, 'form', k, 'This field is required.')

        d = self.post_data.copy()
        d.update(lab=1)
        r = self.register(d)
        self.assertRedirects(r, reverse("gtac:register-complete"))

        d = self.post_data.copy()
        d.update(lab=1)
        r = self.register(d)
        self.assertFormError(r, 'form', 'email', 'This email address is already in use.')

        c = GTACClient.objects.get(user__username=self.post_data['email'],)

        self.failUnlessEqual(c.phone, '123-444-5555')
        self.failUnlessEqual(c.alt_phone, '234-332-3453')
        self.failUnlessEqual(c.regprofile.status, 'unverified')

        self.c.get(reverse("gtac:register-verify", kwargs={'key':c.key}))
        c = GTACClient.objects.get(user__username=self.post_data['email'],)
        self.failUnlessEqual(c.regprofile.status, 'unvalidated')
        
    def test_2_bad_passwords(self):
        """Non-matching passwords"""
        d = self.post_data.copy()
        d['password1'] = 'qwerasd'
        d['password2'] = 'qwerasdf'
        r = self.register(d)
        self.assertFormError(r, 'form', None, "The two password fields don't match.")

        d = self.post_data.copy()
        del d['password1']
        r = self.register(d)
        self.assertFormError(r, 'form', 'password1', "This field is required.")
        self.assertFormError(r, 'form', None, "The two password fields don't match.")

        d = self.post_data.copy()
        del d['password1']
        del d['password2']
        r = self.register(d)
        self.assertFormError(r, 'form', 'password1', "his field is required.")
        self.assertFormError(r, 'form', 'password2', "This field is required.")
        
