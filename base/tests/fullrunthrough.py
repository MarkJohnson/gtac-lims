import os
from django.test import TestCase
from django.test.client import Client
from gtac.models import Lab, Client as GTACClient, RegistrationWorkflow, EmailTrigger, Trigger, DEvent
from django.core.urlresolvers import reverse
from workflow import models as wf
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core import mail
	
class RegistrationTestCase(TestCase):

    def setUp(self):
        # Registration Workflow
        # Roles
        client_role = wf.Role.objects.create(name="Client")
        admin_role = wf.Role.objects.create(name="Admin")
        admin = User.objects.create_superuser('koebbe', 'brian@koebbe.org', 'foo')
        self.admin_role = admin_role
        # A NON-Registration Workflow
        dw = wf.Workflow.objects.create(name="Dummy Workflow" ,slug="dummy-workflow", created_by=admin)

        ds1 = wf.State.objects.create(name='Unverified', workflow=dw, is_start_state=True)
        ds2 = wf.State.objects.create(name='Unvalidated', workflow=dw)
        ds3 = wf.State.objects.create(name="Active", workflow=dw, is_end_state=True)

        dt1 = wf.Transition.objects.create(name="Verify", workflow=dw, from_state=ds1, to_state=ds2)
        dt2 = wf.Transition.objects.create(name="Validate", workflow=dw, from_state=ds2, to_state=ds3)

        ds1.roles.add(client_role)
        ds2.roles.add(admin_role)
        ds3.roles.add(admin_role)
        dt1.roles.add(client_role)
        dt2.roles.add(admin_role)

        dw.activate()
        dw.save()
        self.dw = dw
        
        self.rw = RegistrationWorkflow.objects.create(
            name="Registration Workflow",
            slug="registration-workflow",
            created_by=admin,
            )
        # State
        s1 = wf.State.objects.create(
            name='Unverified',
            workflow=self.rw,
            is_start_state=True,
            )
        s2 = wf.State.objects.create(
            name='Unvalidated',
            workflow=self.rw,
            )
        s3 = wf.State.objects.create(
            name="Active",
            workflow=self.rw,
            is_end_state=True,
            )
        
        # Transitions
        self.t1 = wf.Transition.objects.create(
            name="Verify",
            workflow=self.rw,
            from_state=s1,
            to_state=s2,
            )
        self.t2 = wf.Transition.objects.create(
            name="Validate", 
            workflow=self.rw, 
            from_state=s2, 
            to_state=s3,
            )

        s1.roles.add(client_role)
        s2.roles.add(admin_role)
        s3.roles.add(admin_role)
        self.t1.roles.add(client_role)
        self.t2.roles.add(admin_role)
        
        email1 = EmailTrigger()
        email1.subject="Hi there"
        email1.text = "Hi Text"
        email1.html = "Hi HTML"
        email1.site = Site.objects.all()[0]
        email1.state=s1
        email1.save()
        email1.to_roles.add(client_role)
        Trigger.objects.create(name="Verification Email", workflow=self.rw, state=s1, trigger_object=email1)
        
        self.de = DEvent.objects.create(
            name="Volume",
            type='int',
            description="Volume diluted",
            is_mandatory=True, 
            workflow=self.rw,
            state=s2,
            )
        self.de.roles.add(client_role)
        self.rw.activate()
        self.rw.save()
        self.admin = admin
     
    def test_0_register(self):
        lab = Lab.objects.create(name="foo lab")
        client =  GTACClient.objects.create_client(
            'brian@koebbe.org',
            'first_name',
            'last_name',
            'password',
            lab,
            '555-555-5555',
            alt_phone=None,
            )
        self.assertEquals(len(mail.outbox), 1)        
        client2 =  GTACClient.objects.create_client(
            'brian@koebbe2.org',
            'first_name2',
            'last_name2',
            'password',
            lab,
            '555-555-5555',
            alt_phone=None,
            )
        a = client.registration.activity
        a2 = client2.registration.activity

        a.progress(self.t1, client.user)
        a2.progress(self.t1, client2.user)

        for data in DEvent.objects.filter(state=a.current_state().state):
            data.log_data(a, '23423', client.user)

        for data in DEvent.objects.filter(state=a2.current_state().state):
            data.log_data(a2, '23423', client2.user)
            
        a.progress(self.t2, self.admin)
        a2.progress(self.t2, self.admin)

    
    def test_0_non_registration(self):
        # Make sure mucking with the registration workflow 
        # signal functions don't mess up the non-registration workflows
        wa = wf.WorkflowActivity.objects.create(workflow=self.dw, created_by=self.admin)
        p = wa.participants.create(user=User.objects.get(id=1))
        p.roles.add(self.admin_role)
        wa.start(self.admin)
        
