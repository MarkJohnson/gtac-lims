#!/usr/bin/env python
import string
import random
import os

fn = [n.strip() for n in open(os.path.join(os.path.dirname(__file__),'first_names'))]
ln = [n.strip() for n in open(os.path.join(os.path.dirname(__file__),'last_names'))]

def first(): return random.choice(fn)
def last():  return random.choice(ln)
def name():  return "%s %s" % (first(), last())
def phone(randfmt=False):
    formats = ['%d-%d-%d', '(%d) %d-%d', '%d.%d.%d']
    if randfmt:
        fmt = random.choice(formats)
    else:
        fmt = formats[0]
    
    ph = fmt % (random.randrange(100,1000), random.randrange(100,1000), random.randrange(1000,10000))

    return ph
    
def email():
    domain = random.choice(['com', 'edu', 'net'])
    name = "".join(random.sample(string.letters, 8))
    host = "".join(random.sample(string.letters, 8))
    return "%s@%s.%s" % (name, host, domain)

def password():
    return "".join(random.sample(string.letters+string.digits, 8))
