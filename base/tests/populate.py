#!/usr/bin/env python

import os
import sys
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
sys.path.append("/home/koebbe/prj/dj")
from gtac.models import Client, Lab
from django.contrib.auth.models import User
from randomness import *
from django.core.management import call_command

def cleanup():
    for u in User.objects.all():
        if u.id == 1: continue
        u.delete()
        
    for l in Lab.objects.all():
        l.delete()

def populate(x):
    for i in range(x):
        sys.stderr.write('%d\n' % i)
        if i % 10 == 0:
            lab = Lab.objects.create(name="%s Lab" % name())
        c = Client.objects.create_client(email(), first(), last(), password(), lab, phone())
        
def make():
    out = call_command('dumpdata', 'auth.user', 'gtac.client', 'gtac.lab', 'registration')

if __name__ == '__main__':
    cleanup()
    if len(sys.argv) > 1:
        count = sys.argv[1]
    else:
        count = 100
    populate(count)
    make()
