.. GTAC STS Prototype documentation master file, created by
   sphinx-quickstart on Wed Jul 20 10:04:54 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GTAC NextGen Sequencing Tracking System
=======================================

NextGen Sequencing Tracking System (the "STS") provides a
web-based, database-backed system for managing the NextGen
sequencing operations of the GTAC. Each step of the sequencing
process, from sample submission to publishing of the final analysis
results, are thoroughly documented in a database, and made
available for reporting, billing, and process analysis.

test

Contents:

.. toctree::
   :maxdepth: 2
   
   hardware
   development
   cluster
   code

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. automodule:: libprep.models 
   :members:

.. automodule:: seq.models 
   :members:

.. automodule:: sample.models 
   :members:

