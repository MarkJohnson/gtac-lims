================
STS Development
================

The GTAC STS was created using multple open-source software tools, languages, and practices.
To extend, enhance and gereally make the most of the STS, a general knowledge of the following components is necessary.

`Python Programming Language (v2.7) <http://docs.python.org>`_
    Nearly all code used in the STS is python.  The rather large `official python tutorial <http://docs.python.org/tutorial/index.html>`_ 
    is a good place to start.  Knocking on my door works well too.
     
`Django Web Framework (v1.3) <https://docs.djangoproject.com/en/1.3/>`_
    Django is the foundation of the STS.  Once you've set up your environment (see :ref:`virtualenv`) I recommend closely following the
    `Django Tutorial <https://docs.djangoproject.com/en/1.3/intro/tutorial01/>`_ so you have an understanding of the basic Django concepts.

`PostgreSQL (v8.4) <http://www.postgresql.org/docs/8.4/interactive/index.html>`_ and/or `SQLite (v3) <http://www.sqlite.org/>`_
    The production STS stores its data in PostgreSQL, but your development instance will most likely use SQLite.
    A `basic intro to SQL <http://www.w3schools.com/sql/sql_intro.asp>`_ may be all that you need.
    
`Mercurial <http://mercurial.selenic.com/guide/>`_
    All development changes are captured using the Mercurial (hg) distributed source control management tool.  
    The `quick start <http://mercurial.selenic.com/quickstart/>`_ and 
    `cheatsheets <http://mercurial.selenic.com/wiki/QuickReferenceCardsAndCheatSheets>`_ are probably all you 
    need to get going.
    

.. _quickstart:

Quick Start
-----------

1.  Make sure python-virtualenv is installed
2.  Build and activate your environment::
        
        $ mkdir ~/prj
        $ cd ~/prj
        ~/prj$ mkvirtualenv sts
        ~/prj$ workon sts
        (sts)~/prj$ 

3.  Clone your own GTAC STS source code repository::

        (sts)~/prj$ hg clone http://bitbucket.org/koebbe/sts sts-wc
        (sts)~/prj$ cd sts-wc
        (sts)~/prj/sts-wc$ hg clone http://bitbucket.org/koebbe/djmenu menus
        (sts)~/prj/sts-wc$ hg clone http://bitbucket.org/koebbe/sts-simpla simpla  *PRIVATE*

4.  Install your python and django packages::

        (sts)~/prj/sts-wc$ pip install -r requirements.txt
    
    .. note:: SQLite3 developement libraries will need to be installed to build pysqlite
    
5.  Build your development database::

        (sts)~/prj/sts-wc$ ./manage.py syncdb 
        (sts)~/prj/sts-wc$ ./manage.py migrate 
    
6.  Start up your GTAC STS website::

        (sts)~/prj/sts-wc$ ./manage.py runserver 
        
7.  Browse to http://localhost:8000/

8.  ...

9.  **Profit!**

.. _virtualenv:

Setting up your virtual environment
-----------------------------------

When working on extending/enhancing the STS, having a consistent and dependable STS 
development environment is critical.  The following is a quick explanation of how to set one up.

virtualenv
^^^^^^^^^^

The following is an explanation of virtualenv from its `website <http://www.virtualenv.org/>`_...

``virtualenv`` is a tool to create isolated Python environments.

The basic problem being addressed is one of dependencies and versions,
and indirectly permissions.  Imagine you have an application that
needs version 1 of LibFoo, but another application requires version
2.  How can you use both these applications?  If you install
everything into ``/usr/lib/python2.7/site-packages`` (or whatever your
platform's standard location is), it's easy to end up in a situation
where you unintentionally upgrade an application that shouldn't be
upgraded.

Or more generally, what if you want to install an application *and
leave it be*?  If an application works, any change in its libraries or
the versions of those libraries can break the application.

Also, what if you can't install packages into the global
``site-packages`` directory?  For instance, on a shared host.

In all these cases, ``virtualenv`` can help you.  It creates an
environment that has its own installation directories, that doesn't
share libraries with other virtualenv environments (and optionally
doesn't access the globally installed libraries either).

The basic usage is::

    $ virtualenv ENV

This creates ``ENV/lib/pythonX.X/site-packages``, where any libraries you
install will go.  It also creates ``ENV/bin/python``, which is a Python
interpreter that uses this environment.  Anytime you use that interpreter
(including when a script has ``#!/path/to/ENV/bin/python`` in it) the libraries
in that environment will be used.

It also installs `Setuptools <http://peak.telecommunity.com/DevCenter/setuptools>`_into the environment.

A new virtualenv also includes the `pip <http://pypy.python.org/pypi/pip>`_
installer, so you can use `ENV/bin/pip`` to install additional packages into
the environment.


