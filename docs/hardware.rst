==================
STS Hardware
==================

The GTAC STS is currently hosted on a CGS Dell PowerEdge M600 with 16GB RAM.  

The software itself is stored on 1 Unit (4.6TB, RAID 10) of CGS Storage and a full backup is performed daily.  

The server is accessible to members of GTAC via the CGS Cluster using ssh::

    ssh gtac.wustl.edu
