========================
Pyhton access to the STS
========================

*Python sccess to STS python models is now available from the CGS Cluster!*

An STS python virtual environment has been prepared on the cluster which will facilitate access to the STS by scripts running on sandboxes or jobs running on the cluster.  This virtualenv includes all the python modules necessary to use the Django API to get to the STS data.

The virtual environment is located in: /srv/gtac/sts/envs/sts-cluster

Quick and Dirty Hard Coding
---------------------------

The easiest way to use this environemnt is by using the following for the top of your python scripts... let's call it 'example.py'::

    #!/srv/gtac/sts/envs/sts-cluster/bin/python

    import os
    import sys
    os.environ['DJANGO_SETTINGS_MODULE']='settings'
    sys.path.append('/srv/gtac/sts/sts')


More Portable using the 'workon' method
---------------------------------------

To make the script more portable and future-proof you could instead load up the environment before running the script::

    $ export WORKON_HOME=/srv/gtac/sts/envs
    $ workon sts-cluster

Then your python programs only need the usual line::

    #!/usr/bin/env python

Using this method you can also get to the STS from the python CLI.  For example, to grab the latest submission::

    $ python
    Python 2.7.2+ (default, Oct  4 2011, 20:06:09) 
    [GCC 4.6.1] on linux2
    Type "help", "copyright", "credits" or "license" for more information.
    >>> from sample import models
    >>> s = models.Submission.objects.order_by('-submitted')[0]

Manuevering Around the Models
-----------------------------

You can use http://gtac.wustl.edu/sts/admin/doc/models to help you manuever around the data.
For example, let's try and make a report showing the number of samples each client has submitted.

1.  Grab the Clients

    http://gtac.wustl.edu/sts/admin/doc/models shows that the 'client' model is in 'base'.  So::

        from base import models as basemodels
        
        clients = basemodels.Client.objects.all()

2.  Follow the trail from Client to Sample

    Browsing around http://gtac.wustl.edu/sts/admin/doc/models/sample.submission we see that there are 1 or more 'samples' ('sample_set.all()') per 'submission' and that there is one client per submission.

    From this page we can follow the trail to 'client' by clicking on 'base.Client'.  From here we see that 'client' has access to 'submissions' via 'submission_set.all()'

    Now we can grab a client's submissions::

        for client in clients:
            clients_submissions = client.submission_set.all()



