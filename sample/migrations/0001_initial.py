# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sample.models
import django.utils.timezone
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
                ('client', models.ForeignKey(help_text=b'', to='base.Client')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sample',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('type', models.CharField(default=b'new_sample', help_text=b'', max_length=255, choices=[(b'new_sample', b'New Sample'), (b'reprep_sample', b'Re-prep Sample'), (b'self_prepped', b'Self-prepped Sample'), (b'new_prepped_for_sequencing', b'New prepped-for-sequencing'), (b'resequence', b'Resequence Sample')])),
                ('group', models.CharField(blank=True, max_length=1, null=True, choices=[(b'', b'----'), (b'A', b'A'), (b'B', b'B'), (b'C', b'C'), (b'D', b'D'), (b'E', b'E'), (b'F', b'F'), (b'G', b'G'), (b'H', b'H'), (b'I', b'I'), (b'J', b'J'), (b'K', b'K')])),
            ],
            options={
                'ordering': ['-submission__submitted', 'specimen__gtac_id'],
                'get_latest_by': 'submission__submitted',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Species',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'', unique=True, max_length=255)),
                ('image', models.ImageField(help_text=b'', null=True, upload_to=b'species/', blank=True)),
            ],
            options={
                'verbose_name_plural': 'Species',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Specimen',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(help_text=b'', max_length=255)),
                ('submitted', models.DateTimeField(null=True, blank=True)),
                ('loading_concentration', models.CharField(help_text=b'', max_length=255, null=True, blank=True)),
                ('submitted_concentration', models.CharField(help_text=b'', max_length=255, null=True, blank=True)),
                ('submitted_volume', models.CharField(help_text=b'', max_length=255, null=True, blank=True)),
                ('well_id', models.CharField(help_text=b'', max_length=5, null=True, blank=True)),
                ('slug', django_extensions.db.fields.AutoSlugField(editable=False, populate_from=b'name', blank=True, overwrite=True)),
                ('self_prepped', models.BooleanField(default=False)),
                ('gtac_id', models.IntegerField(null=True)),
                ('organism', models.ForeignKey(blank=True, to='sample.Species', help_text=b'')),
            ],
            options={
                'ordering': ['-submitted'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Submission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('submitted', models.DateField(help_text=b'', null=True, blank=True)),
                ('partial_prep', models.BooleanField(default=False)),
                ('description', models.TextField(help_text=b'', null=True, blank=True)),
                ('client', models.ForeignKey(help_text=b'', to='base.Client')),
                ('project', models.ForeignKey(help_text=b'', to='sample.Project')),
            ],
            options={
                'ordering': ['-submitted'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SubmissionAttachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('name', models.CharField(max_length=255)),
                ('attachment', models.FileField(upload_to=sample.models.get_submission_attachment_path)),
                ('submission', models.ForeignKey(related_name='attachments', to='sample.Submission')),
            ],
            options={
                'ordering': ('-created',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SubmissionNote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now_add=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, help_text=b'', auto_now=True)),
                ('key', django_extensions.db.fields.UUIDField(help_text=b'', editable=False, blank=True)),
                ('note', models.TextField()),
                ('author', models.ForeignKey(to='core.Person')),
                ('submission', models.ForeignKey(related_name='notes', to='sample.Submission')),
            ],
            options={
                'ordering': ('-created',),
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='specimen',
            name='submissions',
            field=models.ManyToManyField(help_text=b'', related_name='specimens', through='sample.Sample', to='sample.Submission'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='specimen',
            name='submitted_by',
            field=models.ForeignKey(related_name='submitted_specimens', blank=True, to='base.Client', help_text=b'', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sample',
            name='specimen',
            field=models.ForeignKey(help_text=b'', to='sample.Specimen'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sample',
            name='submission',
            field=models.ForeignKey(help_text=b'', to='sample.Submission'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='project',
            unique_together=set([('name', 'client')]),
        ),
    ]
