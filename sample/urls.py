from django.conf.urls import *

urlpatterns = patterns('',
    url(r'^note/$', 'sample.views.submission_note', name='submission-note' ),
    url(r'^browse/$', 'sample.views.submission_browse', name='submission-browse' ),
    url(r'^sub/(?P<id>\d+)/$', 'sample.views.submission_detail', name='submission-detail'),
    url(r'^sub/(?P<id>\d+)/notes/$', 'sample.views.submission_note_list', name='submission-note-list' ),
    url(r'^sample_json/$', 'sample.views.sample_json', name='sample-json'),
    url(r'^submission_json/$', 'sample.views.submission_json', name='submission-json'),
    url(r'^samplesubmission_json/$', 'sample.views.samplesubmission_json', name='samplesubmission-json'),
    url(r'^96well_submission.csv$', 'sample.views.submission96_csv', name='submission-96-csv' ),
    url(r'^sample/browse/$', 'sample.views.sample_browse', name='sample-browse'),
    url(r'^sample/(?P<id>\d+)/$','sample.views.sample_details', name='sample-details'),
    url(r'^submission_report/$','sample.views.submission_report', name='submission-report'),
    url(r'^client/(?P<id>\d+)/$','sample.views.getICTS_research_data', name='sample-ICTS-reserch-data'),
    )

