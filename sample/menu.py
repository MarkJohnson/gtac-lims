import menus

menus.named_view_factory('submit', "Submit", '[Sample] Submit')
menus.named_view_factory('specimen-submit', "Specimen Submit", '[Sample] Specimen Submit')
menus.named_view_factory('nextgen-submit', "NextGen Submit", '[Sample] NextGen Submit')
menus.named_view_factory('submission-browse', "Submissions", '[Sample] Submission Browse')
menus.named_view_factory('sample-browse', "Sample Browse", '[Sample] Sample Browse')
