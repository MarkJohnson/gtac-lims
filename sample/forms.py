from django import forms
from django.forms.models import BaseModelFormSet
from django.forms.models import inlineformset_factory, modelformset_factory
from django.forms.formsets import formset_factory
from sample import models as sampmodels
from sample import util
from libprep import models as lpmodels
from seq import models as seqmodels
from analysis import models as anamodels
from base import models as basemodels
from billing import models as billmodels
import datetime
import logging

logger = logging.getLogger('myproject.custom')

GROUP_CHOICES = [('', '----'), ] + [(x,x) for x in "ABCDEFGHIJK"]

class SubmissionNoteForm(forms.ModelForm):
    class Meta:
        model = sampmodels.SubmissionNote
        exclude = ('author', )
        widgets = {
            'submission': forms.HiddenInput(),
        }

class SubmissionBillingForm(forms.ModelForm):
    class Meta:
        model = billmodels.SubmissionBillingDetail
        exclude = ('submission', 'purchase_order')

# class SampleBillingForm(forms.ModelForm):
    # class Meta:
        # model = billmodels.SampleBillingDetail
        # exclude = ('sample', 'purchase_order', 'billed_lab', 'association', 'association_grant', 'libprep_code')

# SampleBillingFormSet = modelformset_factory(billmodels.SampleBillingDetail, form=SampleBillingForm)
SampleBillingAssociationFormSet = modelformset_factory(billmodels.SampleBillingAssociation, fields='__all__')

STATUS_CHOICES=[('Approved','Approved'),
        ('Exempt','Exempt'),
        ('Submitted','Submitted')]


class SampleICTSAssociationForm(forms.ModelForm):
    approval_status = forms.ChoiceField(choices=STATUS_CHOICES, widget=forms.RadioSelect())    
    research = forms.ModelMultipleChoiceField(queryset=billmodels.ICTSResearch.objects.all().order_by('id'), widget=forms.CheckboxSelectMultiple(), required="False") 
    
    class Meta:
        model = billmodels.SampleICTSAssociation
        exclude = ('sampleBillingAssociation',)

#    def clean(self):
#        cdata = self.cleaned_data
#        if 'approval_status' in cdata and cdata['approval_status'] == 'Approved':
#            if not cdata['active_protocol_title']:
#                self._errors['active_protocol_title'] = self.error_class(['This field is required'])
#            if not cdata['active_number']:
#                self._errors['active_number'] = self.error_class(['This field is required'])
#            if not cdata['approval_date']:
#                self._errors['approval_date'] = self.error_class(['This field is required'])
#        return cdata
    
        
SampleICTSAssociationFormSet = modelformset_factory(billmodels.SampleICTSAssociation, form=SampleICTSAssociationForm)

class UploadForm(forms.Form):
    file = forms.FileField(label="Upload Sample Data")

    def clean(self):
        if not self.cleaned_data.get('file', None):
            return self.cleaned_data
        f = self.cleaned_data.get('file')
        self.specimens,errors = util.specimens_from_csv(f)
        if errors:
            self._errors["file"] = self.error_class(errors)
        
        return self.cleaned_data

    def specs(self):
        return self.specimens

class SubmitDetailForm(forms.Form):
    client = forms.ModelChoiceField(basemodels.Client.objects.all())
    is_selfprepped = forms.BooleanField(
        required=False, 
        label='Are Samples self-prepped?', 
        widget=forms.Select(choices=((True,'Self prepped'),(False,'Unprepped')))
        )
    will_upload = forms.BooleanField(
        required=False, 
        label='Upload Sample data using provided spreadsheet?'
        )

class NextGenSpecimenForm(forms.ModelForm):
    group = forms.ChoiceField(required=False, choices=GROUP_CHOICES)

    class Meta:
        model = sampmodels.Specimen
        fields = ('group', )
        
NextGenSpecimenFormSet = modelformset_factory(sampmodels.Specimen, form=NextGenSpecimenForm)

class NextGenSampleFormSet(forms.Form):
    pass


index_choices = (('None', 'None'), ('index', 'Index'), ('dual index', 'Dual Index'), ('barcoded', 'Barcode'))

class SubmissionForm(forms.Form):
    client = forms.ModelChoiceField(basemodels.Client.objects.all())
    department = forms.CharField(
        label="Department to be Billed", 
        required=False, help_text="foo",
        )
    fund = forms.CharField(label="Fund #", required=False)
    po_num = forms.CharField(label="PO#", required=False)
    po_file = forms.FileField(label="Upload PO#", required=False)
    prep_type = forms.ModelChoiceField(lpmodels.PrepType.active.all(), required=False, empty_label="None - Direct to Sequencing", label="Prep Type")
    run_type = forms.ModelChoiceField(seqmodels.RunType.active.all(), label="Run Type", required=False)
    multiplexing = forms.ChoiceField(choices=index_choices, required=False)
    lanes_per_sample = forms.IntegerField(label="Number of Lanes per Sample", initial=1, min_value=1,required=False)
    phix_spike_per_sample = forms.IntegerField(label="PhiX Spike-in per Sample", initial=1, min_value=0,required=False, widget=forms.TextInput)
    partial_prep = forms.BooleanField(required=False)
    dts_preptype = forms.CharField(label="Direct to Sequencing Prep Type", required=False)
    index_length_per_sample = forms.IntegerField(label="Index Length per Sample", initial=0, min_value=0, required=False, widget=forms.TextInput)
    contact = forms.ModelMultipleChoiceField(queryset=basemodels.Client.objects.all(), widget=forms.SelectMultiple(attrs={'class': 'chosen-select'}), required=False)

    samples_per_lane = forms.IntegerField(
        label="Number of Indexed Samples per Lane", 
        initial=1, 
        min_value=1,
        required=False,
        widget=forms.TextInput,
        )

    lanes_per_pool = forms.IntegerField(
        label="Number of Lanes per Pooled Samples", 
        initial=1, 
        min_value=1,
        required=False,
        widget=forms.TextInput,
        )
  
    primer = forms.CharField(required=False, label="Primer")
 
    notes = forms.CharField(widget=forms.Textarea, required=False)
    upload = forms.BooleanField(required=False, label='Upload Spreadsheet')
    #file = forms.FileField(label="Upload Sample Data", required=False)
    analysis_species = forms.ModelChoiceField(anamodels.Species.active.all(), required=False, label="Species")
    analysis_pipeline = forms.ModelChoiceField(anamodels.Pipeline.active.all(), required=False, label="Analysis Type")
    analysis_manual = forms.BooleanField(label="Manual", required=False)
    analysis_notes = forms.CharField(widget=forms.Textarea(attrs={'cols': 40, 'rows': 5}), required=False,)

    def save_submission(self):
        now = datetime.datetime.now()
        sub = sampmodels.Submission.objects.create(
            client=self.cleaned_data['client'],
            submitted=now,
            partial_prep=self.cleaned_data['partial_prep'],
            project=sampmodels.Project.objects.get_or_create(
                        name=now.date(), 
                        client=self.cleaned_data['client'],
                        )[0],
            )
        if self.cleaned_data['notes']:
            sub.notes.create(author=self.cleaned_data['client'].person,note=self.cleaned_data['notes'])
        return sub
    
    def clean(self):
        if not self.cleaned_data['partial_prep']:
            if not self.cleaned_data['run_type']:
                self._errors['run_type'] = self.error_class(['This field is required'])
        
        return self.cleaned_data

    def save(self):
        return self.save_submission()

class SpecimenForm(forms.ModelForm):
    organism = forms.CharField(required=False)
    group = forms.ChoiceField(required=False, choices=GROUP_CHOICES)
       
    class Meta:
        model = sampmodels.Specimen 
        exclude = ('submitted_from', 'submissions')

    def clean_organism(self):
        if self.cleaned_data['organism']:
            try:
                organism = sampmodels.Species.objects.get(name__iexact=self.cleaned_data['organism'])
            except sampmodels.Species.DoesNotExist:
                organism = sampmodels.Species.objects.create(name=self.cleaned_data['organism'])
        else:
            organism = None
        return organism

class SelfPreppedForm(forms.Form):
    name = forms.CharField()
    organism = forms.CharField()
    submitted_concentration = forms.CharField()
    loading_concentration = forms.CharField()
    
    def spec(self):
        cd = self.cleaned_data
        try:
            organism = sampmodels.Species.objects.get(
                           name__iexact=cd['organism']
                           )
        except sampmodels.Species.DoesNotExist:
            organism = sampmodels.Species.objects.create(
                           name=cd['organism']
                           )

        self.specimen = sampmodels.Specimen(
                            name=cd['name'],
                            self_prepped=True,
                            organism=organism,
                            loading_concentration=cd['loading_concentration'],
                            submitted_concentration=cd['submitted_concentration'],
                            )
        return self.specimen

    def save_sequencingsample(self, sampleprepdetail):
        seqsample = lpmodels.SequencingSample()
        seqsample.name = "Direct Sample: %d" % sampleprepdetail.sample.id
        seqsample.loading_concentration = self.cleaned_data['loading_concentration']
        seqsample.submitted_concentration = self.cleaned_data['submitted_concentration']
        seqsample.save()
        seqsampledetail = lpmodels.SequencingSampleDetail.objects.create(sampleprepdetail=sampleprepdetail, sequencingsample=seqsample)
        return seqsample

SelfPreppedFormSet = formset_factory(SelfPreppedForm, extra=8)

class RequiredFormSet(BaseModelFormSet):
    def __init__(self, *args, **kwargs):
        super(RequiredFormSet, self).__init__(*args, **kwargs)
        self.forms[0].empty_permitted = False
         
SpecimenFormset = modelformset_factory(sampmodels.Specimen, form=SpecimenForm, formset=RequiredFormSet, extra=8)
AttachmentFormset = inlineformset_factory(sampmodels.Submission,sampmodels.SubmissionAttachment, can_delete=False, extra=3, fields='__all__')

class SpecimenSelectionForm(forms.Form):
    specimens = forms.ModelMultipleChoiceField(sampmodels.Specimen.objects.order_by('-submitted'))

class ChangeSubmissionTypeForm(forms.Form):
    prep_type = forms.ModelChoiceField(lpmodels.PrepType.active.filter(preptypeversion__is_active=True).distinct(), label="Prep Type")
    run_type = forms.ModelChoiceField(seqmodels.RunType.active.all(), label="Run Type")
