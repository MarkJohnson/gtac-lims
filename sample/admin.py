from django.contrib import admin
from core.admin import CoreModelAdmin
import models
from models import Project,Submission,Specimen,Sample,Species

class SampleInline(admin.TabularInline):
    model = models.Sample
    raw_id_fields = ['specimen', ]

class SpecimenInline(admin.TabularInline):
    model = models.Specimen

class SubmissionInline(admin.TabularInline):
    model = models.Submission
    exclude = ('description',)

class SubmissionAttachmentInline(admin.TabularInline):
    model = models.SubmissionAttachment
    extra = 0

class SubmissionNoteInline(admin.TabularInline):
    model = models.SubmissionNote
    extra = 0

@admin.register(Project)
class ProjectAdmin(CoreModelAdmin):
    list_display = ('__unicode__', 'client', 'created')
    list_filter = ['client']
    search_fields = ['name', 'client__person__first_name', 'client__person__last_name']
    inlines = [ SubmissionInline ]

@admin.register(Submission)
class SubmissionAdmin(CoreModelAdmin):
    list_display = ('__unicode__', 'client', 'submitted')
    list_filter = ['created']
    search_fields = ['name', 'client__person__first_name', 'client__person__last_name',]
    inlines = [ SampleInline, SubmissionAttachmentInline, SubmissionNoteInline, ]

@admin.register(Specimen)
class SpecimenAdmin(CoreModelAdmin):
    def submitted_by(self, obj):
        return obj.submitted_from.client.name
    list_display = ('__unicode__', 'organism', 'submitted_by', 'created')
    list_filter = ['organism', 'created']
    search_fields = ['name', 'organism__name']
    list_editable = ('organism',)

@admin.register(Sample)
class SampleAdmin(CoreModelAdmin):
    list_display = ('specimen', 'created')
    list_filter = ['submission__client', 'created']

@admin.register(Species)
class SpeciesAdmin(admin.ModelAdmin):
    list_display = ('__unicode__',)


