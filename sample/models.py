from django.db import models, transaction
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from core import models as coremodels
from base import models as basemodels
from django_extensions.db.fields import AutoSlugField
from django.conf import settings
from itertools import groupby
from operator import itemgetter

import os
import uuid

def pretty_integer_list(nums):
    # Modified from http://stackoverflow.com/questions/9847601/convert-list-of-numbers-to-string-ranges
    nums.sort()
    str_list = []
    for k, g in groupby(enumerate(nums), lambda (i,x):i-x):
       ilist = map(itemgetter(1), g)
       if len(ilist) > 1:
          str_list.append('%d-%d' % (ilist[0], ilist[-1]))
       else:
          str_list.append('%d' % ilist[0])
    pretty_list = ','.join(str_list)

    return pretty_list

class Project(coremodels.CoreModel):
    name = models.CharField(max_length=255, help_text='')
    client = models.ForeignKey(basemodels.Client, help_text='')
    
    def __unicode__(self):
        return u'%s' % (self.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('project-detail', [str(self.id)])
        
    class Meta:
        unique_together = ('name', 'client')

class Submission(coremodels.CoreModel):
    project = models.ForeignKey(Project, help_text='')
    client = models.ForeignKey(basemodels.Client, help_text='')
    submitted = models.DateField(blank=True, null=True, help_text='')
    partial_prep = models.BooleanField(default=False)
    description = models.TextField(blank=True, null=True, help_text='')  

    def __unicode__(self):
        return u'%s: %s' % (self.project, self.submitted.strftime('%Y-%m-%d'))
    
    def preptypes(self):
        return [ss.libprep.preptype for ss in self.sample_set.all()]

    class Meta:
        ordering = ['-submitted',]
                
    @models.permalink
    def get_absolute_url(self):
        return ('submission-detail', [str(self.id)])

def get_submission_attachment_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('submission/attachments', filename)

class SubmissionAttachment(coremodels.CoreModel):
    submission = models.ForeignKey(Submission, related_name="attachments")
    name = models.CharField(max_length=255)
    attachment = models.FileField(upload_to=get_submission_attachment_path,)

    def __unicode__(self):
        return u'%s' % (self.name)
    
    class Meta:
        ordering = ('-created', )

class SubmissionNote(coremodels.CoreModel):
    submission = models.ForeignKey(Submission, related_name="notes")
    author = models.ForeignKey(coremodels.Person)
    note = models.TextField()

    class Meta:
        ordering = ('-created', )

class Species(models.Model):
    name = models.CharField(max_length=255, unique=True, help_text='')
    image = models.ImageField(blank=True, null=True, upload_to='species/', help_text='')
    
    def __unicode__(self):
        return u'%s' % (self.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('species-view', [str(self.id)])
        
    class Meta:
        verbose_name_plural = "Species"
        pass

class SpecimenManager(models.Manager):
    def pretty_gtac_ids(self):
        qs = self.get_queryset()
        gtac_ids = []
        for s in qs:
            gtac_ids.append(s.gtac_id)
        return pretty_integer_list(gtac_ids)

class Specimen(coremodels.CoreModel):
    name = models.CharField(max_length=255, help_text='')
    submissions = models.ManyToManyField(Submission, through="Sample", related_name="specimens", help_text='')
    submitted = models.DateTimeField(blank=True, null=True)
    submitted_by = models.ForeignKey(basemodels.Client, related_name="submitted_specimens", blank=True, null=True, help_text='')
    organism = models.ForeignKey(Species, blank=True, help_text='')
    loading_concentration = models.CharField(max_length=255, blank=True, null=True, help_text='')
    submitted_concentration = models.CharField(max_length=255, blank=True, null=True, help_text='')
    submitted_volume = models.CharField(max_length=255, blank=True, null=True, help_text='')
    well_id = models.CharField(max_length=5,blank=True, null=True, help_text='')
    slug = AutoSlugField(populate_from='name', overwrite=True)
    self_prepped = models.BooleanField(default=False)
    gtac_id = models.IntegerField(null=True)

    objects = SpecimenManager() 
    def __unicode__(self):
        return u'%s' % (self.name)

    @models.permalink
    def get_absolute_url(self):
        return ('specimen-detail', [str(self.id)])
        
    class Meta:
        ordering = ['-submitted',]
        pass

    def save(self):
        if not self.submitted_volume:
            self.submitted_volume = None
        if not self.loading_concentration:
            self.loading_concentration = None
        if not  self.well_id:
            self.well_id = None
        super(Specimen, self).save()
        self.gtac_id = self.id + settings.GTAC_ID_OFFSET 
        super(Specimen, self).save()
        
GROUP_CHOICES = [('', '----'), ] + [(x,x) for x in "ABCDEFGHIJK"]
class Sample(coremodels.CoreModel):
    SUBMISSION_TYPES = (
        ('new_sample','New Sample'),
        ('reprep_sample','Re-prep Sample'),
        ('self_prepped', 'Self-prepped Sample'),
        ('new_prepped_for_sequencing', 'New prepped-for-sequencing'),
        ('resequence','Resequence Sample'),
        )
    specimen = models.ForeignKey(Specimen, help_text='')
    submission = models.ForeignKey(Submission, help_text='')
    type = models.CharField(max_length=255, default="new_sample", choices=SUBMISSION_TYPES, help_text='')
    group = models.CharField(max_length=1, choices=GROUP_CHOICES, blank=True, null=True)
    
    def __unicode__(self):
        return u'%s' % (self.specimen.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('sample-view', [str(self.id)])
        
    class Meta:
        get_latest_by = "submission__submitted"
        ordering = ['-submission__submitted', 'specimen__gtac_id']
        pass

