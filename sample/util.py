from base.util.gtacemail import GTACEmail
from django.template.loader import render_to_string
from django.contrib.sites.models import Site
import csv
from sample import models as sampmodels

def send_submission_response(submission):
    subject = 'Submission Received'
    title = "Submission Received"
    from_email = 'GTAC <noreply@gtac.wustl.edu>'
    to = submission.user.email
    text = render_to_string('sample/submission_followup.txt', {
        'submission': submission,
        'site': "http://%s" % (Site.objects.get_current().domain),
        })
    msg = GTACEmail(subject, text, from_email, [to], title=title)
    msg.send()
    return


def send_submission_notification(submission):
    subject = 'New submission received'
    from_email = 'GTAC <noreply@gtac.wustl.edu>'
    to = 'tsinnwell@path.wustl.edu'
    text = render_to_string('sample/submission_notification.txt', {
        'submission': submission,
        'site': "http://%s" % (Site.objects.get_current().domain),
        })
    msg = GTACEmail(subject, text, from_email, [to])
    msg.send()
    return
    

def specimens_from_csv(fobj):
    specimens = []
    errors = []
    tmp_fn = fobj.temporary_file_path()
    tmp_fh = open(tmp_fn, 'rU')
    dialect = csv.Sniffer().sniff(tmp_fh.read(1024))
    tmp_fh.seek(0)
    c = csv.DictReader(tmp_fh, dialect=dialect)
    
    for l in c:
        try:
            x = {key.lower().lstrip().rstrip():value.lstrip().rstrip() for (key, value) in l.iteritems()}
            if not x['sample name']: continue
            if not x['organism']: 
                errors.append("Need an organism for well %s" % x['plate well id'])
                continue
            if not x['volume ul']: 
                errors.append("Need a volume for well %s" % x['plate well id'])
                continue
            if not x['concentration ng/ul']: 
                errors.append("Need a concentration for well %s" % x['plate well id'])
                continue
            try:
                species = sampmodels.Species.objects.get(name__iexact=x['organism'])
            except sampmodels.Species.DoesNotExist:
                species = sampmodels.Species.objects.create(name=x['organism'])

            s = sampmodels.Specimen(
                name=x['sample name'],
                organism=species,
                submitted_concentration=x['concentration ng/ul'],
                submitted_volume=x['volume ul'],
                well_id=x['plate well id'],
                )
            #specimens.append((s,l['Group (A-K)']))
            specimens.append(s)
        except Exception, m:
            errors.append(m)

    tmp_fh.close() 
    return (specimens, errors)
