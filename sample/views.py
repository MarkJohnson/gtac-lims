from __future__ import absolute_import
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, render_to_response, get_object_or_404
from django.contrib import messages
from django.core.urlresolvers import reverse
from sample import models as sampmodels
from libprep import models as lpmodels
from seq import models as seqmodels
from analysis import models as anamodels 
from core import models as coremodels
from billing import models as billmodels
from base import models as basemodels
from base import views as baseviews
from sample import forms
from base.views import client_or_admin_required
from base.util import grid, get_database_records
from django.db import transaction
#from libprep.util import libraryprepexcel
from django.core import serializers
from xml.etree.ElementTree import Element, SubElement, tostring
from django.core import serializers
from django.conf import settings
from django.db.models import Count
from django.db.models import Min
from django.db.models import Q
from util import file_manager as filemng
import json
import csv
import datetime
import string
import logging
from datetime import timedelta


logger = logging.getLogger('myproject.custom')

@login_required
def sample_json(request):
    spd_id = request.GET.get('sampleprepdetail_id', None)
    id = request.GET.get('id', None)
    json_d = {}
    if spd_id:
      json_d['name'] = sampmodels.Sample.objects.get(libprep__id=spd_id).specimen.name
      #qs = sampmodels.Sample.objects.select_related().all().filter(libprep__id=spd_id)
    else:
      #qs = sampmodels.Sample.objects.select_related().all().filter(id=id)
      json_d['name'] = sampmodels.Sample.objects.get(id=id).specimen.name
    #data = serializers.serialize('json', qs)
    #return HttpResponse(data, content_type="application/json")
    return HttpResponse(json.dumps(json_d), content_type="application/json")

@login_required
def submission_json(request):
    id = request.GET.get('id', None)
    qs = sampmodels.Submission.objects.select_related().all().filter(id=id)
    data = serializers.serialize('json', qs)
    return HttpResponse(data, content_type="application/json")

@login_required
def samplesubmission_json(request):
    id = request.GET.get('id', None)
    qs = sampmodels.Sample.objects.all().filter(submission__id=id)
    data = serializers.serialize('json', qs)
    return HttpResponse(data, content_type="application/json")


def getICTS_research_data(request, id):
    client = get_object_or_404(basemodels.Client, id=id)
    billing = billmodels.SampleBillingAssociation.objects.filter(submissionbillingdetail__submission__client=client,
                                                    billing_association__isnull=False).order_by('-id')

    resp = {"empty": 'true'};
    if billing:
        icts =  billing[0].billing_association.all()[0]
        resp = { 
            "investigator_first_name": icts.investigator_first_name,
            "investigator_last_name": icts.investigator_last_name,
            "fund_grant_number": icts.fund_grant_number,
            "funding_agency": icts.funding_agency,
            "grant_title": icts.grant_title,
            "grant_pi_first_name": icts.grant_pi_first_name,
            "grant_pi_last_name": icts.grant_pi_last_name,
            "approval_status": icts.approval_status,
            "active_protocol_title": icts.active_protocol_title,
            "active_number": icts.active_number,
            "approval_date": icts.approval_date.strftime('%m/%d/%Y') if icts.approval_date else '',
            "iacuc_number": icts.iacuc_number,
            "iacuc_approval_date": icts.iacuc_approval_date.strftime('%m/%d/%Y') if icts.iacuc_approval_date else '',
            "research": [r.id for r in  icts.research.all()]
        }

    return HttpResponse(json.dumps(resp), content_type="application/json" )


@login_required
def nextgen_submit(request):
    if request.method == 'POST':
        with transaction.atomic():
            #
            #  Get all our forms:
            #  1.  Submission Form
            #  2.  Billing Form
            #  3.  Billing Association FormSet
            #  4.  Sample  (one of the following)
            #  5.  Attachment FormSet
            #  6.  Analysis Contact FormSet
            #
            submissionform = forms.SubmissionForm(request.POST, request.FILES, prefix='sub')
            billform = forms.SubmissionBillingForm(request.POST, prefix='bill')
            billassocformset = forms.SampleBillingAssociationFormSet(request.POST, request.FILES,
                                                                     queryset=billmodels.SampleBillingAssociation.objects.none(),
                                                                     prefix='billassoc')
            attachmentformset = forms.AttachmentFormset(request.POST, request.FILES, prefix='att')
            specform = forms.SpecimenSelectionForm(request.POST, prefix='spec')
            ictsassocformset = forms.SampleICTSAssociationFormSet(request.POST, prefix='ictsassoc')

            #
            # Check the validity of all the forms
            #
            is_valid = True
            is_valid &= submissionform.is_valid()
            is_valid &= billform.is_valid()
            is_valid &= billassocformset.is_valid()
            is_valid &= attachmentformset.is_valid()
            is_valid &= specform.is_valid()

            if is_valid:
                submission = submissionform.save()
                #
                # Step 2: Save Samples
                #
                samples = []
                direct_to_seq = submissionform.cleaned_data['prep_type'] is None

                for specimen in specform.cleaned_data['specimens']:
                    sample = sampmodels.Sample()
                    sample.submission = submission
                    sample.specimen = specimen
                    sample.group = 'A'
                    sample.save()
                    samples.append(sample)
                #
                # save attachments
                #
                for att in attachmentformset.save(commit=False):
                    att.submission = submission
                    att.save()

                #
                # Make libprep, seq, and analysis entries for each sample
                #
                now = datetime.datetime.now()

                status_name = 'Seq Queue' if direct_to_seq else 'New'
                lpstatus = lpmodels.PrepStatus.objects.get(name=status_name)
                seqnew = seqmodels.SequencingStatus.objects.get_or_create(name="New")[0]
                anad_list = []
                for sample in samples:
                    spd = lpmodels.SamplePrepDetail.objects.create(
                        sample=sample,
                        complete=direct_to_seq,
                        name=sample.specimen.name,
                        preptype=submissionform.cleaned_data['prep_type'],
                        multiplex_type=submissionform.cleaned_data['multiplexing'],
                        status=lpstatus,
                        index_length=submissionform.cleaned_data['index_length_per_sample'],
                        dts_preptype=submissionform.cleaned_data['dts_preptype'],
                        )
                    num_lanes = None
                    phix_spike = None
                    if (submissionform.cleaned_data['prep_type'] is None):
                        num_lanes = submissionform.cleaned_data['lanes_per_sample']
                        phix_spike = submissionform.cleaned_data['phix_spike_per_sample']
                    else:
                        num_lanes = submissionform.cleaned_data['lanes_per_pool']

                    if submissionform.cleaned_data['run_type']:
                        sseqd = seqmodels.SampleSequencingDetail.objects.create(
                                sample=sample,
                                runtype=submissionform.cleaned_data['run_type'],
                                num_lanes=num_lanes,
                                phix_spike=phix_spike,
                                ready_date=now.date(),
                                status=seqnew,
                            )

                    acontacts = [client for client in submissionform.cleaned_data['contact'] if 'contact' in submissionform.cleaned_data]
                    anad = anamodels.SampleAnalysisDetail.objects.create(
                        sample=sample,
                        #analysis_clients=acontacts,
                        #alignment_genome=
                        )
                    anad.analysis_clients = acontacts
                    anad_list.append(anad)
                #
                # Step 3: Add analysis info
                #
                species = None
                if submissionform.cleaned_data['analysis_notes']:
                    anote = anamodels.AnalysisNote.objects.create(author=submissionform.cleaned_data['client'].person, note=submissionform.cleaned_data['analysis_notes'])
                    anote.sampleanalysisdetail = anad_list
                    anote.save()
                if submissionform.cleaned_data['analysis_species']:
                     species = submissionform.cleaned_data['analysis_species']

                for detail in anad_list:
                    detail.pipeline = submissionform.cleaned_data['analysis_pipeline']
                    detail.manual = submissionform.cleaned_data['analysis_manual']
                    detail.species = species
                    detail.save()
                #
                # Step 4: Billing
                #
                po = None
                if submissionform.cleaned_data['po_num'] or submissionform.cleaned_data['po_file']:
                    po = billmodels.PurchaseOrder(number=submissionform.cleaned_data['po_num'])
                    po.file = submissionform.cleaned_data['po_file']
                    po.save()
                
                sbd = billform.save(commit=False)
                sbd.submission = submission
                sbd.purchase_order = po
                sbd.save()

                ba_list = billassocformset.save(commit=False)
                for ba in ba_list:
                    ba.submissionbillingdetail = sbd
                    ba.save()
                    if ba.association.name == 'ICTS':
                        is_valid &= ictsassocformset.is_valid()
                        if is_valid:
                            icts_list = ictsassocformset.save(commit=False)
                            for icts in icts_list:
                                icts.sampleBillingAssociation = ba
                                icts.save()
                                for r in request.POST.getlist('ictsassoc-0-research'):
                                    icts.research.add(r)
                                icts.save()
                #
                #  Direct-to-Sequencing submissions need to create SequencingSample instances
                #
                if is_valid and direct_to_seq:
                    for sample in samples:
                        ssample = lpmodels.SequencingSample(name=sample.specimen.name)
                        ssample.client_prepped = True
                        muxtype = submissionform.cleaned_data['multiplexing']
                        if (muxtype is not None):
                            ssample.multiplex_type = muxtype
                        if (muxtype == 'index'):
                            ssample.indexed = True
                        elif (muxtype == 'barcoded'):
                            ssample.barcoded = True
                        ssample.primer = submissionform.cleaned_data['primer']
                        ssample.loading_concentration = sample.specimen.loading_concentration
                        ssample.submitted_concentration = sample.specimen.submitted_concentration
                        ssample.submitted_to_sequencing = datetime.date.today()
                        ssample.save()
                        ssample.sequencingsampledetail_set.create(sampleprepdetail=sample.libprep)

                if is_valid:
                    return HttpResponseRedirect(reverse("submission-browse",))
                else:
                    transaction.rollback()

    else:#Not Post Data
        submissionform = forms.SubmissionForm(prefix='sub')
        billform = forms.SubmissionBillingForm(prefix='bill')
        billassocformset = forms.SampleBillingAssociationFormSet(prefix='billassoc', queryset=billmodels.SampleBillingAssociation.objects.none())
        attachmentformset = forms.AttachmentFormset(prefix='att')
        specform = forms.SpecimenSelectionForm(prefix='spec')
        ictsassocformset = forms.SampleICTSAssociationFormSet(prefix='ictsassoc', queryset=billmodels.SampleICTSAssociation.objects.none())

    clients = basemodels.Client.objects.select_related('lab', 'lab__department').all()
    return render(request, 'sample/nextgen-submit.html', {
        'form': submissionform,
        'billform': billform,
        'billassocformset': billassocformset,
        'attachmentformset': attachmentformset,
        'specimens': sampmodels.Specimen.objects.all(),
        'specform': specform,
        'datatable_select_color': settings.DATATABLE_SELECT_COLOR ,
        'ictsassocformset':ictsassocformset,
        'clients': {c.id: [str(c.lab.campus_box), str(c.lab.department), str(c.lab.institution)] for c in clients},
    })

@login_required
def specimen_submit(request):
    if request.method == 'POST':
        form = forms.SubmitDetailForm(request.POST, prefix='detail')       
        upform = forms.UploadForm(request.POST, request.FILES, prefix='upload')
        now = datetime.datetime.now()
        is_valid = form.is_valid()
        if is_valid and form.cleaned_data['will_upload']:
            if upform.is_valid():
                specimens = upform.specs()
                for spec in specimens:
                    spec.submitted_by = form.cleaned_data['client']
                    spec.submitted = now
                    #spec.submitted_from = submission
                    spec.save()
            else:
                is_valid = False
        elif is_valid:
            specDataList = json.JSONDecoder().decode(request.POST.get('specimenData'))['data']
            field_name = 'loading_concentration'
            if not form.cleaned_data['is_selfprepped']:
                field_name = 'submitted_volume'                
                
            for specData in specDataList:
                if not specData[0]:
                    continue
                data = {'name': specData[0],
                        'organism': specData[1],
                        'self_prepped': form.cleaned_data['is_selfprepped'],                        
                        'submitted_concentration': specData[2],
                        field_name: specData[3],
                        'submitted_by': form.cleaned_data['client'].id,
                        'submitted': now,
                        'gtac_id': 0
                    }
                    
                specForm = forms.SpecimenForm(data)
                if specForm.is_valid():
                    specForm.save()                  
                else:
                    logger.error("form error: %s" % specForm.errors)
        
        if is_valid:
            return HttpResponseRedirect(reverse('nextgen-submit'));
    else:
        form = forms.SubmitDetailForm(prefix='detail')
        upform = forms.UploadForm(prefix='upload')
    
    clients = basemodels.Client.objects.select_related('lab', 'lab__department').all()

    return render(request, 'sample/specimen-submit.html', {
        'form': form,
        'prepspecimenuploadform': upform,
        'upfile_errors': upform.errors['file'] if upform.errors else "",
        'clients': {c.id: [str(c.lab.campus_box), str(c.lab.department), str(c.lab.institution)] for c in clients},
        })

# @login_required
# def submit(request):
    # if request.method == 'POST': 
      # with transaction.commit_on_success():

        #/
        #  Get all our forms:
        #  1.  Submission Form
        #  2.  Billing Form
        #  3.  Billing Association FormSet
        #  4.  Specimen / Sample  (one of the following)
        #      a.  Library Prep Specimen Upload Form
        #      b.  Library Prep Specimen FormSet
        #      c.  Direct-to-Sequencing FormSet
        #
        #  5.  Attachment FormSet
        #  6.  Analysis Contact FormSet
        #/

        # submissionform = forms.SubmissionForm(request.POST, request.FILES, prefix='sub')

        # billform = forms.SampleBillingForm(request.POST, request.FILES, prefix='bill')
        # billassocformset = forms.SampleBillingAssociationFormSet(request.POST, request.FILES, 
                                                                 # queryset=billmodels.SampleBillingAssociation.objects.none(), 
                                                                 # prefix='billassoc')

        # prepspecimenuploadform = forms.PrepSpecimenUploadForm(request.POST, request.FILES, prefix='prepup')
        # specformset = forms.RegularSpecimenFormSet(request.POST, prefix='samp')
        # directformset = forms.DirectToSeqSampleFormSet(request.POST, request.FILES, prefix='direct')

        # attachmentformset = forms.AttachmentFormset(request.POST, request.FILES, prefix='att')
        # analysiscontactformset = forms.AnalysisContactFormset(request.POST, prefix='acon')


        #/
        # Check the validity of all the forms
        #/
        # is_valid = True

        # is_valid &= submissionform.is_valid()
        # is_valid &= billform.is_valid() 
        # is_valid &= billassocformset.is_valid() 
        # is_valid &= attachmentformset.is_valid() 
        # is_valid &= analysiscontactformset.is_valid()

        # if is_valid:

            # if submissionform.cleaned_data['prep_type']:
                # direct_to_seq = False
                # if submissionform.cleaned_data['upload']:
                    # is_valid &= prepspecimenuploadform.is_valid()
                # else:
                    # is_valid &= specformset.is_valid()
            # else: 
                # direct_to_seq = True
                # is_valid &= directformset.is_valid()


        # if is_valid:
            # submission = submissionform.save()

            #/
            # Step 2: Save Samples and Specimens
            #/
                        
            # if submissionform.cleaned_data['prep_type']:
                # if submissionform.cleaned_data['upload']:
                    # specimens,groups = prepspecimenuploadform.save()
                # else:
                    # specimens = []
                    # groups = []
                    # for form in specformset.forms:
                        # if not form.has_changed(): continue
                        # s,g = form.save()
                        # specimens.append(s)
                        # groups.append(g)
            # else:
                # specimens = [f.save_specimen() for f in directformset.forms if f.has_changed()]
                # groups = [None] * len(specimens)

            # samples = []
            # for s,g in zip(specimens, groups):
                # s.submitted_from = submission
                # s.save()
                # sample = sampmodels.Sample()
                # sample.submission = submission
                # sample.specimen = s
                # sample.group = g
                # sample.save()
                # samples.append(sample)

            #/
            # save attachments
            #/
            # for att in attachmentformset.save(commit=False):
                # att.submission = submission
                # att.save()
             
            #/
            # Make libprep, seq, and analysis entries for each sample
            #/
            # now = datetime.datetime.now()

            # lpnew = lpmodels.PrepStatus.objects.get_or_create(name="New")[0]
            # seqnew = seqmodels.SequencingStatus.objects.get_or_create(name="New")[0]
            # anad_list = []
            # for sample in samples:
                # spd = lpmodels.SamplePrepDetail.objects.create(
                    # sample=sample, 
                    # complete=direct_to_seq,
                    # name=sample.specimen.name, 
                    # preptype=submissionform.cleaned_data['prep_type'], 
                    # multiplex_type=submissionform.cleaned_data['multiplexing'],
                    # status=lpnew,
                    # )
                # sseqd = seqmodels.SampleSequencingDetail.objects.create(
                    # sample=sample, 
                    # runtype=submissionform.cleaned_data['run_type'],
                    # num_lanes=submissionform.cleaned_data['lanes_per_sample'],
                    # ready_date=now.date(),
                    # status=seqnew,
                    # )

                # acontacts = [f.cleaned_data['contact'] for f in analysiscontactformset.forms if 'contact' in f.cleaned_data]
                # anad = anamodels.SampleAnalysisDetail.objects.create(
                    # sample=sample, 
                    #/analysis_clients=acontacts,
                    #/alignment_genome=
                    #)
                # anad.analysis_clients = acontacts
                # anad_list.append(anad)
            #/
            # Step 3: Add analysis info
            #
            #/anad.analysis_type = 
            # if submissionform.cleaned_data['analysis_notes']:
                # anote = anamodels.AnalysisNote.objects.create(author=submissionform.cleaned_data['client'].person, note=submissionform.cleaned_data['analysis_notes'])
                # anote.sampleanalysisdetail = anad_list
                # anote.save()
            # if submissionform.cleaned_data['analysis_species']:
                 # species = submissionform.cleaned_data['analysis_species']
            # else:
                 # species = anamodels.Species.objects.get_or_create(common_name=submissionform.cleaned_data['analysis_species_other'])[0]

            # for detail in anad_list:
                # detail.pipeline = submissionform.cleaned_data['analysis_pipeline']
                # detail.alignmentgenome = submissionform.cleaned_data['analysis_align_to']
                # detail.species = species
                # detail.save()

            #/
            # Step 4: Billing
            #/
            # if submissionform.cleaned_data['po_num'] or submissionform.cleaned_data['po_file']:
                # po = billmodels.PurchaseOrder(number=submissionform.cleaned_data['po_num'])
                # po.file = submissionform.cleaned_data['po_file']
                # po.save()
            # else:
                # po = None             

            # sbd = billform.save(commit=False)

            # for ss in samples:
                # sbd.pk = None
                # sbd.billed_lab = submissionform.cleaned_data['client'].lab
                # sbd.sample = ss
                # sbd.purchase_order = po
                # sbd.save()
                
            #/for form,x in enumerate(billassocformset.forms):
                #ba = form.save(commit=False)
                #ba.samplebillingdetail = sbd
                #/ba.save()
            # ba_list = billassocformset.save(commit=False)
            # for ba in ba_list:
                # ba.samplebillingdetail = sbd
                # ba.save()


            #/
            #  Direct-to-Sequencing submissions need to create SequencingSample instances
            #/
            # if submissionform.cleaned_data['prep_type'] is None:
                # for i,sample in enumerate(samples):
                    # directformset.forms[i].save_sequencingsample(sample.libprep) #XXX Rather ugly way to do this

            # return HttpResponseRedirect(reverse("submission-browse",))
        # else:
            # transaction.rollback()
        
    # else:
        # submissionform = forms.SubmissionForm(prefix='sub')
        # specformset = forms.RegularSpecimenFormSet(prefix='samp')
        # prepspecimenuploadform = forms.PrepSpecimenUploadForm(prefix='prepup')
        # billform = forms.SampleBillingForm(prefix='bill')
        # billassocformset = forms.SampleBillingAssociationFormSet(prefix='billassoc', queryset=billmodels.SampleBillingAssociation.objects.none())
        # attachmentformset = forms.AttachmentFormset(prefix='att')
        # analysiscontactformset = forms.AnalysisContactFormset(prefix='acon')
        # directformset = forms.DirectToSeqSampleFormSet(prefix='direct')

    # return render(request, 'sample/submit.html', {
        # 'form': submissionform,
        # 'sampformset': specformset,
        # 'prepspecimenuploadform': prepspecimenuploadform,
        # 'billform': billform,
        # 'directformset': directformset,
        # 'billassocformset': billassocformset,
        # 'analysiscontactformset': analysiscontactformset,
        # 'attachmentformset': attachmentformset,
        # })


@login_required
def submission96_csv(request):
    r = HttpResponse(content_type="text/csv")
    r['Content-Disposition'] = 'attachment; filename=96well_gtac_samples.csv'
    c = csv.writer(r)
    c.writerow([
        'Plate well ID', 
        'Sample Name', 
        'Organism', 
        'Concentration ng/ul',
        'Volume ul',
        #'Group (A-K)',
        ])
    for y in range(1, 13):
        for x in ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']:
            c.writerow(['%s%d' % (x,y),])
    return r

@login_required
def submission_browse(request):
    if not request.GET.get('sEcho', False):
        return render(request, 'sample/submission-browse.html', {})
    
    direct_to_seq = request.GET.get('directToSeq')

    SORT_COLS = ('created', 'specimens__gtac_id__min', 'client__lab__shortname', 
                 'client__person__first_name', 'sample__libprep__preptype', 'sample__count')
    SEARCH_FIELDS = ('client__lab__shortname', 'client__person__first_name',
                     'client__person__last_name', 'sample__libprep__preptype__name',)

    GROUP_COLS = (Count('sample'), Min('specimens__gtac_id'))
    if direct_to_seq in 'true':
        subs = sampmodels.Submission.objects.filter(sample__libprep__preptype__isnull=True).defer('description').distinct()
    else:
        subs = sampmodels.Submission.objects.defer('description').distinct()
    dtr = baseviews.DataTableResponse(request, sampmodels.Submission)
    qs = dtr.filter_qs(subs, SORT_COLS, search_fields=SEARCH_FIELDS, group_cols=GROUP_COLS)
    return render(request, 'sample/submission-browse-data.json', {
        'dtr': dtr,
        'subs': qs,
        })

@login_required
def browse(request):
    if not request.GET.get('sEcho', False):
        return render(request, 'sample/browse.html', {})
    SORT_COLS  = ('sample__name', 'sample__organism__name', 'submission__created')
    ss = sampmodels.Sample.objects.all()
    dtr = baseviews.DataTableResponse(request)
    qs = dtr.filter_qs(ss, SORT_COLS, search_fields=('sample__name',))
    return render(request, 'sample/browse-data.json', {
        'dtr': dtr,
        'ss': qs,
        })

@login_required
def dashboard(request):
    return render_to_response('gtac/dashboard.html', {
        },
        context_instance=RequestContext(request),
        )

@login_required
def submission_detail(request, id=None):
    submission = get_object_or_404(sampmodels.Submission, id=id)
    submission_note_form = forms.SubmissionNoteForm(initial={'submission': submission})
    
    if request.method == 'POST':
        form = forms.ChangeSubmissionTypeForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data

            samp = submission.sample_set.all() 

            lpmodels.SamplePrepDetail.objects.filter(sample__in = samp).update(preptype = cd['prep_type'])

            lpmodels.Project.objects.filter(sampleprepdetail__sample__in=samp).update(preptypeversion = lpmodels.PrepTypeVersion.active.filter(preptype = cd['prep_type'])[0])

            seqmodels.SampleSequencingDetail.objects.filter(sample__in = samp).update(runtype = cd['run_type'])
    else:
        preptype = submission.preptypes()[0]
       
        #BioAnalyzer Only Preps don't have SampleSequencingDetail
        #(they don't get sequenced) 
        try:
            runtype = submission.sample_set.all()[0].seq.runtype
        except seqmodels.SampleSequencingDetail.DoesNotExist:
            runtype = None

        initial_vals = {'prep_type': preptype}
 
        if (runtype is not None):
            initial_vals['run_type'] = runtype

        form = forms.ChangeSubmissionTypeForm(initial=initial_vals) 

    return render(request, "sample/submission-detail.html", {
        'changeTypeForm': form,
        'submission': submission,
        'submission_note_form': submission_note_form,
        'change_type_form': form,
        })


@client_or_admin_required
def submission_dashboard(request):
    if request.method == 'POST':
        form = SubmissionForm(request.user, request.POST)
        if form.is_valid():
            prj = form.save(commit=False)
            prj.user = request.user
            prj.save()
            return HttpResponseRedirect(reverse("gtac:submission-detail", kwargs={'id':prj.id}))
    else:
        form = SubmissionForm(request.user)

    return render_to_response('sample/submission_dashboard.html', {
        'submissions': request.user.submission_set.all(),
        'addform': form,
        },
        context_instance=RequestContext(request),
    )

@login_required
@client_or_admin_required
def sample_detail(request, id=None):
    if request.user.is_superuser:
        sample = get_object_or_404(sampmodels.Specimen, id=id)
    else:
        sample = get_object_or_404(sampmodels.Specimen, user=request.user, id=id)
    
    return render_to_response('sample/sample_detail.html', {
        'sample': sample,
        },
        context_instance=RequestContext(request),
    )

@login_required
def submission_excel(request):
    r = HttpResponse(libraryprepexcel(), content_type="application/vnd.ms-excel")
    r['Content-Disposition'] = 'attachment; filename=gtac_library_prep.xls'
    return r

@login_required
def submission_list(request):
    return render_to_response('submission/submission_list.html', {
        'submissions': sampmodels.Submission.objects.filter(user=request.user),
        },
        context_instance=RequestContext(request),
    )
    
    
@login_required
def submission_note(request):
    person = get_object_or_404(coremodels.Person, user=request.user)
    if request.method == 'POST':
        form = forms.SubmissionNoteForm(request.POST)
        if form.is_valid():
            note = form.save(commit=False)
            note.author = person
            note.save()
            return HttpResponse("success")

    raise Exception

@login_required
def submission_note_list(request, id=None):
    submission = get_object_or_404(sampmodels.Submission, id=id)
    return render(request, "sample/submission-note-list.html", {
        'submission': submission,
        })

@login_required
def sample_browse(request):
    if not request.GET.get('sEcho', False):
        return render(request, 'sample/sample-browse.html', {})
    
    direct_to_seq = request.GET.get('directToSeq')
    
    SORT_COLS = ('specimen__gtac_id',
                 'specimen__name',
                 'libprep__preptype__name',
                 'libprep__sequencingsamples__name',
                 'libprep__projectsample__project__name',
                 'analysis__pipeline',
                 'specimen__submitted_by__lab__shortname',
                 'specimen__submitted',
                 'submission__submitted',
                 'analysis__finish_date')

    samples = sampmodels.Sample.objects.select_related('specimen', 'analysis', 'submission', 'libprep', 'libprep__preptype')

    if direct_to_seq in 'true':
        samples = samples.filter(libprep__preptype__isnull=True)
    else:
        samples = samples.all()   
 
    project_urls          = { }
    project_url_strings   = { }
    seqsample_urls        = { }
    seqsample_url_strings = { }
    sample_urls           = { }
   
    dtr = baseviews.DataTableResponse(request)
    qs = dtr.filter_qs(samples, SORT_COLS, search_fields=('specimen__gtac_id', 'specimen__name', 'specimen__submitted_by__lab__shortname',))
 
    for sample in qs:
        #Some samples don't have any libprep info
        #(Bioanalyzer Only)
        try:
            for project_sample in sample.libprep.projectsample_set.all():
                if (not project_urls.has_key(sample.id)):
                    project_urls[sample.id] = []
                project_urls[sample.id].append('<a href="' + reverse('libprep-project-detail', args=[project_sample.project.id]) + '">' + project_sample.project.name + '</a>')
     
            for seqsample in sample.libprep.sequencingsamples.all():
                if (not seqsample_urls.has_key(sample.id)):
                    seqsample_urls[sample.id] = [ ]
                seqsample_urls[sample.id].append('<a href ="' + reverse('libprep-seqsample-detail', args=[seqsample.key]) + '">' + seqsample.name + '</a>')
        except lpmodels.SamplePrepDetail.DoesNotExist:
            pass
        sample_urls[sample.id] = '<a href="'+ reverse('sample-details', args=[sample.id]) +'">' + str(sample.specimen.gtac_id) + '</a>'
    for sample in samples:
        if (project_urls.has_key(sample.id)): 
            project_url_strings[sample.id]  = string.join(project_urls[sample.id], ',')
        else:
            project_url_strings[sample.id] = None
        if (seqsample_urls.has_key(sample.id)):
            seqsample_url_strings[sample.id] = string.join(seqsample_urls[sample.id], ',')
        else:
            seqsample_url_strings[sample.id] = None

    return render(request, 'sample/sample-browse-data.json', {
        'dtr': dtr,
        'samples': qs,
        'project_url_strings': project_url_strings,
        'seqsample_url_strings': seqsample_url_strings,
        'sample_urls': sample_urls,
        })

def getSpecimenPaginatedData(request):
    if request.GET.get('sEcho'):
        clientId = request.GET.get('clientId') if request.GET.get('clientId') else '-1'
        onlyShowSpec = request.GET.get('onlyShowSpec')
        showNewSpec = True if request.GET.get('showNewSpec')== "true" else False
        
        q_object = Q(submitted_by=clientId)

        if showNewSpec:
            q_object.add(Q(submitted__gte=datetime.date.today()), Q.AND)
        elif onlyShowSpec:
            startDate = datetime.date.today() - timedelta(days = int(onlyShowSpec))
            q_object.add(Q(submitted__gt=startDate), Q.AND)

        SORT_COLS = ('id', 'gtac_id', 'name', 'submitted_by', 'submitted', 'group')
        specimens = sampmodels.Specimen.objects.all().filter(q_object)
        
        dtr = baseviews.DataTableResponse(request)
        qs = dtr.filter_qs(specimens, SORT_COLS, search_fields=('gtac_id', 'name'),)

        aaData = [[s.id, s.gtac_id, s.name, s.submitted_by.name, s.submitted.strftime('%m/%d/%Y'), s.well_id] for s in qs]

        resp = {
                "sEcho":  dtr.echo,
                "iTotalRecords": dtr.total,
                "iTotalDisplayRecords": dtr.totaldisplay,
                "aaData": aaData
                }

        return HttpResponse(json.dumps(resp), content_type="application/json" )


def sample_details(request, id=None):
    sample = get_object_or_404(sampmodels.Sample.objects.select_related(), id=id)
    spd = sample.libprep
    seqSampleDetail = spd.sequencingsampledetail_set.all()

    return render(request, 'sample/sample-details.html', {
        'sample': sample,
        'spd': spd,
        'seqSampleDetail': seqSampleDetail,
    })



def submission_report(request):
    spreadsheet = filemng.Spreadsheet('ICTS Tracking', ['Report'])
    spreadsheet.set_font('Calibri', 220, True)
    
    header = { 0: "Core #", 1: "Type of Service Provided", 2:  "Service Provider", 3: "Service Start Date", 
                4: "Service End Date", 5: "Investigator requesting service (last name)", 
                6: "Investigator requesting service (first name)", 7: "Fund Grant Number", 8: "Funding Agency",
                9: "Grant Title", 10: "Grant PI (last name)", 11: "Grant PI (first name)", 12: "HRPO/IRB Approval Status", 
                13: "Active HRPO/IFB Protocol Title", 14: "Active HRPO/IFB Number", 15: "HRPO/IRB Approval Date", 
                16: "IACUC Number", 17: "IACUC Approval Date", 18: "AIDS Research", 19: "Cancer Research", 
                20: "Digestive Disease Research", 21: "NIH Clinical Trial", 22: "Neurological Disease Research", 
                23: "Pediatric Research", 24: "Immunology Research"
            }
    
    spreadsheet.write_row('Report', 0, header)
    spreadsheet.set_font('Calibri', 220, False)
    row = 1
    for sub in  sampmodels.Submission.objects.all():
        if sub.sample_set.all():
            sample = sub.sample_set.all()[0]
            try:
                type = str(sample.libprep.preptype) if sample.libprep.preptype else str(sample.seq.runtype)
            except:
                type = ''
            
            completionDate = ''
            spds = lpmodels.SamplePrepDetail.objects.filter(sample__in=sub.sample_set.all())
            seqSamples = lpmodels.SequencingSample.objects.filter(sampleprepdetails__in=spds)
            for run in seqmodels.Run.objects.filter(flowcell__lanes__sequencing_sample__in=seqSamples).distinct():
                completionDate = run.sequencing_date.strftime('%Y-%m-%d')
                
            rowdata = {0: 2.13, 1: type, 2: sample.specimen.gtac_id, 3: sub.submitted.strftime('%Y-%m-%d'), 4: completionDate}
        else:
            rowdata = {0: 2.13, 3: sub.submitted.strftime('%Y-%m-%d')}
        
        for billing in sub.billing.all():
            for assoc in billing.associations.all():
                for icts in assoc.billing_association.all():
                    rowdata[5] = icts.investigator_last_name
                    rowdata[6] = icts.investigator_first_name
                    rowdata[7] = icts.fund_grant_number
                    rowdata[8] = icts.funding_agency
                    rowdata[9] = icts.grant_title
                    rowdata[10] = icts.grant_pi_last_name
                    rowdata[11] = icts.grant_pi_first_name
                    rowdata[12] = icts.approval_status
                    rowdata[13] = icts.active_protocol_title
                    rowdata[14] = icts.active_number
                    rowdata[15] = icts.approval_date.strftime('%Y-%m-%d') if icts.approval_date else ''
                    rowdata[16] = icts.iacuc_number
                    rowdata[17] = icts.iacuc_approval_date.strftime('%Y-%m-%d') if icts.iacuc_approval_date else ''
                    list = icts.research.all()
                    col = 18
                    for research in billmodels.ICTSResearch.objects.order_by('id'):
                        rowdata[col] = 'Yes' if research in list else 'No'
                        col += 1
            break;
        spreadsheet.write_row('Report', row, rowdata)
        row += 1

    return spreadsheet.create_excel_sheet()
