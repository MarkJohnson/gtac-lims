from django import template
from django.utils.safestring import mark_safe
from mptt.templatetags.mptt_tags import cache_tree_children
from menus import models

register = template.Library()

class MenuTreeNode(template.Node):
    def __init__(self, template_nodes, menu_var, caps):
        self.template_nodes = template_nodes
        self.menu_var = menu_var
        self.caps = caps

    def _render_page(self, context, page, level=0):
        bits = []
        context.push()
        level += 1
        for childpage in page.children:
            if not childpage.menuitem.is_active: continue
            context['page'] = childpage
            bits.append(self._render_page(context, childpage, level=level))
        context['level'] = level
        context['page'] = page
        context['children'] = mark_safe(u''.join(bits))
        rendered = self.template_nodes.render(context)
        context.pop()
        return rendered

    def render(self, context):
        menuname = self.menu_var.resolve(context)
        caps = {}
        for k,v in self.caps.items():
            caps[k] = v.resolve(context)

        try:
            menu = models.MenuItem.objects.get(name=menuname, parent=None)
        except models.MenuItem.DoesNotExist:
            return ''
        queryset = menu.get_descendants()
        roots = cache_tree_children(queryset)
        bits = []
        for node in roots:
            factory = node.factory()
            for page in factory.make_pages(context['request'], caps, parent=None):
                if page.menuitem.is_active:
                    bits.append(self._render_page(context, page))
        return ''.join(bits)


@register.tag
def menutree(parser, token):
    bits = token.split_contents()
    if len(bits) < 2:
        raise template.TemplateSyntaxError('%s tag requires a menu name' % bits[0])

    menu_var = template.Variable(bits[1])
    caps = {}
    for b in bits[2:]:
        k,v = b.split('=')
        caps[k] = template.Variable(v)

    template_nodes = parser.parse(('endmenutree',))
    parser.delete_first_token()
    return MenuTreeNode(template_nodes, menu_var, caps)
