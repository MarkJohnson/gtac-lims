import menus
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey

class MenuItem(MPTTModel):
    name = models.CharField(null=True, blank=True, max_length=255, 
                            help_text="Leave blank to use the Page's default name")
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    caption = models.CharField(blank=True, null=True, max_length=255, 
                               help_text="Leave blank to use the Page's default caption")
    url = models.CharField(blank=True, null=True, max_length=255)
    key = models.CharField(max_length=255, blank=True, null=True, help_text='')
    is_active = models.BooleanField(default=True, help_text='')
    
    def __unicode__(self):
        return u'%s' % self.name
    
    def factory(self):
        if self.key is None or self.key == 'None':
            return menus.DummyFactory(self)
      
        f = menus.get_registered_factories().get(self.key, None)
        if f is None:
            return menus.DummyFactory(self)

        cls,kwargs = f[1:]
        return cls(self, **kwargs)
