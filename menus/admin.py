from django.contrib import admin
from menus import forms
from menus.models import MenuItem
from feincms.admin import tree_editor


class MenuItemAdmin(tree_editor.TreeEditor): #FeinCMSModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('key', 'name','caption', )
        }),
        ('Advanced options', {
            'classes': ('collapse',),
            'fields': ('url', 'parent', 'is_active')
        }),
    )
    form = forms.MenuItemForm
    #list_filter = ('menu',)
    #ordering = ('level',)
    list_display = ('pk','caption',)
    #raw_id_fields =('parent',)
    #readonly_fields = ('parent', )
    #list_editable = ('caption',)
admin.site.register(MenuItem, MenuItemAdmin)
