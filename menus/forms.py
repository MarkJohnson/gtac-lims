from django import forms
from menus.models import MenuItem
from menus import get_registered_factories

class MenuItemForm(forms.ModelForm):
    key = forms.ChoiceField(label='Page', required=False)

    class Meta:
        model = MenuItem
        fields = '__all__'

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None,
                 initial=None, error_class=forms.utils.ErrorList,
                 label_suffix=':', empty_permitted=False, instance=None):
        if instance:
            if initial is None:
                initial = {}
            initial['key'] = instance.key
        super(MenuItemForm, self).__init__(data, files, auto_id, prefix,
                                           initial, error_class, label_suffix,
                                           empty_permitted, instance)
        self._registered_pages_cache = get_registered_factories()
        self.fields['key'].choices = self.item_choices()
        self.fields['caption'].required = False

    def item_choices(self):
        """
        Returns list of 2-tuples ('item unique key', 'item name')
        to fill the ChoiceField.
        """
        choices = [(None,'----'),]
        for key,val in self._registered_pages_cache.items():
            title, cls = val[:2]
            choices.extend([(key, title)])
        choices.sort(key=lambda x: x[1])
        return choices

    def selected_item(self):
        return self._registered_pages_cache.get(self.cleaned_data['key'], None)

    def clean(self):
        cd = self.cleaned_data
        cd['name'] = cd['name'] or self.selected_item()[1].default_name
        if not cd['caption']:
            if self.selected_item():
                cd['caption'] = self.selected_item()[1].default_caption
        return cd
