import copy
from django.core.urlresolvers import reverse

registry = []

def register(cls):
    registry.append(cls)

def get_registered_factories():
    f = {}
    for Factory in map(copy.deepcopy, registry):
        f.update(dict(Factory.factories()))
    return f

def autodiscover(name='menu'):
    """
    Auto-discover INSTALLED_APPS <name>.py modules and fail silently when
    not present. This forces an import on them to register any bits they
    may want.
    """
    from django.conf import settings
    from importlib import import_module
    from django.utils.module_loading import module_has_submodule

    for app in settings.INSTALLED_APPS:
        mod = import_module(app)
        try:
            import_module('%s.%s' % (app, name))
        except:
            # Decide whether to bubble up this error. If the app just
            # doesn't have a <name> module, we can ignore the error
            # attempting to import it, otherwise we want it to bubble up.
            if module_has_submodule(mod, name):
                raise

class Page(object):
    """
    page is really just a name, caption, and url.

    Given a menuitem and a request, it can give you children pages
    """
    def __init__(self, name, url, request, menuitem, caps=None, caption=None, parent=None):
        self.request = request
        self.menuitem = menuitem
        self.parent = parent
        self._children = None
        self.name = name
        self.url = url
        self.caption = caption
        self.caps = caps or {}
        

    def is_active(self):
        if self.url == self.request.path:
            return True
        else:
            for child in self.children:
                if child.is_active():
                    return True
        return False

    @property
    def children(self):
        if self._children is None:
            self._children = []
            for childmenuitem in self.menuitem.get_children():
                f = childmenuitem.factory()
                for page in f.make_pages(self.request, caps=self.caps, parent=self):
                    self._children.append(page)
        return self._children


class Factory(object):
    admin_name = None
    default_name = None
    default_caption = None

    def __init__(self, menuitem):
        self.menuitem = menuitem

    @classmethod
    def factories(self):
        """return [ (hopefully_unique_key, ('Admin Interface Name', cls, kwarg_dict_for_cls_init)), ... ]"""
        raise NotImplementedError

    def make_pages(self, request, caps, parent=None):
        """return [Page(), ... ]"""
        raise NotImplementedError


class MultiFactory(Factory):
    # qs is required 
    #
    # model_id is added by factories()

    def make_pages(self, request, caps, parent=None):
        try:
            obj = self.qs.model.objects.get(id=self.model_id)
        except self.qs.model.DoesNotExist:
            return []
        newcaps = copy.deepcopy(caps)
        newcaps[self.qs.model.__name__.lower()] = obj

        return [Page(
            self.menuitem.name, 
            obj.get_absolute_url(), 
            request, 
            self.menuitem, 
            caption=self.menuitem.caption,
            caps=newcaps,
            parent=parent), ]

    @classmethod
    def factories(cls):
        i= []
        mname = cls.qs.model.__name__
        for o in cls.qs:
            key = '%s.%d' % (mname, o.id)
            if cls.default_caption_attr:
                dc = getattr(o, cls.default_caption_attr),
            else:
                dc = None
             
            SPF = type("%sFactory" % (mname), (cls,), {
                'default_name': getattr(o, cls.default_name_attr),
                'default_caption': dc,
                'model_id': o.id,
                })
            i.append( ( key, ("[%s] %s" % (mname, getattr(o, cls.admin_name_attr)), SPF, {})))
        return i

def multi_factory(qs, default_name_attr='title', admin_name_attr=None, default_caption_attr=None):
    F = type('%sMultiFactory' % (qs.model.__name__), (MultiFactory,), {
        'qs': qs,
        'admin_name_attr': admin_name_attr or default_name_attr,
        'default_name_attr': default_name_attr,
        'default_caption_attr': default_caption_attr,
        })

    register(F)
    return F

class DummyFactory(Factory):
    def make_pages(self, request, caps, parent=None):
        return [ Page(self.menuitem.name, self.menuitem.url or '', request, 
                      self.menuitem, caps=caps, caption=self.menuitem.caption, 
                      parent=parent), ]
        
class NamedViewPF(Factory):
    """
    Need these
    admin_name = None
    view_name = None
    default_name = None
    default_caption = None

    Optional:
    key = 
    """
    key = None

    def make_pages(self, request, caps, parent=None):
        caption = self.menuitem.caption or self.default_caption 
        name = self.menuitem.name or self.default_name 
        return [ Page(name, reverse(self.view_name), request, self.menuitem, caps=caps, caption=caption, parent=parent), ]

    @classmethod
    def factories(cls):
        key = cls.key or "NamedView: %s" % cls.view_name
        return [(key, (cls.admin_name, cls, {})), ]

def named_view_factory(view_name, default_name, admin_name, key=None, default_caption=None):
    F = type('NewFactory', (NamedViewPF,), {
        'admin_name': admin_name,
        'view_name': view_name,
        'default_name': default_name,
        'default_caption': default_caption,
        'key': key or view_name,
        })

    register(F)
    return F

def multi_named_view_factory(qs, view_name, default_name, admin_name, key=None, 
                             default_caption=None, provides=None, view_kwargs=None):
    F = type('NewFactory', (MultiNamedViewPF,), {
        'qs': qs,
        'provides': provides,
        'view_kwargs': view_kwargs,
        'admin_name': admin_name,
        'view_name': view_name,
        'default_name': default_name,
        'default_caption': default_caption,
        'key': key or view_name,
        })

    register(F)
    return F

class MultiNamedViewPF(NamedViewPF):
    """
    Like NamedViewPF, but generates multiple pages using a named view that takes kwargs.

    Required attributes:
        admin_name - name as shown in admin interface
        menu_name - (suggested) name as it will appear in the actual menu.
                    can be a format string... caps to be passed as parameters.
        view_name - the urls.py view name
        view_kwargs - dict to be passed to reverse() when trying to get the URL. 
                      Values of the dict will be passed to eval() in page_url()
        qs - QuerySet used to generate the pages
        provides = { 'anyname': modeltype of the qs }


    Example:

        class PersonDetailPageFactory(MultiNamedViewPF):
            provides = {'person': coremodels.Person }
            admin_name = "Person Profile"
            view_name = "person-profile"
            view_kwargs={'id': 'page.person.id'}
            qs = coremodels.Person.active.all()

    """

    def make_pages(self, request, caps, parent=None):
        p = []
        key = None
        for k,v in self.provides.items():
            if v == self.qs.model:
                key = k
                break
        else:
            raise Exception, "Queryset model type %s doesn't match any of your 'provides'" % (self.qs.model)

        for o in self.qs:
            newcaps = copy.deepcopy(caps)
            newcaps[self.qs.model.__name__.lower()] = o
            name = self.build_name(newcaps)
            caption = self.build_caption(newcaps)
            vkwargs = self.build_vkwargs(newcaps)

            p.append(Page(name, reverse(self.view_name, kwargs=vkwargs), request, 
                          self.menuitem, caps=newcaps, caption=caption, parent=parent))
        return p

    def build_caption(self, caps):
        c = self.menuitem.caption or self.default_caption 
        if c:
            try:
                c = c.format(**caps)
            except:
                c = ''
        return c

    def build_name(self, caps):
        name = self.menuitem.name or self.default_name 
        if name:
            try:
                name = name.format(**caps)
            except:
                pass
        return name

    def build_vkwargs(self, caps):
        vkwargs = {}
        for k,v in self.view_kwargs.items():
            obj = caps[v.split('.')[0]]
            vkwargs[k] = eval('obj.' + v.split('.', 1)[1])
        return vkwargs

    @classmethod
    def factories(cls):
        key = cls.key or "MultiNamedView: %s" % cls.view_name
        return [(key, (cls.admin_name, cls, {})), ]

