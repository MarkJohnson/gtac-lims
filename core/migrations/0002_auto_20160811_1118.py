# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='address',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='emailaddress',
            name='email',
            field=models.EmailField(help_text=b'', max_length=254),
        ),
        migrations.AlterField(
            model_name='person',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='person',
            name='email',
            field=models.EmailField(help_text=b'Primary Email Address... Computed automatically.', max_length=254, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='person',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='person',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='personbio',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='personbio',
            name='key',
            field=django_extensions.db.fields.UUIDField(editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='personbio',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, editable=False, blank=True),
        ),
    ]
