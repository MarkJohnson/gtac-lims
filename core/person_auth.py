import logging
from django.contrib.auth.models import User
from django.conf import settings
from gdata.apps.service import AppsService, AppsForYourDomainException
from gdata.docs.service import DocsService
from gdata.service import BadAuthentication


logging.debug('PersonAuthBackend')


class PersonAuthBackend:
 """Authenticate using "person" email addresses as username"""

 def authenticate(self, username=None, password=None):
     logging.debug('PersonAuthBackend.authenticate: %s - %s' % (username, '*' * len(password)))

     for user in User.objects.filter(person__email_addresses__email__iexact=username):
         if user.check_password(password):
             return user
     return None

 def get_user(self, user_id):
     user = None
     try:
         logging.debug('PersonAuthBackend.get_user')
         user = User.objects.get(pk=user_id)

     except User.DoesNotExist:
         logging.debug('PersonAuthBackend.get_user - DoesNotExist')
         return None

     return user
