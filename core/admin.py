from django.contrib import admin, messages, auth
from core import models
from django.db import transaction


@transaction.atomic
def make_user_account(modeladmin, request, qs):
    for o in qs:
        if o.user:
            messages.add_message(request, messages.WARNING, '%s alread has user account: %s' % (o.name, o.user.username))
            continue

        password = auth.models.User.objects.make_random_password()
        try:
            u = auth.models.User.objects.create_user('%s.%s' % (o.first_name.lower(), o.last_name.lower()), email='', password=password)
            u.first_name = o.first_name
            u.last_name = o.last_name
            u.is_staff = False
            u.save()
            o.user = u
            o.save()
            #u.groups.add(auth.models.Group.objects.get(name='Data Entry'))
            messages.add_message(request, messages.INFO, '%s / %s' % (u.username,password))
        except Exception, m:
            messages.add_message(request, messages.ERROR, m)
make_user_account.short_description = "Create user account for person"

class EmailAddressInline(admin.TabularInline):
    model = models.EmailAddress
    extra = 0

class PhoneNumberInline(admin.TabularInline):
    model = models.PhoneNumber
    extra = 0

class PhoneNumberTypeAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', )
admin.site.register(models.PhoneNumberType, PhoneNumberTypeAdmin)

class AddressInline(admin.TabularInline):
    model = models.Address.people.through
    extra = 0
    raw_id_fields = ['address',]
    #exclude = ['people',]

class CoreModelAdmin(admin.ModelAdmin):
    # Need to move 'notes' to the bottom of the default form
    def get_fieldsets(self, *args, **kwargs):
        res = super(CoreModelAdmin, self).get_fieldsets(*args, **kwargs)
        if self.declared_fieldsets:
            return res
        res.append(('Advanced options', {
            'classes': ('collapse',),
            'fields': ('created', 'modified', 'key')
        }))
            
        fields = res[0][1]['fields']
        #fields.remove('notes')
        #fields.append('notes')
        for x in ('created', 'modified', 'key'):
            if x in fields:
                fields.remove(x)
            #fields.append(x)
        return res
    readonly_fields = ('created', 'modified', 'key')
    #list_display = ('__unicode__',)

class EmailAddressAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', )
    search_fields = ['email', ]
admin.site.register(models.EmailAddress, EmailAddressAdmin)

class AddressAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'street', 'city', 'state', 'zipcode' )
    #actions = (make_user_account, )
    # fieldsets = []
    # list_filter = []
    search_fields = ['street', ]
    filter_horizontal = ['people',]
    #readonly_fields = ['email', ]
    #inlines = [ AddressInline, PhoneNumberInline, EmailAddressInline, PhotoInline, ]
    #raw_id_fields = ['user',]
    
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.Address, AddressAdmin)

class PersonAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'first_name', 'last_name', 'preferred_first_name', )
    actions = (make_user_account, )
    # fieldsets = []
    # list_filter = []
    search_fields = ['first_name', 'last_name', ]
    # filter_horizontal = []
    readonly_fields = ['email', ]
    inlines = [ AddressInline, PhoneNumberInline, EmailAddressInline, ]
    raw_id_fields = ['user',]
    
    # prepopulated_fields = {"slug": ("title",)}
admin.site.register(models.Person, PersonAdmin)

