from django.contrib.auth import models as authmodels
from localflavor.us.models import USStateField, PhoneNumberField
from django.db import models
import django.dispatch
from django_extensions.db import fields as djefields

# Signals
pre_primary_email_changed = django.dispatch.Signal(providing_args=["instance", "old", "new"])
primary_email_changed = django.dispatch.Signal(providing_args=["instance", "old", "new"])
primary_email_added = django.dispatch.Signal(providing_args=["instance", "email", ])
primary_email_removed = django.dispatch.Signal(providing_args=["instance", "email", ])
    
class PublishedManager(models.Manager):

    def published(self):
        return self.get_query_set().filter(publish=True)

    
class CoreModel(models.Model):
    created = djefields.CreationDateTimeField()
    modified = djefields.ModificationDateTimeField()
    key = djefields.UUIDField()

    def has_changed(self, field):
        if not self.pk:
            return False
            
        old_value = self.__class__._default_manager.\
                 filter(pk=self.pk).values(field).get()[field]
        print old_value, getattr(self, field)
        return not getattr(self, field) == old_value
        
    class Meta:
        abstract = True

class Photo(models.Model):
    image = models.ImageField(upload_to='photos')
    person = models.ForeignKey("Person", related_name="photos", blank=True, null=True)

    def __unicode__(self):
        return u'%s' % (self.person.name)

    class Meta:
        get_latest_by = ('id',)

class AddressBase(CoreModel):
    ADDRESS_TYPES = (
        ('work', 'Work'),
        ('home', 'Home')
        )
    street = models.CharField(max_length=255, help_text='')
    city = models.CharField(max_length=255, default="St. Louis", help_text='')
    state = USStateField(default="MO", help_text='')
    zipcode = models.CharField(max_length=20, help_text='')
    type = models.CharField(max_length=255, choices=ADDRESS_TYPES, help_text='')
    primary = models.BooleanField(default=False, help_text='')
    publish = models.BooleanField(default=True, help_text='')

    objects = PublishedManager()


    def __unicode__(self):
        return u'%s %s, %s, %s' % (self.street, self.city, self.state, self.zipcode)
        
    class Meta(CoreModel.Meta):
        abstract = True
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        verbose_name_plural = "Addresses"

class Address(AddressBase):
    people = models.ManyToManyField("Person", related_name="addresses", help_text='')
        
class PhoneNumberType(models.Model):
    name = models.CharField(max_length=255, unique=True)
    def __unicode__(self):
        return u'%s' % (self.name)

class PhoneNumber(models.Model):
    PHONE_TYPES = (
        ('work', 'Work'),
        ('home', 'Home')
        )
    person = models.ForeignKey("Person", related_name="phone_numbers", help_text='')
    number = PhoneNumberField(help_text='', db_column='ph_number')
    extension = models.PositiveSmallIntegerField(blank=True, null=True, help_text='')
    #type = models.CharField(max_length=255, choices=PHONE_TYPES, blank=True, null=True, help_text='')
    type = models.ForeignKey(PhoneNumberType, blank=True, null=True)
    primary = models.BooleanField(default=False, help_text='')
    publish = models.BooleanField(default=True, help_text='')    

    objects = PublishedManager()
    
    def __unicode__(self):
        if self.extension:
            return u'%s x%s' % (self.number, self.extension)
        else:
            return u'%s' % (self.number)
    
    @models.permalink
    def get_absolute_url(self):
        return ('phonenumber-detail', [str(self.id)])
        
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass

class EmailAddressManager(PublishedManager):
    def primaries(self):
        return self.get_query_set().filter(primary=True)

class EmailAddress(models.Model):
    EMAIL_TYPES = (
        ('work', 'Work'),
        ('home', 'Home')
        )
    person = models.ForeignKey("Person", related_name="email_addresses", help_text='')
    email = models.EmailField(help_text='')
    type = models.CharField(max_length=255, choices=EMAIL_TYPES, blank=True, null=True, help_text='')
    primary = models.BooleanField(default=True, help_text='')
    publish = models.BooleanField(default=True, help_text='')

    objects = EmailAddressManager()

    def __unicode__(self):
        return u'%s -> %s' % (self.person.name, self.email)
            
    class Meta:
        # get_latest_by = ""
        # ordering = []
        # unique_together(('',''), )
        verbose_name_plural = "Email Addresses"
        
    def save(self, *args, **kwargs):
        
        # if we're "updating"
        if self.pk and EmailAddress.objects.filter(pk=self.pk).exists():
            old = EmailAddress.objects.get(pk=self.pk)

            # If we're trying to change the person we raise Exception
            if self.person != old.person:
                raise Exception, "Can't change person"
              
            made_unprimary = old.primary and not self.primary
            made_primary = not old.primary and self.primary
            email_changed = self.email != old.email
            still_unprimary = not self.primary and not old.primary
            still_primary = self.primary and old.primary

            if still_unprimary:
                super(EmailAddress, self).save(*args, **kwargs)
            elif still_primary:
                if email_changed:
                    primary_email_removed.send(sender=self.__class__, instance=self, email=old.email)
                    super(EmailAddress, self).save(*args, **kwargs)
                    primary_email_added.send(sender=self.__class__, instance=self, email=self.email)
                else:
                    super(EmailAddress, self).save(*args, **kwargs)
            elif made_unprimary:
                primary_email_removed.send(sender=self.__class__, instance=self, email=old.email)
                super(EmailAddress, self).save(*args, **kwargs)
            elif made_primary:
                super(EmailAddress, self).save(*args, **kwargs)
                primary_email_added.send(sender=self.__class__, instance=self, email=self.email)
        # We're adding a new one
        else: 
            super(EmailAddress, self).save(*args, **kwargs)
            if self.primary:
                primary_email_added.send(sender=self.__class__, instance=self, email=self.email)
        return

    def delete(self):
        person = self.person
        super(EmailAddress, self).delete()
        person.set_primary_email()

class Person(CoreModel):
    first_name = models.CharField(max_length=255)
    middle_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255)
    preferred_first_name = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(blank=True, null=True, help_text='Primary Email Address... Computed automatically.')
    birthdate = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=1, blank=True, null=True, choices=(('M', 'Male'),('F', 'Female')))
    user = models.OneToOneField(authmodels.User, blank=True, null=True)
    
    def set_primary_email(self):
        old = self.email
        if self.email_addresses.filter(primary=True).exists():
            new = self.email_addresses.filter(primary=True)[0].email
        elif self.email_addresses.exists():
            new = self.email_addresses.all()[0].email
        else:
            new = None
        if new != old:
            print "Emitting pre_primary_email_changed: %s, %s. %s" % (old,new, self.__class__)
            pre_primary_email_changed.send(sender=self.__class__, instance=self, old=old, new=new)
            self.email = new
            self.save()
            print "Emitting primary_email_changed: %s, %s. %s" % (old,new, self.__class__)
            primary_email_changed.send(sender=self.__class__, instance=self, old=old, new=new)
            
    
    @property
    def photo(self):
        return self.photos.latest()
    
    @property
    def bio(self):
        return self.bios.latest()
    @property    
    def name(self):
        if self.preferred_first_name and self.preferred_first_name.strip():
            return "%s %s" % (self.preferred_first_name, self.last_name)
        else:
            return "%s %s" % (self.first_name, self.last_name)
    
    def __unicode__(self):
        return u'%s' % (self.name)

    class Meta:
        # get_latest_by = ""
        ordering = ['last_name', 'first_name']
        verbose_name_plural = "People"
        pass

class PersonBio(CoreModel):
    person = models.ForeignKey(Person, related_name='bios', help_text='')
    bio = models.TextField(help_text='')
    
    def __unicode__(self):
        return u'Bio: %s' % (self.person.name)
    
    @models.permalink
    def get_absolute_url(self):
        return ('personbio-detail', [str(self.id)])
        
    class Meta:
        get_latest_by = ("created",)
        # ordering = []
        # unique_together(('',''), )
        # verbose_name_plural = ""
        pass

PERSONMODELS=[]    
class PersonType(CoreModel):
    person = models.ForeignKey(Person, unique=True, help_text='')  # unique=True ??

    @property
    def first_name(self): return self.person.first_name

    @property
    def last_name(self): return self.person.last_name

    @property
    def photo(self): return self.person.photo

    @property
    def bio(self): return self.person.bio

    @property
    def name(self): return self.person.name

    @classmethod
    def register(cls):
        if cls not in PERSONMODELS:
            PERSONMODELS.append(cls)

    def __unicode__(self):
        return u'%s' % (self.name)
            
    class Meta:
        # get_latest_by = ""
        ordering = ['person__first_name', 'person__last_name']
        # unique_together(('',''), )
        # verbose_name_plural = ""
        abstract = True

